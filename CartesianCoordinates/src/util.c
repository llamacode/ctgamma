#include "cctk.h"

CCTK_INT
CartesianCoordinates_GetBbox(CCTK_POINTER_TO_CONST const GH,
                             CCTK_INT const size,
                             CCTK_INT * const bbox)
{
  const cGH* const cctkGH = (const cGH* const) GH;  

  for (int i=0; i<6; ++i)
    bbox[i] = cctkGH->cctk_bbox[i];

  return 0;
}


CCTK_INT
CartesianCoordinates_GetMap(CCTK_POINTER_TO_CONST const GH)
{
  const cGH* const cctkGH = (const cGH* const) GH;  
  return 0;
}

void
CartesianCoordinates_Util_GetPhysicalBoundaries(CCTK_POINTER_TO_CONST GH, CCTK_INT* do_bc)
{
  CartesianCoordinates_GetBbox(GH, 6, do_bc);

  return;
}

CCTK_INT
CartesianCoordinates_Util_GetGridRanges(CCTK_POINTER_TO_CONST GH,
                                        CCTK_INT* istart,
                                        CCTK_INT* iend)
{
  const cGH* const cctkGH = (const cGH* const) GH;
  DECLARE_CCTK_ARGUMENTS;

  CCTK_INT do_bc[6];

  CartesianCoordinates_Util_GetPhysicalBoundaries (cctkGH, do_bc);

  for (int i=0; i<3; ++i) {
    if (do_bc[2*i]) {
      istart[i] = 0;
    } else {
      istart[i] = cctk_nghostzones[i];
    }
    if (do_bc[2*i+1]) {
      iend[i] = cctk_lsh[i];
    } else {
      iend[i] = cctk_lsh[i] - cctk_nghostzones[i];
    }
  }

  return 0;
}
