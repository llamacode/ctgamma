#include "cctk.h"
#include "util_Table.h"

#include "math.h"

#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"


void
CTGMatter_ApplyRadiativeBC(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;


  static int have_indices = 0;
  static CCTK_INT eTtt_idx, eTtx_idx, eTty_idx, eTtz_idx, eTxx_idx, eTxy_idx,
    eTxz_idx, eTyy_idx, eTyz_idx, eTzz_idx;

  CCTK_INT ierr=0;

  if (verbose)
    CCTK_INFO("Applying radiative bc to sources.");


  if (! have_indices) {
    /* Initialise these only once, since CCTK_VarIndex can be
       annoyingly expensive on small grids  */
    have_indices = 1;

    eTtt_idx = CCTK_VarIndex("TmunuBase::eTtt");
    eTtx_idx = CCTK_VarIndex("TmunuBase::eTtx");
    eTty_idx = CCTK_VarIndex("TmunuBase::eTty");
    eTtz_idx = CCTK_VarIndex("TmunuBase::eTtz");
    eTxx_idx = CCTK_VarIndex("TmunuBase::eTxx");
    eTxy_idx = CCTK_VarIndex("TmunuBase::eTxy");
    eTxz_idx = CCTK_VarIndex("TmunuBase::eTxz");
    eTyy_idx = CCTK_VarIndex("TmunuBase::eTyy");
    eTyz_idx = CCTK_VarIndex("TmunuBase::eTyz");
    eTzz_idx = CCTK_VarIndex("TmunuBase::eTzz");

  }

  /*
  ierr += ApplyBC_Radiative(cctkGH, gamma11_idx, dt_gamma11_idx, 1.0, 1.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma12_idx, dt_gamma12_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma13_idx, dt_gamma13_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma22_idx, dt_gamma22_idx, 1.0, 1.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma23_idx, dt_gamma23_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma33_idx, dt_gamma33_idx, 1.0, 1.0);
  */

  if (ierr)
    CCTK_WARN(0, "Error applying radiative BC.");

  return;
}
