#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <unistd.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "GlobalDerivative.h"
#include "ctgbase_util.h"

#include "loopcontrol.h"

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#define Power(x,y) (pow((CCTK_REAL) (x), (CCTK_REAL) (y)))

/*
 * Main source calculation routine.
 */
void
CTGMatter_CalcRHS(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT * restrict imin[3], * restrict imin_plus[3], * restrict imin_minus[3];
  CCTK_INT * restrict imax[3], * restrict imax_plus[3], * restrict imax_minus[3];
  CCTK_INT * restrict imin2[3], * restrict imax2[3];
  CCTK_REAL * restrict q[3], * restrict q_plus[3], * restrict q_minus[3],
    * restrict q2[3];
  CCTK_REAL ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz;

  if (verbose)
    CCTK_INFO("Calculating matter evolution variable sources.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  /*
   * Grid spacings required by the finite difference operators.
   */
  ihx = 1.0 / CCTK_DELTA_SPACE(0);
  ihy = 1.0 / CCTK_DELTA_SPACE(1);
  ihz = 1.0 / CCTK_DELTA_SPACE(2);
  ihxx = ihx*ihx;
  ihxy = ihx*ihy;
  ihxz = ihx*ihz;
  ihyy = ihy*ihy;
  ihyz = ihy*ihz;
  ihzz = ihz*ihz;

  CCTK_INT conformal_factor_phi = CCTK_EQUALS(conformal_factor_type, "phi");
  CCTK_REAL n = *detg_exponent;

  /*
   * Call SummationByParts for finite-difference operator coefficients.
   */
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax,
                  (CCTK_POINTER_TO_CONST *) q, 1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_plus,
                  (CCTK_POINTER_TO_CONST *) imax_plus,
                  (CCTK_POINTER_TO_CONST *) q_plus, 1, +(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_minus,
                  (CCTK_POINTER_TO_CONST *) imax_minus,
                  (CCTK_POINTER_TO_CONST *) q_minus, 1, -(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin2,
                  (CCTK_POINTER_TO_CONST *) imax2, (CCTK_POINTER_TO_CONST *) q2,
                  2, 0);

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGMatter_calc_rhs,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      CCTK_REAL igt11, igt12, igt13, igt22, igt23, igt33, detg;

      CCTK_REAL phi_ = phi[ijk];

      CCTK_REAL gt11 = gamma11[ijk];
      CCTK_REAL gt12 = gamma12[ijk];
      CCTK_REAL gt13 = gamma13[ijk];
      CCTK_REAL gt22 = gamma22[ijk];
      CCTK_REAL gt23 = gamma23[ijk];
      CCTK_REAL gt33 = gamma33[ijk];

      CTGBase_invert_metric(gt11, gt12, gt13, gt22, gt23, gt33, detg,
			    igt11, igt12, igt13, igt22, igt23, igt33);
      
      CCTK_REAL lapse = alp[ijk];
      CCTK_REAL ilapse = 1./alp[ijk];
      CCTK_REAL beta1 = betax[ijk];
      CCTK_REAL beta2 = betay[ijk];
      CCTK_REAL beta3 = betaz[ijk];

      CCTK_REAL phi_fac;
      if (conformal_factor_phi)
	phi_fac = exp(4.*phi_);
      else
	phi_fac = pow(phi_, 1.0/(3.0*n));

      CCTK_REAL T00 = eTtt[ijk];
      CCTK_REAL T01 = eTtx[ijk];
      CCTK_REAL T02 = eTty[ijk];
      CCTK_REAL T03 = eTtz[ijk];
      CCTK_REAL T11 = eTxx[ijk];
      CCTK_REAL T12 = eTxy[ijk];
      CCTK_REAL T13 = eTxz[ijk];
      CCTK_REAL T22 = eTyy[ijk];
      CCTK_REAL T23 = eTyz[ijk];
      CCTK_REAL T33 = eTzz[ijk];

      CCTK_REAL rho = ilapse*ilapse *
	(T00 - 2.*(T01*beta1 + T02*beta2 + T03*beta3)
	 + T11*beta1*beta1 + T22*beta2*beta2 + T33*beta3*beta3
	 + 2.*(T12*beta1*beta2 + T13*beta1*beta3 + T23*beta2*beta3));

      CCTK_REAL S1 = ilapse*(-T01 + T11*beta1 + T12*beta2 + T13*beta3);
      CCTK_REAL S2 = ilapse*(-T02 + T12*beta1 + T22*beta2 + T23*beta3);
      CCTK_REAL S3 = ilapse*(-T03 + T13*beta1 + T23*beta2 + T33*beta3);

      CCTK_REAL S11 = T11;
      CCTK_REAL S12 = T12;
      CCTK_REAL S13 = T13;
      CCTK_REAL S22 = T22;
      CCTK_REAL S23 = T23;
      CCTK_REAL S33 = T33;

      CCTK_REAL S =  (1./phi_fac) * (igt11*S11 + igt22*S22 + igt33*S33 
				     + 2.*(igt12*S12 + igt13*S13 + igt23*S23));

      CCTK_REAL Sfac = (1./3.) * phi_fac * S;

      CCTK_REAL S11tf = S11 - Sfac * gt11;
      CCTK_REAL S12tf = S12 - Sfac * gt12;
      CCTK_REAL S13tf = S13 - Sfac * gt13;
      CCTK_REAL S22tf = S22 - Sfac * gt22;
      CCTK_REAL S23tf = S23 - Sfac * gt23;
      CCTK_REAL S33tf = S33 - Sfac * gt33;

      CCTK_REAL Afac = 8.*M_PI * (1./phi_fac)*lapse;

      dt_K[ijk] += 4.*M_PI*lapse * (rho + S);

      dt_A11[ijk] -= Afac * S11tf;
      dt_A12[ijk] -= Afac * S12tf;
      dt_A13[ijk] -= Afac * S13tf;
      dt_A22[ijk] -= Afac * S22tf;
      dt_A23[ijk] -= Afac * S23tf;
      dt_A33[ijk] -= Afac * S33tf;

      CCTK_REAL Gammafac = 16.*M_PI * lapse;

      dt_Gamma1[ijk] -= Gammafac * (igt11*S1 + igt12*S2 + igt13*S3);
      dt_Gamma2[ijk] -= Gammafac * (igt12*S1 + igt22*S2 + igt23*S3);
      dt_Gamma3[ijk] -= Gammafac * (igt13*S1 + igt23*S2 + igt33*S3);

      if (EVOL_TYPE == Z4c_TYPE) {
        dt_Theta[ijk] -= 8.*M_PI * lapse * rho;
      }

    }
  LC_ENDLOOP3 (CTGMatter_calc_rhs);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin_minus, imax_minus, q_minus);
  CTGBase_free_stencil(imin_plus, imax_plus, q_plus);
  CTGBase_free_stencil(imin2, imax2, q2);

  return;
}
