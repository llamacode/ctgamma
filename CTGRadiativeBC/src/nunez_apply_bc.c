#include <math.h>
#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "loopcontrol.h"

#include "ctgbase_util.h"
#include "calc_rhs.h"


void
CTGNunezBC_ApplyBC_Faces(const cGH* restrict const GH,
                         CCTK_REAL* var, CCTK_REAL* dt_var,
                         CCTK_INT* do_bc,
                         CCTK_INT * restrict * imin, CCTK_INT * restrict * imax,
                         CCTK_REAL * restrict * q,
                         CCTK_REAL v, CCTK_REAL limit, CCTK_REAL n)
{
  const cGH* const cctkGH = (const cGH* const) GH; 
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL ihx = 1.0 / CCTK_DELTA_SPACE(0);
  CCTK_REAL ihy = 1.0 / CCTK_DELTA_SPACE(1);
  CCTK_REAL ihz = 1.0 / CCTK_DELTA_SPACE(2);

  /* If z is a radial coordinate, then loop over all grid points since
     the edges and corners won't be treated otherwise.  If z is not a
     radial coordinate, then skip the edges and corners, since they
     will receive special treatment in custom routines.  */
  const int offset = *z_is_radial ? 0 : 1;

  int sx[6] = {-1, +1, 0, 0, 0, 0};
  int sy[6] = {0, 0, -1, +1, 0, 0};
  int sz[6] = {0, 0, 0, 0, -1, +1};
  int irange[6][2] = {{0, 1},
                      {cctk_lsh[0]-1, cctk_lsh[0]},
                      {offset, cctk_lsh[0]-offset},
                      {offset, cctk_lsh[0]-offset},
                      {offset, cctk_lsh[0]-offset},
                      {offset, cctk_lsh[0]-offset}};
  int jrange[6][2] = {{offset, cctk_lsh[1]-offset},
                      {offset, cctk_lsh[1]-offset},
                      {0, 1},
                      {cctk_lsh[1]-1, cctk_lsh[1]},
                      {offset, cctk_lsh[1]-offset},
                      {offset, cctk_lsh[1]-offset}};
  int krange[6][2] = {{offset, cctk_lsh[2]-offset},
                      {offset, cctk_lsh[2]-offset},
                      {offset, cctk_lsh[2]-offset},
                      {offset, cctk_lsh[2]-offset},
                      {0, 1},
                      {cctk_lsh[2]-1, cctk_lsh[2]}};
  if (verbose)
    CCTK_INFO("Setting 'radiative' BC on faces.");

  for (int face=0; face<6; ++face)
    {
      CCTK_INT fsx, fsy, fsz;

      if (*z_is_radial)
	{
	  fsx = 0;
	  fsy = 0;
	  fsz = sz[face];
	}
      else
	{
	  fsx = sx[face];
	  fsy = sy[face];
	  fsz = sz[face];
	}

      if (!do_bc[face])
        continue;

      if (verbose)
        CCTK_VInfo(CCTK_THORNSTRING,
               "face  %d | i=(%d,%d) j=(%d,%d), k=(%d,%d) | offset = %d %d %d",
                   face, irange[face][0], irange[face][1], jrange[face][0],
                   jrange[face][1], krange[face][0], krange[face][1],
                   fsx, fsy, fsz);	     

#pragma omp parallel
      LC_LOOP3 (CTGNunezBC_ApplyBC_Faces,
                i, j, k,
                irange[face][0], jrange[face][0], krange[face][0],
                irange[face][1], jrange[face][1], krange[face][1],
                cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
            {
              const CCTK_INT ijk = CCTK_GFINDEX3D(GH, i, j, k);
              const CCTK_INT ijk_p = CCTK_GFINDEX3D(GH, i-fsx, j-fsy, k-fsz);

              if (*z_is_radial)
                  CTGNunezBC_CalcRHS_Rad(cctkGH, var, dt_var, 
					     general_coordinates,
					     cctk_lsh, r, 
					     J11, J12, J13, J21, J22, J23,
					     J31, J32, J33,
					     v, limit, n,
                                             ijk, ijk_p, i, j, k, fsx, fsy,
                                             fsz, ihx, ihy, ihz, imin[0],
                                             imax[0], q[0], imin[1], imax[1],
                                             q[1], imin[2], imax[2], q[2]);
              else
                  CTGNunezBC_CalcRHS(cctkGH, var, dt_var, 
					 general_coordinates,
					 cctk_lsh, x, y, z, r, 
					 J11, J12, J13, J21, J22, J23, J31,
					 J32, J33,
					 v, limit, n,
                                         ijk, ijk_p, i, j, k, fsx, fsy, fsz,
                                         ihx, ihy, ihz, imin[0], imax[0], q[0],
                                         imin[1], imax[1], q[1], imin[2],
                                         imax[2], q[2]);
            }
      LC_ENDLOOP3 (CTGNunezBC_ApplyBC_Faces);
    }

  return;
}


void
CTGNunezBC_ApplyBC_Edges(const cGH* restrict const GH,
                         CCTK_REAL* var, CCTK_REAL* dt_var,
                         CCTK_INT* do_bc,
                         CCTK_INT * restrict * imin, CCTK_INT * restrict * imax,
                         CCTK_REAL * restrict * q,
                         CCTK_REAL v, CCTK_REAL limit, CCTK_REAL n)
{
  const cGH* const cctkGH = (const cGH* const) GH;
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL ihx = 1.0 / CCTK_DELTA_SPACE(0);
  CCTK_REAL ihy = 1.0 / CCTK_DELTA_SPACE(1);
  CCTK_REAL ihz = 1.0 / CCTK_DELTA_SPACE(2);

  int sx[6] = {-1, +1, 0, 0, 0, 0};
  int sy[6] = {0, 0, -1, +1, 0, 0};
  int sz[6] = {0, 0, 0, 0, -1, +1};
  static int edges[12][2] = {{0,2},
                             {0,3},
                             {0,4},
                             {0,5},
                             {1,2},
                             {1,3},
                             {1,4},
                             {1,5},
                             {2,4},
                             {2,5},
                             {3,4},
                             {3,5}};
  int irange[12][2] = {{0,1},
                       {0,1},
                       {0,1},
                       {0,1},
                       {cctk_lsh[0]-1, cctk_lsh[0]},
                       {cctk_lsh[0]-1, cctk_lsh[0]},
                       {cctk_lsh[0]-1, cctk_lsh[0]},
                       {cctk_lsh[0]-1, cctk_lsh[0]},
                       {1, cctk_lsh[0]-1},
                       {1, cctk_lsh[0]-1},
                       {1, cctk_lsh[0]-1},
                       {1, cctk_lsh[0]-1}};
  int jrange[12][2] = {{0,1},
                       {cctk_lsh[1]-1, cctk_lsh[1]},
                       {1, cctk_lsh[1]-1},
                       {1, cctk_lsh[1]-1},
                       {0,1},
                       {cctk_lsh[1]-1, cctk_lsh[1]},		
                       {1, cctk_lsh[1]-1},
                       {1, cctk_lsh[1]-1},
                       {0,1},
                       {0,1},
                       {cctk_lsh[1]-1, cctk_lsh[1]},		
                       {cctk_lsh[1]-1, cctk_lsh[1]}};
  int krange[12][2] = {{1, cctk_lsh[2]-1},
                       {1, cctk_lsh[2]-1},
                       {0,1},
                       {cctk_lsh[2]-1, cctk_lsh[2]},
                       {1, cctk_lsh[2]-1},
                       {1, cctk_lsh[2]-1},
                       {0,1},
                       {cctk_lsh[2]-1, cctk_lsh[2]},
                       {0,1},
                       {cctk_lsh[2]-1, cctk_lsh[2]},
                       {0,1},
                       {cctk_lsh[2]-1, cctk_lsh[2]}};

  if (verbose)
    CCTK_INFO("Setting 'radiative' BC on edges.");
		       
  for (int edge=0; edge<12; ++edge)
    {
      int face0 = edges[edge][0];
      int face1 = edges[edge][1];
      int esx = sx[face0] + sx[face1];
      int esy = sy[face0] + sy[face1];
      int esz = sz[face0] + sz[face1];

      if (!do_bc[face0] || !do_bc[face1])
        continue;

      if (verbose)
        CCTK_VInfo(CCTK_THORNSTRING,
               "edge %d | i=(%d,%d) j=(%d,%d), k=(%d,%d) | offset = %d %d %d",
                   edge, irange[edge][0], irange[edge][1], jrange[edge][0],
                   jrange[edge][1], krange[edge][0], krange[edge][1],
                   esx, esy, esz);

      for (int k=krange[edge][0]; k<krange[edge][1]; ++k)
        for (int j=jrange[edge][0]; j<jrange[edge][1]; ++j)
          for (int i=irange[edge][0]; i<irange[edge][1]; ++i)
            {
              CCTK_INT ijk = CCTK_GFINDEX3D(GH, i, j, k);
              CCTK_INT ijk_p = CCTK_GFINDEX3D(GH, i-esx, j-esy, k-esz);

	      CTGNunezBC_CalcRHS(cctkGH, var, dt_var, 
				     general_coordinates,
				     cctk_lsh, x, y, z, r, 
				     J11, J12, J13, J21, J22, J23, J31, J32,
				     J33,
				     v, limit, n,
				     ijk, ijk_p, i, j, k, esx, esy, esz,
				     ihx, ihy, ihz, imin[0], imax[0], q[0],
				     imin[1], imax[1], q[1], imin[2],
				     imax[2], q[2]);
            }
    }

  return;
}



void
CTGNunezBC_ApplyBC_Corners(const cGH* restrict const GH,
                           CCTK_REAL* var, CCTK_REAL* dt_var,
                           CCTK_INT* do_bc,
                           CCTK_INT * restrict * imin,
                           CCTK_INT * restrict * imax,
                           CCTK_REAL * restrict * q,
                           CCTK_REAL v, CCTK_REAL limit, CCTK_REAL n)
{
  const cGH* const cctkGH = (const cGH* const) GH;
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL ihx = 1.0 / CCTK_DELTA_SPACE(0);
  CCTK_REAL ihy = 1.0 / CCTK_DELTA_SPACE(1);
  CCTK_REAL ihz = 1.0 / CCTK_DELTA_SPACE(2);

  int s[2] = {-1, +1};
  int corners[8][3] = {{0,0,0},
                       {0,0,1},
                       {0,1,0},
                       {0,1,1},
                       {1,0,0},
                       {1,0,1},
                       {1,1,0},
                       {1,1,1}};
  int ipoint[2] = {0, cctk_lsh[0]-1};
  int jpoint[2] = {0, cctk_lsh[1]-1};
  int kpoint[2] = {0, cctk_lsh[2]-1};
  
  if (verbose)
    CCTK_INFO("Setting 'radiative' BC at corners");

  for (int c=0; c<8; ++c)
    {
      int csx = s[corners[c][0]];
      int csy = s[corners[c][1]];
      int csz = s[corners[c][2]];

      int i = ipoint[corners[c][0]];
      int j = jpoint[corners[c][1]];
      int k = kpoint[corners[c][2]];
      
      if (!do_bc[corners[c][0]] || !do_bc[2+corners[c][1]]
          || !do_bc[4+corners[c][2]])
        continue;
      
      if (verbose)
        CCTK_VInfo(CCTK_THORNSTRING,
                   "corner %d | ijk=(%d,%d,%d) | offset = %d %d %d",
                   c, i, j, k, csx, csy, csz);
      
      CCTK_INT ijk = CCTK_GFINDEX3D(GH, i, j, k);
      CCTK_INT ijk_p = CCTK_GFINDEX3D(GH, i-csx, j-csy, k-csz);

      CTGNunezBC_CalcRHS(cctkGH, var, dt_var, 
			     general_coordinates,
			     cctk_lsh, x, y, z, r, 
			     J11, J12, J13, J21, J22, J23, J31, J32, J33,
			     v, limit, n,
			     ijk, ijk_p, i, j, k, csx, csy, csz,
			     ihx, ihy, ihz, imin[0], imax[0], q[0],
			     imin[1], imax[1], q[1], imin[2],
			     imax[2], q[2]);

    }

  return;
}


CCTK_INT
CTGNunezBC_ApplyBC(CCTK_POINTER_TO_CONST GH, CCTK_INT var_index,
                       CCTK_INT dt_var_index, CCTK_REAL v, CCTK_REAL limit)
{
  const cGH* const cctkGH = (const cGH* const) GH;  
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_REAL* restrict var;
  CCTK_REAL* restrict dt_var;
  CCTK_INT do_bc[6];
  CCTK_INT * restrict imin[3], * restrict imax[3];
  CCTK_REAL * restrict q[3];
  CCTK_INT ierr = 0;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Applying 'radiative' BC to: %s",
               CCTK_VarName(var_index));

  var = CCTK_VarDataPtrI(cctkGH, 0, var_index);
  dt_var = CCTK_VarDataPtrI(cctkGH, 0, dt_var_index);

  Util_GetPhysicalBoundaries(GH, do_bc);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q,
                  1, 0);

  CTGNunezBC_ApplyBC_Faces(cctkGH, var, dt_var, do_bc, imin, imax, q,
                               v, limit, falloff_exponent);

  if (! *z_is_radial)
    {
      CTGNunezBC_ApplyBC_Edges(cctkGH, var, dt_var, do_bc, imin, imax, q,
				   v, limit, falloff_exponent);
      CTGNunezBC_ApplyBC_Corners(cctkGH, var, dt_var, do_bc, imin, imax, q,
				     v, limit, falloff_exponent);
    }

  CTGBase_free_stencil(imin, imax, q);

  return ierr;
}
