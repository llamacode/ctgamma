#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

void
CTGRadiativeBC_Init(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (z_is_radial)
    *z_is_radial = CCTK_Equals(coordinate_system, "Thornburg04");
  else
    CCTK_WARN(0, "Couldn't set z_is_radial flag.");
  
  return;
}
