#include<math.h>
#include "GlobalDerivative.h"

static inline __attribute__((always_inline)) void
CTGNunezBC_CalcRHS(cGH const * restrict const GH,
                       const CCTK_REAL* restrict const var,
                       CCTK_REAL* restrict const dt_var,
                       const CCTK_REAL* restrict const A11,
                       const CCTK_REAL* restrict const A12,
                       const CCTK_REAL* restrict const A13,
                       const CCTK_REAL* restrict const A22,
                       const CCTK_REAL* restrict const A23,
                       const CCTK_REAL* restrict const A33,
                       const CCTK_REAL* restrict const phi,
		       const CCTK_INT* restrict general_coordinates,
		       const CCTK_INT* restrict cctk_lsh,
		       const CCTK_REAL* restrict x,
		       const CCTK_REAL* restrict y,
		       const CCTK_REAL* restrict z,
		       const CCTK_REAL* restrict r,
		       const CCTK_REAL* restrict J11,
		       const CCTK_REAL* restrict J12,
		       const CCTK_REAL* restrict J13,
		       const CCTK_REAL* restrict J21,
		       const CCTK_REAL* restrict J22,
		       const CCTK_REAL* restrict J23,
		       const CCTK_REAL* restrict J31,
		       const CCTK_REAL* restrict J32,
		       const CCTK_REAL* restrict J33,
                       const CCTK_REAL v, const CCTK_REAL limit,
                       const CCTK_REAL n,
                       const CCTK_INT ijk, const CCTK_INT ijk_p,
                       const CCTK_INT i, const CCTK_INT j, const CCTK_INT k,
                       const CCTK_INT sx, const CCTK_INT sy,
                       const CCTK_INT sz, const CCTK_REAL ihx,
                       const CCTK_REAL ihy, const CCTK_REAL ihz,
                       const CCTK_INT* restrict const imin,
                       const CCTK_INT* restrict const imax,
                       const CCTK_REAL* restrict const iq,
                       const CCTK_INT* restrict const jmin,
                       const CCTK_INT* restrict const jmax,
                       const CCTK_REAL* restrict const jq,
                       const CCTK_INT* restrict const kmin,
                       const CCTK_INT* restrict const kmax,
                       const CCTK_REAL* restrict const kq)
{
  const cGH* const cctkGH = (const cGH* const) GH;
  CCTK_REAL Ai11, Ai22, Ai33, junk1, junk2, phi1, phi2, phi3;
  CCTK_REAL Ai11_p, Ai22_p, Ai33_p, phi1_p, phi2_p, phi3_p;
  CCTK_REAL dvarx_p, dvary_p, dvarz_p;
  CCTK_REAL dt_var_p, dt_var_diff;

  CCTK_REAL rn = pow(r[ijk_p]/r[ijk], n);
  CCTK_REAL vx = v * x[ijk]/r[ijk];
  CCTK_REAL vy = v * y[ijk]/r[ijk];
  CCTK_REAL vz = v * z[ijk]/r[ijk];

  CCTK_REAL vx_p = v * x[ijk_p]/r[ijk_p];
  CCTK_REAL vy_p = v * y[ijk_p]/r[ijk_p];
  CCTK_REAL vz_p = v * z[ijk_p]/r[ijk_p];

  CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0, dbdz=0,
    dcdz=1;
  if (*general_coordinates)
    {
      dadx = J11[ijk];
      dady = J12[ijk];
      dadz = J13[ijk];
      dbdx = J21[ijk];
      dbdy = J22[ijk];
      dbdz = J23[ijk];
      dcdx = J31[ijk];
      dcdy = J32[ijk];
      dcdz = J33[ijk];
    }

  g_diff(cctkGH, phi, &phi1, &phi2, &phi3,
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, phi, &phi1_p, &phi2_p, &phi3_p,
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  //  if (i<= 2 || i>= cctk_lsh[0]-2){
  g_diff(cctkGH, A11, &Ai11, &junk1, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A12, &junk1, &Ai22, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A13, &junk1, &junk2, &Ai33, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, A11, &Ai11_p, &junk1, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A12, &junk1, &Ai22_p, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A13, &junk1, &junk2, &Ai33_p, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  dt_var[ijk] = -3/2*vx*(Ai11 + Ai22 + Ai33 + 6*(A11[ijk]*phi1 + A12[ijk]*phi2 + A13[ijk]*phi3));

  dt_var_p = -3/2*vx_p*(Ai11_p + Ai22_p + Ai33_p + 6*(A11[ijk_p]*phi1_p + A12[ijk_p]*phi2_p + A13[ijk_p]*phi3_p));
  //  }
  //  else if (j<= 2 || j>= cctk_lsh[1]-2){
  g_diff(cctkGH, A12, &Ai11, &junk1, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A22, &junk1, &Ai22, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A23, &junk1, &junk2, &Ai33, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, A12, &Ai11_p, &junk1, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A22, &junk1, &Ai22_p, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A23, &junk1, &junk2, &Ai33_p, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  dt_var[ijk] += -3/2*vy*(Ai11 + Ai22 + Ai33 + 6*(A12[ijk]*phi1 + A22[ijk]*phi2 + A23[ijk]*phi3));

  dt_var_p += -3/2*vy_p*(Ai11_p + Ai22_p + Ai33_p + 6*(A12[ijk_p]*phi1_p + A22[ijk_p]*phi2_p + A23[ijk_p]*phi3_p));

  //  }
  //  else if (k<= 2 || k>= cctk_lsh[2]-2){
  g_diff(cctkGH, A13, &Ai11, &junk1, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A23, &junk1, &Ai22, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A33, &junk1, &junk2, &Ai33, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, A13, &Ai11_p, &junk1, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  g_diff(cctkGH, A23, &junk1, &Ai22_p, &junk2, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, A33, &junk1, &junk2, &Ai33_p, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  dt_var[ijk] += -3/2*vz*(Ai11 + Ai22 + Ai33 + 6*(A13[ijk]*phi1 + A23[ijk]*phi2 + A33[ijk]*phi3)) - v * (var[ijk] - limit)/(r[ijk]);

  dt_var_p += -3/2*vz_p*(Ai11_p + Ai22_p + Ai33_p + 6*(A13[ijk_p]*phi1_p + A23[ijk_p]*phi2_p + A33[ijk_p]*phi3_p)) - v * (var[ijk_p] - limit)/(r[ijk_p]);

  //  }

  dt_var_diff = dt_var[ijk_p] - dt_var_p;

  dt_var[ijk] += dt_var_diff * rn;

  return;
}

