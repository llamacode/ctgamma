#include<math.h>
#include "GlobalDerivative.h"

static inline __attribute__((always_inline)) void
CTGRadiativeBC_CalcRHS(cGH const * restrict const GH,
                       const CCTK_REAL* restrict const var,
                       CCTK_REAL* restrict const dt_var,
		       const CCTK_INT* restrict general_coordinates,
		       const int* restrict cctk_lsh,
		       const CCTK_REAL* restrict x,
		       const CCTK_REAL* restrict y,
		       const CCTK_REAL* restrict z,
		       const CCTK_REAL* restrict r,
		       const CCTK_REAL* restrict J11,
		       const CCTK_REAL* restrict J12,
		       const CCTK_REAL* restrict J13,
		       const CCTK_REAL* restrict J21,
		       const CCTK_REAL* restrict J22,
		       const CCTK_REAL* restrict J23,
		       const CCTK_REAL* restrict J31,
		       const CCTK_REAL* restrict J32,
		       const CCTK_REAL* restrict J33,
                       const CCTK_REAL v, const CCTK_REAL limit,
                       const CCTK_REAL n,
                       const CCTK_INT ijk, const CCTK_INT ijk_p,
                       const CCTK_INT i, const CCTK_INT j, const CCTK_INT k,
                       const CCTK_INT sx, const CCTK_INT sy,
                       const CCTK_INT sz, const CCTK_REAL ihx,
                       const CCTK_REAL ihy, const CCTK_REAL ihz,
                       const CCTK_INT* restrict const imin,
                       const CCTK_INT* restrict const imax,
                       const CCTK_REAL* restrict const iq,
                       const CCTK_INT* restrict const jmin,
                       const CCTK_INT* restrict const jmax,
                       const CCTK_REAL* restrict const jq,
                       const CCTK_INT* restrict const kmin,
                       const CCTK_INT* restrict const kmax,
                       const CCTK_REAL* restrict const kq)
{
  const cGH* const cctkGH = GH;
  CCTK_REAL dvarx, dvary, dvarz;
  CCTK_REAL dvarx_p, dvary_p, dvarz_p;
  CCTK_REAL dt_var_p, dt_var_diff;

  CCTK_REAL rn = pow(r[ijk_p]/r[ijk], n);
  CCTK_REAL vx = v * x[ijk]/r[ijk];
  CCTK_REAL vy = v * y[ijk]/r[ijk];
  CCTK_REAL vz = v * z[ijk]/r[ijk];

  CCTK_REAL vx_p = v * x[ijk_p]/r[ijk_p];
  CCTK_REAL vy_p = v * y[ijk_p]/r[ijk_p];
  CCTK_REAL vz_p = v * z[ijk_p]/r[ijk_p];

  CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0, dbdz=0,
    dcdz=1;

  if (*general_coordinates)
    {
      dadx = J11[ijk];
      dady = J12[ijk];
      dadz = J13[ijk];
      dbdx = J21[ijk];
      dbdy = J22[ijk];
      dbdz = J23[ijk];
      dcdx = J31[ijk];
      dcdy = J32[ijk];
      dcdz = J33[ijk];
    }

  g_diff(cctkGH, var, &dvarx, &dvary, &dvarz, 
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, var, &dvarx_p, &dvary_p, &dvarz_p,
         *general_coordinates,
         dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);
  
  dt_var[ijk] = -vx*dvarx - vy*dvary - vz*dvarz 
    - v * (var[ijk] - limit)/r[ijk];

  dt_var_p = -vx_p*dvarx_p - vy_p*dvary_p - vz_p*dvarz_p
    - v * (var[ijk_p] - limit)/r[ijk_p];

  dt_var_diff = dt_var[ijk_p] - dt_var_p;

  dt_var[ijk] += dt_var_diff * rn;

  return;
}



static inline __attribute__((always_inline)) void
CTGRadiativeBC_CalcRHS_Rad(cGH const * restrict const GH,
                           const CCTK_REAL* restrict const var,
                           CCTK_REAL* restrict const dt_var,
			   const CCTK_INT* restrict general_coordinates,
			   int* restrict const cctk_lsh,
			   CCTK_REAL* restrict const r,			   
			   const CCTK_REAL* restrict J11,
			   const CCTK_REAL* restrict J12,
			   const CCTK_REAL* restrict J13,
			   const CCTK_REAL* restrict J21,
			   const CCTK_REAL* restrict J22,
			   const CCTK_REAL* restrict J23,
			   const CCTK_REAL* restrict J31,
			   const CCTK_REAL* restrict J32,
			   const CCTK_REAL* restrict J33,
                           const CCTK_REAL v, const CCTK_REAL limit,
                           const CCTK_REAL n,
                           const CCTK_INT ijk, const CCTK_INT ijk_p,
                           const CCTK_INT i, const CCTK_INT j,
                           const CCTK_INT k,
                           const CCTK_INT sx, const CCTK_INT sy,
                           const CCTK_INT sz, const CCTK_REAL ihx,
                           const CCTK_REAL ihy, const CCTK_REAL ihz,
                           const CCTK_INT* restrict const imin,
                           const CCTK_INT* restrict const imax,
                           const CCTK_REAL* restrict const iq,
                           const CCTK_INT* restrict const jmin,
                           const CCTK_INT* restrict const jmax,
                           const CCTK_REAL* restrict const jq,
                           const CCTK_INT* restrict const kmin,
                           const CCTK_INT* restrict const kmax,
                           const CCTK_REAL* restrict const kq)
{
  const cGH* const cctkGH = GH;
  CCTK_REAL dvarx, dvary, dvarz;
  CCTK_REAL dvarx_p, dvary_p, dvarz_p;
  CCTK_REAL dt_var_p, dt_var_diff;
  CCTK_REAL rn = pow(r[ijk_p]/r[ijk], n);

  /* Don't apply the Jacobians since we really want a z (radial) derivative */
  g_diff(cctkGH, var, &dvarx, &dvary, &dvarz, 
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         i, j, k, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  g_diff(cctkGH, var, &dvarx_p, &dvary_p, &dvarz_p,
         0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
         i-sx, j-sy, k-sz, cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
         imin, imax, jmin, jmax, kmin, kmax, iq, jq, kq, ihx, ihy, ihz);

  dt_var[ijk] = -v*dvarz - v * (var[ijk] - limit)/r[ijk];

  dt_var_p = -v*dvarz_p - v * (var[ijk_p] - limit)/r[ijk_p];

  dt_var_diff = dt_var[ijk_p] - dt_var_p;

  dt_var[ijk] += dt_var_diff * rn;

  return;
}
