// damp kappa1 with radius if requested
const CCTK_REAL kap1 =  kappa1_damping_radius > 0 ?
              (r[ijk] >= kappa1_damping_radius ? kappa1 * kappa1_damping_radius / r[ijk]
               : kappa1) : kappa1;


aTheta = beta1*adTheta1 + beta2*adTheta2 + beta3*adTheta3;

// additional term for Khat
zK = alpha * kap1 * (1.0 - kappa2) * Thet;

// \gamma^{ij}\p_j \Theta
CCTK_REAL igtPTheta1 = igt11*dTheta1 + igt12*dTheta2 + igt13*dTheta3;
CCTK_REAL igtPTheta2 = igt12*dTheta1 + igt22*dTheta2 + igt23*dTheta3;
CCTK_REAL igtPTheta3 = igt13*dTheta1 + igt23*dTheta2 + igt33*dTheta3;

// additional terms for \Gamma^i
zGamt1 = -2*alpha*igtPTheta1/3.0 - 2*alpha*kap1*(Gamt1 - localGamt1);

zGamt2 = -2*alpha*igtPTheta2/3.0 - 2*alpha*kap1*(Gamt2 - localGamt2);

zGamt3 = -2*alpha*igtPTheta3/3.0 - 2*alpha*kap1*(Gamt3 - localGamt3);

// eqn. for Theta
zTheta = 0.5 * alpha * (Ric - AAt + 2.0/3.0*localK*localK) - alpha * kap1*(2.0+kappa2)*Thet + aTheta;

// damp evolution of theta with radius if requested
if (theta_damping_radius > 0)
   zTheta = (r[ijk] >= theta_damping_radius ? zTheta * theta_damping_radius / r[ijk]
               : zTheta);