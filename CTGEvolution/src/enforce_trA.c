#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "ctgbase_util.h"
#include "loopcontrol.h"

void
CTGEvolution_SubtractTrA(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT istart[3], iend[3];

  if (verbose)
    CCTK_INFO("Subtracting trA from Aij.");

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGEvolution_SubtractTrA,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          CCTK_INT ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
          CCTK_REAL ig11, ig12, ig13, ig22, ig23, ig33;
          CCTK_REAL detg;

          CTGBase_invert_metric(gamma11[ijk], gamma12[ijk], gamma13[ijk],
                                gamma22[ijk], gamma23[ijk], gamma33[ijk],
                                detg,
                                ig11, ig12, ig13, ig22, ig23, ig33);
          
          CCTK_REAL trk = ig11*A11[ijk] + ig22*A22[ijk] + ig33*A33[ijk] + 
            2.0*(ig12*A12[ijk] + ig13*A13[ijk] + ig23*A23[ijk]);
          
          A11[ijk] -= (trk/3.0) * gamma11[ijk];
          A12[ijk] -= (trk/3.0) * gamma12[ijk];
          A13[ijk] -= (trk/3.0) * gamma13[ijk];
          A22[ijk] -= (trk/3.0) * gamma22[ijk];
          A23[ijk] -= (trk/3.0) * gamma23[ijk];
          A33[ijk] -= (trk/3.0) * gamma33[ijk];
        }
  LC_ENDLOOP3 (CTGEvolution_SubtractTrA);

  return;
}
