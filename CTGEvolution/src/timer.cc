#include "timestat.hh"

namespace CTGEvolution {
  using namespace std;
  using namespace CarpetLib;

  Timer * diff_timer = 0;
  Timer * rhs_timer = 0;

  extern "C" void
  start_diff_timer(void)
  {
    if (not diff_timer) diff_timer = new Timer ("diff");
    diff_timer->start();
  }

  extern "C" void
  stop_diff_timer(void)
  {
    diff_timer->stop(0);
  }

  extern "C" void
  start_rhs_timer(void)
  {
    if (not rhs_timer) rhs_timer = new Timer ("rhs");
    rhs_timer->start();
  }

  extern "C" void
  stop_rhs_timer(void)
  {
    rhs_timer->stop(0);
  }

  extern "C" void
  print_timers(void)
  {
    diff_timer->outputData(cout);
    rhs_timer->outputData(cout);
  }
}
