#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <unistd.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "GlobalDerivative.h"
#include "AllDerivative.h"
#include "AllDerivative_8th.h"
#include "Jacobian.h"
#include "ctgbase_util.h"

#include "loopcontrol.h"

#include "timer.h"


#define Power(x,y) (pow((CCTK_REAL) (x), (CCTK_REAL) (y)))

void
CTGEvolution_PrintTimers(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  print_timers();
  return;
}


void
CTGEvolution_InitRHS(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  CCTK_INT istart[3], iend[3];

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGEvolution_init_rhs,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
       {
          int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

          dt_phi[ijk] = 0.0;

          dt_gamma11[ijk] = 0.0;
          dt_gamma12[ijk] = 0.0;
          dt_gamma13[ijk] = 0.0;
          dt_gamma22[ijk] = 0.0;
          dt_gamma23[ijk] = 0.0;
          dt_gamma33[ijk] = 0.0;

          dt_K[ijk] = 0.0;

          dt_A11[ijk] = 0.0;
          dt_A12[ijk] = 0.0;
          dt_A13[ijk] = 0.0;
          dt_A22[ijk] = 0.0;
          dt_A23[ijk] = 0.0;
          dt_A33[ijk] = 0.0;

          dt_Gamma1[ijk] = 0.0;
          dt_Gamma2[ijk] = 0.0;
          dt_Gamma3[ijk] = 0.0;
          
          if (EVOL_TYPE == Z4c_TYPE)
          {
             dt_Theta[ijk] = 0;
          }
       }
  LC_ENDLOOP3 (CTGEvolution_init_rhs);

  return;
}


/*
 * Main source calculation routine.
 */
void
CTGEvolution_CalcRHS(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT * restrict imin[3], * restrict imin_plus[3], * restrict imin_minus[3];
  CCTK_INT * restrict imax[3], * restrict imax_plus[3], * restrict imax_minus[3];
  CCTK_INT * restrict imin2[3], * restrict imax2[3];
  CCTK_REAL * restrict q[3], * restrict q_plus[3], * restrict q_minus[3],
    * restrict q2[3];
  CCTK_REAL ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz;
  CCTK_REAL *vars[22];
  CCTK_REAL *avars[18];
  CCTK_INT is_cartesian;
  CCTK_INT bsize[6];
  int nvars = 21;
  int navars = 17;

  if (EVOL_TYPE == Z4c_TYPE) {
    nvars = 22;
    navars = 18;
  }

  vars[0] = phi;
  vars[1] = gamma11;
  vars[2] = gamma12;
  vars[3] = gamma13;
  vars[4] = gamma22;
  vars[5] = gamma23;
  vars[6] = gamma33;
  vars[7] = alp;
  vars[8] = betax;
  vars[9] = betay;
  vars[10] = betaz;
  vars[11] = EVOL_TYPE == BSSN_TYPE ? K : Khat;
  vars[12] = A11;
  vars[13] = A12;
  vars[14] = A13;
  vars[15] = A22;
  vars[16] = A23;
  vars[17] = A33;
  vars[18] = Gamma1;
  vars[19] = Gamma2;
  vars[20] = Gamma3;
  vars[21] = Theta;

  avars[0] = phi;
  avars[1] = gamma11;
  avars[2] = gamma12;
  avars[3] = gamma13;
  avars[4] = gamma22;
  avars[5] = gamma23;
  avars[6] = gamma33;
  avars[7] = EVOL_TYPE == BSSN_TYPE ? K : Khat;
  avars[8] = A11;
  avars[9] = A12;
  avars[10] = A13;
  avars[11] = A22;
  avars[12] = A23;
  avars[13] = A33;
  avars[14] = Gamma1;
  avars[15] = Gamma2;
  avars[16] = Gamma3;
  avars[17] = Theta;

  if (verbose)
    CCTK_INFO("Calculating evolution variable sources.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  /*
   * Grid spacings required by the finite difference operators.
   */
  ihx = 1.0 / CCTK_DELTA_SPACE(0);
  ihy = 1.0 / CCTK_DELTA_SPACE(1);
  ihz = 1.0 / CCTK_DELTA_SPACE(2);
  ihxx = ihx*ihx;
  ihxy = ihx*ihy;
  ihxz = ihx*ihz;
  ihyy = ihy*ihy;
  ihyz = ihy*ihz;
  ihzz = ihz*ihz;

  /* 
   * Figure out if we are on a Cartesian patch.
   */
  if ( CCTK_IsFunctionAliased("MultiPatch_GetMap") &&
       CCTK_IsFunctionAliased("MultiPatch_MapIsCartesian") )
  {
    is_cartesian = MultiPatch_MapIsCartesian(MultiPatch_GetMap(cctkGH));
  } else {
    is_cartesian = !(*general_coordinates);
  }

  /*
   * Call SummationByParts for finite-difference operator coefficients.
   */
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax,
                  (CCTK_POINTER_TO_CONST *) q, 1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_plus,
                  (CCTK_POINTER_TO_CONST *) imax_plus,
                  (CCTK_POINTER_TO_CONST *) q_plus, 1, +(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_minus,
                  (CCTK_POINTER_TO_CONST *) imax_minus,
                  (CCTK_POINTER_TO_CONST *) q_minus, 1,
                  -(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin2,
                  (CCTK_POINTER_TO_CONST *) imax2,
                  (CCTK_POINTER_TO_CONST *) q2, 2, 0);

  Util_GetGridRanges(cctkGH, istart, iend);

  GetBoundWidth(cctkGH, bsize, -1);
 
  CCTK_INT fd_order
     = *(CCTK_INT*) CCTK_ParameterGet ("order", "SummationByParts", NULL);

#pragma omp parallel
  LC_LOOP3 (CTGEvolution_calc_rhs,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
       {
          int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

          CCTK_REAL detg;

          CCTK_INT * restrict adv_imin[3];
          CCTK_INT * restrict adv_imax[3];
          CCTK_REAL * restrict adv_q[3];

          CCTK_REAL dvars[22][3];
          CCTK_REAL xdvars[22][3];
          CCTK_REAL davars[18][3];
          CCTK_REAL xdavars[18][3];
          CCTK_REAL d2vars[11][6];
          CCTK_REAL xd2vars[11][6];
          CCTK_REAL up_dir[3];

#include "ctgamma_rhs_declare.h"
#include "ctgamma_Z4c_rhs_declare.h"

          /*
           *  Assign local variables.
           */
          localphi = phi[ijk];

          gt11 = gamma11[ijk];
          gt12 = gamma12[ijk];
          gt13 = gamma13[ijk];
          gt22 = gamma22[ijk];
          gt23 = gamma23[ijk];
          gt33 = gamma33[ijk];

          localK = K[ijk];

          At11 = A11[ijk];
          At12 = A12[ijk];
          At13 = A13[ijk];
          At22 = A22[ijk];
          At23 = A23[ijk];
          At33 = A33[ijk];

          Gamt1 = Gamma1[ijk];
          Gamt2 = Gamma2[ijk];
          Gamt3 = Gamma3[ijk];

          alpha = alp[ijk];
          beta1 = betax[ijk];
          beta2 = betay[ijk];
          beta3 = betaz[ijk];

          em4phi = exp(-4*localphi);

          CTGBase_invert_metric(gt11, gt12, gt13, gt22, gt23, gt33, detg,
                                igt11, igt12, igt13, igt22, igt23, igt33);

          /*
           * If necessary transform the shift vector to local coordinates.
           */
          if (is_cartesian)
          {
            up_dir[0] = betax[ijk];
            up_dir[1] = betay[ijk];
            up_dir[2] = betaz[ijk];
          }
          else
          {
            up_dir[0] = J11[ijk] * betax[ijk] 
                      + J12[ijk] * betay[ijk]
                      + J13[ijk] * betaz[ijk];
            up_dir[1] = J21[ijk] * betax[ijk] 
                      + J22[ijk] * betay[ijk]
                      + J23[ijk] * betaz[ijk];
            up_dir[2] = J31[ijk] * betax[ijk] 
                      + J32[ijk] * betay[ijk]
                      + J33[ijk] * betaz[ijk];
          }

          /*
           * Calculate required partial derivatives.
           */
          CTGBase_set_advective_stencil(adv_q[0], adv_imin[0], adv_imax[0],
                                        up_dir[0], q[0], imin[0], imax[0],
                                        q_minus[0], imin_minus[0],
                                        imax_minus[0], q_plus[0], imin_plus[0],
                                        imax_plus[0]);
          CTGBase_set_advective_stencil(adv_q[1], adv_imin[1], adv_imax[1],
                                        up_dir[1], q[1], imin[1], imax[1],
                                        q_minus[1], imin_minus[1],
                                        imax_minus[1], q_plus[1], imin_plus[1],
                                        imax_plus[1]);
          CTGBase_set_advective_stencil(adv_q[2], adv_imin[2], adv_imax[2],
                                        up_dir[2], q[2], imin[2], imax[2],
                                        q_minus[2], imin_minus[2],
                                        imax_minus[2], q_plus[2], imin_plus[2],
                                        imax_plus[2]);

          if (timer_info)
            start_diff_timer();
#include "derivatives.h"
          if (timer_info)
            stop_diff_timer();

          /*
           * Calculate source functions.
           */

          if (timer_info)
            start_rhs_timer();
#include "ctgamma_rhs_calc.h"
          if (timer_info)
            stop_rhs_timer();

          /*
           * Assign RHS = source + advection terms.
           */         
          dt_phi[ijk] += sphi + aphi;

          dt_gamma11[ijk] += sgt11 + agt11;
          dt_gamma12[ijk] += sgt12 + agt12;
          dt_gamma13[ijk] += sgt13 + agt13;
          dt_gamma22[ijk] += sgt22 + agt22;
          dt_gamma23[ijk] += sgt23 + agt23;
          dt_gamma33[ijk] += sgt33 + agt33;

          dt_K[ijk] += sK + aK;

          dt_A11[ijk] += sAt11 + aAt11;
          dt_A12[ijk] += sAt12 + aAt12;
          dt_A13[ijk] += sAt13 + aAt13;
          dt_A22[ijk] += sAt22 + aAt22;
          dt_A23[ijk] += sAt23 + aAt23;
          dt_A33[ijk] += sAt33 + aAt33;

          dt_Gamma1[ijk] += sGamt1 + aGamt1;
          dt_Gamma2[ijk] += sGamt2 + aGamt2;
          dt_Gamma3[ijk] += sGamt3 + aGamt3;
          
          if (EVOL_TYPE == Z4c_TYPE)
          {
             CCTK_REAL Thet = Theta[ijk];

#include "ctgamma_Z4c_rhs_calc.h"

             dt_K[ijk] += zK;

             dt_Theta[ijk] += zTheta;

             dt_Gamma1[ijk] += zGamt1;
             dt_Gamma2[ijk] += zGamt2;
             dt_Gamma3[ijk] += zGamt3;

          }

        }
  LC_ENDLOOP3 (CTGEvolution_calc_rhs);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin_minus, imax_minus, q_minus);
  CTGBase_free_stencil(imin_plus, imax_plus, q_plus);
  CTGBase_free_stencil(imin2, imax2, q2);

  return;
}
