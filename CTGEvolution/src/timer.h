#ifndef TIMER_H
#define TIMER_H

void start_diff_timer(void);
void stop_diff_timer(void);

void start_rhs_timer(void);
void stop_rhs_timer(void);

void print_timers(void);

#endif  /* #ifndef TIMER_H */
