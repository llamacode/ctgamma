#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "ctgbase_util.h"
#include "loopcontrol.h"

void
CTGEvolution_Compute_K_from_Khat(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT istart[3], iend[3];

  if (verbose)
    CCTK_INFO("Z4c: Compute K from Khat.");

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGEvolution_ComputeK,
            i, j, k,
            0, 0, 0,
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          CCTK_INT ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

          K[ijk] = Khat[ijk] + 2.0*Theta[ijk];
        }
  LC_ENDLOOP3 (CTGEvolution_ComputeK);

  return;
}
