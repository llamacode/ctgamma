#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Symmetry.h"

#include "ctgbase_util.h"


void
CTGEvolution_MoLRegister(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  int ierr=0;

  CCTK_INFO("Registering evolution variables with MoL.");

  if (EVOL_TYPE == BSSN_TYPE) {
     if (MaxNumEvolvedVars != 17 || MaxNumConstrainedVars != 12)
        CCTK_WARN(1, "Set MaxNumEvolvedVars=17 and MaxNumConstrainedVars=12 for BSSN system.");
  } else if (EVOL_TYPE == Z4c_TYPE) {
     if (MaxNumEvolvedVars != 18 || MaxNumConstrainedVars != 13)
        CCTK_WARN(0, "Set MaxNumEvolvedVars=18 and MaxNumConstrainedVars=13 for Z4c system.");
  }

  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::phi"),
                             CCTK_VarIndex("CTGEvolution::dt_phi"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::gamma11"),
                             CCTK_VarIndex("CTGEvolution::dt_gamma11"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::gamma12"),
                             CCTK_VarIndex("CTGEvolution::dt_gamma12"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::gamma13"),
                             CCTK_VarIndex("CTGEvolution::dt_gamma13"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::gamma22"),
                             CCTK_VarIndex("CTGEvolution::dt_gamma22"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::gamma23"),
                             CCTK_VarIndex("CTGEvolution::dt_gamma23"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::gamma33"),
                             CCTK_VarIndex("CTGEvolution::dt_gamma33"));

  if (EVOL_TYPE == BSSN_TYPE) {
     ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::K"),
                                CCTK_VarIndex("CTGEvolution::dt_K"));
  } else if (EVOL_TYPE == Z4c_TYPE) {
     ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::Khat"),
                                CCTK_VarIndex("CTGEvolution::dt_K"));
     ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::Theta"),
                                CCTK_VarIndex("CTGEvolution::dt_Theta"));
  }
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::A11"),
                             CCTK_VarIndex("CTGEvolution::dt_A11"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::A12"),
                             CCTK_VarIndex("CTGEvolution::dt_A12"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::A13"),
                             CCTK_VarIndex("CTGEvolution::dt_A13"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::A22"),
                             CCTK_VarIndex("CTGEvolution::dt_A22"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::A23"),
                             CCTK_VarIndex("CTGEvolution::dt_A23"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::A33"),
                             CCTK_VarIndex("CTGEvolution::dt_A33"));

  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::Gamma1"),
                             CCTK_VarIndex("CTGEvolution::dt_Gamma1"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::Gamma2"),
                             CCTK_VarIndex("CTGEvolution::dt_Gamma2"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGBase::Gamma3"),
                             CCTK_VarIndex("CTGEvolution::dt_Gamma3"));


  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::gxx"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::gxy"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::gxz"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::gyy"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::gyz"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::gzz"));

  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::kxx"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::kxy"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::kxz"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::kyy"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::kyz"));
  ierr += MoLRegisterConstrained(CCTK_VarIndex("ADMBase::kzz"));
  
  if (EVOL_TYPE == Z4c_TYPE) {
    ierr += MoLRegisterConstrained(CCTK_VarIndex("CTGBase::K"));
  }
  
  if (ierr != 0)
    CCTK_WARN(0,"Problems registering variables with MoL");

  return;
}

void
CTGEvolution_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  int sym[3];
  int ierr=0;

  CCTK_INFO("Setting symmetries for source variables.");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = +1;
  
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_phi");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_gamma11");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_gamma22");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_gamma33");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_K");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_A11");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_A22");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_A33");

  if (EVOL_TYPE == Z4c_TYPE) {
    ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_Theta");
  }

  sym[0] = -1;
  sym[1] = -1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_gamma12");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_A12");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_gamma13");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_A13");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_gamma23");
  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_A23");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_Gamma1");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_Gamma2");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGEvolution::dt_Gamma3");

  if (ierr != 0)
    CCTK_WARN(0, "Couldn't set GF symmetries.");
  
  return;
}
