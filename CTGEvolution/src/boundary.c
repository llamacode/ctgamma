#include "cctk.h"
#include "util_Table.h"

#include "math.h"

#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

#include "ctgbase_util.h"

void
CTGEvolution_SelectBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  CCTK_INT faces;

  static int v_gauge_lim_0 = -2;
  static int v_gauge_lim_1 = -2;
  static int v_1_lim_0 = -2;
  static int v_1_lim_1 = -2;
  int ierr = 0;

  faces = CCTK_ALL_FACES;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Selecting boundary condition: %s", bc);

  if (CCTK_EQUALS(bc, "none") || CCTK_EQUALS(bc, "radiative"))
    {
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::conformal_factor", "None");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::conformal_metric", "None");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::curvature_tensor", "None");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::Gamma", "None");
      if (EVOL_TYPE == Z4c_TYPE) {
         ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::curvature_scalar_a", "None");
         ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::curvature_scalar_b", "None");
      } else if (EVOL_TYPE == BSSN_TYPE) {
         ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::curvature_scalar", "None");
      }
    }

  else if (CCTK_EQUALS(bc, "reflective"))
    {
      if (EVOL_TYPE == Z4c_TYPE) {
         CCTK_WARN(0, "Reflective boundary condition not implemented for Z4c");
      }
      
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::conformal_factor", "Flat");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::conformal_metric", "Flat");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::curvature_scalar", "Flat");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::curvature_tensor", "Flat");
      ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                        "CTGBase::Gamma", "Flat");
    }

  else if (CCTK_EQUALS(bc, "cactus-radiative"))
    {
      if (EVOL_TYPE == Z4c_TYPE) {
         CCTK_WARN(0, "cactus-radiative boundary condition not implemented for Z4c");
      }
    
      if (cctk_iteration==0)
        {
          /* phi, K propagate at the gauge speed set by the lapse */
          if (v_gauge_lim_0 == -2)
            {
              CCTK_REAL gauge_v0 = GetGaugeSpeed(cctkGH);
              CCTK_REAL limit = 0.0;
              v_gauge_lim_0 = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              ierr += Util_TableSetReal(v_gauge_lim_0, gauge_v0, "SPEED");
              ierr += Util_TableSetReal(v_gauge_lim_0, limit, "LIMIT");
            }

          if (v_gauge_lim_1 == -2)
            {
              CCTK_REAL gauge_v0 = GetGaugeSpeed(cctkGH);
              CCTK_REAL limit = 1.0;
              v_gauge_lim_1 = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              ierr += Util_TableSetReal(v_gauge_lim_1, gauge_v0, "SPEED");
              ierr += Util_TableSetReal(v_gauge_lim_1, limit, "LIMIT");
            }

          /* diagonal metric has speed 1.0, and outer limit of 1.0 */
          if (v_1_lim_1 == -2)
            {
              v_1_lim_1 = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              ierr += Util_TableSetReal(v_1_lim_1, 1.0, "SPEED");
              ierr += Util_TableSetReal(v_1_lim_1, 1.0, "LIMIT");
            }
	      
          /* off-diagonal metric components, Gamma, Aij */
          if (v_1_lim_0 == -2)
            {
              v_1_lim_0 = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              ierr += Util_TableSetReal(v_1_lim_0, 1.0, "SPEED");
              ierr += Util_TableSetReal(v_1_lim_0, 0.0, "LIMIT");
            }
	  
          ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                            "CTGBase::conformal_factor",
                                            "None");
          ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                            "CTGBase::conformal_metric",
                                            "None");
          ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                            "CTGBase::curvature_scalar",
                                            "None");
          ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                            "CTGBase::curvature_tensor",
                                            "None");
          ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                            "CTGBase::Gamma", "None");
        }
      else
        {
          if (CCTK_EQUALS(conformal_factor_type, "phi"))
            ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_gauge_lim_0,
                                            "CTGBase::phi", "Radiation");
          else
            ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_gauge_lim_1,
                                            "CTGBase::phi", "Radiation");

          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_1,
                                          "CTGBase::gamma11", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::gamma12", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::gamma13", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_1,
                                          "CTGBase::gamma22", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::gamma23", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_1,
                                          "CTGBase::gamma33", "Radiation");

          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_gauge_lim_0,
                                          "CTGBase::K", "Radiation"); 

          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::A11", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::A12", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::A13", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::A22", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::A23", "Radiation");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::A33", "Radiation");

          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::Gamma1", "None");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::Gamma2", "None");
          ierr += Boundary_SelectVarForBC(cctkGH, faces, 1, v_1_lim_0,
                                          "CTGBase::Gamma3", "None");
        }
    }

  else
    ierr += 1;

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}


void
CTGEvolution_ApplyRadiativeBC(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  static int have_indices = 0;
  static CCTK_INT phi_idx;
  static CCTK_INT gamma11_idx, gamma12_idx, gamma13_idx, gamma22_idx,
    gamma23_idx, gamma33_idx;
  static CCTK_INT K_idx, Theta_idx;
  static CCTK_INT A11_idx, A12_idx, A13_idx, A22_idx, A23_idx, A33_idx;
  static CCTK_INT Gamma1_idx, Gamma2_idx, Gamma3_idx;

  static CCTK_INT dt_phi_idx;
  static CCTK_INT dt_gamma11_idx, dt_gamma12_idx, dt_gamma13_idx,
    dt_gamma22_idx, dt_gamma23_idx, dt_gamma33_idx;
  static CCTK_INT dt_K_idx, dt_Theta_idx;
  static CCTK_INT dt_A11_idx, dt_A12_idx, dt_A13_idx, dt_A22_idx,
    dt_A23_idx, dt_A33_idx;
  static CCTK_INT dt_Gamma1_idx, dt_Gamma2_idx, dt_Gamma3_idx;

  static CCTK_REAL gauge_v0;

  CCTK_INT ierr=0;

  if (verbose)
    CCTK_INFO("Applying radiative bc to sources.");


  if (! have_indices) {
    /* Initialise these only once, since CCTK_VarIndex can be
       annoyingly expensive on small grids  */
    have_indices = 1;

    gauge_v0 =  GetGaugeSpeed(cctkGH);

    phi_idx = CCTK_VarIndex("CTGBase::phi");
    gamma11_idx = CCTK_VarIndex("CTGBase::gamma11");
    gamma12_idx = CCTK_VarIndex("CTGBase::gamma12");
    gamma13_idx = CCTK_VarIndex("CTGBase::gamma13");
    gamma22_idx = CCTK_VarIndex("CTGBase::gamma22");
    gamma23_idx = CCTK_VarIndex("CTGBase::gamma23");
    gamma33_idx = CCTK_VarIndex("CTGBase::gamma33");
    //K_idx = CCTK_VarIndex("CTGBase::K");
    A11_idx = CCTK_VarIndex("CTGBase::A11");
    A12_idx = CCTK_VarIndex("CTGBase::A12");
    A13_idx = CCTK_VarIndex("CTGBase::A13");
    A22_idx = CCTK_VarIndex("CTGBase::A22");
    A23_idx = CCTK_VarIndex("CTGBase::A23");
    A33_idx = CCTK_VarIndex("CTGBase::A33");
    Gamma1_idx = CCTK_VarIndex("CTGBase::Gamma1");
    Gamma2_idx = CCTK_VarIndex("CTGBase::Gamma2");
    Gamma3_idx = CCTK_VarIndex("CTGBase::Gamma3");

    dt_phi_idx = CCTK_VarIndex("CTGEvolution::dt_phi");
    dt_gamma11_idx = CCTK_VarIndex("CTGEvolution::dt_gamma11");
    dt_gamma12_idx = CCTK_VarIndex("CTGEvolution::dt_gamma12");
    dt_gamma13_idx = CCTK_VarIndex("CTGEvolution::dt_gamma13");
    dt_gamma22_idx = CCTK_VarIndex("CTGEvolution::dt_gamma22");
    dt_gamma23_idx = CCTK_VarIndex("CTGEvolution::dt_gamma23");
    dt_gamma33_idx = CCTK_VarIndex("CTGEvolution::dt_gamma33");
    dt_K_idx = CCTK_VarIndex("CTGEvolution::dt_K");
    dt_A11_idx = CCTK_VarIndex("CTGEvolution::dt_A11");
    dt_A12_idx = CCTK_VarIndex("CTGEvolution::dt_A12");
    dt_A13_idx = CCTK_VarIndex("CTGEvolution::dt_A13");
    dt_A22_idx = CCTK_VarIndex("CTGEvolution::dt_A22");
    dt_A23_idx = CCTK_VarIndex("CTGEvolution::dt_A23");
    dt_A33_idx = CCTK_VarIndex("CTGEvolution::dt_A33");
    dt_Gamma1_idx = CCTK_VarIndex("CTGEvolution::dt_Gamma1");
    dt_Gamma2_idx = CCTK_VarIndex("CTGEvolution::dt_Gamma2");
    dt_Gamma3_idx = CCTK_VarIndex("CTGEvolution::dt_Gamma3");
    
    if (EVOL_TYPE == Z4c_TYPE) {
      K_idx = CCTK_VarIndex("CTGBase::Khat");
      Theta_idx = CCTK_VarIndex("CTGBase::Theta");
      dt_Theta_idx = CCTK_VarIndex("CTGEvolution::dt_Theta");
    } else if (EVOL_TYPE == BSSN_TYPE) {
      K_idx = CCTK_VarIndex("CTGBase::K");
    }
  }

  if (CCTK_EQUALS(conformal_factor_type, "phi"))
    ierr += ApplyBC_Radiative(cctkGH, phi_idx, dt_phi_idx, gauge_v0, 0.0);
  else
    ierr += ApplyBC_Radiative(cctkGH, phi_idx, dt_phi_idx, gauge_v0, 1.0);
	  
  ierr += ApplyBC_Radiative(cctkGH, gamma11_idx, dt_gamma11_idx, 1.0, 1.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma12_idx, dt_gamma12_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma13_idx, dt_gamma13_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma22_idx, dt_gamma22_idx, 1.0, 1.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma23_idx, dt_gamma23_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, gamma33_idx, dt_gamma33_idx, 1.0, 1.0);

  ierr += ApplyBC_Radiative(cctkGH, K_idx, dt_K_idx, gauge_v0, 0.0);
	  
  ierr += ApplyBC_Radiative(cctkGH, A11_idx, dt_A11_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, A12_idx, dt_A12_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, A13_idx, dt_A13_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, A22_idx, dt_A22_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, A23_idx, dt_A23_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, A33_idx, dt_A33_idx, 1.0, 0.0);
	  
  ierr += ApplyBC_Radiative(cctkGH, Gamma1_idx, dt_Gamma1_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, Gamma2_idx, dt_Gamma2_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, Gamma3_idx, dt_Gamma3_idx, 1.0, 0.0);

  if (EVOL_TYPE == Z4c_TYPE)
     ierr += ApplyBC_Radiative(cctkGH, Theta_idx, dt_Theta_idx, 1.0, 0.0);

  if (ierr)
    CCTK_WARN(0, "Error applying radiative BC.");

  return;
}


void
CTGEvolution_SelectRHSBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT faces;
  int ierr = 0;

  faces = CCTK_ALL_FACES;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Selecting boundary condition");

  ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                    "CTGEvolution::conformal_factor_source",
                                    "None");
  ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                    "CTGEvolution::conformal_metric_source",
                                    "None");
  ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                    "CTGEvolution::curvature_scalar_source",
                                    "None");
  ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                    "CTGEvolution::curvature_tensor_source",
                                    "None");
  ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
                                    "CTGEvolution::Gamma_source", "None");
  
  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}
