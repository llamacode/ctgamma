Q
  = 1/(12.*N);
em4phi
  = Power(localphi,-1/(3.*N));
em4phim1
  = Power(localphi,-1 - 1/(3.*N));
em4phim2
  = Power(localphi,-2 - 1/(3.*N));
g11
  = gt11/em4phi;
g12
  = gt12/em4phi;
g13
  = gt13/em4phi;
g22
  = gt22/em4phi;
g23
  = gt23/em4phi;
g33
  = gt33/em4phi;
ChrDt111
  = dgt111/2.;
ChrDt112
  = dgt112/2.;
ChrDt113
  = dgt113/2.;
ChrDt122
  = dgt122 - dgt221/2.;
ChrDt123
  = (dgt123 + dgt132 - dgt231)/2.;
ChrDt133
  = dgt133 - dgt331/2.;
ChrDt211
  = -dgt112/2. + dgt121;
ChrDt212
  = dgt221/2.;
ChrDt213
  = (dgt123 - dgt132 + dgt231)/2.;
ChrDt222
  = dgt222/2.;
ChrDt223
  = dgt223/2.;
ChrDt233
  = dgt233 - dgt332/2.;
ChrDt311
  = -dgt113/2. + dgt131;
ChrDt312
  = (-dgt123 + dgt132 + dgt231)/2.;
ChrDt313
  = dgt331/2.;
ChrDt322
  = -dgt223/2. + dgt232;
ChrDt323
  = dgt332/2.;
ChrDt333
  = dgt333/2.;
Chrt111
  = ChrDt111*igt11 + ChrDt211*igt12 + ChrDt311*igt13;
Chrt112
  = ChrDt112*igt11 + ChrDt212*igt12 + ChrDt312*igt13;
Chrt113
  = ChrDt113*igt11 + ChrDt213*igt12 + ChrDt313*igt13;
Chrt122
  = ChrDt122*igt11 + ChrDt222*igt12 + ChrDt322*igt13;
Chrt123
  = ChrDt123*igt11 + ChrDt223*igt12 + ChrDt323*igt13;
Chrt133
  = ChrDt133*igt11 + ChrDt233*igt12 + ChrDt333*igt13;
Chrt211
  = ChrDt111*igt12 + ChrDt211*igt22 + ChrDt311*igt23;
Chrt212
  = ChrDt112*igt12 + ChrDt212*igt22 + ChrDt312*igt23;
Chrt213
  = ChrDt113*igt12 + ChrDt213*igt22 + ChrDt313*igt23;
Chrt222
  = ChrDt122*igt12 + ChrDt222*igt22 + ChrDt322*igt23;
Chrt223
  = ChrDt123*igt12 + ChrDt223*igt22 + ChrDt323*igt23;
Chrt233
  = ChrDt133*igt12 + ChrDt233*igt22 + ChrDt333*igt23;
Chrt311
  = ChrDt111*igt13 + ChrDt211*igt23 + ChrDt311*igt33;
Chrt312
  = ChrDt112*igt13 + ChrDt212*igt23 + ChrDt312*igt33;
Chrt313
  = ChrDt113*igt13 + ChrDt213*igt23 + ChrDt313*igt33;
Chrt322
  = ChrDt122*igt13 + ChrDt222*igt23 + ChrDt322*igt33;
Chrt323
  = ChrDt123*igt13 + ChrDt223*igt23 + ChrDt323*igt33;
Chrt333
  = ChrDt133*igt13 + ChrDt233*igt23 + ChrDt333*igt33;
localGamt1
  = Chrt111*igt11 + Chrt122*igt22 + 2*(Chrt112*igt12 + Chrt113*igt13 + Chrt123*igt23) + Chrt133*igt33;
localGamt2
  = Chrt211*igt11 + Chrt222*igt22 + 2*(Chrt212*igt12 + Chrt213*igt13 + Chrt223*igt23) + Chrt233*igt33;
localGamt3
  = Chrt311*igt11 + Chrt322*igt22 + 2*(Chrt312*igt12 + Chrt313*igt13 + Chrt323*igt23) + Chrt333*igt33;
dalphadphi
  = dalpha1*dphi1*igt11 + (dalpha2*dphi1 + dalpha1*dphi2)*igt12 + (dalpha3*dphi1 + dalpha1*dphi3)*igt13 + dalpha2*dphi2*igt22 + (dalpha3*dphi2 + dalpha2*dphi3)*igt23 + dalpha3*dphi3*igt33;
pDDalpha11
  = (-(Chrt111*dalpha1) - Chrt211*dalpha2 - Chrt311*dalpha3 + ddalpha11)*localphi + (-4*dalpha1*dphi1 + 2*dalphadphi*gt11)*Q;
pDDalpha12
  = (-(Chrt112*dalpha1) - Chrt212*dalpha2 - Chrt312*dalpha3 + ddalpha12)*localphi + 2*(-(dalpha2*dphi1) - dalpha1*dphi2 + dalphadphi*gt12)*Q;
pDDalpha13
  = (-(Chrt113*dalpha1) - Chrt213*dalpha2 - Chrt313*dalpha3 + ddalpha13)*localphi + 2*(-(dalpha3*dphi1) - dalpha1*dphi3 + dalphadphi*gt13)*Q;
pDDalpha22
  = (-(Chrt122*dalpha1) - Chrt222*dalpha2 - Chrt322*dalpha3 + ddalpha22)*localphi + (-4*dalpha2*dphi2 + 2*dalphadphi*gt22)*Q;
pDDalpha23
  = (-(Chrt123*dalpha1) - Chrt223*dalpha2 - Chrt323*dalpha3 + ddalpha23)*localphi + 2*(-(dalpha3*dphi2) - dalpha2*dphi3 + dalphadphi*gt23)*Q;
pDDalpha33
  = (-(Chrt133*dalpha1) - Chrt233*dalpha2 - Chrt333*dalpha3 + ddalpha33)*localphi + (-4*dalpha3*dphi3 + 2*dalphadphi*gt33)*Q;
DDalpha
  = em4phi*(ddalpha11*igt11 + ddalpha22*igt22 + 2*(ddalpha12*igt12 + ddalpha13*igt13 + ddalpha23*igt23) + ddalpha33*igt33 - dalpha1*localGamt1 - dalpha2*localGamt2 - dalpha3*localGamt3) + 2*dalphadphi*em4phim1*Q;
pDDalphaTF11
  = -(DDalpha*g11*localphi)/3. + pDDalpha11;
pDDalphaTF12
  = -(DDalpha*g12*localphi)/3. + pDDalpha12;
pDDalphaTF13
  = -(DDalpha*g13*localphi)/3. + pDDalpha13;
pDDalphaTF22
  = -(DDalpha*g22*localphi)/3. + pDDalpha22;
pDDalphaTF23
  = -(DDalpha*g23*localphi)/3. + pDDalpha23;
pDDalphaTF33
  = -(DDalpha*g33*localphi)/3. + pDDalpha33;
divbeta
  = dbeta11 + dbeta22 + dbeta33;
ddivbeta1
  = ddbeta111 + ddbeta212 + ddbeta313;
ddivbeta2
  = ddbeta112 + ddbeta222 + ddbeta323;
ddivbeta3
  = ddbeta113 + ddbeta223 + ddbeta333;
DDphit11
  = ddphi11 - Chrt111*dphi1 - Chrt211*dphi2 - Chrt311*dphi3;
DDphit12
  = ddphi12 - Chrt112*dphi1 - Chrt212*dphi2 - Chrt312*dphi3;
DDphit13
  = ddphi13 - Chrt113*dphi1 - Chrt213*dphi2 - Chrt313*dphi3;
DDphit22
  = ddphi22 - Chrt122*dphi1 - Chrt222*dphi2 - Chrt322*dphi3;
DDphit23
  = ddphi23 - Chrt123*dphi1 - Chrt223*dphi2 - Chrt323*dphi3;
DDphit33
  = ddphi33 - Chrt133*dphi1 - Chrt233*dphi2 - Chrt333*dphi3;
DDphit
  = DDphit11*igt11 + DDphit22*igt22 + 2*(DDphit12*igt12 + DDphit13*igt13 + DDphit23*igt23) + DDphit33*igt33;
AAt11
  = Power(At11,2)*igt11 + Power(At12,2)*igt22 + 2*(At11*(At12*igt12 + At13*igt13) + At12*At13*igt23) + Power(At13,2)*igt33;
AAt12
  = (Power(At12,2) + At11*At22)*igt12 + (At12*At13 + At11*At23)*igt13 + At12*(At11*igt11 + At22*igt22) + (At13*At22 + At12*At23)*igt23 + At13*At23*igt33;
AAt13
  = (At12*At13 + At11*At23)*igt12 + (Power(At13,2) + At11*At33)*igt13 + At12*At23*igt22 + (At13*At23 + At12*At33)*igt23 + At13*(At11*igt11 + At33*igt33);
AAt22
  = Power(At12,2)*igt11 + Power(At22,2)*igt22 + 2*(At12*(At22*igt12 + At23*igt13) + At22*At23*igt23) + Power(At23,2)*igt33;
AAt23
  = At12*At13*igt11 + (At13*At22 + At12*At23)*igt12 + (At13*At23 + At12*At33)*igt13 + (Power(At23,2) + At22*At33)*igt23 + At23*(At22*igt22 + At33*igt33);
AAt33
  = Power(At13,2)*igt11 + Power(At23,2)*igt22 + 2*(At13*(At23*igt12 + At33*igt13) + At23*At33*igt23) + Power(At33,2)*igt33;
AAt
  = AAt11*igt11 + AAt22*igt22 + 2*(AAt12*igt12 + AAt13*igt13 + AAt23*igt23) + AAt33*igt33;
iAt11
  = At11*Power(igt11,2) + At22*Power(igt12,2) + At33*Power(igt13,2) + 2*(At23*igt12*igt13 + igt11*(At12*igt12 + At13*igt13));
iAt12
  = At12*Power(igt12,2) + igt11*(At11*igt12 + At12*igt22 + At13*igt23) + igt12*(At13*igt13 + At22*igt22 + At23*igt23) + igt13*(At23*igt22 + At33*igt23);
iAt13
  = At13*Power(igt13,2) + igt11*(At11*igt13 + At12*igt23 + At13*igt33) + igt12*(At12*igt13 + At22*igt23 + At23*igt33) + igt13*(At23*igt23 + At33*igt33);
iAt22
  = At11*Power(igt12,2) + At22*Power(igt22,2) + At33*Power(igt23,2) + 2*(At23*igt22*igt23 + igt12*(At12*igt22 + At13*igt23));
iAt23
  = At23*Power(igt23,2) + igt13*(At12*igt22 + At13*igt23) + At33*igt23*igt33 + igt12*(At11*igt13 + At12*igt23 + At13*igt33) + igt22*(At22*igt23 + At23*igt33);
iAt33
  = At11*Power(igt13,2) + At22*Power(igt23,2) + At33*Power(igt33,2) + 2*(At23*igt23*igt33 + igt13*(At12*igt23 + At13*igt33));
Adamp
  = (At11*igt11 + At22*igt22 + 2*(At12*igt12 + At13*igt13 + At23*igt23) + At33*igt33)*trADampCoeff;
RicGG1111
  = 3*Power(ChrDt111,2)*igt11 + ChrDt111*((2*ChrDt112 + 4*ChrDt211)*igt12 + (2*ChrDt113 + 4*ChrDt311)*igt13) + (2*ChrDt112*ChrDt211 + Power(ChrDt211,2))*igt22 + 2*(ChrDt112*ChrDt311 + ChrDt211*(ChrDt113 + ChrDt311))*igt23 + (2*ChrDt113*ChrDt311 + Power(ChrDt311,2))*igt33;
RicGG1112
  = 3*ChrDt111*ChrDt112*igt11 + (Power(ChrDt112,2) + 2*ChrDt112*ChrDt211 + ChrDt111*(ChrDt122 + 2*ChrDt212))*igt12 + (ChrDt112*(ChrDt113 + 2*ChrDt311) + ChrDt111*(ChrDt123 + 2*ChrDt312))*igt13 + (ChrDt112*ChrDt212 + ChrDt211*(ChrDt122 + ChrDt212))*igt22 + (ChrDt122*ChrDt311 + ChrDt212*(ChrDt113 + ChrDt311) + ChrDt112*ChrDt312 + ChrDt211*(ChrDt123 + ChrDt312))*igt23 + (ChrDt113*ChrDt312 + ChrDt311*(ChrDt123 + ChrDt312))*igt33;
RicGG1113
  = 3*ChrDt111*ChrDt113*igt11 + (ChrDt113*(ChrDt112 + 2*ChrDt211) + ChrDt111*(ChrDt123 + 2*ChrDt213))*igt12 + (Power(ChrDt113,2) + 2*ChrDt113*ChrDt311 + ChrDt111*(ChrDt133 + 2*ChrDt313))*igt13 + (ChrDt112*ChrDt213 + ChrDt211*(ChrDt123 + ChrDt213))*igt22 + (ChrDt123*ChrDt311 + ChrDt213*(ChrDt113 + ChrDt311) + ChrDt112*ChrDt313 + ChrDt211*(ChrDt133 + ChrDt313))*igt23 + (ChrDt113*ChrDt313 + ChrDt311*(ChrDt133 + ChrDt313))*igt33;
RicGG1122
  = 3*Power(ChrDt112,2)*igt11 + ChrDt112*((2*ChrDt122 + 4*ChrDt212)*igt12 + (2*ChrDt123 + 4*ChrDt312)*igt13) + (2*ChrDt122*ChrDt212 + Power(ChrDt212,2))*igt22 + 2*(ChrDt122*ChrDt312 + ChrDt212*(ChrDt123 + ChrDt312))*igt23 + (2*ChrDt123*ChrDt312 + Power(ChrDt312,2))*igt33;
RicGG1123
  = 3*ChrDt112*ChrDt113*igt11 + (ChrDt113*(ChrDt122 + 2*ChrDt212) + ChrDt112*(ChrDt123 + 2*ChrDt213))*igt12 + (ChrDt113*(ChrDt123 + 2*ChrDt312) + ChrDt112*(ChrDt133 + 2*ChrDt313))*igt13 + (ChrDt122*ChrDt213 + ChrDt212*(ChrDt123 + ChrDt213))*igt22 + (ChrDt213*ChrDt312 + ChrDt123*(ChrDt213 + ChrDt312) + ChrDt122*ChrDt313 + ChrDt212*(ChrDt133 + ChrDt313))*igt23 + (ChrDt123*ChrDt313 + ChrDt312*(ChrDt133 + ChrDt313))*igt33;
RicGG1133
  = 3*Power(ChrDt113,2)*igt11 + ChrDt113*((2*ChrDt123 + 4*ChrDt213)*igt12 + (2*ChrDt133 + 4*ChrDt313)*igt13) + (2*ChrDt123*ChrDt213 + Power(ChrDt213,2))*igt22 + 2*(ChrDt123*ChrDt313 + ChrDt213*(ChrDt133 + ChrDt313))*igt23 + (2*ChrDt133*ChrDt313 + Power(ChrDt313,2))*igt33;
RicGG1211
  = ChrDt111*(2*ChrDt112 + ChrDt211)*igt11 + (Power(ChrDt112,2) + ChrDt112*ChrDt211 + Power(ChrDt211,2) + 3*ChrDt111*ChrDt212)*igt12 + (ChrDt211*ChrDt311 + ChrDt112*(ChrDt113 + ChrDt311) + ChrDt111*(ChrDt213 + 2*ChrDt312))*igt13 + (ChrDt112 + 2*ChrDt211)*ChrDt212*igt22 + (ChrDt212*(ChrDt113 + 2*ChrDt311) + ChrDt112*ChrDt312 + ChrDt211*(ChrDt213 + ChrDt312))*igt23 + (ChrDt113*ChrDt312 + ChrDt311*(ChrDt213 + ChrDt312))*igt33;
RicGG1212
  = (Power(ChrDt112,2) + (ChrDt112*ChrDt211)/2. + ChrDt111*(ChrDt122 + ChrDt212/2.))*igt11 + (ChrDt122*(ChrDt112 + ChrDt211/2.) + (2*ChrDt112 + ChrDt211)*ChrDt212 + (3*ChrDt111*ChrDt222)/2.)*igt12 + ((3*ChrDt112*ChrDt312)/2. + (ChrDt112*(ChrDt123 + ChrDt213) + ChrDt111*ChrDt223 + ChrDt212*ChrDt311 + ChrDt122*(ChrDt113 + ChrDt311) + ChrDt211*ChrDt312)/2. + ChrDt111*ChrDt322)*igt13 + (Power(ChrDt212,2) + ChrDt211*ChrDt222 + (ChrDt122*ChrDt212 + ChrDt112*ChrDt222)/2.)*igt22 + (ChrDt222*ChrDt311 + (3*ChrDt212*ChrDt312)/2. + (ChrDt212*(ChrDt123 + ChrDt213) + ChrDt113*ChrDt222 + ChrDt122*ChrDt312 + ChrDt112*ChrDt322 + ChrDt211*(ChrDt223 + ChrDt322))/2.)*igt23 + (((ChrDt123 + ChrDt213)*ChrDt312 + Power(ChrDt312,2) + ChrDt113*ChrDt322 + ChrDt311*(ChrDt223 + ChrDt322))*igt33)/2.;
RicGG1213
  = (ChrDt113*(ChrDt112 + ChrDt211/2.) + ChrDt111*(ChrDt123 + ChrDt213/2.))*igt11 + (ChrDt123*(ChrDt112 + ChrDt211/2.) + (ChrDt112/2. + ChrDt211)*ChrDt213 + (3*(ChrDt113*ChrDt212 + ChrDt111*ChrDt223))/2.)*igt12 + (ChrDt113*ChrDt312 + (ChrDt111*ChrDt233 + (ChrDt123 + ChrDt213)*(ChrDt113 + ChrDt311) + ChrDt211*ChrDt313 + ChrDt112*(ChrDt133 + ChrDt313))/2. + ChrDt111*ChrDt323)*igt13 + (ChrDt212*(ChrDt123/2. + ChrDt213) + (ChrDt112/2. + ChrDt211)*ChrDt223)*igt22 + (ChrDt223*ChrDt311 + ChrDt212*ChrDt313 + (ChrDt133*ChrDt212 + Power(ChrDt213,2) + ChrDt113*ChrDt223 + (ChrDt123 + ChrDt213)*ChrDt312 + ChrDt112*ChrDt323 + ChrDt211*(ChrDt233 + ChrDt323))/2.)*igt23 + ((ChrDt213*ChrDt313 + ChrDt312*(ChrDt133 + ChrDt313) + ChrDt113*ChrDt323 + ChrDt311*(ChrDt233 + ChrDt323))*igt33)/2.;
RicGG1222
  = ChrDt112*(2*ChrDt122 + ChrDt212)*igt11 + (Power(ChrDt122,2) + ChrDt122*ChrDt212 + Power(ChrDt212,2) + 3*ChrDt112*ChrDt222)*igt12 + (ChrDt212*ChrDt312 + ChrDt122*(ChrDt123 + ChrDt312) + ChrDt112*(ChrDt223 + 2*ChrDt322))*igt13 + (ChrDt122 + 2*ChrDt212)*ChrDt222*igt22 + (ChrDt222*(ChrDt123 + 2*ChrDt312) + ChrDt122*ChrDt322 + ChrDt212*(ChrDt223 + ChrDt322))*igt23 + (ChrDt123*ChrDt322 + ChrDt312*(ChrDt223 + ChrDt322))*igt33;
RicGG1223
  = (ChrDt113*(ChrDt122 + ChrDt212/2.) + ChrDt112*(ChrDt123 + ChrDt213/2.))*igt11 + (ChrDt123*(ChrDt122 + ChrDt212/2.) + (ChrDt122/2. + ChrDt212)*ChrDt213 + (3*(ChrDt113*ChrDt222 + ChrDt112*ChrDt223))/2.)*igt12 + ((Power(ChrDt123,2) + ChrDt113*ChrDt223 + ChrDt112*ChrDt233 + (ChrDt123 + ChrDt213)*ChrDt312 + ChrDt212*ChrDt313 + ChrDt122*(ChrDt133 + ChrDt313))/2. + ChrDt113*ChrDt322 + ChrDt112*ChrDt323)*igt13 + ((ChrDt123/2. + ChrDt213)*ChrDt222 + (ChrDt122/2. + ChrDt212)*ChrDt223)*igt22 + (ChrDt223*ChrDt312 + ChrDt222*ChrDt313 + (ChrDt133*ChrDt222 + (ChrDt123 + ChrDt213)*(ChrDt223 + ChrDt322) + ChrDt122*ChrDt323 + ChrDt212*(ChrDt233 + ChrDt323))/2.)*igt23 + ((ChrDt133*ChrDt322 + ChrDt313*(ChrDt223 + ChrDt322) + ChrDt123*ChrDt323 + ChrDt312*(ChrDt233 + ChrDt323))*igt33)/2.;
RicGG1233
  = ChrDt113*(2*ChrDt123 + ChrDt213)*igt11 + (Power(ChrDt123,2) + ChrDt123*ChrDt213 + Power(ChrDt213,2) + 3*ChrDt113*ChrDt223)*igt12 + (ChrDt213*ChrDt313 + ChrDt123*(ChrDt133 + ChrDt313) + ChrDt113*(ChrDt233 + 2*ChrDt323))*igt13 + (ChrDt123 + 2*ChrDt213)*ChrDt223*igt22 + (ChrDt223*(ChrDt133 + 2*ChrDt313) + ChrDt123*ChrDt323 + ChrDt213*(ChrDt233 + ChrDt323))*igt23 + (ChrDt133*ChrDt323 + ChrDt313*(ChrDt233 + ChrDt323))*igt33;
RicGG1311
  = ChrDt111*(2*ChrDt113 + ChrDt311)*igt11 + (ChrDt113*(ChrDt112 + ChrDt211) + ChrDt211*ChrDt311 + ChrDt111*(2*ChrDt213 + ChrDt312))*igt12 + (Power(ChrDt113,2) + ChrDt113*ChrDt311 + Power(ChrDt311,2) + 3*ChrDt111*ChrDt313)*igt13 + ((ChrDt112 + ChrDt211)*ChrDt213 + ChrDt211*ChrDt312)*igt22 + (ChrDt213*(ChrDt113 + ChrDt311) + ChrDt311*ChrDt312 + (ChrDt112 + 2*ChrDt211)*ChrDt313)*igt23 + (ChrDt113 + 2*ChrDt311)*ChrDt313*igt33;
RicGG1312
  = (ChrDt112*(ChrDt113 + ChrDt311/2.) + ChrDt111*(ChrDt123 + ChrDt312/2.))*igt11 + (ChrDt112*(ChrDt213 + ChrDt312/2.) + (ChrDt123*(ChrDt112 + ChrDt211) + ChrDt113*(ChrDt122 + ChrDt212) + ChrDt212*ChrDt311 + ChrDt211*ChrDt312)/2. + ChrDt111*(ChrDt223 + ChrDt322/2.))*igt12 + (ChrDt123*(ChrDt113 + ChrDt311/2.) + (ChrDt113/2. + ChrDt311)*ChrDt312 + (3*(ChrDt112*ChrDt313 + ChrDt111*ChrDt323))/2.)*igt13 + (((ChrDt122 + ChrDt212)*ChrDt213 + (ChrDt112 + ChrDt211)*ChrDt223 + ChrDt212*ChrDt312 + ChrDt211*ChrDt322)*igt22)/2. + (ChrDt212*ChrDt313 + ChrDt211*ChrDt323 + (ChrDt223*(ChrDt113 + ChrDt311) + Power(ChrDt312,2) + ChrDt213*(ChrDt123 + ChrDt312) + ChrDt122*ChrDt313 + ChrDt311*ChrDt322 + ChrDt112*ChrDt323)/2.)*igt23 + ((ChrDt123/2. + ChrDt312)*ChrDt313 + (ChrDt113/2. + ChrDt311)*ChrDt323)*igt33;
RicGG1313
  = (Power(ChrDt113,2) + (ChrDt113*ChrDt311)/2. + ChrDt111*(ChrDt133 + ChrDt313/2.))*igt11 + (ChrDt113*((3*ChrDt213)/2. + ChrDt312/2.) + (ChrDt113*ChrDt123 + ChrDt133*(ChrDt112 + ChrDt211) + ChrDt213*ChrDt311 + ChrDt211*ChrDt313)/2. + ChrDt111*(ChrDt233 + ChrDt323/2.))*igt12 + (ChrDt133*(ChrDt113 + ChrDt311/2.) + (2*ChrDt113 + ChrDt311)*ChrDt313 + (3*ChrDt111*ChrDt333)/2.)*igt13 + ((Power(ChrDt213,2) + (ChrDt112 + ChrDt211)*ChrDt233 + ChrDt213*(ChrDt123 + ChrDt312) + ChrDt211*ChrDt323)*igt22)/2. + (((3*ChrDt213)/2. + ChrDt312/2.)*ChrDt313 + ChrDt211*ChrDt333 + (ChrDt133*ChrDt213 + ChrDt233*(ChrDt113 + ChrDt311) + ChrDt123*ChrDt313 + ChrDt311*ChrDt323 + ChrDt112*ChrDt333)/2.)*igt23 + (Power(ChrDt313,2) + ChrDt311*ChrDt333 + (ChrDt133*ChrDt313 + ChrDt113*ChrDt333)/2.)*igt33;
RicGG1322
  = ChrDt112*(2*ChrDt123 + ChrDt312)*igt11 + (ChrDt123*(ChrDt122 + ChrDt212) + ChrDt212*ChrDt312 + ChrDt112*(2*ChrDt223 + ChrDt322))*igt12 + (Power(ChrDt123,2) + ChrDt123*ChrDt312 + Power(ChrDt312,2) + 3*ChrDt112*ChrDt323)*igt13 + ((ChrDt122 + ChrDt212)*ChrDt223 + ChrDt212*ChrDt322)*igt22 + (ChrDt223*(ChrDt123 + ChrDt312) + ChrDt312*ChrDt322 + (ChrDt122 + 2*ChrDt212)*ChrDt323)*igt23 + (ChrDt123 + 2*ChrDt312)*ChrDt323*igt33;
RicGG1323
  = (ChrDt113*(ChrDt123 + ChrDt312/2.) + ChrDt112*(ChrDt133 + ChrDt313/2.))*igt11 + ((Power(ChrDt123,2) + ChrDt133*(ChrDt122 + ChrDt212) + ChrDt213*(ChrDt123 + ChrDt312) + ChrDt212*ChrDt313)/2. + ChrDt113*(ChrDt223 + ChrDt322/2.) + ChrDt112*(ChrDt233 + ChrDt323/2.))*igt12 + (ChrDt133*(ChrDt123 + ChrDt312/2.) + (ChrDt123/2. + ChrDt312)*ChrDt313 + (3*(ChrDt113*ChrDt323 + ChrDt112*ChrDt333))/2.)*igt13 + (((ChrDt123 + ChrDt213)*ChrDt223 + (ChrDt122 + ChrDt212)*ChrDt233 + ChrDt213*ChrDt322 + ChrDt212*ChrDt323)*igt22)/2. + ((ChrDt213 + ChrDt312/2.)*ChrDt323 + ChrDt212*ChrDt333 + (ChrDt233*(ChrDt123 + ChrDt312) + ChrDt223*(ChrDt133 + ChrDt313) + ChrDt313*ChrDt322 + ChrDt123*ChrDt323 + ChrDt122*ChrDt333)/2.)*igt23 + ((ChrDt133/2. + ChrDt313)*ChrDt323 + (ChrDt123/2. + ChrDt312)*ChrDt333)*igt33;
RicGG1333
  = ChrDt113*(2*ChrDt133 + ChrDt313)*igt11 + (ChrDt133*(ChrDt123 + ChrDt213) + ChrDt213*ChrDt313 + ChrDt113*(2*ChrDt233 + ChrDt323))*igt12 + (Power(ChrDt133,2) + ChrDt133*ChrDt313 + Power(ChrDt313,2) + 3*ChrDt113*ChrDt333)*igt13 + ((ChrDt123 + ChrDt213)*ChrDt233 + ChrDt213*ChrDt323)*igt22 + (ChrDt233*(ChrDt133 + ChrDt313) + ChrDt313*ChrDt323 + (ChrDt123 + 2*ChrDt213)*ChrDt333)*igt23 + (ChrDt133 + 2*ChrDt313)*ChrDt333*igt33;
RicGG2211
  = (Power(ChrDt112,2) + 2*ChrDt112*ChrDt211)*igt11 + 2*(ChrDt211*ChrDt312 + ChrDt112*(ChrDt213 + ChrDt312))*igt13 + 3*Power(ChrDt212,2)*igt22 + ChrDt212*((4*ChrDt112 + 2*ChrDt211)*igt12 + (2*ChrDt213 + 4*ChrDt312)*igt23) + (2*ChrDt213*ChrDt312 + Power(ChrDt312,2))*igt33;
RicGG2212
  = (ChrDt122*(ChrDt112 + ChrDt211) + ChrDt112*ChrDt212)*igt11 + (Power(ChrDt212,2) + ChrDt211*ChrDt222 + 2*(ChrDt122*ChrDt212 + ChrDt112*ChrDt222))*igt12 + (ChrDt212*ChrDt312 + ChrDt122*(ChrDt213 + ChrDt312) + ChrDt211*ChrDt322 + ChrDt112*(ChrDt223 + ChrDt322))*igt13 + 3*ChrDt212*ChrDt222*igt22 + (ChrDt222*(ChrDt213 + 2*ChrDt312) + ChrDt212*(ChrDt223 + 2*ChrDt322))*igt23 + (ChrDt213*ChrDt322 + ChrDt312*(ChrDt223 + ChrDt322))*igt33;
RicGG2213
  = (ChrDt123*(ChrDt112 + ChrDt211) + ChrDt112*ChrDt213)*igt11 + (ChrDt212*(2*ChrDt123 + ChrDt213) + (2*ChrDt112 + ChrDt211)*ChrDt223)*igt12 + (ChrDt213*ChrDt312 + ChrDt123*(ChrDt213 + ChrDt312) + ChrDt211*ChrDt323 + ChrDt112*(ChrDt233 + ChrDt323))*igt13 + 3*ChrDt212*ChrDt223*igt22 + (ChrDt223*(ChrDt213 + 2*ChrDt312) + ChrDt212*(ChrDt233 + 2*ChrDt323))*igt23 + (ChrDt213*ChrDt323 + ChrDt312*(ChrDt233 + ChrDt323))*igt33;
RicGG2222
  = (Power(ChrDt122,2) + 2*ChrDt122*ChrDt212)*igt11 + 2*(ChrDt212*ChrDt322 + ChrDt122*(ChrDt223 + ChrDt322))*igt13 + 3*Power(ChrDt222,2)*igt22 + ChrDt222*((4*ChrDt122 + 2*ChrDt212)*igt12 + (2*ChrDt223 + 4*ChrDt322)*igt23) + (2*ChrDt223*ChrDt322 + Power(ChrDt322,2))*igt33;
RicGG2223
  = (ChrDt123*(ChrDt122 + ChrDt212) + ChrDt122*ChrDt213)*igt11 + ((2*ChrDt123 + ChrDt213)*ChrDt222 + (2*ChrDt122 + ChrDt212)*ChrDt223)*igt12 + (ChrDt213*ChrDt322 + ChrDt123*(ChrDt223 + ChrDt322) + ChrDt212*ChrDt323 + ChrDt122*(ChrDt233 + ChrDt323))*igt13 + 3*ChrDt222*ChrDt223*igt22 + (Power(ChrDt223,2) + 2*ChrDt223*ChrDt322 + ChrDt222*(ChrDt233 + 2*ChrDt323))*igt23 + (ChrDt223*ChrDt323 + ChrDt322*(ChrDt233 + ChrDt323))*igt33;
RicGG2233
  = (Power(ChrDt123,2) + 2*ChrDt123*ChrDt213)*igt11 + 2*(ChrDt213*ChrDt323 + ChrDt123*(ChrDt233 + ChrDt323))*igt13 + 3*Power(ChrDt223,2)*igt22 + ChrDt223*((4*ChrDt123 + 2*ChrDt213)*igt12 + (2*ChrDt233 + 4*ChrDt323)*igt23) + (2*ChrDt233*ChrDt323 + Power(ChrDt323,2))*igt33;
RicGG2311
  = (ChrDt113*(ChrDt112 + ChrDt211) + ChrDt112*ChrDt311)*igt11 + ((ChrDt112 + ChrDt211)*ChrDt213 + ChrDt212*(2*ChrDt113 + ChrDt311) + ChrDt112*ChrDt312)*igt12 + (ChrDt311*ChrDt312 + ChrDt113*(ChrDt213 + ChrDt312) + (2*ChrDt112 + ChrDt211)*ChrDt313)*igt13 + ChrDt212*(2*ChrDt213 + ChrDt312)*igt22 + (Power(ChrDt213,2) + ChrDt213*ChrDt312 + Power(ChrDt312,2) + 3*ChrDt212*ChrDt313)*igt23 + (ChrDt213 + 2*ChrDt312)*ChrDt313*igt33;
RicGG2312
  = ((ChrDt123*(ChrDt112 + ChrDt211) + ChrDt113*(ChrDt122 + ChrDt212) + ChrDt122*ChrDt311 + ChrDt112*ChrDt312)*igt11)/2. + (ChrDt222*(ChrDt113 + ChrDt311/2.) + ChrDt212*(ChrDt123 + (ChrDt213 + ChrDt312)/2.) + ((ChrDt112 + ChrDt211)*ChrDt223 + ChrDt122*(ChrDt213 + ChrDt312) + ChrDt112*ChrDt322)/2.)*igt12 + (ChrDt122*ChrDt313 + ChrDt112*ChrDt323 + (Power(ChrDt312,2) + ChrDt123*(ChrDt213 + ChrDt312) + ChrDt212*ChrDt313 + ChrDt311*ChrDt322 + ChrDt113*(ChrDt223 + ChrDt322) + ChrDt211*ChrDt323)/2.)*igt13 + (ChrDt222*(ChrDt213 + ChrDt312/2.) + ChrDt212*(ChrDt223 + ChrDt322/2.))*igt22 + (ChrDt223*(ChrDt213 + ChrDt312/2.) + (ChrDt213/2. + ChrDt312)*ChrDt322 + (3*(ChrDt222*ChrDt313 + ChrDt212*ChrDt323))/2.)*igt23 + (ChrDt313*(ChrDt223/2. + ChrDt322) + (ChrDt213/2. + ChrDt312)*ChrDt323)*igt33;
RicGG2313
  = ((ChrDt133*(ChrDt112 + ChrDt211) + ChrDt113*(ChrDt123 + ChrDt213) + ChrDt123*ChrDt311 + ChrDt112*ChrDt313)*igt11)/2. + (ChrDt223*(ChrDt113 + ChrDt311/2.) + ChrDt212*(ChrDt133 + ChrDt313/2.) + (Power(ChrDt213,2) + (ChrDt112 + ChrDt211)*ChrDt233 + ChrDt123*(ChrDt213 + ChrDt312) + ChrDt112*ChrDt323)/2.)*igt12 + ((ChrDt123 + ChrDt312/2.)*ChrDt313 + ChrDt112*ChrDt333 + (ChrDt133*(ChrDt213 + ChrDt312) + ChrDt213*ChrDt313 + ChrDt311*ChrDt323 + ChrDt113*(ChrDt233 + ChrDt323) + ChrDt211*ChrDt333)/2.)*igt13 + (ChrDt223*(ChrDt213 + ChrDt312/2.) + ChrDt212*(ChrDt233 + ChrDt323/2.))*igt22 + (ChrDt233*(ChrDt213 + ChrDt312/2.) + (ChrDt213/2. + ChrDt312)*ChrDt323 + (3*(ChrDt223*ChrDt313 + ChrDt212*ChrDt333))/2.)*igt23 + (ChrDt313*(ChrDt233/2. + ChrDt323) + (ChrDt213/2. + ChrDt312)*ChrDt333)*igt33;
RicGG2322
  = (ChrDt123*(ChrDt122 + ChrDt212) + ChrDt122*ChrDt312)*igt11 + ((ChrDt122 + ChrDt212)*ChrDt223 + ChrDt222*(2*ChrDt123 + ChrDt312) + ChrDt122*ChrDt322)*igt12 + (ChrDt312*ChrDt322 + ChrDt123*(ChrDt223 + ChrDt322) + (2*ChrDt122 + ChrDt212)*ChrDt323)*igt13 + ChrDt222*(2*ChrDt223 + ChrDt322)*igt22 + (Power(ChrDt223,2) + ChrDt223*ChrDt322 + Power(ChrDt322,2) + 3*ChrDt222*ChrDt323)*igt23 + (ChrDt223 + 2*ChrDt322)*ChrDt323*igt33;
RicGG2323
  = ((Power(ChrDt123,2) + ChrDt133*(ChrDt122 + ChrDt212) + ChrDt123*(ChrDt213 + ChrDt312) + ChrDt122*ChrDt313)*igt11)/2. + (ChrDt223*((3*ChrDt123)/2. + (ChrDt213 + ChrDt312)/2.) + ChrDt222*(ChrDt133 + ChrDt313/2.) + ((ChrDt122 + ChrDt212)*ChrDt233 + ChrDt123*ChrDt322 + ChrDt122*ChrDt323)/2.)*igt12 + (((3*ChrDt123)/2. + ChrDt312/2.)*ChrDt323 + ChrDt122*ChrDt333 + (ChrDt123*ChrDt233 + ChrDt313*ChrDt322 + ChrDt133*(ChrDt223 + ChrDt322) + ChrDt213*ChrDt323 + ChrDt212*ChrDt333)/2.)*igt13 + (Power(ChrDt223,2) + (ChrDt223*ChrDt322)/2. + ChrDt222*(ChrDt233 + ChrDt323/2.))*igt22 + (ChrDt233*(ChrDt223 + ChrDt322/2.) + (2*ChrDt223 + ChrDt322)*ChrDt323 + (3*ChrDt222*ChrDt333)/2.)*igt23 + (Power(ChrDt323,2) + ChrDt322*ChrDt333 + (ChrDt233*ChrDt323 + ChrDt223*ChrDt333)/2.)*igt33;
RicGG2333
  = (ChrDt133*(ChrDt123 + ChrDt213) + ChrDt123*ChrDt313)*igt11 + ((ChrDt123 + ChrDt213)*ChrDt233 + ChrDt223*(2*ChrDt133 + ChrDt313) + ChrDt123*ChrDt323)*igt12 + (ChrDt313*ChrDt323 + ChrDt133*(ChrDt233 + ChrDt323) + (2*ChrDt123 + ChrDt213)*ChrDt333)*igt13 + ChrDt223*(2*ChrDt233 + ChrDt323)*igt22 + (Power(ChrDt233,2) + ChrDt233*ChrDt323 + Power(ChrDt323,2) + 3*ChrDt223*ChrDt333)*igt23 + (ChrDt233 + 2*ChrDt323)*ChrDt333*igt33;
RicGG3311
  = (Power(ChrDt113,2) + 2*ChrDt113*ChrDt311)*igt11 + 2*(ChrDt213*(ChrDt113 + ChrDt311) + ChrDt113*ChrDt312)*igt12 + (Power(ChrDt213,2) + 2*ChrDt213*ChrDt312)*igt22 + ChrDt313*((4*ChrDt113 + 2*ChrDt311)*igt13 + (4*ChrDt213 + 2*ChrDt312)*igt23) + 3*Power(ChrDt313,2)*igt33;
RicGG3312
  = (ChrDt123*(ChrDt113 + ChrDt311) + ChrDt113*ChrDt312)*igt11 + (ChrDt223*(ChrDt113 + ChrDt311) + ChrDt213*ChrDt312 + ChrDt123*(ChrDt213 + ChrDt312) + ChrDt113*ChrDt322)*igt12 + ((2*ChrDt123 + ChrDt312)*ChrDt313 + (2*ChrDt113 + ChrDt311)*ChrDt323)*igt13 + (ChrDt223*(ChrDt213 + ChrDt312) + ChrDt213*ChrDt322)*igt22 + (ChrDt313*(2*ChrDt223 + ChrDt322) + (2*ChrDt213 + ChrDt312)*ChrDt323)*igt23 + 3*ChrDt313*ChrDt323*igt33;
RicGG3313
  = (ChrDt133*(ChrDt113 + ChrDt311) + ChrDt113*ChrDt313)*igt11 + (ChrDt233*(ChrDt113 + ChrDt311) + ChrDt133*(ChrDt213 + ChrDt312) + ChrDt213*ChrDt313 + ChrDt113*ChrDt323)*igt12 + (Power(ChrDt313,2) + ChrDt311*ChrDt333 + 2*(ChrDt133*ChrDt313 + ChrDt113*ChrDt333))*igt13 + (ChrDt233*(ChrDt213 + ChrDt312) + ChrDt213*ChrDt323)*igt22 + (ChrDt313*(2*ChrDt233 + ChrDt323) + (2*ChrDt213 + ChrDt312)*ChrDt333)*igt23 + 3*ChrDt313*ChrDt333*igt33;
RicGG3322
  = (Power(ChrDt123,2) + 2*ChrDt123*ChrDt312)*igt11 + 2*(ChrDt223*(ChrDt123 + ChrDt312) + ChrDt123*ChrDt322)*igt12 + (Power(ChrDt223,2) + 2*ChrDt223*ChrDt322)*igt22 + ChrDt323*((4*ChrDt123 + 2*ChrDt312)*igt13 + (4*ChrDt223 + 2*ChrDt322)*igt23) + 3*Power(ChrDt323,2)*igt33;
RicGG3323
  = (ChrDt133*(ChrDt123 + ChrDt312) + ChrDt123*ChrDt313)*igt11 + (ChrDt233*(ChrDt123 + ChrDt312) + ChrDt223*(ChrDt133 + ChrDt313) + ChrDt133*ChrDt322 + ChrDt123*ChrDt323)*igt12 + ((2*ChrDt133 + ChrDt313)*ChrDt323 + (2*ChrDt123 + ChrDt312)*ChrDt333)*igt13 + (ChrDt233*(ChrDt223 + ChrDt322) + ChrDt223*ChrDt323)*igt22 + (Power(ChrDt323,2) + ChrDt322*ChrDt333 + 2*(ChrDt233*ChrDt323 + ChrDt223*ChrDt333))*igt23 + 3*ChrDt323*ChrDt333*igt33;
RicGG3333
  = (Power(ChrDt133,2) + 2*ChrDt133*ChrDt313)*igt11 + 2*(ChrDt233*(ChrDt133 + ChrDt313) + ChrDt133*ChrDt323)*igt12 + (Power(ChrDt233,2) + 2*ChrDt233*ChrDt323)*igt22 + ChrDt333*((4*ChrDt133 + 2*ChrDt313)*igt13 + (4*ChrDt233 + 2*ChrDt323)*igt23) + 3*Power(ChrDt333,2)*igt33;
Rict11
  = dGamt11*gt11 + dGamt21*gt12 + dGamt31*gt13 + ChrDt111*localGamt1 + ChrDt112*localGamt2 + ChrDt113*localGamt3 + igt11*(-ddgt1111/2. + RicGG1111) + igt12*(-ddgt1112 + 2*RicGG1112) + igt13*(-ddgt1113 + 2*RicGG1113) + igt22*(-ddgt1122/2. + RicGG1122) + igt23*(-ddgt1123 + 2*RicGG1123) + igt33*(-ddgt1133/2. + RicGG1133);
Rict12
  = (dGamt12*gt11 + (dGamt11 + dGamt22)*gt12 + dGamt32*gt13 + dGamt21*gt22 + dGamt31*gt23 + (ChrDt112 + ChrDt211)*localGamt1 + (ChrDt122 + ChrDt212)*localGamt2 + (ChrDt123 + ChrDt213)*localGamt3)/2. + igt11*(-ddgt1211/2. + RicGG1211) + igt12*(-ddgt1212 + 2*RicGG1212) + igt13*(-ddgt1213 + 2*RicGG1213) + igt22*(-ddgt1222/2. + RicGG1222) + igt23*(-ddgt1223 + 2*RicGG1223) + igt33*(-ddgt1233/2. + RicGG1233);
Rict13
  = (dGamt13*gt11 + dGamt23*gt12 + (dGamt11 + dGamt33)*gt13 + dGamt21*gt23 + dGamt31*gt33 + (ChrDt113 + ChrDt311)*localGamt1 + (ChrDt123 + ChrDt312)*localGamt2 + (ChrDt133 + ChrDt313)*localGamt3)/2. + igt11*(-ddgt1311/2. + RicGG1311) + igt12*(-ddgt1312 + 2*RicGG1312) + igt13*(-ddgt1313 + 2*RicGG1313) + igt22*(-ddgt1322/2. + RicGG1322) + igt23*(-ddgt1323 + 2*RicGG1323) + igt33*(-ddgt1333/2. + RicGG1333);
Rict22
  = dGamt12*gt12 + dGamt22*gt22 + dGamt32*gt23 + ChrDt212*localGamt1 + ChrDt222*localGamt2 + ChrDt223*localGamt3 + igt11*(-ddgt2211/2. + RicGG2211) + igt12*(-ddgt2212 + 2*RicGG2212) + igt13*(-ddgt2213 + 2*RicGG2213) + igt22*(-ddgt2222/2. + RicGG2222) + igt23*(-ddgt2223 + 2*RicGG2223) + igt33*(-ddgt2233/2. + RicGG2233);
Rict23
  = (dGamt13*gt12 + dGamt12*gt13 + dGamt23*gt22 + (dGamt22 + dGamt33)*gt23 + dGamt32*gt33 + (ChrDt213 + ChrDt312)*localGamt1 + (ChrDt223 + ChrDt322)*localGamt2 + (ChrDt233 + ChrDt323)*localGamt3)/2. + igt11*(-ddgt2311/2. + RicGG2311) + igt12*(-ddgt2312 + 2*RicGG2312) + igt13*(-ddgt2313 + 2*RicGG2313) + igt22*(-ddgt2322/2. + RicGG2322) + igt23*(-ddgt2323 + 2*RicGG2323) + igt33*(-ddgt2333/2. + RicGG2333);
Rict33
  = dGamt13*gt13 + dGamt23*gt23 + dGamt33*gt33 + ChrDt313*localGamt1 + ChrDt323*localGamt2 + ChrDt333*localGamt3 + igt11*(-ddgt3311/2. + RicGG3311) + igt12*(-ddgt3312 + 2*RicGG3312) + igt13*(-ddgt3313 + 2*RicGG3313) + igt22*(-ddgt3322/2. + RicGG3322) + igt23*(-ddgt3323 + 2*RicGG3323) + igt33*(-ddgt3333/2. + RicGG3333);
p2Ricdetg11
  = 2*(Power(dphi1,2) - (DDphit11 + DDphit*gt11)*localphi)*Q + Power(dphi1,2)*(4*Power(Q,2) + gt11*igt11*(2*Q - 4*Power(Q,2))) + gt11*((dphi1*(dphi2*igt12 + dphi3*igt13) + dphi2*dphi3*igt23)*(4*Q - 8*Power(Q,2)) + (Power(dphi2,2)*igt22 + Power(dphi3,2)*igt33)*(2*Q - 4*Power(Q,2)));
p2Ricdetg12
  = 2*(dphi1*dphi2 - (DDphit12 + DDphit*gt12)*localphi)*Q + dphi1*dphi2*(4*Power(Q,2) + gt12*igt12*(4*Q - 8*Power(Q,2))) + gt12*(dphi3*(dphi1*igt13 + dphi2*igt23)*(4*Q - 8*Power(Q,2)) + (Power(dphi1,2)*igt11 + Power(dphi2,2)*igt22 + Power(dphi3,2)*igt33)*(2*Q - 4*Power(Q,2)));
p2Ricdetg13
  = 2*(dphi1*dphi3 - (DDphit13 + DDphit*gt13)*localphi)*Q + dphi1*(4*dphi3*Power(Q,2) + dphi2*gt13*igt12*(4*Q - 8*Power(Q,2))) + gt13*(dphi3*(dphi1*igt13 + dphi2*igt23)*(4*Q - 8*Power(Q,2)) + (Power(dphi1,2)*igt11 + Power(dphi2,2)*igt22 + Power(dphi3,2)*igt33)*(2*Q - 4*Power(Q,2)));
p2Ricdetg22
  = 2*(Power(dphi2,2) - (DDphit22 + DDphit*gt22)*localphi)*Q + Power(dphi2,2)*(4*Power(Q,2) + gt22*igt22*(2*Q - 4*Power(Q,2))) + gt22*((dphi1*(dphi2*igt12 + dphi3*igt13) + dphi2*dphi3*igt23)*(4*Q - 8*Power(Q,2)) + (Power(dphi1,2)*igt11 + Power(dphi3,2)*igt33)*(2*Q - 4*Power(Q,2)));
p2Ricdetg23
  = 2*(dphi2*dphi3 - (DDphit23 + DDphit*gt23)*localphi)*Q + dphi2*dphi3*(4*Power(Q,2) + gt23*igt23*(4*Q - 8*Power(Q,2))) + gt23*(dphi1*(dphi2*igt12 + dphi3*igt13)*(4*Q - 8*Power(Q,2)) + (Power(dphi1,2)*igt11 + Power(dphi2,2)*igt22 + Power(dphi3,2)*igt33)*(2*Q - 4*Power(Q,2)));
p2Ricdetg33
  = 2*(Power(dphi3,2) - (DDphit33 + DDphit*gt33)*localphi)*Q + gt33*((dphi1*(dphi2*igt12 + dphi3*igt13) + dphi2*dphi3*igt23)*(4*Q - 8*Power(Q,2)) + (Power(dphi1,2)*igt11 + Power(dphi2,2)*igt22)*(2*Q - 4*Power(Q,2))) + Power(dphi3,2)*(4*Power(Q,2) + gt33*igt33*(2*Q - 4*Power(Q,2)));
p2Ric11
  = p2Ricdetg11 + Power(localphi,2)*Rict11;
p2Ric12
  = p2Ricdetg12 + Power(localphi,2)*Rict12;
p2Ric13
  = p2Ricdetg13 + Power(localphi,2)*Rict13;
p2Ric22
  = p2Ricdetg22 + Power(localphi,2)*Rict22;
p2Ric23
  = p2Ricdetg23 + Power(localphi,2)*Rict23;
p2Ric33
  = p2Ricdetg33 + Power(localphi,2)*Rict33;
p2Ric
  = em4phi*(igt11*p2Ric11 + igt22*p2Ric22 + 2*(igt12*p2Ric12 + igt13*p2Ric13 + igt23*p2Ric23) + igt33*p2Ric33);
p2RicTF11
  = -(g11*p2Ric)/3. + p2Ric11;
p2RicTF12
  = -(g12*p2Ric)/3. + p2Ric12;
p2RicTF13
  = -(g13*p2Ric)/3. + p2Ric13;
p2RicTF22
  = -(g22*p2Ric)/3. + p2Ric22;
p2RicTF23
  = -(g23*p2Ric)/3. + p2Ric23;
p2RicTF33
  = -(g33*p2Ric)/3. + p2Ric33;
t1TF11
  = alpha*p2RicTF11 - localphi*pDDalphaTF11;
t1TF12
  = alpha*p2RicTF12 - localphi*pDDalphaTF12;
t1TF13
  = alpha*p2RicTF13 - localphi*pDDalphaTF13;
t1TF22
  = alpha*p2RicTF22 - localphi*pDDalphaTF22;
t1TF23
  = alpha*p2RicTF23 - localphi*pDDalphaTF23;
t1TF33
  = alpha*p2RicTF33 - localphi*pDDalphaTF33;
sphi
  = -2*alpha*localK*localphi*N;
sgt11
  = -2*alpha*At11;
sgt12
  = -2*alpha*At12;
sgt13
  = -2*alpha*At13;
sgt22
  = -2*alpha*At22;
sgt23
  = -2*alpha*At23;
sgt33
  = -2*alpha*At33;
sK
  = -DDalpha + alpha*(AAt + Power(localK,2)/3.);
sAt11
  = -(Adamp*gt11) + alpha*(-2*AAt11 + At11*localK) + em4phim2*t1TF11;
sAt12
  = -(Adamp*gt12) + alpha*(-2*AAt12 + At12*localK) + em4phim2*t1TF12;
sAt13
  = -(Adamp*gt13) + alpha*(-2*AAt13 + At13*localK) + em4phim2*t1TF13;
sAt22
  = -(Adamp*gt22) + alpha*(-2*AAt22 + At22*localK) + em4phim2*t1TF22;
sAt23
  = -(Adamp*gt23) + alpha*(-2*AAt23 + At23*localK) + em4phim2*t1TF23;
sAt33
  = -(Adamp*gt33) + alpha*(-2*AAt33 + At33*localK) + em4phim2*t1TF33;
sGamt1
  = alpha*(4*Chrt123*iAt23 + 2*(Chrt122*iAt22 + Chrt133*iAt33)) + (ddbeta111 + ddivbeta1/3. - (4*alpha*dK1)/3.)*igt11 + (2*ddbeta112 + ddivbeta2/3. - (4*alpha*dK2)/3.)*igt12 + (2*ddbeta113 + ddivbeta3/3. - (4*alpha*dK3)/3.)*igt13 + ddbeta122*igt22 + 2*ddbeta123*igt23 + ddbeta133*igt33 + (-dbeta11 + (2*divbeta)/3.)*localGamt1 - dbeta12*localGamt2 - dbeta13*localGamt3 + iAt11*(2*(alpha*Chrt111 - dalpha1) + (12*alpha*dphi1*Q)/localphi) + iAt12*(-2*dalpha2 + alpha*(4*Chrt112 + (12*dphi2*Q)/localphi)) + iAt13*(-2*dalpha3 + alpha*(4*Chrt113 + (12*dphi3*Q)/localphi));
sGamt2
  = ddbeta211*igt11 + (2*ddbeta212 + ddivbeta1/3. - (4*alpha*dK1)/3.)*igt12 + 2*(-(dalpha1*iAt12) - dalpha2*iAt22 - dalpha3*iAt23 + ddbeta213*igt13) + (ddbeta222 + ddivbeta2/3. - (4*alpha*dK2)/3.)*igt22 + (2*ddbeta223 + ddivbeta3/3. - (4*alpha*dK3)/3.)*igt23 + ddbeta233*igt33 - dbeta21*localGamt1 + (-dbeta22 + (2*divbeta)/3.)*localGamt2 - dbeta23*localGamt3 + alpha*(4*(Chrt212*iAt12 + Chrt213*iAt13 + Chrt223*iAt23) + 2*(Chrt211*iAt11 + Chrt222*iAt22 + Chrt233*iAt33) + (12*(dphi1*iAt12 + dphi2*iAt22 + dphi3*iAt23)*Q)/localphi);
sGamt3
  = ddbeta311*igt11 + 2*(-(dalpha1*iAt13) - dalpha2*iAt23 - dalpha3*iAt33 + ddbeta312*igt12) + (2*ddbeta313 + ddivbeta1/3. - (4*alpha*dK1)/3.)*igt13 + ddbeta322*igt22 + (2*ddbeta323 + ddivbeta2/3. - (4*alpha*dK2)/3.)*igt23 + (ddbeta333 + ddivbeta3/3. - (4*alpha*dK3)/3.)*igt33 - dbeta31*localGamt1 - dbeta32*localGamt2 + (-dbeta33 + (2*divbeta)/3.)*localGamt3 + alpha*(4*(Chrt312*iAt12 + Chrt313*iAt13 + Chrt323*iAt23) + 2*(Chrt311*iAt11 + Chrt322*iAt22 + Chrt333*iAt33) + (12*(dphi1*iAt13 + dphi2*iAt23 + dphi3*iAt33)*Q)/localphi);
aphi
  = adphi1*beta1 + adphi2*beta2 + adphi3*beta3 + 2*divbeta*localphi*N;
agt11
  = adgt111*beta1 + adgt112*beta2 + adgt113*beta3 + (2*dbeta11 - (2*divbeta)/3.)*gt11 + 2*(dbeta21*gt12 + dbeta31*gt13);
agt12
  = adgt121*beta1 + adgt122*beta2 + adgt123*beta3 + dbeta12*gt11 + (dbeta11 + dbeta22 - (2*divbeta)/3.)*gt12 + dbeta32*gt13 + dbeta21*gt22 + dbeta31*gt23;
agt13
  = adgt131*beta1 + adgt132*beta2 + adgt133*beta3 + dbeta13*gt11 + dbeta23*gt12 + (dbeta11 + dbeta33 - (2*divbeta)/3.)*gt13 + dbeta21*gt23 + dbeta31*gt33;
agt22
  = adgt221*beta1 + adgt222*beta2 + adgt223*beta3 - (2*divbeta*gt22)/3. + 2*(dbeta12*gt12 + dbeta22*gt22 + dbeta32*gt23);
agt23
  = adgt231*beta1 + adgt232*beta2 + adgt233*beta3 + dbeta13*gt12 + dbeta12*gt13 + dbeta23*gt22 + (dbeta22 + dbeta33 - (2*divbeta)/3.)*gt23 + dbeta32*gt33;
agt33
  = adgt331*beta1 + adgt332*beta2 + adgt333*beta3 - (2*divbeta*gt33)/3. + 2*(dbeta13*gt13 + dbeta23*gt23 + dbeta33*gt33);
aK
  = adK1*beta1 + adK2*beta2 + adK3*beta3;
aAt11
  = adAt111*beta1 + adAt112*beta2 + adAt113*beta3 + 2*(At11*dbeta11 + At12*dbeta21 + At13*dbeta31) - (2*At11*divbeta)/3.;
aAt12
  = adAt121*beta1 + adAt122*beta2 + adAt123*beta3 + At11*dbeta12 + At22*dbeta21 + At23*dbeta31 + At13*dbeta32 + At12*(dbeta11 + dbeta22 - (2*divbeta)/3.);
aAt13
  = adAt131*beta1 + adAt132*beta2 + adAt133*beta3 + At11*dbeta13 + At23*dbeta21 + At12*dbeta23 + At33*dbeta31 + At13*(dbeta11 + dbeta33 - (2*divbeta)/3.);
aAt22
  = adAt221*beta1 + adAt222*beta2 + adAt223*beta3 + 2*(At12*dbeta12 + At22*dbeta22 + At23*dbeta32) - (2*At22*divbeta)/3.;
aAt23
  = adAt231*beta1 + adAt232*beta2 + adAt233*beta3 + At13*dbeta12 + At12*dbeta13 + At22*dbeta23 + At33*dbeta32 + At23*(dbeta22 + dbeta33 - (2*divbeta)/3.);
aAt33
  = adAt331*beta1 + adAt332*beta2 + adAt333*beta3 + 2*(At13*dbeta13 + At23*dbeta23 + At33*dbeta33) - (2*At33*divbeta)/3.;
aGamt1
  = adGamt11*beta1 + adGamt12*beta2 + adGamt13*beta3;
aGamt2
  = adGamt21*beta1 + adGamt22*beta2 + adGamt23*beta3;
aGamt3
  = adGamt31*beta1 + adGamt32*beta2 + adGamt33*beta3;
