#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

#include "ctgbase_util.h"
#include "loopcontrol.h"

#include <math.h>

void
CTGEvolution_ForceDetgPositive(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT istart[3], iend[3];

  if (verbose)
    CCTK_INFO("Enforcing positive detg");

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGEvolution_EnforceDetgPositive,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          CCTK_INT ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

	  if (phi[ijk] <= 0.0)
	    phi[ijk] = 1e-10;
        }
  LC_ENDLOOP3 (CTGEvolution_EnforceDetgPositive);

  return;
}


void
CTGEvolution_ForceLnDetgZero(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT istart[3], iend[3];

  if (verbose)
    CCTK_INFO("Enforcing ln(det g) = 0");

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGEvolution_EnforceLnDetgZero,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          const CCTK_INT ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

          const CCTK_REAL g11 = gamma11[ijk];
          const CCTK_REAL g12 = gamma12[ijk];
          const CCTK_REAL g13 = gamma13[ijk];
          const CCTK_REAL g22 = gamma22[ijk];
          const CCTK_REAL g23 = gamma23[ijk];
          const CCTK_REAL g33 = gamma33[ijk];

	  const CCTK_REAL detg11 = g22*g33 - g23*g23;
          const CCTK_REAL detg12 = g13*g23 - g12*g33;
          const CCTK_REAL detg13 = g12*g23 - g13*g22;
          const CCTK_REAL detg22 = g11*g33 - g13*g13;
          const CCTK_REAL detg23 = g12*g13 - g11*g23;
          const CCTK_REAL detg33 = g11*g22 - g12*g12;

          const CCTK_REAL detg = detg11*g11 + detg12*g12 + detg13*g13;

	  const CCTK_REAL aux = 1.0/pow(detg, 1.0/3.0);
	  
	  gamma11[ijk] *= aux;
	  gamma12[ijk] *= aux;
	  gamma13[ijk] *= aux;
	  gamma22[ijk] *= aux;
	  gamma23[ijk] *= aux;
	  gamma33[ijk] *= aux;
	  
        }
  LC_ENDLOOP3 (CTGEvolution_EnforceLnDetgZero);

  return;
}
