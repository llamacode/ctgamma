switch(fd_order) {
  case 8: {
    all_diff_8(cctkGH, nvars, vars, dvars, i, j, k, ni, nj, nk,
             imin[0], imax[0], imin[1], imax[1], imin[2], imax[2],
             q[0], q[1], q[2], bsize, ihx, ihy, ihz);
    all_diff2_8(cctkGH, 11, vars, d2vars, i, j, k, ni, nj, nk,
             imin[0], imax[0], imin[1], imax[1], imin[2], imax[2],
             q[0], q[1], q[2],
             imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
             q2[0], q2[1], q2[2], bsize, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);
    adv_diff_8(cctkGH, navars, avars, davars, up_dir, i, j, k, ni, nj, nk,
             adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1], adv_imin[2],
             adv_imax[2], adv_q[0], adv_q[1], adv_q[2], bsize,
             ihx, ihy, ihz);
    break;
  }
  default: {
    all_diff(cctkGH, nvars, vars, dvars, i, j, k, ni, nj, nk,
             imin[0], imax[0], imin[1], imax[1], imin[2], imax[2],
             q[0], q[1], q[2], ihx, ihy, ihz);
    all_diff2(cctkGH, 11, vars, d2vars, i, j, k, ni, nj, nk,
              imin[0], imax[0], imin[1], imax[1], imin[2], imax[2],
              q[0], q[1], q[2],
              imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
              q2[0], q2[1], q2[2], ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);
    all_diff(cctkGH, navars, avars, davars, i, j, k, ni, nj, nk,
             adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1], adv_imin[2],
             adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
             ihx, ihy, ihz);
  }
}

if (!is_cartesian)
  {
    CCTK_REAL J[3][3];
    CCTK_REAL K[3][6];

    J[0][0] = J11[ijk]; J[0][1] = J12[ijk]; J[0][2] = J13[ijk];
    J[1][0] = J21[ijk]; J[1][1] = J22[ijk]; J[1][2] = J23[ijk];
    J[2][0] = J31[ijk]; J[2][1] = J32[ijk]; J[2][2] = J33[ijk];

    K[0][0] = dJ111[ijk]; K[0][1] = dJ112[ijk]; K[0][2] = dJ113[ijk];
    K[0][3] = dJ122[ijk]; K[0][4] = dJ123[ijk]; K[0][5] = dJ133[ijk];

    K[1][0] = dJ211[ijk]; K[1][1] = dJ212[ijk]; K[1][2] = dJ213[ijk];
    K[1][3] = dJ222[ijk]; K[1][4] = dJ223[ijk]; K[1][5] = dJ233[ijk];

    K[2][0] = dJ311[ijk]; K[2][1] = dJ312[ijk]; K[2][2] = dJ313[ijk];
    K[2][3] = dJ322[ijk]; K[2][4] = dJ323[ijk]; K[2][5] = dJ333[ijk];

    apply_jacobian(cctkGH, nvars, dvars, xdvars, J); 
    apply_jacobian2(cctkGH, 11, dvars, d2vars, xd2vars, J, K);
    apply_jacobian(cctkGH, navars, davars, xdavars, J);

    for (int v=0; v<nvars; ++v)
      for (int i=0; i<3; ++i)
        dvars[v][i] = xdvars[v][i];
    for (int v=0; v<11; ++v)
      for (int i=0; i<6; ++i)
      d2vars[v][i] = xd2vars[v][i];
    for (int v=0; v<navars; ++v)
      for (int i=0; i<3; ++i)
        davars[v][i] = xdavars[v][i];
  }

dphi1 = dvars[0][0]; dphi2 = dvars[0][1]; dphi3 = dvars[0][2];

dgt111 = dvars[1][0]; dgt112 = dvars[1][1]; dgt113 = dvars[1][2];
dgt121 = dvars[2][0]; dgt122 = dvars[2][1]; dgt123 = dvars[2][2];
dgt131 = dvars[3][0]; dgt132 = dvars[3][1]; dgt133 = dvars[3][2];
dgt221 = dvars[4][0]; dgt222 = dvars[4][1]; dgt223 = dvars[4][2];
dgt231 = dvars[5][0]; dgt232 = dvars[5][1]; dgt233 = dvars[5][2];
dgt331 = dvars[6][0]; dgt332 = dvars[6][1]; dgt333 = dvars[6][2];

dalpha1 = dvars[7][0]; dalpha2 = dvars[7][1]; dalpha3 = dvars[7][2];

dbeta11 = dvars[8][0]; dbeta12 = dvars[8][1]; dbeta13 = dvars[8][2];
dbeta21 = dvars[9][0]; dbeta22 = dvars[9][1]; dbeta23 = dvars[9][2];
dbeta31 = dvars[10][0]; dbeta32 = dvars[10][1]; dbeta33 = dvars[10][2];

dK1 = dvars[11][0]; dK2 = dvars[11][1]; dK3 = dvars[11][2];

dAt111 = dvars[12][0]; dAt112 = dvars[12][1]; dAt113 = dvars[12][2];
dAt121 = dvars[13][0]; dAt122 = dvars[13][1]; dAt123 = dvars[13][2];
dAt131 = dvars[14][0]; dAt132 = dvars[14][1]; dAt133 = dvars[14][2];
dAt221 = dvars[15][0]; dAt222 = dvars[15][1]; dAt223 = dvars[15][2];
dAt231 = dvars[16][0]; dAt232 = dvars[16][1]; dAt233 = dvars[16][2];
dAt331 = dvars[17][0]; dAt332 = dvars[17][1]; dAt333 = dvars[17][2];

dGamt11 = dvars[18][0]; dGamt12 = dvars[18][1]; dGamt13 = dvars[18][2];
dGamt21 = dvars[19][0]; dGamt22 = dvars[19][1]; dGamt23 = dvars[19][2];
dGamt31 = dvars[20][0]; dGamt32 = dvars[20][1]; dGamt33 = dvars[20][2];

ddphi11 = d2vars[0][0]; ddphi12 = d2vars[0][1]; ddphi13 = d2vars[0][2];
ddphi22 = d2vars[0][3]; ddphi23 = d2vars[0][4]; ddphi33 = d2vars[0][5];

ddgt1111 = d2vars[1][0]; ddgt1112 = d2vars[1][1];
ddgt1113 = d2vars[1][2]; ddgt1122 = d2vars[1][3];
ddgt1123 = d2vars[1][4]; ddgt1133 = d2vars[1][5];

ddgt1211 = d2vars[2][0]; ddgt1212 = d2vars[2][1];
ddgt1213 = d2vars[2][2]; ddgt1222 = d2vars[2][3];
ddgt1223 = d2vars[2][4]; ddgt1233 = d2vars[2][5];

ddgt1311 = d2vars[3][0]; ddgt1312 = d2vars[3][1];
ddgt1313 = d2vars[3][2]; ddgt1322 = d2vars[3][3];
ddgt1323 = d2vars[3][4]; ddgt1333 = d2vars[3][5];

ddgt2211 = d2vars[4][0]; ddgt2212 = d2vars[4][1];
ddgt2213 = d2vars[4][2]; ddgt2222 = d2vars[4][3];
ddgt2223 = d2vars[4][4]; ddgt2233 = d2vars[4][5];

ddgt2311 = d2vars[5][0]; ddgt2312 = d2vars[5][1];
ddgt2313 = d2vars[5][2]; ddgt2322 = d2vars[5][3];
ddgt2323 = d2vars[5][4]; ddgt2333 = d2vars[5][5];

ddgt3311 = d2vars[6][0]; ddgt3312 = d2vars[6][1];
ddgt3313 = d2vars[6][2]; ddgt3322 = d2vars[6][3];
ddgt3323 = d2vars[6][4]; ddgt3333 = d2vars[6][5];

ddalpha11 = d2vars[7][0]; ddalpha12 = d2vars[7][1];
ddalpha13 = d2vars[7][2]; ddalpha22 = d2vars[7][3];
ddalpha23 = d2vars[7][4]; ddalpha33 = d2vars[7][5];

ddbeta111 = d2vars[8][0]; ddbeta112 = d2vars[8][1];
ddbeta113 = d2vars[8][2]; ddbeta122 = d2vars[8][3];
ddbeta123 = d2vars[8][4]; ddbeta133 = d2vars[8][5];

ddbeta211 = d2vars[9][0]; ddbeta212 = d2vars[9][1];
ddbeta213 = d2vars[9][2]; ddbeta222 = d2vars[9][3];
ddbeta223 = d2vars[9][4]; ddbeta233 = d2vars[9][5];

ddbeta311 = d2vars[10][0]; ddbeta312 = d2vars[10][1];
ddbeta313 = d2vars[10][2]; ddbeta322 = d2vars[10][3];
ddbeta323 = d2vars[10][4]; ddbeta333 = d2vars[10][5];

adphi1 = davars[0][0]; adphi2 = davars[0][1]; adphi3 = davars[0][2];

adgt111 = davars[1][0]; adgt112 = davars[1][1]; adgt113 = davars[1][2];
adgt121 = davars[2][0]; adgt122 = davars[2][1]; adgt123 = davars[2][2];
adgt131 = davars[3][0]; adgt132 = davars[3][1]; adgt133 = davars[3][2];
adgt221 = davars[4][0]; adgt222 = davars[4][1]; adgt223 = davars[4][2];
adgt231 = davars[5][0]; adgt232 = davars[5][1]; adgt233 = davars[5][2];
adgt331 = davars[6][0]; adgt332 = davars[6][1]; adgt333 = davars[6][2];

adK1 = davars[7][0]; adK2 = davars[7][1]; adK3 = davars[7][2];

adAt111 = davars[8][0]; adAt112 = davars[8][1]; adAt113 = davars[8][2];
adAt121 = davars[9][0]; adAt122 = davars[9][1]; adAt123 = davars[9][2];
adAt131 = davars[10][0]; adAt132 = davars[10][1]; adAt133 = davars[10][2];
adAt221 = davars[11][0]; adAt222 = davars[11][1]; adAt223 = davars[11][2];
adAt231 = davars[12][0]; adAt232 = davars[12][1]; adAt233 = davars[12][2];
adAt331 = davars[13][0]; adAt332 = davars[13][1]; adAt333 = davars[13][2];

adGamt11 = davars[14][0]; adGamt12 = davars[14][1]; adGamt13 = davars[14][2];
adGamt21 = davars[15][0]; adGamt22 = davars[15][1]; adGamt23 = davars[15][2];
adGamt31 = davars[16][0]; adGamt32 = davars[16][1]; adGamt33 = davars[16][2];

if (EVOL_TYPE == Z4c_TYPE) {
  dTheta1 = dvars[21][0]; dTheta2 = dvars[21][1]; dTheta3 = dvars[21][2];
  adTheta1 = davars[17][0]; adTheta2 = davars[17][1]; adTheta3 = davars[17][2];
}
