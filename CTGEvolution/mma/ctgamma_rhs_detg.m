(* ctgamma_rhs.m

Calculate source term for the CTGamma evolution system.

This Mathematica script generates a set of scalar equations which
correspond to the RHS of the evolution equations. Two output
files are created:
  ctgamma_rhs_declare.h - declares the scalars and intermediate
                          variables used in the calculations;
  ctgamma_rhs_calc.h    - contains the computations based on
                          the declared variables.
The files are included in the main calculation loop in ../src/calc_rhs.c.
They assume that certain variables are available and have been
computed beforehand, such as local scalars corresponding to each GF
at a point, as well as all of the required derivatives.

The evolution equations are transcribed from:
  Alcubierre et al., PRD 67, 084023 (2003).

To run this file, open a Mathematica terminal and at the prompt type:
  Get["ctgamma_rhs.m"]

*)

DeclareFile = "ctgamma_detg_rhs_declare.h"
RHSFile = "ctgamma_detg_rhs_calc.h"

kdelta[a_,b_] := 0 /; a != b
kdelta[a_,b_] := 1 /; a == b


(*
 * Turns X[a,b] into {X[1,1], X[1,2], ..., X[3,3]
 *)
FreeIndexExpand[x_, a_Symbol] := Union[Flatten[Table[x, {a, 1, 3}]]]
FreeIndexExpand[x_, {a_Symbol}] := FreeIndexExpand[x, a]
FreeIndexExpand[x_, {a_Symbol, b__}] :=
  Union[Flatten[Table[FreeIndexExpand[x, {b}], {a,1,3}]]]

ForcePositiveRule = -x_ :> x

ExpandFreeExpr[x_Symbol] := x
ExpandFreeExpr[x_[a__]] :=
  DeleteCases[
    Union[Flatten[FreeIndexExpand[x[a], {a}] /. ForcePositiveRule]],
    0]

ExpandFreeEqn[x_Symbol==y_] := x==y
ExpandFreeEqn[x_[a__]==y_] := 
 DeleteCases[Union[Flatten[FreeIndexExpand[x[a]==y,{a}]]], True]

ExpandFreeIndices = {
  x_ :> Flatten[Map[ExpandFreeEqn,x]] /; !FreeQ[x,y_==z_],
  x_ :> Flatten[Map[ExpandFreeExpr,x]] /; FreeQ[x,y_==z_]
}


(*
 * Turn g[1,2] into g12
 *)
SetScalarIndex[a_,b_] := ToExpression[ToString[a] <> ToString[b]]; 
SetScalarVal[x_] :=
  x //. y_Symbol[a_,b___] :> SetScalarIndex[y,a][b] /. y_[] :> y

SetIndexMapRules[variables_] := Module[
  {AllVars, VarsToReplace, Replacements},
  AllVars = Flatten[Map[ExpandFreeExpr, variables]];
  VarsToReplace = Cases[AllVars, x_[a__]];
  Replacements = Map[SetScalarVal, VarsToReplace];
  Table[VarsToReplace[[i]] -> Replacements[[i]], {i, 1, Length[Replacements]}]
]

ContractIndexRules = {
  T1_[a___, b_Symbol, c___, d_Symbol, e___] \
    T2_[f___, b_Symbol, g___] T3_[h___, d_Symbol, i___] ->
    Sum[T1[a,b,c,d,e] T2[f,b,g] T3[h,d,i], {b,1,3}, {d,1,3}],

  T1_[a___,b_Symbol,c___] T2_[d___,b_Symbol,e___] ->
    Sum[T1[a,b,c] T2[d,b,e], {b,1,3}]
}
    
(*
 * Expression simplification rules
 *)

(* collect the inverse metrics *)
igvars = {ig[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseMetricRule = x_ == y_ :> x == Collect[y, igvars]
igtvars = {igt[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseConformalMetricRule = x_ == y_ :> x == Collect[y, igtvars]

(* trivial factorisation *)
CollectRule = a_ x_ + a_ y_ -> a (x + y)

(* collect negative values *)
ReplaceNegRule = a_ /; a < 0 -> neg (-a)
UndoReplaceNegRule = neg -> -1


(*
 * C-output
 *)

WriteCAssignment[e_] := Module[{x},
  x = ToString[InputForm[e]];
  x = StringReplace[x, "]" -> "]]"];
  x = StringReplace[x, "[" -> "[["];  
  x = ToExpression[x];
  PutAppend[CForm[x[[1]]], RHSFile];
  WriteString[RHSFP, "  = " <> ToString[CForm[x[[2]]]] <> ";"];
  WriteString[RHSFP, "\n"]
]

WriteCDeclare[e_] := Module[{x},
  x = ToString[InputForm[e]];
  WriteString[DeclareFP, "CCTK_REAL " <> x <> ";\n"];
]


(*
 * Model specific...
 *)

EvoVariables = {
  sphi, sgt[a,b], sK, sAt[a,b], sGamt[a],
  aphi, agt[a,b], aK, aAt[a,b], aGamt[a]
}

AuxVariables = {
  em4phi, em4phim1, em4phim2,
  Q, alpha, beta[a], localphi, gt[a,b], localK, At[a,b], Gamt[a],
  igt[a,b], g[a,b], iAt[a,b],
  dphi[a], dgt[a,b,c], ddphi[a,b], ddgt[a,b,c,d], DDphit[a,b], DDphit,
  Chrt[a,b,c], ChrDt[a,b,c],
  dalpha[a], ddalpha[a,b], DDalpha,pDDalpha, pDDalpha[a,b], pDDalphaTF[a,b],
  dbeta[a,b], ddbeta[a,b,c], divbeta, ddivbeta[a],
  dGamt[a,b],
  Ric, p2Ric, p2Ric[a,b], p2Ricdetg[a,b], Rict[a,b], p2RicTF[a,b],
  AAt[a,b], AAt, Adamp,
  dK[a], dAt[a,b,c],
  t1TF[a,b],
  dalphadphi,
  adphi[a], adgt[a,b,c], adK[a], adAt[a,b,c],
  adGamt[a,b],
  localGamt[a],
  RicGG[a,b,c,d]
}

(* index symmetries *)

gt[a_,b_] := gt[b,a] /; !OrderedQ[{a,b}]
At[a_,b_] := At[b,a] /; !OrderedQ[{a,b}]
kdelta[a_,b_] := kdelta[b,a] /; !OrderedQ[{a,b}]
igt[a_,b_] := igt[b,a] /; !OrderedQ[{a,b}]
g[a_,b_] := g[b,a] /; !OrderedQ[{a,b}]
iAt[a_,b_] := iAt[b,a] /; !OrderedQ[{a,b}]
dgt[a_,b_,c_] := dgt[b,a,c] /; !OrderedQ[{a,b}]
ddphi[a_,b_] := ddphi[b,a] /; !OrderedQ[{a,b}]
ddgt[a_,b_,c_,d_] := ddgt[b,a,c,d] /; !OrderedQ[{a,b}]
ddgt[a_,b_,c_,d_] := ddgt[a,b,d,c] /; !OrderedQ[{c,d}]
DDphit[a_,b_] := DDphit[b,a] /; !OrderedQ[{a,b}]
Chrt[a_,b_,c_] := Chrt[a,c,b] /; !OrderedQ[{b,c}]
ChrDt[a_,b_,c_] := ChrDt[a,c,b] /; !OrderedQ[{b,c}]
ddalpha[a_,b_] := ddalpha[b,a] /; !OrderedQ[{a,b}]
pDDalpha[a_,b_] := pDDalpha[b,a] /; !OrderedQ[{a,b}]
pDDalphaTF[a_,b_] := pDDalphaTF[b,a] /; !OrderedQ[{a,b}]
ddbeta[a_,b_,c_] := ddbeta[a,c,b] /; !OrderedQ[{b,c}]
p2Ric[a_,b_] := p2Ric[b,a] /; !OrderedQ[{a,b}]
Rict[a_,b_] := Rict[b,a] /; !OrderedQ[{a,b}]
p2RicTF[a_,b_] := p2RicTF[b,a] /; !OrderedQ[{a,b}]
p2Ricdetg[a_,b_] := p2Ricdetg[b,a] /; !OrderedQ[{a,b}]
RicGG[a_,b_,c_,d_] := RicGG[b,a,c,d] /; !OrderedQ[{a,b}]
RicGG[a_,b_,c_,d_] := RicGG[a,b,d,c] /; !OrderedQ[{c,d}]
AAt[a_,b_] := AAt[b,a] /; !OrderedQ[{a,b}]
dAt[a_,b_,c_] := dAt[b,a,c] /; !OrderedQ[{a,b}]
t1TF[a_,b_] := t1TF[b,a] /; !OrderedQ[{a,b}]
sgt[a_,b_] := sgt[b,a] /; !OrderedQ[{a,b}]
sAt[a_,b_] := sAt[b,a] /; !OrderedQ[{a,b}]
agt[a_,b_] := agt[b,a] /; !OrderedQ[{a,b}]
aAt[a_,b_] := aAt[b,a] /; !OrderedQ[{a,b}]
adgt[a_,b_,c_] := adgt[b,a,c] /; !OrderedQ[{a,b}]
adAt[a_,b_,c_] := adAt[b,a,c] /; !OrderedQ[{a,b}]

EvolutionEquations = {
  Q == 1/(12 N),

  em4phi == localphi^(-1/(3N)),
  em4phim1 == localphi^(-1/(3N)-1),
  em4phim2 == localphi^(-1/(3N)-2),

  g[a,b] == gt[a,b]/em4phi,

  ChrDt[c,a,b] == (1/2) (dgt[a,c,b] + dgt[b,c,a] - dgt[a,b,c]),
  Chrt[c,a,b] == igt[c,d] ChrDt[d,a,b],

  localGamt[a] == igt[b,c] Chrt[a,b,c],

  dalphadphi == igt[a,b] dalpha[a] dphi[b],

  pDDalpha[a,b] == localphi (ddalpha[a,b] - Chrt[c,a,b] dalpha[c])
    - 2 Q (dalpha[a] dphi[b] + dalpha[b] dphi[a] - gt[a,b] dalphadphi),

  DDalpha == em4phi ( igt[a,b] ddalpha[a,b] - localGamt[a] dalpha[a])
    + 2 Q em4phim1 dalphadphi,

  pDDalphaTF[a,b] == pDDalpha[a,b] - (1/3) g[a,b] localphi DDalpha,

  divbeta == kdelta[a,b] dbeta[a,b],
  ddivbeta[a] == kdelta[b,c] ddbeta[b,c,a],

  DDphit[a,b] == ddphi[a,b] - Chrt[c,a,b] dphi[c],

  DDphit == igt[a,b] DDphit[a,b],

  AAt[a,b] == igt[c,d] At[a,c] At[b,d],
  AAt == igt[a,b] AAt[a,b],
  iAt[a,b] == igt[a,c] igt[b,d] At[c,d],

  Adamp == trADampCoeff igt[c,d] At[c,d],

  RicGG[a,b,c,d] == (1/2) igt[e,f] (
      ChrDt[e,c,a] ChrDt[b,f,d] + ChrDt[e,d,a] ChrDt[b,f,c]
    + ChrDt[e,c,b] ChrDt[a,f,d] + ChrDt[e,d,b] ChrDt[a,f,c]
    + ChrDt[e,a,d] ChrDt[f,c,b] + ChrDt[e,b,d] ChrDt[f,a,c]),

  Rict[a,b] == -(1/2) igt[c,d] ddgt[a,b,c,d]
    + (1/2) (gt[c,a] dGamt[c,b] + gt[c,b] dGamt[c,a])
    + (1/2) localGamt[c] (ChrDt[a,b,c] + ChrDt[b,a,c])
    + igt[c,d] RicGG[a,b,c,d],

  p2Ricdetg[a,b] == -Q localphi (2 DDphit[a,b] + 2 gt[a,b] DDphit) +
    Q(2+4Q) dphi[a] dphi[b] + Q(2-4Q) gt[a,b] igt[c,d] dphi[c] dphi[d],

  p2Ric[a,b] == localphi^2 Rict[a,b] + p2Ricdetg[a,b],

  p2Ric == igt[a,b] p2Ric[a,b] em4phi,

  p2RicTF[a,b] == p2Ric[a,b] - (1/3) g[a,b] p2Ric,

  t1TF[a,b] == -localphi pDDalphaTF[a,b] +  alpha p2RicTF[a,b],

  sphi == -2 N localphi alpha localK,
  sgt[a,b] == -2 alpha At[a,b],

  sK == -DDalpha + alpha (AAt + (1/3) localK localK),
  sAt[a,b] == em4phim2 t1TF[a,b] + alpha (localK At[a,b] - 2 AAt[a,b])
    - Adamp gt[a,b],

  sGamt[a] == igt[c,d] ddbeta[a,c,d] + (1/3) igt[a,c] ddivbeta[c]
    -localGamt[c] dbeta[a,c] + (2/3) localGamt[a] divbeta
    - 2 iAt[a,c] dalpha[c] + 2 alpha (Chrt[a,c,d] iAt[c,d]
    + 6 Q iAt[a,c] dphi[c]/localphi - (2/3) igt[a,c] dK[c]),

  aphi == beta[a] adphi[a] + 2 N localphi divbeta,

  agt[a,b] == beta[c] adgt[a,b,c] + gt[a,c] dbeta[c,b] + gt[b,c] dbeta[c,a]
    - (2/3) gt[a,b] divbeta,

  aK == beta[a] adK[a],

  aAt[a,b] == beta[c] adAt[a,b,c] + At[a,c] dbeta[c,b] + At[b,c] dbeta[c,a]
    - (2/3) At[a,b] divbeta,

  aGamt[a] == beta[c] adGamt[a,c]
}

AllVars = Join[EvoVariables, AuxVariables]
IndexMapRules = SetIndexMapRules[AllVars]

ScalarEvoVars = AllVars /. ExpandFreeIndices /. IndexMapRules

Print["Processing evolution equations ..."]
CEquations = EvolutionEquations
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = CEquations /. ExpandFreeIndices
CEquations = CEquations /. IndexMapRules

Print["Simplifying expressions..."]
CEquations = CEquations /. CollectInverseMetricRule
CEquations = CEquations /. CollectInverseConformalMetricRule
CEquations = CEquations //. CollectRule
CEquations = CEquations /. ReplaceNegRule //. CollectRule /. UndoReplaceNegRule

(*
 *  Write c-code to file.
 *)
Print["Writing variable declarations to " <> DeclareFile]
DeclareFP = OpenWrite[DeclareFile]
Map[WriteCDeclare, ScalarEvoVars]
Close[DeclareFP]

Print["Writing RHS computation to " <> RHSFile]
RHSFP = OpenWrite[RHSFile]
Map[WriteCAssignment, CEquations]
Close[RHSFP]
