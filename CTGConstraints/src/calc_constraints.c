#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include <unistd.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "GlobalDerivative.h"
#include "ctgbase_util.h"

#include "loopcontrol.h"

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#define Power(x,y) (pow((CCTK_REAL) (x), (CCTK_REAL) (y)))

/*
 * Main constraint calculation routine.
 */
void
CTGCalcConstraints(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT * restrict imin[3], * restrict imax[3];
  CCTK_INT * restrict imin2[3], * restrict imax2[3];
  CCTK_REAL * restrict q[3], * restrict q2[3];
  CCTK_REAL ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz;

  if (verbose)
    CCTK_INFO("Calculating constraints.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  /*
   * Grid spacings required by the finite difference operators.
   */
  ihx = 1.0 / CCTK_DELTA_SPACE(0);
  ihy = 1.0 / CCTK_DELTA_SPACE(1);
  ihz = 1.0 / CCTK_DELTA_SPACE(2);
  ihxx = ihx*ihx;
  ihxy = ihx*ihy;
  ihxz = ihx*ihz;
  ihyy = ihy*ihy;
  ihyz = ihy*ihz;
  ihzz = ihz*ihz;

  /*
   * Call SummationByParts for finite-difference operator coefficients.
   */

  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax,
                  (CCTK_POINTER_TO_CONST *) q, 1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin2,
                  (CCTK_POINTER_TO_CONST *) imax2,
                  (CCTK_POINTER_TO_CONST *) q2, 2, 0);

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGConstraints_calc_constraints_init,
            i, j, k,
	    0, 0, 0,
	    cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      H[ijk] = 0.0;
      M1[ijk] = 0.0;
      M2[ijk] = 0.0;
      M3[ijk] = 0.0;
      G1[ijk] = 0.0;
      G2[ijk] = 0.0;
      G3[ijk] = 0.0;
      A[ijk] = 0.0;
      gamma[ijk] = 0.0;
    }
  LC_ENDLOOP3 (CTGConstraints_calc_constraints_init);

#pragma omp parallel
  LC_LOOP3 (CTGConstraints_calc_constraints,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      CCTK_REAL detg11, detg12, detg13, detg22, detg23, detg33;
      CCTK_REAL detg, detgt;
      CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0,
        dbdz=0, dcdz=1;
      CCTK_REAL ddadxx=0, ddadxy=0, ddadxz=0, ddadyy=0, ddadyz=0, ddadzz=0;
      CCTK_REAL ddbdxx=0, ddbdxy=0, ddbdxz=0, ddbdyy=0, ddbdyz=0, ddbdzz=0;
      CCTK_REAL ddcdxx=0, ddcdxy=0, ddcdxz=0, ddcdyy=0, ddcdyz=0, ddcdzz=0;

#include "ctgamma_constraints_declare.h"

      /*
       *  Assign local variables.
       */
      g11 = gxx[ijk];
      g12 = gxy[ijk];
      g13 = gxz[ijk];
      g22 = gyy[ijk];
      g23 = gyz[ijk];
      g33 = gzz[ijk];

      K11 = kxx[ijk];
      K12 = kxy[ijk];
      K13 = kxz[ijk];
      K22 = kyy[ijk];
      K23 = kyz[ijk];
      K33 = kzz[ijk];

      gt11 = gamma11[ijk];
      gt12 = gamma12[ijk];
      gt13 = gamma13[ijk];
      gt22 = gamma22[ijk];
      gt23 = gamma23[ijk];
      gt33 = gamma33[ijk];

      localA11 = A11[ijk];
      localA12 = A12[ijk];
      localA13 = A13[ijk];
      localA22 = A22[ijk];
      localA23 = A23[ijk];
      localA33 = A33[ijk];

      localGamma1 = Gamma1[ijk];
      localGamma2 = Gamma2[ijk];
      localGamma3 = Gamma3[ijk];

      CTGBase_invert_metric(g11, g12, g13, g22, g23, g33, detg,
                            ig11, ig12, ig13, ig22, ig23, ig33);
      CTGBase_invert_metric(gt11, gt12, gt13, gt22, gt23, gt33, detgt,
                            igt11, igt12, igt13, igt22, igt23, igt33);

      /*
       * Calculate required partial derivatives.
       */

#include "derivatives.h"

      /*
       * Calculate source functions.
       */

#include "ctgamma_constraints_calc.h"

      /*
       * Assign constraints.
       */
      H[ijk] = LH;

      M1[ijk] = LM1;
      M2[ijk] = LM2;
      M3[ijk] = LM3;

      G1[ijk] = LG1;
      G2[ijk] = LG2;
      G3[ijk] = LG3;

      A[ijk] = trA;

      gamma[ijk] = detgt - 1.0;
	  
    }
  LC_ENDLOOP3 (CTGConstraints_calc_constraints);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin2, imax2, q2);

  return;
}


void
CTGCalcConstraints_Matter(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (verbose)
    CCTK_INFO("Calculating matter terms for the constraints.");

  CCTK_INT istart[3], iend[3];
  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGConstraints_calc_constraints_matter,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);


      CCTK_REAL lapse = alp[ijk];
      CCTK_REAL ilapse = 1./alp[ijk];
      CCTK_REAL beta1 = betax[ijk];
      CCTK_REAL beta2 = betay[ijk];
      CCTK_REAL beta3 = betaz[ijk];

      CCTK_REAL T00 = eTtt[ijk];
      CCTK_REAL T01 = eTtx[ijk];
      CCTK_REAL T02 = eTty[ijk];
      CCTK_REAL T03 = eTtz[ijk];
      CCTK_REAL T11 = eTxx[ijk];
      CCTK_REAL T12 = eTxy[ijk];
      CCTK_REAL T13 = eTxz[ijk];
      CCTK_REAL T22 = eTyy[ijk];
      CCTK_REAL T23 = eTyz[ijk];
      CCTK_REAL T33 = eTzz[ijk];

      CCTK_REAL rho = ilapse*ilapse *
	(T00 - 2.*(T01*beta1 + T02*beta2 + T03*beta3)
	 + T11*beta1*beta1 + T22*beta2*beta2 + T33*beta3*beta3
	 + 2.*(T12*beta1*beta2 + T13*beta1*beta3 + T23*beta2*beta3));

      CCTK_REAL S1 = ilapse*(-T01 + T11*beta1 + T12*beta2 + T13*beta3);
      CCTK_REAL S2 = ilapse*(-T02 + T12*beta1 + T22*beta2 + T23*beta3);
      CCTK_REAL S3 = ilapse*(-T03 + T13*beta1 + T23*beta2 + T33*beta3);

      H[ijk] -= 16.*M_PI*rho;

      M1[ijk] -= 8.*M_PI*S1;
      M2[ijk] -= 8.*M_PI*S2;
      M3[ijk] -= 8.*M_PI*S3;

    }
  LC_ENDLOOP3 (CTGConstraints_calc_constraints_matter);

  return;
}
