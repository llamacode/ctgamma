if (*general_coordinates)
  {
    dadx = J11[ijk];
    dady = J12[ijk];
    dadz = J13[ijk];
    dbdx = J21[ijk];
    dbdy = J22[ijk];
    dbdz = J23[ijk];
    dcdx = J31[ijk];
    dcdy = J32[ijk];
    dcdz = J33[ijk];
    
    ddadxx = dJ111[ijk];
    ddadxy = dJ112[ijk];
    ddadxz = dJ113[ijk];
    ddadyy = dJ122[ijk];
    ddadyz = dJ123[ijk];
    ddadzz = dJ133[ijk];
    
    ddbdxx = dJ211[ijk];
    ddbdxy = dJ212[ijk];
    ddbdxz = dJ213[ijk];
    ddbdyy = dJ222[ijk];
    ddbdyz = dJ223[ijk];
    ddbdzz = dJ233[ijk];
    
    ddcdxx = dJ311[ijk];
    ddcdxy = dJ312[ijk];
    ddcdxz = dJ313[ijk];
    ddcdyy = dJ322[ijk];
    ddcdyz = dJ323[ijk];
    ddcdzz = dJ333[ijk];
  }

/*
 * Metric metric.
 */
g_diff(cctkGH, gxx, &dg111, &dg112, &dg113,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff2(cctkGH, gxx, &ddg1111, &ddg1112, &ddg1113, &ddg1122, &ddg1123,
        &ddg1133,
	*general_coordinates,
        dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
        ddadxx, ddbdxx, ddcdxx, ddadxy, ddbdxy, ddcdxy, ddadxz, ddbdxz,
        ddcdxz, ddadyy, ddbdyy, ddcdyy, ddadyz, ddbdyz, ddcdyz,
        ddadzz, ddbdzz, ddcdzz,
        i, j, k, ni, nj, nk,
        imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
        imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
        q2[0], q2[1], q2[2],
        ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);

g_diff(cctkGH, gxy, &dg121, &dg122, &dg123,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff2(cctkGH, gxy, &ddg1211, &ddg1212, &ddg1213, &ddg1222, &ddg1223,
        &ddg1233,
	*general_coordinates,
        dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
        ddadxx, ddbdxx, ddcdxx, ddadxy, ddbdxy, ddcdxy, ddadxz, ddbdxz,
        ddcdxz, ddadyy, ddbdyy, ddcdyy, ddadyz, ddbdyz, ddcdyz,
        ddadzz, ddbdzz, ddcdzz,
        i, j, k, ni, nj, nk,
        imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
        imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
        q2[0], q2[1], q2[2],
        ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);


g_diff(cctkGH, gxz, &dg131, &dg132, &dg133,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff2(cctkGH, gxz, &ddg1311, &ddg1312, &ddg1313, &ddg1322, &ddg1323,
        &ddg1333,
	*general_coordinates,
        dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
        ddadxx, ddbdxx, ddcdxx, ddadxy, ddbdxy, ddcdxy, ddadxz, ddbdxz,
        ddcdxz, ddadyy, ddbdyy, ddcdyy, ddadyz, ddbdyz, ddcdyz,
        ddadzz, ddbdzz, ddcdzz,
        i, j, k, ni, nj, nk,
        imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
        imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
        q2[0], q2[1], q2[2],
        ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);

g_diff(cctkGH, gyy, &dg221, &dg222, &dg223,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff2(cctkGH, gyy, &ddg2211, &ddg2212, &ddg2213, &ddg2222, &ddg2223,
        &ddg2233,
	*general_coordinates,
        dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
        ddadxx, ddbdxx, ddcdxx, ddadxy, ddbdxy, ddcdxy, ddadxz, ddbdxz,
        ddcdxz, ddadyy, ddbdyy, ddcdyy, ddadyz, ddbdyz, ddcdyz,
        ddadzz, ddbdzz, ddcdzz,
        i, j, k, ni, nj, nk,
        imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
        imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
        q2[0], q2[1], q2[2],
        ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);

g_diff(cctkGH, gyz, &dg231, &dg232, &dg233,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff2(cctkGH, gyz, &ddg2311, &ddg2312, &ddg2313, &ddg2322, &ddg2323,
        &ddg2333,
	*general_coordinates,
        dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
        ddadxx, ddbdxx, ddcdxx, ddadxy, ddbdxy, ddcdxy, ddadxz, ddbdxz,
        ddcdxz, ddadyy, ddbdyy, ddcdyy, ddadyz, ddbdyz, ddcdyz,
        ddadzz, ddbdzz, ddcdzz,
        i, j, k, ni, nj, nk,
        imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
        imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
        q2[0], q2[1], q2[2],
        ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);

g_diff(cctkGH, gzz, &dg331, &dg332, &dg333,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff2(cctkGH, gzz, &ddg3311, &ddg3312, &ddg3313, &ddg3322, &ddg3323,
        &ddg3333,
	*general_coordinates,
        dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
        ddadxx, ddbdxx, ddcdxx, ddadxy, ddbdxy, ddcdxy, ddadxz, ddbdxz,
        ddcdxz, ddadyy, ddbdyy, ddcdyy, ddadyz, ddbdyz, ddcdyz,
        ddadzz, ddbdzz, ddcdzz,
        i, j, k, ni, nj, nk,
        imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
        imin2[0], imax2[0], imin2[1], imax2[1], imin2[2], imax2[2],
        q2[0], q2[1], q2[2],
        ihx, ihy, ihz, ihxx, ihxy, ihxz, ihyy, ihyz, ihzz);

/*
 * Curvature tensor.
 */
g_diff(cctkGH, kxx, &dK111, &dK112, &dK113,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, kxy, &dK121, &dK122, &dK123,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, kxz, &dK131, &dK132, &dK133,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, kyy, &dK221, &dK222, &dK223,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, kyz, &dK231, &dK232, &dK233,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, kzz, &dK331, &dK332, &dK333,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);


/*
 * Conformal metric.
 */
g_diff(cctkGH, gamma11, &dgt111, &dgt112, &dgt113,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gamma12, &dgt121, &dgt122, &dgt123,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gamma13, &dgt131, &dgt132, &dgt133,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gamma22, &dgt221, &dgt222, &dgt223,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gamma23, &dgt231, &dgt232, &dgt233,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gamma33, &dgt331, &dgt332, &dgt333,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
