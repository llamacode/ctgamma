#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Symmetry.h"

void
CTGConstraints_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int sym[3];
  int ierr=0;

  CCTK_INFO("Setting symmetries for constraint variables.");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = +1;
  
  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::H");
  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::A");
  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::gamma");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::M1");
  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::G1");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::M2");
  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::G2");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::M3");
  ierr += SetCartSymVN(cctkGH, sym, "CTGConstraints::G3");

  if (ierr != 0)
    CCTK_WARN(0, "Couldn't set GF symmetries.");
  
  return;
}
