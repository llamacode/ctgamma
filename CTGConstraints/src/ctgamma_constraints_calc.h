ChrD111
  = dg111/2.;
ChrD112
  = dg112/2.;
ChrD113
  = dg113/2.;
ChrD122
  = dg122 - dg221/2.;
ChrD123
  = (dg123 + dg132 - dg231)/2.;
ChrD133
  = dg133 - dg331/2.;
ChrD211
  = -dg112/2. + dg121;
ChrD212
  = dg221/2.;
ChrD213
  = (dg123 - dg132 + dg231)/2.;
ChrD222
  = dg222/2.;
ChrD223
  = dg223/2.;
ChrD233
  = dg233 - dg332/2.;
ChrD311
  = -dg113/2. + dg131;
ChrD312
  = (-dg123 + dg132 + dg231)/2.;
ChrD313
  = dg331/2.;
ChrD322
  = -dg223/2. + dg232;
ChrD323
  = dg332/2.;
ChrD333
  = dg333/2.;
Chr111
  = ChrD111*ig11 + ChrD211*ig12 + ChrD311*ig13;
Chr112
  = ChrD112*ig11 + ChrD212*ig12 + ChrD312*ig13;
Chr113
  = ChrD113*ig11 + ChrD213*ig12 + ChrD313*ig13;
Chr122
  = ChrD122*ig11 + ChrD222*ig12 + ChrD322*ig13;
Chr123
  = ChrD123*ig11 + ChrD223*ig12 + ChrD323*ig13;
Chr133
  = ChrD133*ig11 + ChrD233*ig12 + ChrD333*ig13;
Chr211
  = ChrD111*ig12 + ChrD211*ig22 + ChrD311*ig23;
Chr212
  = ChrD112*ig12 + ChrD212*ig22 + ChrD312*ig23;
Chr213
  = ChrD113*ig12 + ChrD213*ig22 + ChrD313*ig23;
Chr222
  = ChrD122*ig12 + ChrD222*ig22 + ChrD322*ig23;
Chr223
  = ChrD123*ig12 + ChrD223*ig22 + ChrD323*ig23;
Chr233
  = ChrD133*ig12 + ChrD233*ig22 + ChrD333*ig23;
Chr311
  = ChrD111*ig13 + ChrD211*ig23 + ChrD311*ig33;
Chr312
  = ChrD112*ig13 + ChrD212*ig23 + ChrD312*ig33;
Chr313
  = ChrD113*ig13 + ChrD213*ig23 + ChrD313*ig33;
Chr322
  = ChrD122*ig13 + ChrD222*ig23 + ChrD322*ig33;
Chr323
  = ChrD123*ig13 + ChrD223*ig23 + ChrD323*ig33;
Chr333
  = ChrD133*ig13 + ChrD233*ig23 + ChrD333*ig33;
dig111
  = -(dg111*Power(ig11,2)) - dg221*Power(ig12,2) - dg331*Power(ig13,2) - 2*(dg231*ig12*ig13 + ig11*(dg121*ig12 + dg131*ig13));
dig112
  = -(dg112*Power(ig11,2)) - dg222*Power(ig12,2) - dg332*Power(ig13,2) - 2*(dg232*ig12*ig13 + ig11*(dg122*ig12 + dg132*ig13));
dig113
  = -(dg113*Power(ig11,2)) - dg223*Power(ig12,2) - dg333*Power(ig13,2) - 2*(dg233*ig12*ig13 + ig11*(dg123*ig12 + dg133*ig13));
dig121
  = -(dg121*Power(ig12,2)) - ig11*(dg111*ig12 + dg121*ig22 + dg131*ig23) - ig12*(dg131*ig13 + dg221*ig22 + dg231*ig23) - ig13*(dg231*ig22 + dg331*ig23);
dig122
  = -(dg122*Power(ig12,2)) - ig11*(dg112*ig12 + dg122*ig22 + dg132*ig23) - ig12*(dg132*ig13 + dg222*ig22 + dg232*ig23) - ig13*(dg232*ig22 + dg332*ig23);
dig123
  = -(dg123*Power(ig12,2)) - ig11*(dg113*ig12 + dg123*ig22 + dg133*ig23) - ig12*(dg133*ig13 + dg223*ig22 + dg233*ig23) - ig13*(dg233*ig22 + dg333*ig23);
dig131
  = -(dg131*Power(ig13,2)) - ig11*(dg111*ig13 + dg121*ig23 + dg131*ig33) - ig12*(dg121*ig13 + dg221*ig23 + dg231*ig33) - ig13*(dg231*ig23 + dg331*ig33);
dig132
  = -(dg132*Power(ig13,2)) - ig11*(dg112*ig13 + dg122*ig23 + dg132*ig33) - ig12*(dg122*ig13 + dg222*ig23 + dg232*ig33) - ig13*(dg232*ig23 + dg332*ig33);
dig133
  = -(dg133*Power(ig13,2)) - ig11*(dg113*ig13 + dg123*ig23 + dg133*ig33) - ig12*(dg123*ig13 + dg223*ig23 + dg233*ig33) - ig13*(dg233*ig23 + dg333*ig33);
dig221
  = -(dg111*Power(ig12,2)) - dg221*Power(ig22,2) - dg331*Power(ig23,2) - 2*(dg231*ig22*ig23 + ig12*(dg121*ig22 + dg131*ig23));
dig222
  = -(dg112*Power(ig12,2)) - dg222*Power(ig22,2) - dg332*Power(ig23,2) - 2*(dg232*ig22*ig23 + ig12*(dg122*ig22 + dg132*ig23));
dig223
  = -(dg113*Power(ig12,2)) - dg223*Power(ig22,2) - dg333*Power(ig23,2) - 2*(dg233*ig22*ig23 + ig12*(dg123*ig22 + dg133*ig23));
dig231
  = -(dg231*Power(ig23,2)) - ig13*(dg121*ig22 + dg131*ig23) - dg331*ig23*ig33 - ig12*(dg111*ig13 + dg121*ig23 + dg131*ig33) - ig22*(dg221*ig23 + dg231*ig33);
dig232
  = -(dg232*Power(ig23,2)) - ig13*(dg122*ig22 + dg132*ig23) - dg332*ig23*ig33 - ig12*(dg112*ig13 + dg122*ig23 + dg132*ig33) - ig22*(dg222*ig23 + dg232*ig33);
dig233
  = -(dg233*Power(ig23,2)) - ig13*(dg123*ig22 + dg133*ig23) - dg333*ig23*ig33 - ig12*(dg113*ig13 + dg123*ig23 + dg133*ig33) - ig22*(dg223*ig23 + dg233*ig33);
dig331
  = -(dg111*Power(ig13,2)) - dg221*Power(ig23,2) - dg331*Power(ig33,2) - 2*(dg231*ig23*ig33 + ig13*(dg121*ig23 + dg131*ig33));
dig332
  = -(dg112*Power(ig13,2)) - dg222*Power(ig23,2) - dg332*Power(ig33,2) - 2*(dg232*ig23*ig33 + ig13*(dg122*ig23 + dg132*ig33));
dig333
  = -(dg113*Power(ig13,2)) - dg223*Power(ig23,2) - dg333*Power(ig33,2) - 2*(dg233*ig23*ig33 + ig13*(dg123*ig23 + dg133*ig33));
Ric11
  = -Power(Chr212,2) - 2*Chr213*Chr312 - Power(Chr313,2) + Chr111*(Chr212 + Chr313) + Chr211*(-Chr112 + Chr222 + Chr323) + Chr311*(-Chr113 + Chr223 + Chr333) + (-(dg112*dig121) + dg111*(dig122 + dig133))/2. - dg231*dig231 + dg121*(dig222 + dig233) + dg131*(dig232 + dig333) + (-(dg221*dig221) - dg112*(dig222 + dig233) - dg331*dig331 - dg113*(dig131 + dig232 + dig333))/2. + (ddg1212 + (-ddg1122 - ddg2211)/2.)*ig22 + (-ddg1123 + ddg1213 + ddg1312 - ddg2311)*ig23 + (ddg1313 + (-ddg1133 - ddg3311)/2.)*ig33;
Ric12
  = -(Chr122*Chr211) - Chr123*Chr311 + Chr112*(Chr212 + Chr313) - Chr213*Chr322 + (Chr212 - Chr313)*Chr323 + Chr312*Chr333 - dg122*dig121 - dg232*dig231 + (-(dg132*dig131) - dg222*dig221 - dg332*dig331 - dg123*(dig232 + dig333))/2. + (-ddg1212 + (ddg1122 + ddg2211)/2.)*ig12 + (-(dg123*dig131) + dg112*(dig122 + dig133) + dg221*(dig121 + dig222 + dig233) + dg132*(dig232 + dig333) + dg231*(dig131 + dig232 + dig333) + (ddg1123 - ddg1213 - ddg1312 + ddg2311)*ig13 + (-ddg1223 + ddg1322 + ddg2213 - ddg2312)*ig23 + (-ddg1233 + ddg1323 + ddg2313 - ddg3312)*ig33)/2.;
Ric12
  = -(Chr122*Chr211) - Chr123*Chr311 + Chr112*(Chr212 + Chr313) - Chr213*Chr322 + (Chr212 - Chr313)*Chr323 + Chr312*Chr333 - dg121*dig122 - dg131*dig132 + (-(dg231*dig232) - dg331*dig332 - dg123*(dig131 + dig232 + dig333))/2. + (-ddg1212 + (ddg1122 + ddg2211)/2.)*ig12 + (-(dg111*dig112) + (dg132 + dg231)*dig131 + dg112*(dig111 + dig122 + dig133) + dg221*(dig121 + dig233) + dg231*dig333 + dg132*(dig232 + dig333) + (ddg1123 - ddg1213 - ddg1312 + ddg2311)*ig13 + (-ddg1223 + ddg1322 + ddg2213 - ddg2312)*ig23 + (-ddg1233 + ddg1323 + ddg2313 - ddg3312)*ig33)/2.;
Ric13
  = -(Chr123*Chr211) + Chr213*Chr222 - Chr133*Chr311 - Chr233*Chr312 + Chr223*(-Chr212 + Chr313) + Chr113*(Chr212 + Chr313) - dg121*dig123 - dg131*dig133 + (-(dg221*dig223) - dg231*dig233 - dg132*(dig121 + dig222 + dig233))/2. + (-ddg1313 + (ddg1133 + ddg3311)/2.)*ig13 + (-(dg111*dig113) + dg113*(dig111 + dig122 + dig133) + (dg123 + dg231)*(dig121 + dig222) + dg331*(dig131 + dig232) + dg123*dig233 + (ddg1123 - ddg1213 - ddg1312 + ddg2311)*ig12 + (ddg1223 - ddg1322 - ddg2213 + ddg2312)*ig22 + (ddg1233 - ddg1323 - ddg2313 + ddg3312)*ig23)/2.;
Ric13
  = -(Chr123*Chr211) + Chr213*Chr222 - Chr133*Chr311 - Chr233*Chr312 + Chr223*(-Chr212 + Chr313) + Chr113*(Chr212 + Chr313) - dg133*dig131 - dg233*dig231 + (-(dg223*dig221) - dg132*(dig121 + dig222 + dig233) - dg333*dig331)/2. + (-ddg1313 + (ddg1133 + ddg3311)/2.)*ig13 + (dg113*(dig122 + dig133) + dg123*(-dig121 + dig222 + dig233) + dg231*(dig121 + dig222 + dig233) + dg331*(dig131 + dig232 + dig333) + (ddg1123 - ddg1213 - ddg1312 + ddg2311)*ig12 + (ddg1223 - ddg1322 - ddg2213 + ddg2312)*ig22 + (ddg1233 - ddg1323 - ddg2313 + ddg3312)*ig23)/2.;
Ric22
  = -Power(Chr112,2) - 2*Chr123*Chr312 + Chr122*(Chr111 - Chr212 + Chr313) - Power(Chr323,2) + Chr222*(Chr112 + Chr323) + Chr322*(Chr113 - Chr223 + Chr333) - dg132*dig132 + dg122*(dig111 + dig133) + (-(dg221*dig111) + dg222*(dig121 + dig233))/2. + dg232*(dig131 + dig333) + (-(dg112*dig112) - dg221*(dig122 + dig133) - dg332*dig332 - dg223*(dig131 + dig232 + dig333))/2. + (ddg1212 + (-ddg1122 - ddg2211)/2.)*ig11 + (ddg1223 - ddg1322 - ddg2213 + ddg2312)*ig13 + (ddg2323 + (-ddg2233 - ddg3322)/2.)*ig33;
Ric23
  = Chr111*Chr123 - Chr122*Chr213 - Chr133*Chr312 - Chr233*Chr322 + Chr113*(-Chr112 + Chr323) + Chr223*(Chr112 + Chr323) - dg122*dig123 + (-(dg112*dig113) - dg132*dig133 - dg231*(dig122 + dig133) - dg222*dig223)/2. - dg232*dig233 + ((dg123 + dg132 - dg231)*dig111 + (dg123 + dg132)*dig122 + dg123*dig133 + dg332*(dig131 + dig232) + dg223*(dig121 + dig222 + dig233) + (-ddg1123 + ddg1213 + ddg1312 - ddg2311)*ig11 + (-ddg1223 + ddg1322 + ddg2213 - ddg2312)*ig12 + (ddg1233 - ddg1323 - ddg2313 + ddg3312)*ig13)/2. + (-ddg2323 + (ddg2233 + ddg3322)/2.)*ig23;
Ric23
  = Chr111*Chr123 - Chr122*Chr213 - Chr133*Chr312 - Chr233*Chr322 + Chr113*(-Chr112 + Chr323) + Chr223*(Chr112 + Chr323) - dg133*dig132 - dg233*dig232 + (-(dg113*dig112) - (dg123 + dg231)*dig122 - dg231*dig133 - dg333*dig332)/2. + ((dg123 + dg132 - dg231)*dig111 + dg123*dig133 + dg132*(dig122 + dig133) + dg223*(dig121 + dig233) + dg332*(dig131 + dig232 + dig333) + (-ddg1123 + ddg1213 + ddg1312 - ddg2311)*ig11 + (-ddg1223 + ddg1322 + ddg2213 - ddg2312)*ig12 + (ddg1233 - ddg1323 - ddg2313 + ddg3312)*ig13)/2. + (-ddg2323 + (ddg2233 + ddg3322)/2.)*ig23;
Ric33
  = -Power(Chr113,2) - 2*Chr123*Chr213 - Power(Chr223,2) + Chr133*(Chr111 + Chr212 - Chr313) + Chr233*(Chr112 + Chr222 - Chr323) + (Chr113 + Chr223)*Chr333 + dg133*(dig111 + dig122) - dg123*dig123 + dg233*(dig121 + dig222) + (-(dg331*dig111) + dg333*(dig131 + dig232))/2. + (-(dg113*dig113) - dg331*(dig122 + dig133) - dg223*dig223 - dg332*(dig121 + dig222 + dig233))/2. + (ddg1313 + (-ddg1133 - ddg3311)/2.)*ig11 + (-ddg1233 + ddg1323 + ddg2313 - ddg3312)*ig12 + (ddg2323 + (-ddg2233 - ddg3322)/2.)*ig22;
Ric
  = ig11*Ric11 + ig22*Ric22 + 2*(ig12*Ric12 + ig13*Ric13 + ig23*Ric23) + ig33*Ric33;
mK11
  = ig11*K11 + ig12*K12 + ig13*K13;
mK12
  = ig11*K12 + ig12*K22 + ig13*K23;
mK13
  = ig11*K13 + ig12*K23 + ig13*K33;
mK21
  = ig12*K11 + ig22*K12 + ig23*K13;
mK22
  = ig12*K12 + ig22*K22 + ig23*K23;
mK23
  = ig12*K13 + ig22*K23 + ig23*K33;
mK31
  = ig13*K11 + ig23*K12 + ig33*K13;
mK32
  = ig13*K12 + ig23*K22 + ig33*K23;
mK33
  = ig13*K13 + ig23*K23 + ig33*K33;
trK
  = mK11 + mK22 + mK33;
gKtemp111
  = -2*(Chr111*K11 + Chr211*K12 + Chr311*K13);
gKtemp112
  = -2*(Chr112*K11 + Chr212*K12 + Chr312*K13);
gKtemp113
  = -2*(Chr113*K11 + Chr213*K12 + Chr313*K13);
gKtemp121
  = -(Chr112*K11) - (Chr111 + Chr212)*K12 - Chr312*K13 - Chr211*K22 - Chr311*K23;
gKtemp122
  = -(Chr122*K11) - (Chr112 + Chr222)*K12 - Chr322*K13 - Chr212*K22 - Chr312*K23;
gKtemp123
  = -(Chr123*K11) - (Chr113 + Chr223)*K12 - Chr323*K13 - Chr213*K22 - Chr313*K23;
gKtemp131
  = -(Chr113*K11) - Chr213*K12 - (Chr111 + Chr313)*K13 - Chr211*K23 - Chr311*K33;
gKtemp132
  = -(Chr123*K11) - Chr223*K12 - (Chr112 + Chr323)*K13 - Chr212*K23 - Chr312*K33;
gKtemp133
  = -(Chr133*K11) - Chr233*K12 - (Chr113 + Chr333)*K13 - Chr213*K23 - Chr313*K33;
gKtemp221
  = -2*(Chr112*K12 + Chr212*K22 + Chr312*K23);
gKtemp222
  = -2*(Chr122*K12 + Chr222*K22 + Chr322*K23);
gKtemp223
  = -2*(Chr123*K12 + Chr223*K22 + Chr323*K23);
gKtemp231
  = -(Chr113*K12) - Chr112*K13 - Chr213*K22 - (Chr212 + Chr313)*K23 - Chr312*K33;
gKtemp232
  = -(Chr123*K12) - Chr122*K13 - Chr223*K22 - (Chr222 + Chr323)*K23 - Chr322*K33;
gKtemp233
  = -(Chr133*K12) - Chr123*K13 - Chr233*K22 - (Chr223 + Chr333)*K23 - Chr323*K33;
gKtemp331
  = -2*(Chr113*K13 + Chr213*K23 + Chr313*K33);
gKtemp332
  = -2*(Chr123*K13 + Chr223*K23 + Chr323*K33);
gKtemp333
  = -2*(Chr133*K13 + Chr233*K23 + Chr333*K33);
gK111
  = dK111 + gKtemp111;
gK112
  = dK112 + gKtemp112;
gK113
  = dK113 + gKtemp113;
gK121
  = dK121 + gKtemp121;
gK122
  = dK122 + gKtemp122;
gK123
  = dK123 + gKtemp123;
gK131
  = dK131 + gKtemp131;
gK132
  = dK132 + gKtemp132;
gK133
  = dK133 + gKtemp133;
gK221
  = dK221 + gKtemp221;
gK222
  = dK222 + gKtemp222;
gK223
  = dK223 + gKtemp223;
gK231
  = dK231 + gKtemp231;
gK232
  = dK232 + gKtemp232;
gK233
  = dK233 + gKtemp233;
gK331
  = dK331 + gKtemp331;
gK332
  = dK332 + gKtemp332;
gK333
  = dK333 + gKtemp333;
LH
  = -Power(mK11,2) - Power(mK22,2) - 2*(mK12*mK21 + mK13*mK31 + mK23*mK32) - Power(mK33,2) + Ric + Power(trK,2);
LM1
  = (gK112 - gK121)*ig12 + (gK113 - gK131)*ig13 + (gK122 - gK221)*ig22 + (gK123 + gK132 - 2*gK231)*ig23 + (gK133 - gK331)*ig33;
LM2
  = (-gK112 + gK121)*ig11 + (-gK122 + gK221)*ig12 + (gK123 - 2*gK132 + gK231)*ig13 + (gK223 - gK232)*ig23 + (gK233 - gK332)*ig33;
LM3
  = (-gK113 + gK131)*ig11 + (-2*gK123 + gK132 + gK231)*ig12 + (-gK133 + gK331)*ig13 + (-gK223 + gK232)*ig22 + (-gK233 + gK332)*ig23;
ChrDt111
  = dgt111/2.;
ChrDt112
  = dgt112/2.;
ChrDt113
  = dgt113/2.;
ChrDt122
  = dgt122 - dgt221/2.;
ChrDt123
  = (dgt123 + dgt132 - dgt231)/2.;
ChrDt133
  = dgt133 - dgt331/2.;
ChrDt211
  = -dgt112/2. + dgt121;
ChrDt212
  = dgt221/2.;
ChrDt213
  = (dgt123 - dgt132 + dgt231)/2.;
ChrDt222
  = dgt222/2.;
ChrDt223
  = dgt223/2.;
ChrDt233
  = dgt233 - dgt332/2.;
ChrDt311
  = -dgt113/2. + dgt131;
ChrDt312
  = (-dgt123 + dgt132 + dgt231)/2.;
ChrDt313
  = dgt331/2.;
ChrDt322
  = -dgt223/2. + dgt232;
ChrDt323
  = dgt332/2.;
ChrDt333
  = dgt333/2.;
Chrt111
  = ChrDt111*igt11 + ChrDt211*igt12 + ChrDt311*igt13;
Chrt112
  = ChrDt112*igt11 + ChrDt212*igt12 + ChrDt312*igt13;
Chrt113
  = ChrDt113*igt11 + ChrDt213*igt12 + ChrDt313*igt13;
Chrt122
  = ChrDt122*igt11 + ChrDt222*igt12 + ChrDt322*igt13;
Chrt123
  = ChrDt123*igt11 + ChrDt223*igt12 + ChrDt323*igt13;
Chrt133
  = ChrDt133*igt11 + ChrDt233*igt12 + ChrDt333*igt13;
Chrt211
  = ChrDt111*igt12 + ChrDt211*igt22 + ChrDt311*igt23;
Chrt212
  = ChrDt112*igt12 + ChrDt212*igt22 + ChrDt312*igt23;
Chrt213
  = ChrDt113*igt12 + ChrDt213*igt22 + ChrDt313*igt23;
Chrt222
  = ChrDt122*igt12 + ChrDt222*igt22 + ChrDt322*igt23;
Chrt223
  = ChrDt123*igt12 + ChrDt223*igt22 + ChrDt323*igt23;
Chrt233
  = ChrDt133*igt12 + ChrDt233*igt22 + ChrDt333*igt23;
Chrt311
  = ChrDt111*igt13 + ChrDt211*igt23 + ChrDt311*igt33;
Chrt312
  = ChrDt112*igt13 + ChrDt212*igt23 + ChrDt312*igt33;
Chrt313
  = ChrDt113*igt13 + ChrDt213*igt23 + ChrDt313*igt33;
Chrt322
  = ChrDt122*igt13 + ChrDt222*igt23 + ChrDt322*igt33;
Chrt323
  = ChrDt123*igt13 + ChrDt223*igt23 + ChrDt323*igt33;
Chrt333
  = ChrDt133*igt13 + ChrDt233*igt23 + ChrDt333*igt33;
LG1
  = -(Chrt111*igt11) - Chrt122*igt22 - 2*(Chrt112*igt12 + Chrt113*igt13 + Chrt123*igt23) - Chrt133*igt33 + localGamma1;
LG2
  = -(Chrt211*igt11) - Chrt222*igt22 - 2*(Chrt212*igt12 + Chrt213*igt13 + Chrt223*igt23) - Chrt233*igt33 + localGamma2;
LG3
  = -(Chrt311*igt11) - Chrt322*igt22 - 2*(Chrt312*igt12 + Chrt313*igt13 + Chrt323*igt23) - Chrt333*igt33 + localGamma3;
mA11
  = igt11*localA11 + igt12*localA12 + igt13*localA13;
mA12
  = igt11*localA12 + igt12*localA22 + igt13*localA23;
mA13
  = igt11*localA13 + igt12*localA23 + igt13*localA33;
mA21
  = igt12*localA11 + igt22*localA12 + igt23*localA13;
mA22
  = igt12*localA12 + igt22*localA22 + igt23*localA23;
mA23
  = igt12*localA13 + igt22*localA23 + igt23*localA33;
mA31
  = igt13*localA11 + igt23*localA12 + igt33*localA13;
mA32
  = igt13*localA12 + igt23*localA22 + igt33*localA23;
mA33
  = igt13*localA13 + igt23*localA23 + igt33*localA33;
trA
  = mA11 + mA22 + mA33;
