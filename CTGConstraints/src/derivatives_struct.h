#include "cctk.h"

struct derivatives {
  /*
   * Conformal factor.
   */
  CCTK_REAL localphi;
  CCTK_REAL dphi1, dphi2, dphi3;
  CCTK_REAL adphi1, adphi2, adphi3;
  CCTK_REAL ddphi11, ddphi12, ddphi13, ddphi22, ddphi23, ddphi33;

  /*
   * Conformal metric.
   */
  CCTK_REAL gt11, gt12, gt13, gt22, gt23, gt33;
  CCTK_REAL dgt111, dgt121, dgt131, dgt221, dgt231, dgt331;
  CCTK_REAL dgt112, dgt122, dgt132, dgt222, dgt232, dgt332;
  CCTK_REAL dgt113, dgt123, dgt133, dgt223, dgt233, dgt333;
  CCTK_REAL adgt111, adgt121, adgt131, adgt221, adgt231, adgt331;
  CCTK_REAL adgt112, adgt122, adgt132, adgt222, adgt232, adgt332;
  CCTK_REAL adgt113, adgt123, adgt133, adgt223, adgt233, adgt333;
  CCTK_REAL ddgt1111, ddgt1211, ddgt1311, ddgt2211, ddgt2311, ddgt3311;
  CCTK_REAL ddgt1112, ddgt1212, ddgt1312, ddgt2212, ddgt2312, ddgt3312;
  CCTK_REAL ddgt1113, ddgt1213, ddgt1313, ddgt2213, ddgt2313, ddgt3313;
  CCTK_REAL ddgt1122, ddgt1222, ddgt1322, ddgt2222, ddgt2322, ddgt3322;
  CCTK_REAL ddgt1123, ddgt1223, ddgt1323, ddgt2223, ddgt2323, ddgt3323;
  CCTK_REAL ddgt1133, ddgt1233, ddgt1333, ddgt2233, ddgt2333, ddgt3333;

  /*
   * Curvature scalar.
   */
  CCTK_REAL localK;
  CCTK_REAL dK1, dK2, dK3;
  CCTK_REAL adK1, adK2, adK3;

  /*
   * Curvature tensor.
   */
  CCTK_REAL At11, At12, At13, At22, At23, At33;
  CCTK_REAL dAt111, dAt121, dAt131, dAt221, dAt231, dAt331;
  CCTK_REAL dAt112, dAt122, dAt132, dAt222, dAt232, dAt332;
  CCTK_REAL dAt113, dAt123, dAt133, dAt223, dAt233, dAt333;
  CCTK_REAL adAt111, adAt121, adAt131, adAt221, adAt231, adAt331;
  CCTK_REAL adAt112, adAt122, adAt132, adAt222, adAt232, adAt332;
  CCTK_REAL adAt113, adAt123, adAt133, adAt223, adAt233, adAt333;

  /*
   * Gammas.
   */
  CCTK_REAL Gamt1, Gamt2, Gamt3;
  CCTK_REAL dGamt11, dGamt21, dGamt31;
  CCTK_REAL dGamt12, dGamt22, dGamt32;
  CCTK_REAL dGamt13, dGamt23, dGamt33;

  /*
   * Lapse.
   */
  CCTK_REAL alpha;
  CCTK_REAL dalpha1, dalpha2, dalpha3;
  CCTK_REAL ddalpha11, ddalpha12, ddalpha13, ddalpha22, ddalpha23, ddalpha33;

  /*
   * Shift.
   */
  CCTK_REAL beta1, beta2, beta3;
  CCTK_REAL dbeta11, dbeta21, dbeta31;
  CCTK_REAL dbeta12, dbeta22, dbeta32;
  CCTK_REAL dbeta13, dbeta23, dbeta33;
  CCTK_REAL ddbeta111, ddbeta211, ddbeta311;
  CCTK_REAL ddbeta112, ddbeta212, ddbeta312;
  CCTK_REAL ddbeta113, ddbeta213, ddbeta313;
  CCTK_REAL ddbeta122, ddbeta222, ddbeta322;
  CCTK_REAL ddbeta123, ddbeta223, ddbeta323;
  CCTK_REAL ddbeta133, ddbeta233, ddbeta333;

  /*
   * RHS.
   */
  CCTK_REAL dt_phi;

  CCTK_REAL dt_gamma11;
  CCTK_REAL dt_gamma12;
  CCTK_REAL dt_gamma13;
  CCTK_REAL dt_gamma22;
  CCTK_REAL dt_gamma23;
  CCTK_REAL dt_gamma33;

  CCTK_REAL dt_K;

  CCTK_REAL dt_A11;
  CCTK_REAL dt_A12;
  CCTK_REAL dt_A13;
  CCTK_REAL dt_A22;
  CCTK_REAL dt_A23;
  CCTK_REAL dt_A33;

  CCTK_REAL dt_Gamma1;
  CCTK_REAL dt_Gamma2;
  CCTK_REAL dt_Gamma3;
};



#define COPY_TO_STRUCT(s,n)                     \
  do {                                          \
                                                \
  /*                                            \
   * Conformal factor.                          \
   */                                           \
    s[n].localphi = localphi;                   \
    s[n].dphi1    = dphi1   ;                   \
    s[n].dphi2    = dphi2   ;                   \
    s[n].dphi3    = dphi3   ;                   \
    s[n].adphi1   = adphi1  ;                   \
    s[n].adphi2   = adphi2  ;                   \
    s[n].adphi3   = adphi3  ;                   \
    s[n].ddphi11  = ddphi11 ;                   \
    s[n].ddphi12  = ddphi12 ;                   \
    s[n].ddphi13  = ddphi13 ;                   \
    s[n].ddphi22  = ddphi22 ;                   \
    s[n].ddphi23  = ddphi23 ;                   \
    s[n].ddphi33  = ddphi33 ;                   \
                                                \
    /*                                          \
     * Conformal metric.                        \
     */                                         \
    s[n].gt11 = gt11;                           \
    s[n].gt12 = gt12;                           \
    s[n].gt13 = gt13;                           \
    s[n].gt22 = gt22;                           \
    s[n].gt23 = gt23;                           \
    s[n].gt33 = gt33;                           \
    s[n].dgt111 = dgt111;                       \
    s[n].dgt121 = dgt121;                       \
    s[n].dgt131 = dgt131;                       \
    s[n].dgt221 = dgt221;                       \
    s[n].dgt231 = dgt231;                       \
    s[n].dgt331 = dgt331;                       \
    s[n].dgt112 = dgt112;                       \
    s[n].dgt122 = dgt122;                       \
    s[n].dgt132 = dgt132;                       \
    s[n].dgt222 = dgt222;                       \
    s[n].dgt232 = dgt232;                       \
    s[n].dgt332 = dgt332;                       \
    s[n].dgt113 = dgt113;                       \
    s[n].dgt123 = dgt123;                       \
    s[n].dgt133 = dgt133;                       \
    s[n].dgt223 = dgt223;                       \
    s[n].dgt233 = dgt233;                       \
    s[n].dgt333 = dgt333;                       \
    s[n].adgt111 = adgt111;                     \
    s[n].adgt121 = adgt121;                     \
    s[n].adgt131 = adgt131;                     \
    s[n].adgt221 = adgt221;                     \
    s[n].adgt231 = adgt231;                     \
    s[n].adgt331 = adgt331;                     \
    s[n].adgt112 = adgt112;                     \
    s[n].adgt122 = adgt122;                     \
    s[n].adgt132 = adgt132;                     \
    s[n].adgt222 = adgt222;                     \
    s[n].adgt232 = adgt232;                     \
    s[n].adgt332 = adgt332;                     \
    s[n].adgt113 = adgt113;                     \
    s[n].adgt123 = adgt123;                     \
    s[n].adgt133 = adgt133;                     \
    s[n].adgt223 = adgt223;                     \
    s[n].adgt233 = adgt233;                     \
    s[n].adgt333 = adgt333;                     \
    s[n].ddgt1111 = ddgt1111;                   \
    s[n].ddgt1211 = ddgt1211;                   \
    s[n].ddgt1311 = ddgt1311;                   \
    s[n].ddgt2211 = ddgt2211;                   \
    s[n].ddgt2311 = ddgt2311;                   \
    s[n].ddgt3311 = ddgt3311;                   \
    s[n].ddgt1112 = ddgt1112;                   \
    s[n].ddgt1212 = ddgt1212;                   \
    s[n].ddgt1312 = ddgt1312;                   \
    s[n].ddgt2212 = ddgt2212;                   \
    s[n].ddgt2312 = ddgt2312;                   \
    s[n].ddgt3312 = ddgt3312;                   \
    s[n].ddgt1113 = ddgt1113;                   \
    s[n].ddgt1213 = ddgt1213;                   \
    s[n].ddgt1313 = ddgt1313;                   \
    s[n].ddgt2213 = ddgt2213;                   \
    s[n].ddgt2313 = ddgt2313;                   \
    s[n].ddgt3313 = ddgt3313;                   \
    s[n].ddgt1122 = ddgt1122;                   \
    s[n].ddgt1222 = ddgt1222;                   \
    s[n].ddgt1322 = ddgt1322;                   \
    s[n].ddgt2222 = ddgt2222;                   \
    s[n].ddgt2322 = ddgt2322;                   \
    s[n].ddgt3322 = ddgt3322;                   \
    s[n].ddgt1123 = ddgt1123;                   \
    s[n].ddgt1223 = ddgt1223;                   \
    s[n].ddgt1323 = ddgt1323;                   \
    s[n].ddgt2223 = ddgt2223;                   \
    s[n].ddgt2323 = ddgt2323;                   \
    s[n].ddgt3323 = ddgt3323;                   \
    s[n].ddgt1133 = ddgt1133;                   \
    s[n].ddgt1233 = ddgt1233;                   \
    s[n].ddgt1333 = ddgt1333;                   \
    s[n].ddgt2233 = ddgt2233;                   \
    s[n].ddgt2333 = ddgt2333;                   \
    s[n].ddgt3333 = ddgt3333;                   \
                                                \
    /*                                          \
     * Curvature scalar.                        \
     */                                         \
    s[n].localK = localK;                       \
    s[n].dK1 = dK1;                             \
    s[n].dK2 = dK2;                             \
    s[n].dK3 = dK3;                             \
    s[n].adK1 = adK1;                           \
    s[n].adK2 = adK2;                           \
    s[n].adK3 = adK3;                           \
                                                \
    /*                                          \
     * Curvature tensor.                        \
     */                                         \
    s[n].At11 = At11;                           \
    s[n].At12 = At12;                           \
    s[n].At13 = At13;                           \
    s[n].At22 = At22;                           \
    s[n].At23 = At23;                           \
    s[n].At33 = At33;                           \
    s[n].dAt111 = dAt111;                       \
    s[n].dAt121 = dAt121;                       \
    s[n].dAt131 = dAt131;                       \
    s[n].dAt221 = dAt221;                       \
    s[n].dAt231 = dAt231;                       \
    s[n].dAt331 = dAt331;                       \
    s[n].dAt112 = dAt112;                       \
    s[n].dAt122 = dAt122;                       \
    s[n].dAt132 = dAt132;                       \
    s[n].dAt222 = dAt222;                       \
    s[n].dAt232 = dAt232;                       \
    s[n].dAt332 = dAt332;                       \
    s[n].dAt113 = dAt113;                       \
    s[n].dAt123 = dAt123;                       \
    s[n].dAt133 = dAt133;                       \
    s[n].dAt223 = dAt223;                       \
    s[n].dAt233 = dAt233;                       \
    s[n].dAt333 = dAt333;                       \
    s[n].adAt111 = adAt111;                     \
    s[n].adAt121 = adAt121;                     \
    s[n].adAt131 = adAt131;                     \
    s[n].adAt221 = adAt221;                     \
    s[n].adAt231 = adAt231;                     \
    s[n].adAt331 = adAt331;                     \
    s[n].adAt112 = adAt112;                     \
    s[n].adAt122 = adAt122;                     \
    s[n].adAt132 = adAt132;                     \
    s[n].adAt222 = adAt222;                     \
    s[n].adAt232 = adAt232;                     \
    s[n].adAt332 = adAt332;                     \
    s[n].adAt113 = adAt113;                     \
    s[n].adAt123 = adAt123;                     \
    s[n].adAt133 = adAt133;                     \
    s[n].adAt223 = adAt223;                     \
    s[n].adAt233 = adAt233;                     \
    s[n].adAt333 = adAt333;                     \
                                                \
    /*                                          \
     * Gammas.                                  \
     */                                         \
    s[n].Gamt1 = Gamt1;                         \
    s[n].Gamt2 = Gamt2;                         \
    s[n].Gamt3 = Gamt3;                         \
    s[n].dGamt11 = dGamt11;                     \
    s[n].dGamt21 = dGamt21;                     \
    s[n].dGamt31 = dGamt31;                     \
    s[n].dGamt12 = dGamt12;                     \
    s[n].dGamt22 = dGamt22;                     \
    s[n].dGamt32 = dGamt32;                     \
    s[n].dGamt13 = dGamt13;                     \
    s[n].dGamt23 = dGamt23;                     \
    s[n].dGamt33 = dGamt33;                     \
                                                \
    /*                                          \
     * Lapse.                                   \
     */                                         \
    s[n].alpha = alpha;                         \
    s[n].dalpha1 = dalpha1;                     \
    s[n].dalpha2 = dalpha2;                     \
    s[n].dalpha3 = dalpha3;                     \
    s[n].ddalpha11 = ddalpha11;                 \
    s[n].ddalpha12 = ddalpha12;                 \
    s[n].ddalpha13 = ddalpha13;                 \
    s[n].ddalpha22 = ddalpha22;                 \
    s[n].ddalpha23 = ddalpha23;                 \
    s[n].ddalpha33 = ddalpha33;                 \
                                                \
    /*                                          \
     * Shift.                                   \
     */                                         \
    s[n].beta1 = beta1;                         \
    s[n].beta2 = beta2;                         \
    s[n].beta3 = beta3;                         \
    s[n].dbeta11 = dbeta11;                     \
    s[n].dbeta21 = dbeta21;                     \
    s[n].dbeta31 = dbeta31;                     \
    s[n].dbeta12 = dbeta12;                     \
    s[n].dbeta22 = dbeta22;                     \
    s[n].dbeta32 = dbeta32;                     \
    s[n].dbeta13 = dbeta13;                     \
    s[n].dbeta23 = dbeta23;                     \
    s[n].dbeta33 = dbeta33;                     \
    s[n].ddbeta111 = ddbeta111;                 \
    s[n].ddbeta211 = ddbeta211;                 \
    s[n].ddbeta311 = ddbeta311;                 \
    s[n].ddbeta112 = ddbeta112;                 \
    s[n].ddbeta212 = ddbeta212;                 \
    s[n].ddbeta312 = ddbeta312;                 \
    s[n].ddbeta113 = ddbeta113;                 \
    s[n].ddbeta213 = ddbeta213;                 \
    s[n].ddbeta313 = ddbeta313;                 \
    s[n].ddbeta122 = ddbeta122;                 \
    s[n].ddbeta222 = ddbeta222;                 \
    s[n].ddbeta322 = ddbeta322;                 \
    s[n].ddbeta123 = ddbeta123;                 \
    s[n].ddbeta223 = ddbeta223;                 \
    s[n].ddbeta323 = ddbeta323;                 \
    s[n].ddbeta133 = ddbeta133;                 \
    s[n].ddbeta233 = ddbeta233;                 \
    s[n].ddbeta333 = ddbeta333;                 \
                                                \
  } while (0)



#define COPY_FROM_STRUCT(s,n)                   \
  do {                                          \
                                                \
  /*                                            \
   * Conformal factor.                          \
   */                                           \
    localphi = s[n].localphi;                   \
    dphi1    = s[n].dphi1   ;                   \
    dphi2    = s[n].dphi2   ;                   \
    dphi3    = s[n].dphi3   ;                   \
    adphi1   = s[n].adphi1  ;                   \
    adphi2   = s[n].adphi2  ;                   \
    adphi3   = s[n].adphi3  ;                   \
    ddphi11  = s[n].ddphi11 ;                   \
    ddphi12  = s[n].ddphi12 ;                   \
    ddphi13  = s[n].ddphi13 ;                   \
    ddphi22  = s[n].ddphi22 ;                   \
    ddphi23  = s[n].ddphi23 ;                   \
    ddphi33  = s[n].ddphi33 ;                   \
                                                \
    /*                                          \
     * Conformal metric.                        \
     */                                         \
    gt11 = s[n].gt11;                           \
    gt12 = s[n].gt12;                           \
    gt13 = s[n].gt13;                           \
    gt22 = s[n].gt22;                           \
    gt23 = s[n].gt23;                           \
    gt33 = s[n].gt33;                           \
    dgt111 = s[n].dgt111;                       \
    dgt121 = s[n].dgt121;                       \
    dgt131 = s[n].dgt131;                       \
    dgt221 = s[n].dgt221;                       \
    dgt231 = s[n].dgt231;                       \
    dgt331 = s[n].dgt331;                       \
    dgt112 = s[n].dgt112;                       \
    dgt122 = s[n].dgt122;                       \
    dgt132 = s[n].dgt132;                       \
    dgt222 = s[n].dgt222;                       \
    dgt232 = s[n].dgt232;                       \
    dgt332 = s[n].dgt332;                       \
    dgt113 = s[n].dgt113;                       \
    dgt123 = s[n].dgt123;                       \
    dgt133 = s[n].dgt133;                       \
    dgt223 = s[n].dgt223;                       \
    dgt233 = s[n].dgt233;                       \
    dgt333 = s[n].dgt333;                       \
    adgt111 = s[n].adgt111;                     \
    adgt121 = s[n].adgt121;                     \
    adgt131 = s[n].adgt131;                     \
    adgt221 = s[n].adgt221;                     \
    adgt231 = s[n].adgt231;                     \
    adgt331 = s[n].adgt331;                     \
    adgt112 = s[n].adgt112;                     \
    adgt122 = s[n].adgt122;                     \
    adgt132 = s[n].adgt132;                     \
    adgt222 = s[n].adgt222;                     \
    adgt232 = s[n].adgt232;                     \
    adgt332 = s[n].adgt332;                     \
    adgt113 = s[n].adgt113;                     \
    adgt123 = s[n].adgt123;                     \
    adgt133 = s[n].adgt133;                     \
    adgt223 = s[n].adgt223;                     \
    adgt233 = s[n].adgt233;                     \
    adgt333 = s[n].adgt333;                     \
    ddgt1111 = s[n].ddgt1111;                   \
    ddgt1211 = s[n].ddgt1211;                   \
    ddgt1311 = s[n].ddgt1311;                   \
    ddgt2211 = s[n].ddgt2211;                   \
    ddgt2311 = s[n].ddgt2311;                   \
    ddgt3311 = s[n].ddgt3311;                   \
    ddgt1112 = s[n].ddgt1112;                   \
    ddgt1212 = s[n].ddgt1212;                   \
    ddgt1312 = s[n].ddgt1312;                   \
    ddgt2212 = s[n].ddgt2212;                   \
    ddgt2312 = s[n].ddgt2312;                   \
    ddgt3312 = s[n].ddgt3312;                   \
    ddgt1113 = s[n].ddgt1113;                   \
    ddgt1213 = s[n].ddgt1213;                   \
    ddgt1313 = s[n].ddgt1313;                   \
    ddgt2213 = s[n].ddgt2213;                   \
    ddgt2313 = s[n].ddgt2313;                   \
    ddgt3313 = s[n].ddgt3313;                   \
    ddgt1122 = s[n].ddgt1122;                   \
    ddgt1222 = s[n].ddgt1222;                   \
    ddgt1322 = s[n].ddgt1322;                   \
    ddgt2222 = s[n].ddgt2222;                   \
    ddgt2322 = s[n].ddgt2322;                   \
    ddgt3322 = s[n].ddgt3322;                   \
    ddgt1123 = s[n].ddgt1123;                   \
    ddgt1223 = s[n].ddgt1223;                   \
    ddgt1323 = s[n].ddgt1323;                   \
    ddgt2223 = s[n].ddgt2223;                   \
    ddgt2323 = s[n].ddgt2323;                   \
    ddgt3323 = s[n].ddgt3323;                   \
    ddgt1133 = s[n].ddgt1133;                   \
    ddgt1233 = s[n].ddgt1233;                   \
    ddgt1333 = s[n].ddgt1333;                   \
    ddgt2233 = s[n].ddgt2233;                   \
    ddgt2333 = s[n].ddgt2333;                   \
    ddgt3333 = s[n].ddgt3333;                   \
                                                \
    /*                                          \
     * Curvature scalar.                        \
     */                                         \
    localK = s[n].localK;                       \
    dK1 = s[n].dK1;                             \
    dK2 = s[n].dK2;                             \
    dK3 = s[n].dK3;                             \
    adK1 = s[n].adK1;                           \
    adK2 = s[n].adK2;                           \
    adK3 = s[n].adK3;                           \
                                                \
    /*                                          \
     * Curvature tensor.                        \
     */                                         \
    At11 = s[n].At11;                           \
    At12 = s[n].At12;                           \
    At13 = s[n].At13;                           \
    At22 = s[n].At22;                           \
    At23 = s[n].At23;                           \
    At33 = s[n].At33;                           \
    dAt111 = s[n].dAt111;                       \
    dAt121 = s[n].dAt121;                       \
    dAt131 = s[n].dAt131;                       \
    dAt221 = s[n].dAt221;                       \
    dAt231 = s[n].dAt231;                       \
    dAt331 = s[n].dAt331;                       \
    dAt112 = s[n].dAt112;                       \
    dAt122 = s[n].dAt122;                       \
    dAt132 = s[n].dAt132;                       \
    dAt222 = s[n].dAt222;                       \
    dAt232 = s[n].dAt232;                       \
    dAt332 = s[n].dAt332;                       \
    dAt113 = s[n].dAt113;                       \
    dAt123 = s[n].dAt123;                       \
    dAt133 = s[n].dAt133;                       \
    dAt223 = s[n].dAt223;                       \
    dAt233 = s[n].dAt233;                       \
    dAt333 = s[n].dAt333;                       \
    adAt111 = s[n].adAt111;                     \
    adAt121 = s[n].adAt121;                     \
    adAt131 = s[n].adAt131;                     \
    adAt221 = s[n].adAt221;                     \
    adAt231 = s[n].adAt231;                     \
    adAt331 = s[n].adAt331;                     \
    adAt112 = s[n].adAt112;                     \
    adAt122 = s[n].adAt122;                     \
    adAt132 = s[n].adAt132;                     \
    adAt222 = s[n].adAt222;                     \
    adAt232 = s[n].adAt232;                     \
    adAt332 = s[n].adAt332;                     \
    adAt113 = s[n].adAt113;                     \
    adAt123 = s[n].adAt123;                     \
    adAt133 = s[n].adAt133;                     \
    adAt223 = s[n].adAt223;                     \
    adAt233 = s[n].adAt233;                     \
    adAt333 = s[n].adAt333;                     \
                                                \
    /*                                          \
     * Gammas.                                  \
     */                                         \
    Gamt1 = s[n].Gamt1;                         \
    Gamt2 = s[n].Gamt2;                         \
    Gamt3 = s[n].Gamt3;                         \
    dGamt11 = s[n].dGamt11;                     \
    dGamt21 = s[n].dGamt21;                     \
    dGamt31 = s[n].dGamt31;                     \
    dGamt12 = s[n].dGamt12;                     \
    dGamt22 = s[n].dGamt22;                     \
    dGamt32 = s[n].dGamt32;                     \
    dGamt13 = s[n].dGamt13;                     \
    dGamt23 = s[n].dGamt23;                     \
    dGamt33 = s[n].dGamt33;                     \
                                                \
    /*                                          \
     * Lapse.                                   \
     */                                         \
    alpha = s[n].alpha;                         \
    dalpha1 = s[n].dalpha1;                     \
    dalpha2 = s[n].dalpha2;                     \
    dalpha3 = s[n].dalpha3;                     \
    ddalpha11 = s[n].ddalpha11;                 \
    ddalpha12 = s[n].ddalpha12;                 \
    ddalpha13 = s[n].ddalpha13;                 \
    ddalpha22 = s[n].ddalpha22;                 \
    ddalpha23 = s[n].ddalpha23;                 \
    ddalpha33 = s[n].ddalpha33;                 \
                                                \
    /*                                          \
     * Shift.                                   \
     */                                         \
    beta1 = s[n].beta1;                         \
    beta2 = s[n].beta2;                         \
    beta3 = s[n].beta3;                         \
    dbeta11 = s[n].dbeta11;                     \
    dbeta21 = s[n].dbeta21;                     \
    dbeta31 = s[n].dbeta31;                     \
    dbeta12 = s[n].dbeta12;                     \
    dbeta22 = s[n].dbeta22;                     \
    dbeta32 = s[n].dbeta32;                     \
    dbeta13 = s[n].dbeta13;                     \
    dbeta23 = s[n].dbeta23;                     \
    dbeta33 = s[n].dbeta33;                     \
    ddbeta111 = s[n].ddbeta111;                 \
    ddbeta211 = s[n].ddbeta211;                 \
    ddbeta311 = s[n].ddbeta311;                 \
    ddbeta112 = s[n].ddbeta112;                 \
    ddbeta212 = s[n].ddbeta212;                 \
    ddbeta312 = s[n].ddbeta312;                 \
    ddbeta113 = s[n].ddbeta113;                 \
    ddbeta213 = s[n].ddbeta213;                 \
    ddbeta313 = s[n].ddbeta313;                 \
    ddbeta122 = s[n].ddbeta122;                 \
    ddbeta222 = s[n].ddbeta222;                 \
    ddbeta322 = s[n].ddbeta322;                 \
    ddbeta123 = s[n].ddbeta123;                 \
    ddbeta223 = s[n].ddbeta223;                 \
    ddbeta323 = s[n].ddbeta323;                 \
    ddbeta133 = s[n].ddbeta133;                 \
    ddbeta233 = s[n].ddbeta233;                 \
    ddbeta333 = s[n].ddbeta333;                 \
                                                \
  } while (0)



#define COPY_RHS(s,ijk,f)                       \
  for (int n=0; n<VECTORLENGTH; ++n) {          \
    f[ijk+n] = s[n].f;                          \
  }
