#include "cctk.h"
#include "util_Table.h"

#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

void
CTGConstraints_SelectBoundaries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT const faces = CCTK_ALL_FACES;
  int ierr = 0;

  ierr += Boundary_SelectGroupForBC
    (cctkGH, faces, 1, -1, "CTGConstraints::Hamiltonian_constraint", "Scalar");
  ierr += Boundary_SelectGroupForBC
    (cctkGH, faces, 1, -1, "CTGConstraints::momentum_constraint", "Scalar");
  ierr += Boundary_SelectGroupForBC
    (cctkGH, faces, 1, -1, "CTGConstraints::Gamma_constraint", "Scalar");

  if (ierr) CCTK_WARN(0, "Error applying boundary condition.");

  return;
}
