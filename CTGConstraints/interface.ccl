implements: CTGConstraints
inherits: CTGBase SummationByParts ADMBase Coordinates TmunuBase

uses include: ctgbase_diffops.h
uses include: ctgbase_utils.h
uses include: loopcontrol.h
uses include: GlobalDerivative.h


### From CactusBase/Boundary

CCTK_INT FUNCTION Boundary_SelectVarForBC(CCTK_POINTER_TO_CONST IN GH, \
  CCTK_INT IN faces, CCTK_INT IN boundary_width, CCTK_INT IN table_handle, \
  CCTK_STRING IN var_name, CCTK_STRING IN bc_name)
CCTK_INT FUNCTION Boundary_SelectGroupForBC(CCTK_POINTER_TO_CONST IN GH, \
  CCTK_INT IN faces, CCTK_INT IN boundary_width, CCTK_INT IN table_handle, \
  CCTK_STRING IN group_name, CCTK_STRING IN bc_name)

REQUIRES FUNCTION Boundary_SelectVarForBC
REQUIRES FUNCTION Boundary_SelectGroupForBC


### From CactusBase/SymBase

CCTK_INT FUNCTION \
    SymmetryTableHandleForGrid (CCTK_POINTER_TO_CONST IN cctkGH)
REQUIRES FUNCTION SymmetryTableHandleForGrid


### From CTGBase

CCTK_INT FUNCTION Util_GetGridRanges (CCTK_POINTER_TO_CONST IN GH, \
                                      CCTK_INT ARRAY OUT istart, \
                                      CCTK_INT ARRAY OUT iend)
REQUIRES FUNCTION Util_GetGridRanges

CCTK_INT FUNCTION Util_SetStencil (CCTK_POINTER_TO_CONST IN GH, \
                                   CCTK_POINTER_TO_CONST OUT imin, \
                                   CCTK_POINTER_TO_CONST OUT imax, \
                                   CCTK_POINTER_TO_CONST OUT q, \
                                   CCTK_INT IN deriv, \
                                   CCTK_INT IN dir)
REQUIRES FUNCTION Util_SetStencil


### Local

public:

real Hamiltonian_constraint type=GF timelevels=3 tags='tensortypealias="Scalar" ProlongationParameter="CTGConstraints::constraints_prolongation_type"'
{
  H
} "Hamiltonian constraint"

real momentum_constraint type=GF timelevels=3 tags='tensortypealias="d" ProlongationParameter="CTGConstraints::constraints_prolongation_type"'
{
  M1, M2, M3
} "Momentum constraint"

real Gamma_constraint type=GF timelevels=3 tags='tensortypealias="U" tensorweight=0.66666666666666667 tensorspecial="gamma" tensormetric="CTGamma::metric" ProlongationParameter="CTGConstraints::constraints_prolongation_type"'
{
  G1, G2, G3
} "Gamma^a=igammat[b,c]Chrt[a,b,c] constraint"

real trA_constraint type=GF timelevels=3 tags='tensortypealias="Scalar" ProlongationParameter="CTGConstraints::constraints_prolongation_type"'
{
  A
} "trA=0 constraint"

real detg_constraint type=GF timelevels=3 tags='tensortypealias="Scalar" ProlongationParameter="CTGConstraints::constraints_prolongation_type"'
{
  gamma
} "gamma=1 constraint"

