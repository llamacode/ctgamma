(* ctgamma_constraints.m

Calculate the constraints for the CTGamma evolution system.

This Mathematica script generates a set of scalar equations which
correspond to the constraints of the evolution equations. Two output
files are created:
  ctgamma_constraints_declare.h - declares the scalars and intermediate
                                  variables used in the calculations;
  ctgamma_constraints_calc.h    - contains the computations based on
                                  the declared variables.

The files are included in the main calculation loop in
../src/calc_constraints.c.  They assume that certain variables are
available and have been computed beforehand, such as local scalars
corresponding to each GF at a point, as well as all of the required
derivatives.

To run this file, open a Mathematica terminal and at the prompt type:
  Get["ctgamma_constraints.m"]

*)

DeclareFile = "ctgamma_constraints_declare.h"
ConsFile = "ctgamma_constraints_calc.h"

kdelta[a_,b_] := 0 /; a != b
kdelta[a_,b_] := 1 /; a == b


(*
 * Turns X[a,b] into {X[1,1], X[1,2], ..., X[3,3]
 *)
FreeIndexExpand[x_, a_Symbol] := Union[Flatten[Table[x, {a, 1, 3}]]]
FreeIndexExpand[x_, {a_Symbol}] := FreeIndexExpand[x, a]
FreeIndexExpand[x_, {a_Symbol, b__}] :=
  Union[Flatten[Table[FreeIndexExpand[x, {b}], {a,1,3}]]]

ForcePositiveRule = -x_ :> x

ExpandFreeExpr[x_Symbol] := x
ExpandFreeExpr[x_[a__]] :=
  DeleteCases[
    Union[Flatten[FreeIndexExpand[x[a], {a}] /. ForcePositiveRule]],
    0]

ExpandFreeEqn[x_Symbol==y_] := x==y
ExpandFreeEqn[x_[a__]==y_] := 
 DeleteCases[Union[Flatten[FreeIndexExpand[x[a]==y,{a}]]], True]

ExpandFreeIndices = {
  x_ :> Flatten[Map[ExpandFreeEqn,x]] /; !FreeQ[x,y_==z_],
  x_ :> Flatten[Map[ExpandFreeExpr,x]] /; FreeQ[x,y_==z_]
}


(*
 * Turn g[1,2] into g12
 *)
SetScalarIndex[a_,b_] := ToExpression[ToString[a] <> ToString[b]]; 
SetScalarVal[x_] :=
  x //. y_Symbol[a_,b___] :> SetScalarIndex[y,a][b] /. y_[] :> y

SetIndexMapRules[variables_] := Module[
  {AllVars, VarsToReplace, Replacements},
  AllVars = Flatten[Map[ExpandFreeExpr, variables]];
  VarsToReplace = Cases[AllVars, x_[a__]];
  Replacements = Map[SetScalarVal, VarsToReplace];
  Table[VarsToReplace[[i]] -> Replacements[[i]], {i, 1, Length[Replacements]}]
]

ContractIndexRules = {
  T1_[a___, b_Symbol, c___, d_Symbol, e___] \
    T2_[f___, b_Symbol, g___] T3_[h___, d_Symbol, i___] ->
    Sum[T1[a,b,c,d,e] T2[f,b,g] T3[h,d,i], {b,1,3}, {d,1,3}],

  T1_[a___,b_Symbol,c___] T2_[d___,b_Symbol,e___] ->
    Sum[T1[a,b,c] T2[d,b,e], {b,1,3}],

  T1_[a___,b_Symbol,c___,b_Symbol,d___] ->
    Sum[T1[a,b,c,b,d], {b,1,3}]
}
    
(*
 * Expression simplification rules
 *)

(* collect the inverse metrics *)
igvars = {ig[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseMetricRule = x_ == y_ :> x == Collect[y, igvars]
igtvars = {igt[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseConformalMetricRule = x_ == y_ :> x == Collect[y, igtvars]

(* trivial factorisation *)
CollectRule = a_ x_ + a_ y_ -> a (x + y)

(* collect negative values *)
ReplaceNegRule = a_ /; a < 0 -> neg (-a)
UndoReplaceNegRule = neg -> -1


(*
 * C-output
 *)

WriteCAssignment[e_] := Module[{x},
  x = ToString[InputForm[e]];
  x = StringReplace[x, "]" -> "]]"];
  x = StringReplace[x, "[" -> "[["];  
  x = ToExpression[x];
  PutAppend[CForm[x[[1]]], ConsFile];
  WriteString[ConsFP, "  = " <> ToString[CForm[x[[2]]]] <> ";"];
  WriteString[ConsFP, "\n"]
]

WriteCDeclare[e_] := Module[{x},
  x = ToString[InputForm[e]];
  WriteString[DeclareFP, "CCTK_REAL " <> x <> ";\n"];
]


(*
 * Model specific...
 *)

ConsVariables = {
  LH, LM[a], LG[a]
}

AuxVariables = {
  g[a,b], gt[a,b], K[a,b], localA[a,b],
  ig[a,b], igt[a,b], mK[a,b], mA[a,b],
  dg[a,b,c], dig[a,b,c], ddg[a,b,c,d],
  dgt[a,b,c],
  ChrD[a,b,c], Chr[a,b,c], ChrDt[a,b,c], Chrt[a,b,c],
  Ric[a,b], Ric,
  dK[a,b,c], gK[a,b,c], gKtemp[a,b,c],
  trK, trA,
  localGamma[a]
}

(* index symmetries *)

g[a_,b_] := g[b,a] /; !OrderedQ[{a,b}]
gt[a_,b_] := gt[b,a] /; !OrderedQ[{a,b}]
K[a_,b_] := K[b,a] /; !OrderedQ[{a,b}]
localA[a_,b_] := localA[b,a] /; !OrderedQ[{a,b}]
kdelta[a_,b_] := kdelta[b,a] /; !OrderedQ[{a,b}]
ig[a_,b_] := ig[b,a] /; !OrderedQ[{a,b}]
igt[a_,b_] := igt[b,a] /; !OrderedQ[{a,b}]
dg[a_,b_,c_] := dg[b,a,c] /; !OrderedQ[{a,b}]
dgt[a_,b_,c_] := dgt[b,a,c] /; !OrderedQ[{a,b}]
dig[a_,b_,c_] := dig[b,a,c] /; !OrderedQ[{a,b}]
ddg[a_,b_,c_,d_] := ddg[b,a,c,d] /; !OrderedQ[{a,b}]
ddg[a_,b_,c_,d_] := ddg[a,b,d,c] /; !OrderedQ[{c,d}]
ChrD[a_,b_,c_] := ChrD[a,c,b] /; !OrderedQ[{b,c}]
Chr[a_,b_,c_] := Chr[a,c,b] /; !OrderedQ[{b,c}]
ChrDt[a_,b_,c_] := ChrDt[a,c,b] /; !OrderedQ[{b,c}]
Chrt[a_,b_,c_] := Chrt[a,c,b] /; !OrderedQ[{b,c}]
Ric[a_,b_] := Ric[b,a] /; !OrderedQ[{a,b}]
dK[a_,b_,c_] := dK[b,a,c] /; !OrderedQ[{a,b}]
gK[a_,b_,c_] := gK[b,a,c] /; !OrderedQ[{a,b}]
gKtemp[a_,b_,c_] := gKtemp[b,a,c] /; !OrderedQ[{a,b}]

ConstraintEquations = {
  ChrD[c,a,b] == (1/2) (dg[a,c,b] + dg[b,c,a] - dg[a,b,c]),
  Chr[c,a,b] == ig[c,d] ChrD[d,a,b],

  dig[b,c,a] == -ig[b,d] ig[c,e] dg[d,e,a],

  Ric[a,b] == -Chr[c,d,a] Chr[d,c,b] + Chr[c,a,b] Chr[d,c,d] +
              (1/2) ig[c,d] *
              (- ddg[c,d,a,b] + ddg[c,a,d,b] - ddg[a,b,c,d] + ddg[d,b,c,a]) +
              (1/2) dig[c,d,c] * (dg[d,b,a]+dg[d,a,b]-dg[b,a,d]) -
              (1/2) dig[c,d,b] * (dg[d,c,a]+dg[d,a,c]-dg[c,a,d]),
  Ric == ig[a,b] Ric[a,b],

  mK[a,b] == ig[a,c] K[c,b],
  trK == mK[a,a],

  gKtemp[a,b,c] == - Chr[d,a,c] K[d,b] - Chr[d,b,c] K[a,d],
  gK[a,b,c] == dK[a,b,c] + gKtemp[a,b,c],

  LH == Ric - mK[a,b] mK[b,a] + trK^2,

  LM[a] == ig[b,c] (gK[a,b,c] - gK[b,c,a]),

  ChrDt[c,a,b] == (1/2) (dgt[a,c,b] + dgt[b,c,a] - dgt[a,b,c]),
  Chrt[c,a,b] == igt[c,d] ChrDt[d,a,b],

  LG[a] == localGamma[a] - igt[b,c] Chrt[a,b,c],

  mA[a,b] == igt[a,c] localA[c,b],
  trA == mA[a,a]

}

AllVars = Join[ConsVariables, AuxVariables]
IndexMapRules = SetIndexMapRules[AllVars]

ScalarConsVars = AllVars /. ExpandFreeIndices /. IndexMapRules

Print["Processing constraint equations ..."]
CEquations = ConstraintEquations
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = CEquations /. ExpandFreeIndices
CEquations = CEquations /. IndexMapRules

Print["Simplifying expressions..."]
CEquations = CEquations /. CollectInverseMetricRule
CEquations = CEquations /. CollectInverseConformalMetricRule
CEquations = CEquations //. CollectRule
CEquations = CEquations /. ReplaceNegRule //. CollectRule /. UndoReplaceNegRule

(*
 *  Write C-code to file.
 *)
Print["Writing variable declarations to " <> DeclareFile]
DeclareFP = OpenWrite[DeclareFile]
Map[WriteCDeclare, ScalarConsVars]
Close[DeclareFP]

Print["Writing constraint computation to " <> ConsFile]
ConsFP = OpenWrite[ConsFile]
Map[WriteCAssignment, CEquations]
Close[ConsFP]
