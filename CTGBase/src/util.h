#ifndef CTGBASE_UTIL_H
#define CTGBASE_UTIL_H

#include <stdlib.h>

#define CTGBASE_SMALL 1e-10

#include "Symmetry.h"

#define CTGBase_free_stencil(imin, imax, q)     \
  do                                            \
  {                                             \
    for (int d=0; d<3; ++d)                     \
    {                                           \
      free(imin[d]);                            \
      free(imax[d]);                            \
      free(q[d]);                               \
    }                                           \
  }                                             \
  while (0)



#define CTGBase_set_advective_stencil(adv_q, adv_imin, adv_imax, beta,  \
                                      q_centre, imin_centre, imax_centre, \
                                      q_minus, imin_minus, imax_minus,  \
                                      q_plus, imin_plus, imax_plus)     \
  do                                                                    \
  {                                                                     \
    if (beta < -CTGBASE_SMALL)                                           \
    {                                                                   \
      adv_q = q_minus;                                                  \
      adv_imin = imin_minus;                                            \
      adv_imax = imax_minus;                                            \
    }                                                                   \
    else if (beta > CTGBASE_SMALL)                                      \
    {                                                                   \
      adv_q = q_plus;                                                   \
      adv_imin = imin_plus;                                             \
      adv_imax = imax_plus;                                             \
    }                                                                   \
    else                                                                \
    {                                                                   \
      adv_q = q_centre;                                                 \
      adv_imin = imin_centre;                                           \
      adv_imax = imax_centre;                                           \
    }                                                                   \
  }                                                                     \
  while (0)


#define CTGBase_invert_metric(g11, g12, g13, g22, g23, g33, detg, \
                              ig11, ig12, ig13, ig22, ig23, ig33) \
  do                                                              \
    {                                                             \
      CCTK_REAL detg11 = g22*g33 - g23*g23;                       \
      CCTK_REAL detg12 = g13*g23 - g12*g33;                       \
      CCTK_REAL detg13 = g12*g23 - g13*g22;                       \
      CCTK_REAL detg22 = g11*g33 - g13*g13;                       \
      CCTK_REAL detg23 = g12*g13 - g11*g23;                       \
      CCTK_REAL detg33 = g11*g22 - g12*g12;                       \
                                                                  \
      detg = detg11*g11 + detg12*g12 + detg13*g13;                \
                                                                  \
      ig11 = detg11 / detg;                                       \
      ig12 = detg12 / detg;                                       \
      ig13 = detg13 / detg;                                       \
      ig22 = detg22 / detg;                                       \
      ig23 = detg23 / detg;                                       \
      ig33 = detg33 / detg;                                       \
    }                                                             \
  while (0)


#define BSSN_TYPE 0
#define Z4c_TYPE  1

#define DECLARE_EVOL_TYPE \
  int EVOL_TYPE = 0; \
  if (CCTK_EQUALS(evolution_system_type, "bssn")) EVOL_TYPE = BSSN_TYPE; \
  if (CCTK_EQUALS(evolution_system_type, "Z4c")) EVOL_TYPE = Z4c_TYPE;



#endif
