#include <math.h>
#include <stdio.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"

#define REFLEVEL (ilogb(cctk_levfac[0]))

void
CTGBase_print_fd_stencils(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT * restrict imin[3], * restrict imax[3];
  CCTK_REAL * restrict q[3];
  int reflvl = REFLEVEL;
  CCTK_INT istart, iend;
  int stride=cctk_lsh[0];

  printf("reflevel: %d\n", reflvl);

  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin, (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q, 1, 0);
  Util_GetGridRanges(cctkGH, &istart, &iend);
  
  for (int i=iend-16; i<iend; ++i)
    {
      int ijk = CCTK_GFINDEX3D(cctkGH, i, 0, 0);
      printf("i=%d x=%f: ", i, (float) x[ijk]);
      for (int j=imin[0][i]; j<=imax[0][i]; ++j)
	printf ("%d:%f ", j, q[0][j+i*stride]);
      printf("\n");
    }

  return;
}
