#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void
CTGBase_SetUpwindOffset(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (upwind_advection)
    {
      if (verbose)
        CCTK_INFO("Upwinding advection terms.");
      (*upwind_offset) = 1;
    }
  else
    (*upwind_offset) = 0;

  return;
}

void
CTGBase_SetConformalFactorExponent(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (CCTK_EQUALS(conformal_factor_type, "W"))
    {
      CCTK_INFO("Using 'W' form of the conformal factor.");
      *detg_exponent = -1.0/6.0;
    }
  else if (CCTK_EQUALS(conformal_factor_type, "chi"))
    {
      CCTK_INFO("Using 'chi' form of the conformal factor.");
      *detg_exponent = -1.0/3.0;
    }
  else
    {
      CCTK_INFO("Using 'phi' (traditional) form of the conformal factor.");
      /* In this case, detg_exponent will not be used in any calculation */
      *detg_exponent = 0.0;
    }

  return;
}

