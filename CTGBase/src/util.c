#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "util_String.h"

#include "Symmetry.h"
#include "GlobalDerivative.h"

#include "util.h"
#include "util_String.h"

#define REFLEVEL (ilogb(cctk_levfac[0]))

CCTK_INT
CTGBase_Util_SetStencil(CCTK_POINTER_TO_CONST _cctkGH,
                        CCTK_POINTER_TO_CONST* _imin,
                        CCTK_POINTER_TO_CONST* _imax,
                        CCTK_POINTER_TO_CONST* _q,
                        CCTK_INT deriv, CCTK_INT dir)
{
  const cGH* const cctkGH = _cctkGH;  
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT** imin = (CCTK_INT**) _imin;
  CCTK_INT** imax = (CCTK_INT**) _imax;
  CCTK_REAL** q = (CCTK_REAL**) _q;
  int n, i, j;
  int reflvl = REFLEVEL;
  int local_dir = dir;
  int map = 0;

  int orig_fd_order =
    *(const CCTK_INT*)CCTK_ParameterGet("order", "SummationByParts", NULL);
  CCTK_REAL orig_epsdis =
    *(const CCTK_REAL*)CCTK_ParameterGet("epsdis", "SummationByParts", NULL);

  if (CCTK_IsFunctionAliased("MultiPatch_GetMap"))
     map = MultiPatch_GetMap(cctkGH);

  if (order_for_level[reflvl] > 0)
  {
     char fd_order_str[100];
     Util_snprintf(fd_order_str, sizeof fd_order_str,
                    "%d", (int) order_for_level[reflvl]);
     CCTK_ParameterSet("order", "SummationByParts", fd_order_str);
  }

  int fd_order_on_non_Cart_maps =
    *(const CCTK_INT*)CCTK_ParameterGet("fd_order_on_non_Cart_maps", "GlobalDerivative", NULL);
  if (fd_order_on_non_Cart_maps >= 2 && map != 0)
  {
     char fd_order_str[100];
     Util_snprintf(fd_order_str, sizeof fd_order_str,
                    "%d", (int) fd_order_on_non_Cart_maps);
     CCTK_ParameterSet("order", "SummationByParts", fd_order_str);
  }

  if ((reflvl==0) && (coarse_grid_o2==1))
    CCTK_ParameterSet("order", "SummationByParts", "2");

  if ((reflvl==0) && (coarse_grid_upwind_advection==0))
    local_dir = 0;

  if (upwind_advection == 0)
    local_dir = 0;

  if (epsdis_for_level[reflvl] >= 0.0)
    {
      char epsdis_str[100];
      Util_snprintf(epsdis_str, sizeof epsdis_str,
                    "%g", (double) epsdis_for_level[reflvl]);
      CCTK_ParameterSet("epsdis", "SummationByParts", epsdis_str);
    }


  for (i=0; i<3; ++i)
    {
      n = cctk_lsh[i];
      imin[i] = (CCTK_INT *) malloc(n*sizeof(CCTK_INT));
      imax[i] = (CCTK_INT *) malloc(n*sizeof(CCTK_INT));
      q[i] = (CCTK_REAL *) malloc(n*n*sizeof(CCTK_REAL));

      if ((imin[i]==NULL) || (imax[i]==NULL) || (q[i]==NULL))
        CCTK_WARN(0, "Could not allocated derivative coefficient arrays.");

      if (deriv==1)
        {
          if (local_dir==0)
            Diff_coeff(cctkGH, i, n, imin[i], imax[i], q[i], -1);
          else
            Diff_up_coeff(cctkGH, i, n, imin[i], imax[i], q[i], local_dir, -1);
        }
      else if (deriv==2)
        {
          if (local_dir != 0)
            CCTK_WARN(0, "Upwind 2nd derivatives not implemented.");
          Diff2_coeff(cctkGH, i, n, imin[i], imax[i], q[i], -1);
        }
      else
        CCTK_WARN(0, "Only 1st and 2nd derivatives are implemented.");

      for (j=0; j<n; ++j)
        {
          imin[i][j] -= 1;
          imax[i][j] -= 1;
        }
    }

  if (((coarse_grid_o2==1)&&(reflvl==0)) || (fd_order_on_non_Cart_maps >= 2 && map != 0) || (order_for_level[reflvl] > 0))
    {
      char fd_order_str[100];
      Util_snprintf(fd_order_str, sizeof fd_order_str,
                    "%d", orig_fd_order);
      CCTK_ParameterSet("order", "SummationByParts", fd_order_str);
    }

  if (epsdis_for_level[reflvl] >= 0.0)
    {
      char epsdis_str[100];
      Util_snprintf(epsdis_str, sizeof epsdis_str,
                    "%g", (double) orig_epsdis);
      CCTK_ParameterSet("epsdis", "SummationByParts", epsdis_str);
    }      

  return 0;
}

void
CTGBase_PrintStencils(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT * restrict imin[3];
  CCTK_INT * restrict imax[3];
  CCTK_REAL * restrict q[3];

  int fd_order =
    *(const CCTK_INT*)CCTK_ParameterGet("order", "SummationByParts", NULL);
  CCTK_VInfo(CCTK_THORNSTRING, "Interior finite difference order: %d",
	     fd_order);

  int patch = MultiPatch_GetMap(cctkGH);

  if (!upwind_advection)
    CCTK_INFO("Advection is turned off.");

  CCTK_VInfo(CCTK_THORNSTRING, "1st-derivatives (patch %d):", patch);
  CTGBase_Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin, (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q, 1, 0);
  for (int i=0; i<=2*cctk_nghostzones[0]; ++i)
    {
      printf("%d [%ld,%ld] ::", i, (long)imin[0][i], (long)imax[0][i]);
      for (int j=imin[0][i]; j<=imax[0][i]; ++j)
        printf("\t%f", (double)q[0][i*cctk_lsh[0]+j]);
      printf("\n");
    }
  CTGBase_free_stencil(imin, imax, q);

  if (upwind_advection)
    {
      CCTK_INFO("Advected stencils: +direction");
      CTGBase_Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin, (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q, 1, +1);
      for (int i=0; i<=2*cctk_nghostzones[0]; ++i)
	{
	  printf("%d [%ld,%ld] ::", i, (long)imin[0][i], (long)imax[0][i]);
	  for (int j=imin[0][i]; j<=imax[0][i]; ++j)
	    printf("\t%f", (double)q[0][i*cctk_lsh[0]+j]);
	  printf("\n");
	}
      CTGBase_free_stencil(imin, imax, q);

      CCTK_INFO("Advected stencils: -direction");
      CTGBase_Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin, (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q, 1, -1);
      for (int i=0; i<=2*cctk_nghostzones[0]; ++i)
	{
	  printf("%d [%ld,%ld] ::", i, (long)imin[0][i], (long)imax[0][i]);
	  for (int j=imin[0][i]; j<=imax[0][i]; ++j)
	    printf("\t%f", (double)q[0][i*cctk_lsh[0]+j]);
	  printf("\n");
	}
      CTGBase_free_stencil(imin, imax, q);
    }

  CCTK_VInfo(CCTK_THORNSTRING, "2nd-derivatives (patch %d):", patch);
  CTGBase_Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin, (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q, 2, 0);
  for (int i=0; i<=2*cctk_nghostzones[0]; ++i)
    {
      printf("%d [%ld,%ld] ::", i, (long)imin[0][i], (long)imax[0][i]);
      for (int j=imin[0][i]; j<=imax[0][i]; ++j)
        printf("\t%f", (double)q[0][i*cctk_lsh[0]+j]);
      printf("\n");
    }
  CTGBase_free_stencil(imin, imax, q);

  return;
}
