#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "Symmetry.h"

#include "util.h"

void
CTGBase_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  int sym[3];
  int ierr=0;

  CCTK_INFO("Setting symmetries for evolution variables.");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = +1;
  
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::phi");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::gamma11");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::gamma22");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::gamma33");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::K");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::A11");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::A22");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::A33");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::igamma11");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::igamma22");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::igamma33");
  if (EVOL_TYPE == Z4c_TYPE) {
    ierr += SetCartSymVN(cctkGH, sym, "CTGBase::Khat");
    ierr += SetCartSymVN(cctkGH, sym, "CTGBase::Theta");
  }
  
  sym[0] = -1;
  sym[1] = -1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::gamma12");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::A12");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::igamma12");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::gamma13");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::A13");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::igamma13");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::gamma23");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::A23");
  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::igamma23");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::Gamma1");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = +1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::Gamma2");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = -1;

  ierr += SetCartSymVN(cctkGH, sym, "CTGBase::Gamma3");

  if (ierr != 0)
    CCTK_WARN(0, "Couldn't set GF symmetries.");

  return;
}
