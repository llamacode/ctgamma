#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "util_Table.h"

#include "Slicing.h"
#include "Boundary.h"
#include "Symmetry.h"
#include "GlobalDerivative.h"
#include "ctgbase_util.h"

#include "loopcontrol.h"


int CTGGauge_BM_CoordRegister(void);
void CTGGauge_BM_ParamCheck(cGH*);
void CTGGauge_BM_MoLRegister(cGH*);
void CTGGauge_BM_SetSymmetries(cGH*);
void CTGGauge_SelectBoundaries_Lapse(cGH*);
void LapseBonaMasso(cGH*);

int
CTGGauge_BM_CoordRegister(void)
{
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("Registering slicings.");

  Einstein_RegisterSlicing("generic bona-masso");
  Einstein_RegisterSlicing("1+log");
  Einstein_RegisterSlicing("bm-harmonic");

  return 0;
}


void
CTGGauge_BM_ParamCheck(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("Processing Bona-Masso lapse parameters.");

  if (bm_f_A==NULL || bm_f_n==NULL)
    CCTK_WARN(0, "No storage for Bona-Masso lapse parameters.");
  
  if (CCTK_EQUALS(lapse_evolution_method, "1+log"))
    {
      CCTK_INFO("Using 1+log slicing:");
      CCTK_INFO("  *** Ignoring manual setting of (f_A,f_n) parameters. ***");
      CCTK_INFO("  dt_alpha = -2 alpha K + beta^i d_i alpha");
      (*bm_f_A) = 2.0;
      (*bm_f_n) = -1.0;
      
      if (!CCTK_EQUALS(dtlapse_evolution_method, "1+log"))
        CCTK_WARN(1, "You should set dtlapse_evolution method = 1+log in order to prevent unessecary syncs within ADMBase!");
    }
  else if (CCTK_EQUALS(lapse_evolution_method, "bm-harmonic"))
    {
      CCTK_INFO("Using harmonic form of the Bona-Masso slicing:");
      CCTK_INFO("  *** Ignoring manual setting of (f_A,f_n) parameters. ***");
      CCTK_INFO("  dt_alpha = -alpha^2 K + beta^i d_i alpha");
      (*bm_f_A) = 1.0;
      (*bm_f_n) = 0.0;
    }
  else if (CCTK_EQUALS(lapse_evolution_method, "generic bona-masso"))
    {
      CCTK_INFO("Using generic Bona-Masso slicing:");
      CCTK_INFO("  dt_alpha = -alpha^2 f(alpha) K + beta^i d_i alpha");
      CCTK_INFO("      f(alpha) = A alpha^n");
      CCTK_VInfo(CCTK_THORNSTRING, "      A = %g", (double) f_A);
      CCTK_VInfo(CCTK_THORNSTRING, "      A = %g", (double) f_n);
      (*bm_f_A) = f_A;
      (*bm_f_n) = f_n;
    }

  return;
}


void
CTGGauge_BM_MoLRegister(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr=0;

  CCTK_INFO("Registering Bona-Masso lapse variables with MoL.");

  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::alp"),
			     CCTK_VarIndex("ADMBase::dtalp"));

  if (ierr)
    CCTK_WARN(0,"Problems registering variables with MoL");

  return;
}


void
CTGGauge_BM_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;

  int sym[3];
  int ierr = 0;
 
  CCTK_INFO("Setting symmetries for Bona-masso lapse variables.");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::alp");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtalp");

  if (ierr)
    CCTK_WARN(0, "Couldn't set GF symmetries.");
    
  return;
}
  
void
CTGGauge_SelectBoundaries_Lapse(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_REAL v0;

  static int bc_parameters = -2;
  int ierr = 0;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Selecting boundary condition: %s", bc);


  if (CCTK_EQUALS(bc, "none") || CCTK_EQUALS(bc, "radiative"))
    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                      "ADMBase::lapse", "None");

  else if (CCTK_EQUALS(bc, "reflective"))
    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                      "ADMBase::lapse", "Flat");

  /* test */

  else if (CCTK_EQUALS(bc, "cactus-radiative"))
    {
      if (cctk_iteration==0)
        {
          if (bc_parameters == -2)
            {
              bc_parameters = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              v0 = CTGGauge_GetGaugeSpeed(cctkGH);
              ierr += Util_TableSetReal(bc_parameters, v0, "SPEED");
              ierr += Util_TableSetReal(bc_parameters, 1.0, "LIMIT");
            }

          ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                            "ADMBase::lapse", "None");
        }
      else
        {
          ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1,
                                            bc_parameters, "ADMBase::lapse",
                                            "Radiation");
        }
    }
  else
    ierr += 1;

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}


void
CTGGauge_ApplyRadiativeBC_BonaMasso(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  static int have_indices = 0;
  static CCTK_INT lapse_idx, dt_lapse_idx;
  static CCTK_REAL gauge_v0;
  int ierr=0;

  if (verbose)
    CCTK_INFO("Applying radiative bc to lapse source.");      

  if (! have_indices) {
    have_indices = 1;

    gauge_v0 = CTGGauge_GetGaugeSpeed(cctkGH);

    lapse_idx = CCTK_VarIndex("ADMBase::alp");
    dt_lapse_idx = CCTK_VarIndex("ADMBase::dtalp");
  }

  ierr += ApplyBC_Radiative(cctkGH, lapse_idx, dt_lapse_idx, gauge_v0, 1.0);

  if (ierr)
    CCTK_WARN(0, "Error applying radiative BC.");
}


void
LapseBonaMasso(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  DECLARE_EVOL_TYPE

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT * restrict imin[3], * restrict imin_plus[3], * restrict imin_minus[3];
  CCTK_INT * restrict imax[3], * restrict imax_plus[3], * restrict imax_minus[3];
  CCTK_REAL * restrict q[3], * restrict q_plus[3], * restrict q_minus[3];
  CCTK_REAL ihx, ihy, ihz;
  CCTK_REAL A, n;
  CCTK_INT is_cartesian;

  if (verbose)
    CCTK_INFO("Calculating lapse source.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  ihx = 1.0 / CCTK_DELTA_SPACE(0);
  ihy = 1.0 / CCTK_DELTA_SPACE(1);
  ihz = 1.0 / CCTK_DELTA_SPACE(2);

  /* 
   * Figure out if we are on a Cartesian patch.
   */
  if ( CCTK_IsFunctionAliased("MultiPatch_GetMap") &&
       CCTK_IsFunctionAliased("MultiPatch_MapIsCartesian") )
  {
    is_cartesian = MultiPatch_MapIsCartesian(MultiPatch_GetMap(cctkGH));
  } else {
    is_cartesian = !(*general_coordinates);
  }

  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q,
                  1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_plus,
                  (CCTK_POINTER_TO_CONST *) imax_plus,
                  (CCTK_POINTER_TO_CONST *) q_plus, 1, +(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_minus,
                  (CCTK_POINTER_TO_CONST *) imax_minus,
                  (CCTK_POINTER_TO_CONST *) q_minus, 1, -(*upwind_offset));

  A = (*bm_f_A);
  n = (*bm_f_n);

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGGauge_LapseBonaMasso,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

          CCTK_REAL alpha, adalpha1, adalpha2, adalpha3;
          CCTK_REAL bm_coeff;
          CCTK_REAL adv_alpha;

          CCTK_INT * restrict adv_imin[3];
          CCTK_INT * restrict adv_imax[3];
          CCTK_REAL * restrict adv_q[3];
          CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0,
            dbdz=0, dcdz=1;
          CCTK_REAL up_dir[3];

          /*
           * If necessary transform the shift vector to local coordinates.
           */
          if (is_cartesian)
          {
            up_dir[0] = betax[ijk];
            up_dir[1] = betay[ijk];
            up_dir[2] = betaz[ijk];
          }
          else
          {
            up_dir[0] = J11[ijk] * betax[ijk]
                      + J12[ijk] * betay[ijk]
                      + J13[ijk] * betaz[ijk];
            up_dir[1] = J21[ijk] * betax[ijk]
                      + J22[ijk] * betay[ijk]
                      + J23[ijk] * betaz[ijk];
            up_dir[2] = J31[ijk] * betax[ijk]
                      + J32[ijk] * betay[ijk]
                      + J33[ijk] * betaz[ijk];

            dadx = J11[ijk];
            dady = J12[ijk];
            dadz = J13[ijk];
            dbdx = J21[ijk];
            dbdy = J22[ijk];
            dbdz = J23[ijk];
            dcdx = J31[ijk];
            dcdy = J32[ijk];
            dcdz = J33[ijk];
          }

          alpha = alp[ijk];
          bm_coeff = A * pow(alpha, 2.0 + n);


	   /* Calculate required partial derivatives. */
          
          CTGBase_set_advective_stencil(adv_q[0], adv_imin[0], adv_imax[0],
                                        up_dir[0], q[0], imin[0], imax[0],
                                        q_minus[0], imin_minus[0],
                                        imax_minus[0], q_plus[0], imin_plus[0],
                                        imax_plus[0]);
          CTGBase_set_advective_stencil(adv_q[1], adv_imin[1], adv_imax[1],
                                        up_dir[1], q[1], imin[1], imax[1],
                                        q_minus[1], imin_minus[1],
                                        imax_minus[1], q_plus[1], imin_plus[1],
                                        imax_plus[1]);
          CTGBase_set_advective_stencil(adv_q[2], adv_imin[2], adv_imax[2],
                                        up_dir[2], q[2], imin[2], imax[2],
                                        q_minus[2], imin_minus[2],
                                        imax_minus[2], q_plus[2], imin_plus[2],
                                        imax_plus[2]);

          g_diff(cctkGH, alp, &adalpha1, &adalpha2, &adalpha3,
		 !is_cartesian,
                 dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
                 i, j, k, ni, nj, nk,
                 adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1],
                 adv_imin[2], adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
                 ihx, ihy, ihz);

          adv_alpha = betax[ijk]*adalpha1 + betay[ijk]*adalpha2 
            + betaz[ijk]*adalpha3;
          
          if (EVOL_TYPE == BSSN_TYPE) {
             dtalp[ijk] = -bm_coeff * K[ijk] + adv_alpha;
          } else if (EVOL_TYPE == Z4c_TYPE) {
             dtalp[ijk] = -bm_coeff * Khat[ijk] + adv_alpha;
          }
        }
  LC_ENDLOOP3 (CTGGauge_LapseBonaMasso);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin_minus, imax_minus, q_minus);
  CTGBase_free_stencil(imin_plus, imax_plus, q_plus);

  return;
}


void
CTGGauge_SelectRHSBoundaries_BonaMasso(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = 0;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Selecting boundary condition");

  ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                    "CTGGauge::lapse_source", "None");

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}



void
CTGGauge_Relax_LapseBonaMasso(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr;
  CCTK_REAL R;

  CCTK_INT norm2_handle = CCTK_ReductionHandle("norm2");
  if (norm2_handle < 0)
    CCTK_WARN(0, "Unable to get reduction handle.");

  CCTK_INT istart[3], iend[3];
  Util_GetGridRanges(cctkGH, istart, iend);

  for (int it=0; it<relax_lapse_iterations; ++it)
    {
      LapseBonaMasso(CCTK_PASS_CTOC);

#pragma omp parallel
      LC_LOOP3 (CTGGauge_Relax_LapseBonaMasso,
                i, j, k,
                istart[0], istart[1], istart[2],
                iend[0], iend[1], iend[2],
                cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
          alp[ijk] += dtalp[ijk]*cctk_delta_time;
        }
      LC_ENDLOOP3 (CTGGauge_Relax_LapseBonaMasso);

      ierr = CCTK_SyncGroup(cctkGH, "ADMBase::lapse");

      ierr = CCTK_Reduce(cctkGH, -1, norm2_handle, 1, CCTK_VARIABLE_REAL,
                         &R, 1, "ADMBase::dtalp");
      printf("  it=%d  R=%f\n", it, (float) R);
    }

  return;
}
