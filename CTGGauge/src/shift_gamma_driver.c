#include <math.h>
#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "util_Table.h"

#include "Boundary.h"
#include "Symmetry.h"

#include "GlobalDerivative.h"
#include "AllDerivative.h"
#include "AllDerivative_8th.h"
#include "Jacobian.h"
#include "ctgbase_util.h"

#include "loopcontrol.h"

#include "ctggauge.h"


void CTGGauge_GammaDriver_MoLRegister(cGH*);
void CTGGauge_GammaDriver_SetSymmetries(cGH*);
void CTGGauge_Init_etaE(cGH*);
void ShiftGammaDriver(cGH*);
void CTGGauge_SelectBoundaries_GammaDriver(cGH*);
void CTGGauge_ApplyRadiativeBC_GammaDriver(cGH*);
void CTGGauge_SelectRHSBoundaries_GammaDriver(cGH*);

void
CTGGauge_GammaDriver_ParamCheck(CCTK_ARGUMENTS)
{
   DECLARE_CCTK_PARAMETERS;

   if (!(CCTK_EQUALS(dtshift_evolution_method, "gamma-driver") || CCTK_EQUALS (dtshift_evolution_method, "integrated gamma-driver")))
     CCTK_WARN(1, "You should set dtshift_evolution method = gamma-driver or = integrated-gamma-driver in order to prevent unessecary syncs within ADMBase!");
}



void
CTGGauge_GammaDriver_MoLRegister(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr=0;

  CCTK_INFO("Registering Gamma-driver shift variables with MoL.");
  
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::betax"),
                             CCTK_VarIndex("ADMBase::dtbetax"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::betay"),
                             CCTK_VarIndex("ADMBase::dtbetay"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::betaz"),
                             CCTK_VarIndex("ADMBase::dtbetaz"));

  if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
     ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGGauge::B1"),
                                CCTK_VarIndex("CTGGauge::dt_B1"));
     ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGGauge::B2"),
                                CCTK_VarIndex("CTGGauge::dt_B2"));
     ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGGauge::B3"),
                                CCTK_VarIndex("CTGGauge::dt_B3"));
  }

  if (CCTK_EQUALS(damping_factor_method, "evolved")){
    ierr += MoLRegisterEvolved(CCTK_VarIndex("CTGGauge::etaE"),
			       CCTK_VarIndex("CTGGauge::dt_etaE"));}

  if (ierr) CCTK_WARN(0,"Problems registering variables with MoL");

  return;
}

void
CTGGauge_Init_etaE(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel
  LC_LOOP3 (CTGGauge_Init_etaE, i, j, k, 0, 0, 0,
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      etaE[CCTK_GFINDEX3D(cctkGH, i, j, k)] = 0.0;
    }
  LC_ENDLOOP3(CTGGauge_Init_etaE);

  return;
}

void
CTGGauge_GammaDriver_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int sym[3];
  int ierr=0;

  CCTK_INFO("Setting symmetries for Gamma-driver shift variables.");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = +1;

  if (CCTK_EQUALS(damping_factor_method, "prescribed") ||
      CCTK_EQUALS(damping_factor_method, "evolved")){
    ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::etaP");}

  if (CCTK_EQUALS(damping_factor_method, "evolved")){
    ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::etaE");
    ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::dt_etaE");}

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betax");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtbetax");
  if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
    ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::B1");
    ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::dt_B1");
  }

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betay");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtbetay");
  if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
     ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::B2");
     ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::dt_B2");
  }

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = -1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betaz");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtbetaz");
  if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
     ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::B3");
     ierr += SetCartSymVN(cctkGH, sym, "CTGGauge::dt_B3");
  }

  if (ierr)
    CCTK_WARN(0, "Couldn't set GF symmetries.");

  return;
}

void
CTGGauge_SelectBoundaries_GammaDriver(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  static int bc_parameters = -2;
  int ierr = 0;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Selecting boundary condition: %s", bc);


  if (CCTK_EQUALS(bc, "none") || CCTK_EQUALS(bc, "radiative"))
    {
      ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                        "ADMBase::shift", "None");
      if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver"))
        ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                          "CTGGauge::shift_aux", "None");
      if (CCTK_EQUALS(damping_factor_method, "evolved")){
	ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
					  "CTGGauge::shift_damp", "None");}
    }

  else if (CCTK_EQUALS(bc, "reflective"))
    {
      ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                        "ADMBase::shift", "Flat");
      if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver"))
        ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                          "CTGGauge::shift_aux", "Flat");
      if (CCTK_EQUALS(damping_factor_method, "evolved")){
	ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
					  "CTGGauge::shift_damp", "Flat");}
    }

  else if (CCTK_EQUALS(bc, "cactus-radiative"))
    {
      if (cctk_iteration==0)
        {
          if (bc_parameters == -2)
            {
              bc_parameters = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              ierr += Util_TableSetReal(bc_parameters, 1.0, "SPEED");
              ierr += Util_TableSetReal(bc_parameters, 0.0, "LIMIT");
            }
      
          ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                            "ADMBase::shift", "None");
          if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver"))
            ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                              "CTGGauge::shift_aux", "None");
          if (CCTK_EQUALS(damping_factor_method, "evolved")){
	    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
					      "CTGGauge::shift_damp", "None");}
        }
      else
        {
          ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1,
                                            bc_parameters, "ADMBase::shift",
                                            "None");
          if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver"))
            ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1,
                                              bc_parameters,
                                              "CTGGauge::shift_aux",
                                              "Radiation");
          if (CCTK_EQUALS(damping_factor_method, "evolved")){
	    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1,
					      bc_parameters,
					      "CTGGauge::shift_damp",
					      "Radiation");}
        }
    }

  else
    ierr += 1;

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}


void
CTGGauge_ApplyRadiativeBC_GammaDriver(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  static int have_indices = 0;
  static CCTK_INT beta1_idx, beta2_idx, beta3_idx;
  static CCTK_INT B1_idx, B2_idx, B3_idx, etaE_idx;
  static CCTK_INT dt_beta1_idx, dt_beta2_idx, dt_beta3_idx;
  static CCTK_INT dt_B1_idx, dt_B2_idx, dt_B3_idx, dt_etaE_idx;
  int ierr=0;

  if (verbose)
    CCTK_INFO("Applying radiative bc to shift sources.");
      
  if (! have_indices) {
    have_indices = 1;

    beta1_idx = CCTK_VarIndex("ADMBase::betax");
    beta2_idx = CCTK_VarIndex("ADMBase::betay");
    beta3_idx = CCTK_VarIndex("ADMBase::betaz");

    if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
       B1_idx = CCTK_VarIndex("CTGGauge::B1");
       B2_idx = CCTK_VarIndex("CTGGauge::B2");
       B3_idx = CCTK_VarIndex("CTGGauge::B3");
    }
    
    if (CCTK_EQUALS(damping_factor_method, "evolved")){
      etaE_idx = CCTK_VarIndex("CTGGauge::etaE");}

    dt_beta1_idx = CCTK_VarIndex("ADMBase::dtbetax");
    dt_beta2_idx = CCTK_VarIndex("ADMBase::dtbetay");
    dt_beta3_idx = CCTK_VarIndex("ADMBase::dtbetaz");
    
    if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
       dt_B1_idx = CCTK_VarIndex("CTGGauge::dt_B1");
       dt_B2_idx = CCTK_VarIndex("CTGGauge::dt_B2");
       dt_B3_idx = CCTK_VarIndex("CTGGauge::dt_B3");
    }

    if (CCTK_EQUALS(damping_factor_method, "evolved")){
      dt_etaE_idx = CCTK_VarIndex("CTGGauge::dt_etaE");}
  }


  ierr += ApplyBC_Radiative(cctkGH, beta1_idx, dt_beta1_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, beta2_idx, dt_beta2_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, beta3_idx, dt_beta3_idx, 1.0, 0.0);
  
  if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
     ierr += ApplyBC_Radiative(cctkGH, B1_idx, dt_B1_idx, 1.0, 0.0);
     ierr += ApplyBC_Radiative(cctkGH, B2_idx, dt_B2_idx, 1.0, 0.0);
     ierr += ApplyBC_Radiative(cctkGH, B3_idx, dt_B3_idx, 1.0, 0.0);
  }

  if (CCTK_EQUALS(damping_factor_method, "evolved")){
    ierr += ApplyBC_Radiative(cctkGH, etaE_idx, dt_etaE_idx, 1.0, 0.0);}

  if (ierr)
    CCTK_WARN(0, "Error applying radiative BC.");

  return;
}


void
CTGGauge_SelectRHSBoundaries_GammaDriver(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = 0;

  ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                    "ADMBase::dtshift", "None");
  if (CCTK_EQUALS (dtshift_evolution_method, "gamma-driver")) {
     ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                       "CTGGauge::shift_aux_source", "None");
  }
  if (CCTK_EQUALS(damping_factor_method, "evolved")){
    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
				      "CTGGauge::shift_damp_source", "None");}
  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}





void
ShiftGammaDriver(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT * restrict imin[3], * restrict imin_plus[3], * restrict imin_minus[3];
  CCTK_INT * restrict imax[3], * restrict imax_plus[3], * restrict imax_minus[3];
  CCTK_REAL * restrict q[3], * restrict q_plus[3], * restrict q_minus[3];
  CCTK_REAL ihx, ihy, ihz;

  CCTK_INT nr_avars;
  if (CCTK_EQUALS(damping_factor_method, "evolved"))
    {
      nr_avars = 10;
    }
  else
    {
      nr_avars = 9;
    }
  CCTK_REAL *avars[10];
  CCTK_INT is_cartesian;
  CCTK_INT bsize[6];

  avars[0] = betax;
  avars[1] = betay;
  avars[2] = betaz;
  avars[3] = Gamma1;
  avars[4] = Gamma2;
  avars[5] = Gamma3;
  avars[6] = B1;
  avars[7] = B2;
  avars[8] = B3;
  if (CCTK_EQUALS(damping_factor_method, "evolved")){
    avars[9] = etaE;}

  if (verbose)
    CCTK_INFO("Calculating shift source.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  ihx = 1.0 / CCTK_DELTA_SPACE(0);
  ihy = 1.0 / CCTK_DELTA_SPACE(1);
  ihz = 1.0 / CCTK_DELTA_SPACE(2);

  /* 
   * Figure out if we are on a Cartesian patch.
   */
  if ( CCTK_IsFunctionAliased("MultiPatch_GetMap") &&
       CCTK_IsFunctionAliased("MultiPatch_MapIsCartesian") )
    {
      is_cartesian = MultiPatch_MapIsCartesian(MultiPatch_GetMap(cctkGH));
    } else {
    is_cartesian = !(*general_coordinates);
  }

  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q, 1,
                  0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_plus,
                  (CCTK_POINTER_TO_CONST *) imax_plus,
                  (CCTK_POINTER_TO_CONST *) q_plus, 1, +(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_minus,
                  (CCTK_POINTER_TO_CONST *) imax_minus,
                  (CCTK_POINTER_TO_CONST *) q_minus, 1,
                  -(*upwind_offset));

  Util_GetGridRanges(cctkGH, istart, iend);

  GetBoundWidth(cctkGH, bsize, -1);

  CCTK_INT fd_order
    = *(CCTK_INT*) CCTK_ParameterGet ("order", "SummationByParts", NULL);

  int df_type = DF_TYPE_NONE;
  if (CCTK_EQUALS(damping_factor_type, "Mueller"))
    df_type = DF_TYPE_MUELLER;
  else if (CCTK_EQUALS(damping_factor_type, "Schnetter"))
    df_type = DF_TYPE_SCHNETTER;
  else if (CCTK_EQUALS(damping_factor_type, "Mueller*Schnetter"))
    df_type = DF_TYPE_MUELLER_SCHNETTER;
  else if (CCTK_EQUALS(damping_factor_type, "Schnetter-Simple"))
    df_type = DF_TYPE_SCHNETTER_SIMPLE;
  else
    CCTK_WARN(0, "Unknown damping_factor_type");

  int df_method = DF_METHOD_NONE;
  if (CCTK_EQUALS(damping_factor_method, "evolved"))
    df_method = DF_METHOD_EVOLVED;
  else if (CCTK_EQUALS(damping_factor_method, "constant"))
    df_method = DF_METHOD_CONSTANT;
  else if (CCTK_EQUALS(damping_factor_method, "prescribed"))
    df_method = DF_METHOD_PRESCRIBED;
  else
    CCTK_WARN(0, "Unknown damping_factor_method");
    
#pragma omp parallel
  LC_LOOP3 (CTGGauge_ShiftGammaDriver,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      CCTK_INT * restrict adv_imin[3];
      CCTK_INT * restrict adv_imax[3];
      CCTK_REAL * restrict adv_q[3];

      CCTK_REAL davars[10][3];
      CCTK_REAL xdavars[10][3];
      CCTK_REAL up_dir[3];

      CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0,
        dbdz=0, dcdz=1;

      CCTK_REAL beta1, beta2, beta3;
      CCTK_REAL adbeta11, adbeta12, adbeta13;
      CCTK_REAL adbeta21, adbeta22, adbeta23;
      CCTK_REAL adbeta31, adbeta32, adbeta33;
      CCTK_REAL adGamt11, adGamt12, adGamt13;
      CCTK_REAL adGamt21, adGamt22, adGamt23;
      CCTK_REAL adGamt31, adGamt32, adGamt33;
      CCTK_REAL adB11, adB12, adB13;
      CCTK_REAL adB21, adB22, adB23;
      CCTK_REAL adB31, adB32, adB33;

      CCTK_REAL adeta1, adeta2, adeta3;
      CCTK_REAL detg;
      CCTK_REAL igt11, igt12, igt13, igt22, igt23, igt33;
      CCTK_REAL gt11, gt12, gt13, gt22, gt23, gt33;
      CCTK_REAL dphi1, dphi2, dphi3;
      CCTK_REAL rc1x, rc1y, rc1z, rc2x, rc2y, rc2z;

      if (*general_coordinates)
	{
	  dadx = J11[ijk];
	  dady = J12[ijk];
	  dadz = J13[ijk];
	  dbdx = J21[ijk];
	  dbdy = J22[ijk];
	  dbdz = J23[ijk];
	  dcdx = J31[ijk];
	  dcdy = J32[ijk];
	  dcdz = J33[ijk];
	}

      beta1 = betax[ijk];
      beta2 = betay[ijk];
      beta3 = betaz[ijk];

      /*
       * If necessary transform the shift vector to local coordinates.
       */
      if (is_cartesian)
	{
	  up_dir[0] = betax[ijk];
	  up_dir[1] = betay[ijk];
	  up_dir[2] = betaz[ijk];
	}
      else
	{
	  up_dir[0] = J11[ijk] * betax[ijk]
	    + J12[ijk] * betay[ijk]
	    + J13[ijk] * betaz[ijk];
	  up_dir[1] = J21[ijk] * betax[ijk]
	    + J22[ijk] * betay[ijk]
	    + J23[ijk] * betaz[ijk];
	  up_dir[2] = J31[ijk] * betax[ijk]
	    + J32[ijk] * betay[ijk]
	    + J33[ijk] * betaz[ijk];
	}

      CTGBase_set_advective_stencil(adv_q[0], adv_imin[0], adv_imax[0],
                                    up_dir[0], q[0], imin[0], imax[0],
                                    q_minus[0], imin_minus[0],
                                    imax_minus[0], q_plus[0], imin_plus[0],
                                    imax_plus[0]);
      CTGBase_set_advective_stencil(adv_q[1], adv_imin[1], adv_imax[1],
                                    up_dir[1], q[1], imin[1], imax[1],
                                    q_minus[1], imin_minus[1],
                                    imax_minus[1], q_plus[1], imin_plus[1],
                                    imax_plus[1]);
      CTGBase_set_advective_stencil(adv_q[2], adv_imin[2], adv_imax[2],
                                    up_dir[2], q[2], imin[2], imax[2],
                                    q_minus[2], imin_minus[2],
                                    imax_minus[2], q_plus[2], imin_plus[2],
                                    imax_plus[2]);

      switch(fd_order) 
	{
	case 8: 
	  {
	    adv_diff_8(cctkGH, nr_avars, avars, davars, up_dir, i, j, k, ni,
		       nj, nk, adv_imin[0], adv_imax[0], adv_imin[1],
		       adv_imax[1], adv_imin[2], adv_imax[2], adv_q[0],
		       adv_q[1], adv_q[2], bsize, ihx, ihy, ihz);
	    break;
	  }
	default:
	  {
	    all_diff(cctkGH, nr_avars, avars, davars, i, j, k, ni, nj, nk,
		     adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1],
		     adv_imin[2], adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
		     ihx, ihy, ihz);
	  }
	}

      if (!is_cartesian)
	{
	  CCTK_REAL J[3][3];

	  J[0][0] = J11[ijk]; J[0][1] = J12[ijk]; J[0][2] = J13[ijk];
	  J[1][0] = J21[ijk]; J[1][1] = J22[ijk]; J[1][2] = J23[ijk];
	  J[2][0] = J31[ijk]; J[2][1] = J32[ijk]; J[2][2] = J33[ijk];

	  apply_jacobian(cctkGH, nr_avars, davars, xdavars, J);

	  for (int v=0; v<nr_avars; ++v)
	    for (int ii=0; ii<3; ++ii)
	      davars[v][ii] = xdavars[v][ii];
	}

      adbeta11 = davars[0][0]; adbeta12 = davars[0][1]; adbeta13 = davars[0][2];
      adbeta21 = davars[1][0]; adbeta22 = davars[1][1]; adbeta23 = davars[1][2];
      adbeta31 = davars[2][0]; adbeta32 = davars[2][1]; adbeta33 = davars[2][2];

      adGamt11 = davars[3][0]; adGamt12 = davars[3][1]; adGamt13 = davars[3][2];
      adGamt21 = davars[4][0]; adGamt22 = davars[4][1]; adGamt23 = davars[4][2];
      adGamt31 = davars[5][0]; adGamt32 = davars[5][1]; adGamt33 = davars[5][2];

      adB11 = davars[6][0]; adB12 = davars[6][1]; adB13 = davars[6][2];
      adB21 = davars[7][0]; adB22 = davars[7][1]; adB23 = davars[7][2];
      adB31 = davars[8][0]; adB32 = davars[8][1]; adB33 = davars[8][2];

      dtbetax[ijk] = vfactor * B1[ijk]
        + (beta1*adbeta11 + beta2*adbeta12 + beta3*adbeta13);
      dt_B1[ijk] = dt_Gamma1[ijk]
        + (beta1*(adB11 - adGamt11) + beta2*(adB12 - adGamt12) 
           + beta3*(adB13 - adGamt13));

      dtbetay[ijk] = vfactor * B2[ijk]
        + (beta1*adbeta21 + beta2*adbeta22 + beta3*adbeta23);
      dt_B2[ijk] = dt_Gamma2[ijk]
        + (beta1*(adB21 - adGamt21) + beta2*(adB22 - adGamt22) 
           + beta3*(adB23 - adGamt23));

      dtbetaz[ijk] = vfactor * B3[ijk]
        + (beta1*adbeta31 + beta2*adbeta32 + beta3*adbeta33);
      dt_B3[ijk] = dt_Gamma3[ijk]
        + (beta1*(adB31 - adGamt31) + beta2*(adB32 - adGamt32) 
           + beta3*(adB33 - adGamt33));

      if (df_method == DF_METHOD_CONSTANT)
	{
	  dt_B1[ijk] -= eta * B1[ijk];
	  dt_B2[ijk] -= eta * B2[ijk];
	  dt_B3[ijk] -= eta * B3[ijk];
	}
      else
	{
	  CCTK_REAL etaM = 0.0;
	  CCTK_REAL etaS = 0.0;

	  gt11 = gamma11[ijk];
	  gt12 = gamma12[ijk];
	  gt13 = gamma13[ijk];
	  gt22 = gamma22[ijk];
	  gt23 = gamma23[ijk];
	  gt33 = gamma33[ijk];

	  CTGBase_invert_metric(gt11, gt12, gt13, gt22, gt23, gt33, detg,
				igt11, igt12, igt13, igt22, igt23, igt33);

	  g_diff(cctkGH, phi, &dphi1, &dphi2, &dphi3,
		 *general_coordinates,
		 dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
		 i, j, k, ni, nj, nk, imin[0], imax[0], imin[1], imax[1],
		 imin[2], imax[2], q[0], q[1], q[2], ihx, ihy, ihz);
 
	  /* 
	   * Damping factor proposed by Mueller in arXiv:0912.3125, page 4, eq.4
	   */
	  if ((df_type == DF_TYPE_MUELLER) || 
	      (df_type == DF_TYPE_MUELLER_SCHNETTER))
	    etaM = 1.31241 * pow((igt11*dphi1*dphi1 + 2*igt12*dphi1*dphi2 
				  + 2*igt13*dphi1*dphi3 + 2*igt23*dphi2*dphi3 
				  + igt22*dphi2*dphi2 + igt33*dphi3*dphi3), 0.5)
	      / (pow((1.0 - phi[ijk]), 2.0));

	  /* 
	   * Damping factor proposed by Schnetter in
	   * arXiv:1003.0859, page 2, eq.11
	   */
	  if ((df_type == DF_TYPE_SCHNETTER) || 
	      (df_type == DF_TYPE_MUELLER_SCHNETTER))
	    {
	      CCTK_REAL r1sq, r2sq;

	      if (damping_factor_num_centres == 2)
		{
		  /* 
		   * Center of the first black hole 
		   */
		  rc1x = sf_centroid_x[gauge_surface_0];
		  rc1y = sf_centroid_y[gauge_surface_0];
		  rc1z = sf_centroid_z[gauge_surface_0];

		  /* 
		   * Center of the second black hole 
		   */
		  rc2x = sf_centroid_x[gauge_surface_1];
		  rc2y = sf_centroid_y[gauge_surface_1];
		  rc2z = sf_centroid_z[gauge_surface_1];

		  /* 
		   * Position of the first black hole (r1sq = r1^2)
		   */
		  r1sq = (x[ijk] - rc1x)*(x[ijk] - rc1x) + 
		    (y[ijk] - rc1y)*(y[ijk] - rc1y) + 
		    (z[ijk] - rc1z)*(z[ijk] - rc1z);

		  /* 
		   * Position of the second black hole (r2sq = r2^2)
		   */
		  r2sq = (x[ijk] - rc2x)*(x[ijk] - rc2x) + 
		    (y[ijk] - rc2y)*(y[ijk] - rc2y) + 
		    (z[ijk] - rc2z)*(z[ijk] - rc2z);
		}
	      else if (damping_factor_num_centres == 1)
		{
		  r1sq = r[ijk]*r[ijk];
		  r2sq = 0.0;
		}
	      else
		CCTK_WARN(0, "You need to set the number of black holes.");

	      etaS = eta*Rct*Rct/(r1sq + r2sq + Rct*Rct);
	    }

	  if (df_type == DF_TYPE_MUELLER)
	    etaP[ijk] = etaM;
	  else if (df_type == DF_TYPE_SCHNETTER)
	    etaP[ijk] = etaS;
	  else if (df_type == DF_TYPE_MUELLER_SCHNETTER)
	    etaP[ijk] = etaM*etaS;
	  else if (df_type == DF_TYPE_SCHNETTER_SIMPLE)
	    etaP[ijk] = eta_damping_radius > 0 
	      ? (r[ijk] >= eta_damping_radius ? eta*eta_damping_radius/r[ijk] 
		 : eta) : eta;;

	  if (df_method == DF_METHOD_PRESCRIBED)
	    {
	      /* 
	       * Use a non-constant prescribed damping parameter 
	       */
	      dt_B1[ijk] -= etaP[ijk] * B1[ijk];
	      dt_B2[ijk] -= etaP[ijk] * B2[ijk];
	      dt_B3[ijk] -= etaP[ijk] * B3[ijk];
	    }
	  else if (df_method == DF_METHOD_EVOLVED)
	    {
	      /* 
	       * Use a prescribed damping parameter as a source for an evolved
	       * damping parameter. Damping factor proposed in
               * arXiv:1008.2212, page 4, eq.9
	       */
	      adeta1 = davars[9][0];
	      adeta2 = davars[9][1];
	      adeta3 = davars[9][2];

	      dt_etaE[ijk] = (beta1*adeta1 + beta2*adeta2 + beta3*adeta3) + 
		(etaP[ijk] - etaE[ijk]) / Mt;

	      dt_B1[ijk] -= etaE[ijk] * B1[ijk];
	      dt_B2[ijk] -= etaE[ijk] * B2[ijk];
	      dt_B3[ijk] -= etaE[ijk] * B3[ijk];
	    }
	}
    }

  LC_ENDLOOP3 (CTGGauge_ShiftGammaDriver);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin_minus, imax_minus, q_minus);
  CTGBase_free_stencil(imin_plus, imax_plus, q_plus);

  return;
}


void
IntegratedShiftGammaDriver (CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT *restrict imin[3], *restrict imin_plus[3], *restrict imin_minus[3];
  CCTK_INT *restrict imax[3], *restrict imax_plus[3], *restrict imax_minus[3];
  CCTK_REAL *restrict q[3], *restrict q_plus[3], *restrict q_minus[3];
  CCTK_REAL ihx, ihy, ihz;

  CCTK_INT nr_avars = 3;

  if (CCTK_EQUALS (damping_factor_method, "evolved"))
    {
      nr_avars++;
    }
  CCTK_REAL *avars[10];
  CCTK_INT is_cartesian;
  CCTK_INT bsize[6];

  avars[0] = betax;
  avars[1] = betay;
  avars[2] = betaz;
  if (CCTK_EQUALS (damping_factor_method, "evolved"))
    {
      avars[3] = etaE;
    }

  if (verbose)
    CCTK_INFO ("Calculating shift source.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  ihx = 1.0 / CCTK_DELTA_SPACE (0);
  ihy = 1.0 / CCTK_DELTA_SPACE (1);
  ihz = 1.0 / CCTK_DELTA_SPACE (2);

  /* 
   * Figure out if we are on a Cartesian patch.
   */
  if (CCTK_IsFunctionAliased ("MultiPatch_GetMap") &&
      CCTK_IsFunctionAliased ("MultiPatch_MapIsCartesian"))
    {
      is_cartesian = MultiPatch_MapIsCartesian (MultiPatch_GetMap (cctkGH));
    }
  else
    {
      is_cartesian = !(*general_coordinates);
    }

  Util_SetStencil (cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                   (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q,
                   1, 0);
  Util_SetStencil (cctkGH, (CCTK_POINTER_TO_CONST *) imin_plus,
                   (CCTK_POINTER_TO_CONST *) imax_plus,
                   (CCTK_POINTER_TO_CONST *) q_plus, 1, +(*upwind_offset));
  Util_SetStencil (cctkGH, (CCTK_POINTER_TO_CONST *) imin_minus,
                   (CCTK_POINTER_TO_CONST *) imax_minus,
                   (CCTK_POINTER_TO_CONST *) q_minus, 1, -(*upwind_offset));

  Util_GetGridRanges (cctkGH, istart, iend);

  GetBoundWidth (cctkGH, bsize, -1);

  CCTK_INT fd_order
    = *(CCTK_INT *) CCTK_ParameterGet ("order", "SummationByParts", NULL);

  int df_type = DF_TYPE_NONE;
  if (CCTK_EQUALS (damping_factor_type, "Mueller"))
    df_type = DF_TYPE_MUELLER;
  else if (CCTK_EQUALS (damping_factor_type, "Schnetter"))
    df_type = DF_TYPE_SCHNETTER;
  else if (CCTK_EQUALS (damping_factor_type, "Mueller*Schnetter"))
    df_type = DF_TYPE_MUELLER_SCHNETTER;
  else if (CCTK_EQUALS (damping_factor_type, "Schnetter-Simple"))
    df_type = DF_TYPE_SCHNETTER_SIMPLE;
  else
    CCTK_WARN (0, "Unknown damping_factor_type");

  int df_method = DF_METHOD_NONE;
  if (CCTK_EQUALS (damping_factor_method, "evolved"))
    df_method = DF_METHOD_EVOLVED;
  else if (CCTK_EQUALS (damping_factor_method, "constant"))
    df_method = DF_METHOD_CONSTANT;
  else if (CCTK_EQUALS (damping_factor_method, "prescribed"))
    df_method = DF_METHOD_PRESCRIBED;
  else
    CCTK_WARN (0, "Unknown damping_factor_method");


// Main loop
#pragma omp parallel
  LC_LOOP3 (CTGGauge_ShiftGammaDriver,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2], cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
  {
    int const ijk = CCTK_GFINDEX3D (cctkGH, i, j, k);

    CCTK_INT *restrict adv_imin[3];
    CCTK_INT *restrict adv_imax[3];
    CCTK_REAL *restrict adv_q[3];

    CCTK_REAL davars[10][3];
    CCTK_REAL xdavars[10][3];
    CCTK_REAL up_dir[3];

    CCTK_REAL dadx = 1, dbdx = 0, dcdx = 0, dady = 0, dbdy = 1, dcdy =
      0, dadz = 0, dbdz = 0, dcdz = 1;

    CCTK_REAL beta1, beta2, beta3;
    CCTK_REAL adbeta11, adbeta12, adbeta13;
    CCTK_REAL adbeta21, adbeta22, adbeta23;
    CCTK_REAL adbeta31, adbeta32, adbeta33;

    CCTK_REAL adeta1, adeta2, adeta3;
    CCTK_REAL detg;
    CCTK_REAL igt11, igt12, igt13, igt22, igt23, igt33;
    CCTK_REAL gt11, gt12, gt13, gt22, gt23, gt33;
    CCTK_REAL dphi1, dphi2, dphi3;
    CCTK_REAL rc1x, rc1y, rc1z, rc2x, rc2y, rc2z;

    if (*general_coordinates)
      {
        dadx = J11[ijk];
        dady = J12[ijk];
        dadz = J13[ijk];
        dbdx = J21[ijk];
        dbdy = J22[ijk];
        dbdz = J23[ijk];
        dcdx = J31[ijk];
        dcdy = J32[ijk];
        dcdz = J33[ijk];
      }

    beta1 = betax[ijk];
    beta2 = betay[ijk];
    beta3 = betaz[ijk];

    /*
     * If necessary transform the shift vector to local coordinates.
     */
    if (is_cartesian)
      {
        up_dir[0] = betax[ijk];
        up_dir[1] = betay[ijk];
        up_dir[2] = betaz[ijk];
      }
    else
      {
        up_dir[0] = J11[ijk] * betax[ijk]
          + J12[ijk] * betay[ijk] + J13[ijk] * betaz[ijk];
        up_dir[1] = J21[ijk] * betax[ijk]
          + J22[ijk] * betay[ijk] + J23[ijk] * betaz[ijk];
        up_dir[2] = J31[ijk] * betax[ijk]
          + J32[ijk] * betay[ijk] + J33[ijk] * betaz[ijk];
      }

    CTGBase_set_advective_stencil (adv_q[0], adv_imin[0], adv_imax[0],
                                   up_dir[0], q[0], imin[0], imax[0],
                                   q_minus[0], imin_minus[0],
                                   imax_minus[0], q_plus[0],
                                   imin_plus[0], imax_plus[0]);
    CTGBase_set_advective_stencil (adv_q[1], adv_imin[1], adv_imax[1],
                                   up_dir[1], q[1], imin[1], imax[1],
                                   q_minus[1], imin_minus[1],
                                   imax_minus[1], q_plus[1],
                                   imin_plus[1], imax_plus[1]);
    CTGBase_set_advective_stencil (adv_q[2], adv_imin[2], adv_imax[2],
                                   up_dir[2], q[2], imin[2], imax[2],
                                   q_minus[2], imin_minus[2],
                                   imax_minus[2], q_plus[2],
                                   imin_plus[2], imax_plus[2]);

    switch (fd_order)
      {
      case 8:
        {
          adv_diff_8 (cctkGH, nr_avars, avars, davars, up_dir, i, j,
                      k, ni, nj, nk, adv_imin[0], adv_imax[0],
                      adv_imin[1], adv_imax[1], adv_imin[2],
                      adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
                      bsize, ihx, ihy, ihz);
          break;
        }
      default:
        {
          all_diff (cctkGH, nr_avars, avars, davars, i, j, k, ni, nj,
                    nk, adv_imin[0], adv_imax[0], adv_imin[1],
                    adv_imax[1], adv_imin[2], adv_imax[2], adv_q[0],
                    adv_q[1], adv_q[2], ihx, ihy, ihz);
        }
      }

    if (!is_cartesian)
      {
        CCTK_REAL J[3][3];

        J[0][0] = J11[ijk];
        J[0][1] = J12[ijk];
        J[0][2] = J13[ijk];
        J[1][0] = J21[ijk];
        J[1][1] = J22[ijk];
        J[1][2] = J23[ijk];
        J[2][0] = J31[ijk];
        J[2][1] = J32[ijk];
        J[2][2] = J33[ijk];

        apply_jacobian (cctkGH, nr_avars, davars, xdavars, J);

        for (int v = 0; v < nr_avars; ++v)
          for (int ii = 0; ii < 3; ++ii)
            davars[v][ii] = xdavars[v][ii];
      }

    adbeta11 = davars[0][0];
    adbeta12 = davars[0][1];
    adbeta13 = davars[0][2];
    adbeta21 = davars[1][0];
    adbeta22 = davars[1][1];
    adbeta23 = davars[1][2];
    adbeta31 = davars[2][0];
    adbeta32 = davars[2][1];
    adbeta33 = davars[2][2];


    dtbetax[ijk] = vfactor * Gamma1[ijk]
      + (beta1 * adbeta11 + beta2 * adbeta12 + beta3 * adbeta13);

    dtbetay[ijk] = vfactor * Gamma2[ijk]
      + (beta1 * adbeta21 + beta2 * adbeta22 + beta3 * adbeta23);

    dtbetaz[ijk] = vfactor * Gamma3[ijk]
      + (beta1 * adbeta31 + beta2 * adbeta32 + beta3 * adbeta33);


    if (df_method == DF_METHOD_CONSTANT)
      {
        dtbetax[ijk] -= eta * betax[ijk];
        dtbetay[ijk] -= eta * betay[ijk];
        dtbetaz[ijk] -= eta * betaz[ijk];
      }
    else
      {
        CCTK_REAL etaM = 0.0;
        CCTK_REAL etaS = 0.0;

        gt11 = gamma11[ijk];
        gt12 = gamma12[ijk];
        gt13 = gamma13[ijk];
        gt22 = gamma22[ijk];
        gt23 = gamma23[ijk];
        gt33 = gamma33[ijk];

        CTGBase_invert_metric (gt11, gt12, gt13, gt22, gt23, gt33, detg,
                               igt11, igt12, igt13, igt22, igt23, igt33);

        g_diff (cctkGH, phi, &dphi1, &dphi2, &dphi3,
                *general_coordinates,
                dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
                i, j, k, ni, nj, nk, imin[0], imax[0], imin[1], imax[1],
                imin[2], imax[2], q[0], q[1], q[2], ihx, ihy, ihz);

        /* 
         * Damping factor proposed by Mueller in arXiv:0912.3125, page 4, eq.4
         */
        if ((df_type == DF_TYPE_MUELLER) ||
            (df_type == DF_TYPE_MUELLER_SCHNETTER))
          etaM =
            1.31241 *
            pow ((igt11 * dphi1 * dphi1 +
                  2 * igt12 * dphi1 * dphi2 +
                  2 * igt13 * dphi1 * dphi3 +
                  2 * igt23 * dphi2 * dphi3 +
                  igt22 * dphi2 * dphi2 + igt33 * dphi3 * dphi3),
                 0.5) / (pow ((1.0 - phi[ijk]), 2.0));

        /* 
         * Damping factor proposed by Schnetter in
         * arXiv:1003.0859, page 2, eq.11
         */
        if ((df_type == DF_TYPE_SCHNETTER) ||
            (df_type == DF_TYPE_MUELLER_SCHNETTER))
          {
            CCTK_REAL r1sq, r2sq;

            if (damping_factor_num_centres == 2)
              {
                /* 
                 * Center of the first black hole 
                 */
                rc1x = sf_centroid_x[gauge_surface_0];
                rc1y = sf_centroid_y[gauge_surface_0];
                rc1z = sf_centroid_z[gauge_surface_0];

                /* 
                 * Center of the second black hole 
                 */
                rc2x = sf_centroid_x[gauge_surface_1];
                rc2y = sf_centroid_y[gauge_surface_1];
                rc2z = sf_centroid_z[gauge_surface_1];

                /* 
                 * Position of the first black hole (r1sq = r1^2)
                 */
                r1sq = (x[ijk] - rc1x) * (x[ijk] - rc1x) +
                  (y[ijk] - rc1y) * (y[ijk] - rc1y) +
                  (z[ijk] - rc1z) * (z[ijk] - rc1z);

                /* 
                 * Position of the second black hole (r2sq = r2^2)
                 */
                r2sq = (x[ijk] - rc2x) * (x[ijk] - rc2x) +
                  (y[ijk] - rc2y) * (y[ijk] - rc2y) +
                  (z[ijk] - rc2z) * (z[ijk] - rc2z);
              }
            else if (damping_factor_num_centres == 1)
              {
                r1sq = r[ijk] * r[ijk];
                r2sq = 0.0;
              }
            else
              CCTK_WARN (0, "You need to set the number of black holes.");

            etaS = eta * Rct * Rct / (r1sq + r2sq + Rct * Rct);
          }

        if (df_type == DF_TYPE_MUELLER)
          etaP[ijk] = etaM;
        else if (df_type == DF_TYPE_SCHNETTER)
          etaP[ijk] = etaS;
        else if (df_type == DF_TYPE_MUELLER_SCHNETTER)
          etaP[ijk] = etaM * etaS;
        else if (df_type == DF_TYPE_SCHNETTER_SIMPLE)
          etaP[ijk] = eta_damping_radius > 0
            ? (r[ijk] >=
               eta_damping_radius ? eta * eta_damping_radius /
               r[ijk] : eta) : eta;;

        if (df_method == DF_METHOD_PRESCRIBED)
          {
            /* 
             * Use a non-constant prescribed damping parameter 
             */
            dtbetax[ijk] -= etaP[ijk] * betax[ijk];
            dtbetay[ijk] -= etaP[ijk] * betay[ijk];
            dtbetaz[ijk] -= etaP[ijk] * betaz[ijk];
          }
        else if (df_method == DF_METHOD_EVOLVED)
          {
            /* 
             * Use a prescribed damping parameter as a source for an evolved
             * damping parameter. Damping factor proposed in
             * arXiv:1008.2212, page 4, eq.9
             */
            adeta1 = davars[9][0];
            adeta2 = davars[9][1];
            adeta3 = davars[9][2];

            dt_etaE[ijk] =
              (beta1 * adeta1 + beta2 * adeta2 + beta3 * adeta3) +
              (etaP[ijk] - etaE[ijk]) / Mt;

            dtbetax[ijk] -= etaE[ijk] * betax[ijk];
            dtbetay[ijk] -= etaE[ijk] * betay[ijk];
            dtbetaz[ijk] -= etaE[ijk] * betaz[ijk];
          }
      }
  }

  LC_ENDLOOP3 (CTGGauge_ShiftGammaDriver);

  CTGBase_free_stencil (imin, imax, q);
  CTGBase_free_stencil (imin_minus, imax_minus, q_minus);
  CTGBase_free_stencil (imin_plus, imax_plus, q_plus);

  return;
}

