#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "loopcontrol.h"

void
CTGGauge_Convert_ADM_to_ShiftGammaDriver(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

#pragma omp parallel
  LC_LOOP3 (CTGGauge_Convert_ADM_to_ShiftGamma,
            i, j, k,
	    0, 0, 0,
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
        {
          CCTK_INT ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

          B1[ijk] = 0;
          B2[ijk] = 0;
          B3[ijk] = 0;
        }
  LC_ENDLOOP3 (CTGGauge_Convert_ADM_to_ShiftGamma);

  return;
}

