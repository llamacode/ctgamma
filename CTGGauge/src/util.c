#include <math.h>
#include <assert.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"
#include "util_Table.h"

CCTK_REAL
CTGGauge_GetGaugeSpeed(CCTK_POINTER_TO_CONST _cctkGH)
{
  const cGH* const cctkGH = (const cGH*) _cctkGH;  
  DECLARE_CCTK_PARAMETERS;
  DECLARE_CCTK_ARGUMENTS;

  if (CCTK_EQUALS(lapse_evolution_method, "generic bona-masso") ||
      CCTK_EQUALS(lapse_evolution_method, "bm-harmonic") ||
      CCTK_EQUALS(lapse_evolution_method, "1+log"))
    return sqrt(*bm_f_A);

  return 1.0;
}

void
CTGGauge_SetDtLapseStorageFlag(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *dtlapse_state = 1;

  return;
}

void
CTGGauge_SetDtShiftStorageFlag(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  *dtshift_state = 1;

  return;
}

int
CTGGauge_TurnOffDTProlongation(void)
{
  int ierr;

  int const lapse_gi = CCTK_GroupIndex("ADMBase::dtlapse");
  if (lapse_gi >= 0)
    {
      int const lapse_tags = CCTK_GroupTagsTableI(lapse_gi);
      ierr = Util_TableDeleteKey(lapse_tags, "ProlongationParameter");
      ierr = Util_TableSetString(lapse_tags, "none", "Prolongation");
    }

  int const shift_gi = CCTK_GroupIndex("ADMBase::dtshift");
  if (shift_gi >= 0)
    {
      int const shift_tags = CCTK_GroupTagsTableI(shift_gi);
      ierr = Util_TableDeleteKey(shift_tags, "ProlongationParameter");
      ierr = Util_TableSetString(shift_tags, "none", "Prolongation");
    }

  return 0;
}


