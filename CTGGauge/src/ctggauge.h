#ifndef CTGGAUGE_CTGGAUGE_H
#define CTGGAUGE_CTGGAUGE_H

#define DF_TYPE_NONE                   -1
#define DF_TYPE_MUELLER                 0
#define DF_TYPE_SCHNETTER               1
#define DF_TYPE_MUELLER_SCHNETTER       2
#define DF_TYPE_SCHNETTER_SIMPLE        3

#define DF_METHOD_NONE                 -1
#define DF_METHOD_EVOLVED               0
#define DF_METHOD_CONSTANT              1
#define DF_METHOD_PRESCRIBED            2

#endif
