#include <math.h>
#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "util_Table.h"

#include "Slicing.h"
#include "Symmetry.h"

int CTGGauge_lapse_fixed_CoordRegister(void);
void CTGGauge_lapse_fixed_SetSymmetries(cGH*);

int
CTGGauge_lapse_fixed_CoordRegister(void)
{
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("Registering slicings.");

  Einstein_RegisterSlicing("fixed");

  return 0;
}
