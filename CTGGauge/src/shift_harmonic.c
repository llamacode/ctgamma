/*
 * shift_harmonic.c
 *
 * This file contains an implementation of the harmonic shift condition,
 * as specified in Eq (2.31) of:
 *   Friedrich, Rendall (2000) [gr-qc/0002074]
 *
 * In the actual implementation below, we make note of the fact that
 *   (1)  d log(alpha) = dalpha/alpha
 * and
 *   (2)  Gamma^k = gamma^ij Gamma^k_ij
 *                = e^-4phi (Gamma~^k - 2 gamma~^kl phi,l)
 * to express the evolution equation in terms of CTGamma variables.
 *
 * The resulting evolution equation is:
 *
 *   dt beta^a + beta^b beta^a,b 
 *     = alpha^2 e^-4phi (Gamma~^b - 2 gamma~^kl phi,l)
 *       - alpha gamma~^ab alpha,b
 */
#include <math.h>
#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "util_Table.h"

#include "Boundary.h"
#include "Symmetry.h"

#include "GlobalDerivative.h"
#include "ctgbase_util.h"

#include "loopcontrol.h"

void CTGGauge_ShiftHarmonic_MoLRegister(cGH*);
void CTGGauge_ShiftHarmonic_SetSymmetries(cGH*);
void ShiftHarmonic(cGH*);

void
CTGGauge_ShiftHarmonic_MoLRegister(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr=0;

  CCTK_INFO("Registering harmonic shift variables with MoL.");
  
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::betax"),
                             CCTK_VarIndex("ADMBase::dtbetax"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::betay"),
                             CCTK_VarIndex("ADMBase::dtbetay"));
  ierr += MoLRegisterEvolved(CCTK_VarIndex("ADMBase::betaz"),
                             CCTK_VarIndex("ADMBase::dtbetaz"));

  if (ierr) CCTK_WARN(0,"Problems registering variables with MoL");

  return;
}


void
CTGGauge_ShiftHarmonic_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int sym[3];
  int ierr=0;

  CCTK_INFO("Setting symmetries for harmonic shift variables.");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betax");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtbetax");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betay");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtbetay");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = -1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betaz");
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::dtbetaz");

  if (ierr)
    CCTK_WARN(0, "Couldn't set GF symmetries.");

  return;
}

void
CTGGauge_SelectBoundaries_ShiftHarmonic(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  static int bc_parameters = -2;
  int ierr = 0;

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Applying boundary condition: %s", bc);


  if (CCTK_EQUALS(bc, "none") || CCTK_EQUALS(bc, "radiative"))
    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                      "ADMBase::shift", "None");

  else if (CCTK_EQUALS(bc, "reflective"))
    ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                      "ADMBase::shift", "Flat");

  else if (CCTK_EQUALS(bc, "cactus-radiative"))
    {
      if (cctk_iteration==0)
        {
          if (bc_parameters == -2)
            {
              bc_parameters = Util_TableCreate(UTIL_TABLE_FLAGS_DEFAULT);
              ierr += Util_TableSetReal(bc_parameters, 1.0, "SPEED");
              ierr += Util_TableSetReal(bc_parameters, 0.0, "LIMIT");
            }
      
          ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                            "ADMBase::shift", "None");
        }
      else
        {
          ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1,
                                            bc_parameters, "ADMBase::shift",
                                            "None");
        }
    }

  else
    ierr += 1;

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}


void
CTGGauge_ApplyRadiativeBC_ShiftHarmonic(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  static CCTK_INT beta1_idx, beta2_idx, beta3_idx;
  static CCTK_INT dt_beta1_idx, dt_beta2_idx, dt_beta3_idx;
  int ierr=0;

  if (verbose)
    CCTK_INFO("Applying radiative bc to shift sources.");
      
  beta1_idx = CCTK_VarIndex("ADMBase::betax");
  beta2_idx = CCTK_VarIndex("ADMBase::betay");
  beta3_idx = CCTK_VarIndex("ADMBase::betaz");
	  
  dt_beta1_idx = CCTK_VarIndex("ADMBase::dtbetax");
  dt_beta2_idx = CCTK_VarIndex("ADMBase::dtbetay");
  dt_beta3_idx = CCTK_VarIndex("ADMBase::dtbetaz");
	  
  ierr += ApplyBC_Radiative(cctkGH, beta1_idx, dt_beta1_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, beta2_idx, dt_beta2_idx, 1.0, 0.0);
  ierr += ApplyBC_Radiative(cctkGH, beta3_idx, dt_beta3_idx, 1.0, 0.0);

  if (ierr)
    CCTK_WARN(0, "Error applying radiative BC.");

  return;
}


void
CTGGauge_SelectRHSBoundaries_ShiftHarmonic(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ierr = 0;

  ierr += Boundary_SelectGroupForBC(cctkGH, CCTK_ALL_FACES, 1, -1,
                                    "ADMBase::dtshift", "None");

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");

  return;
}


void
ShiftHarmonic(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ni, nj, nk;

  CCTK_INT istart[3], iend[3];

  CCTK_INT * restrict imin[3], * restrict imin_plus[3], * restrict imin_minus[3];
  CCTK_INT * restrict imax[3], * restrict imax_plus[3], * restrict imax_minus[3];
  CCTK_REAL * restrict q[3], * restrict q_plus[3], * restrict q_minus[3];
  CCTK_REAL up_dir[3];
  CCTK_INT is_cartesian;
  CCTK_REAL ihx, ihy, ihz;
  int use_phi = CCTK_EQUALS(conformal_factor_type, "phi");
  CCTK_REAL N = *detg_exponent;

  if (verbose)
    CCTK_INFO("Calculating shift source.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  ihx = 1.0 / CCTK_DELTA_SPACE(0);
  ihy = 1.0 / CCTK_DELTA_SPACE(1);
  ihz = 1.0 / CCTK_DELTA_SPACE(2);

/*
  * Figure out if we are on a Cartesian patch.
  */
 if ( CCTK_IsFunctionAliased("MultiPatch_GetMap") &&
      CCTK_IsFunctionAliased("MultiPatch_MapIsCartesian") )
 {
   is_cartesian = MultiPatch_MapIsCartesian(MultiPatch_GetMap(cctkGH));
 } else {
   is_cartesian = !(*general_coordinates);
 }

  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax,
                  (CCTK_POINTER_TO_CONST *) q, 1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_plus,
                 (CCTK_POINTER_TO_CONST *) imax_plus,
                 (CCTK_POINTER_TO_CONST *) q_plus, 1, +(*upwind_offset));
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin_minus,
                  (CCTK_POINTER_TO_CONST *) imax_minus,
                  (CCTK_POINTER_TO_CONST *) q_minus, 1, -(*upwind_offset));

  Util_GetGridRanges(cctkGH, istart, iend);

#pragma omp parallel
  LC_LOOP3 (CTGGauge_ShiftHarmonic_rhs,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      CCTK_INT * restrict adv_imin[3];
      CCTK_INT * restrict adv_imax[3];
      CCTK_REAL * restrict adv_q[3];
      CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0,
        dbdz=0, dcdz=1;

      CCTK_REAL beta1, beta2, beta3;
      CCTK_REAL dalp1, dalp2, dalp3;
      CCTK_REAL dphi1, dphi2, dphi3;
      CCTK_REAL adbeta11, adbeta12, adbeta13;
      CCTK_REAL adbeta21, adbeta22, adbeta23;
      CCTK_REAL adbeta31, adbeta32, adbeta33;
      CCTK_REAL a2, a_factor_1, a_factor_2, a_factor_3, a_coeff;
      CCTK_REAL gamma1, gamma2, gamma3;
      CCTK_REAL em4phi;
      CCTK_REAL detg, ig11, ig12, ig13, ig22, ig23, ig33;

      beta1 = betax[ijk];
      beta2 = betay[ijk];
      beta3 = betaz[ijk];

           if (is_cartesian) 
             { 
              up_dir[0] = betax[ijk];
              up_dir[1] = betay[ijk];
              up_dir[2] = betaz[ijk];
             } 
           else 
             { 
              up_dir[0] = J11[ijk] * betax[ijk] 
                        + J12[ijk] * betay[ijk] 
                        + J13[ijk] * betaz[ijk]; 
              up_dir[1] = J21[ijk] * betax[ijk] 
                        + J22[ijk] * betay[ijk] 
                        + J23[ijk] * betaz[ijk]; 
              up_dir[2] = J31[ijk] * betax[ijk] 
                        + J32[ijk] * betay[ijk] 
                        + J33[ijk] * betaz[ijk]; 

            dadx = J11[ijk];
            dady = J12[ijk];
            dadz = J13[ijk];
            dbdx = J21[ijk];
            dbdy = J22[ijk];
            dbdz = J23[ijk];
            dcdx = J31[ijk];
            dcdy = J32[ijk];
            dcdz = J33[ijk];
             } 

         /* Calculate required partial derivatives. */
          
         CTGBase_set_advective_stencil(adv_q[0], adv_imin[0], adv_imax[0],
                                       up_dir[0], q[0], imin[0], imax[0],
                                       q_minus[0], imin_minus[0],
                                       imax_minus[0], q_plus[0], imin_plus[0],
                                       imax_plus[0]);
         CTGBase_set_advective_stencil(adv_q[1], adv_imin[1], adv_imax[1],
                                       up_dir[1], q[1], imin[1], imax[1],
                                       q_minus[1], imin_minus[1],
                                       imax_minus[1], q_plus[1], imin_plus[1],
                                       imax_plus[1]);
         CTGBase_set_advective_stencil(adv_q[2], adv_imin[2], adv_imax[2],
                                       up_dir[2], q[2], imin[2], imax[2],
                                       q_minus[2], imin_minus[2],
                                       imax_minus[2], q_plus[2], imin_plus[2],
                                       imax_plus[2]);

      g_diff(cctkGH, betax, &adbeta11, &adbeta12, &adbeta13,
             !is_cartesian,
             dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
             i, j, k, ni, nj, nk,
             adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1],
             adv_imin[2], adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
             ihx, ihy, ihz);
      g_diff(cctkGH, betay, &adbeta21, &adbeta22, &adbeta23,
             !is_cartesian,
             dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
             i, j, k, ni, nj, nk,
             adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1],
             adv_imin[2], adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
             ihx, ihy, ihz);
      g_diff(cctkGH, betaz, &adbeta31, &adbeta32, &adbeta33, 
            !is_cartesian,
             dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
             i, j, k, ni, nj, nk,
             adv_imin[0], adv_imax[0], adv_imin[1], adv_imax[1],
             adv_imin[2], adv_imax[2], adv_q[0], adv_q[1], adv_q[2],
             ihx, ihy, ihz);

      g_diff(cctkGH, alp, &dalp1, &dalp2, &dalp3,
             *general_coordinates,
             dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
             i, j, k, ni, nj, nk, imin[0], imax[0], imin[1], imax[1],
             imin[2], imax[2], q[0], q[1], q[2], ihx, ihy, ihz);

      g_diff(cctkGH, phi, &dphi1, &dphi2, &dphi3,
             *general_coordinates,
             dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
             i, j, k, ni, nj, nk, imin[0], imax[0], imin[1], imax[1],
             imin[2], imax[2], q[0], q[1], q[2], ihx, ihy, ihz);

      CTGBase_invert_metric(gamma11[ijk], gamma12[ijk], gamma13[ijk],
                            gamma22[ijk], gamma23[ijk], gamma33[ijk],
                            detg, ig11, ig12, ig13, ig22, ig23, ig33);

      if (use_phi)
	em4phi = exp(-4*phi[ijk]);
      else
	em4phi = pow(phi[ijk], -1.0/(3.0*N));

      a_factor_1 = alp[ijk] * em4phi * (ig11*dalp1 + ig12*dalp2 + ig13*dalp3);
      a_factor_2 = alp[ijk] * em4phi * (ig12*dalp1 + ig22*dalp2 + ig23*dalp3);
      a_factor_3 = alp[ijk] * em4phi * (ig13*dalp1 + ig23*dalp2 + ig33*dalp3);

      a2 = alp[ijk] * alp[ijk];

      if (use_phi)
        a_coeff = 2.0;
      else
        a_coeff = 1/(6*N*phi[ijk]);

      gamma1 = em4phi * (Gamma1[ijk] - a_coeff * (ig11*dphi1 + ig12*dphi2
                                              + ig13*dphi3));
      gamma2 = em4phi * (Gamma2[ijk] - a_coeff * (ig12*dphi1 + ig22*dphi2
                                              + ig23*dphi3));
      gamma3 = em4phi * (Gamma3[ijk] - a_coeff * (ig13*dphi1 + ig23*dphi2
                                              + ig33*dphi3));

      dtbetax[ijk] = a2 * gamma1 - a_factor_1
        + (beta1*adbeta11 + beta2*adbeta12 + beta3*adbeta13);
      dtbetay[ijk] = a2 * gamma2 - a_factor_2
        + (beta1*adbeta21 + beta2*adbeta22 + beta3*adbeta23);
      dtbetaz[ijk] = a2 * gamma3 - a_factor_3
      + (beta1*adbeta31 + beta2*adbeta32 + beta3*adbeta33);

    }
  LC_ENDLOOP3 (CTGGauge_ShiftHarmonic_rhs);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin_minus, imax_minus, q_minus);
  CTGBase_free_stencil(imin_plus, imax_plus, q_plus);

  return;
}
