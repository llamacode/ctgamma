#include <math.h>
#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "util_Table.h"

#include "Slicing.h"
#include "Symmetry.h"

void
CTGGauge_Fixed_Shift_SetSymmetries(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;

  int sym[3];
  int ierr = 0;
 
  CCTK_INFO("Setting symmetries for fixed shift.");

  sym[0] = -1;
  sym[1] = +1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betax");

  sym[0] = +1;
  sym[1] = -1;
  sym[2] = +1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betay");

  sym[0] = +1;
  sym[1] = +1;
  sym[2] = -1;
  ierr += SetCartSymVN(cctkGH, sym, "ADMBase::betaz");

  if (ierr)
    CCTK_WARN(0, "Couldn't set GF symmetries.");
    
  return;
}
