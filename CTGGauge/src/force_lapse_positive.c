#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void
CTGGauge_ForceLapsePositive(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  int ni, nj, nk;
  CCTK_REAL smallest_lapse = 1e-10;

  if (verbose)
    CCTK_INFO("Enforcing lapse[ijk]>0.");

  ni = cctk_lsh[0];
  nj = cctk_lsh[1];
  nk = cctk_lsh[2];

  for (int k=0; k<nk; ++k)
    for (int j=0; j<nj; ++j)
      for (int i=0; i<ni; ++i)
        {
          int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
	  if (alp[ijk] < smallest_lapse)
	    alp[ijk] = smallest_lapse;
	}

  return;
}
