zz1
  = ig11*z1 + ig12*z2 + ig13*z3;
zz2
  = ig12*z1 + ig22*z2 + ig23*z3;
zz3
  = ig13*z1 + ig23*z2 + ig33*z3;
zmag
  = g11*Power(zz1,2) + g22*Power(zz2,2) + g33*Power(zz3,2) + 2*(g23*zz2*zz3 + zz1*(g12*zz2 + g13*zz3));
z1
  = zz1/Power(zmag,0.5);
z2
  = zz2/Power(zmag,0.5);
z3
  = zz3/Power(zmag,0.5);
xx1
  = x1 - (g11*x1 + g12*x2 + g13*x3)*Power(z1,2) - z1*((g12*x1 + g22*x2 + g23*x3)*z2 + (g13*x1 + g23*x2 + g33*x3)*z3);
xx2
  = x2 - (g12*x1 + g22*x2)*Power(z2,2) - z2*((g11*x1 + g12*x2)*z1 + (g13*x1 + g23*x2)*z3) - x3*(g23*Power(z2,2) + z2*(g13*z1 + g33*z3));
xx3
  = x3 - ((g11*x1 + g12*x2 + g13*x3)*z1 + (g12*x1 + g22*x2 + g23*x3)*z2)*z3 - (g13*x1 + g23*x2 + g33*x3)*Power(z3,2);
xmag
  = g11*Power(xx1,2) + g22*Power(xx2,2) + g33*Power(xx3,2) + 2*(g23*xx2*xx3 + xx1*(g12*xx2 + g13*xx3));
x1
  = xx1/Power(xmag,0.5);
x2
  = xx2/Power(xmag,0.5);
x3
  = xx3/Power(xmag,0.5);
yy1
  = y1 - y1*(x1*(g12*x2 + g13*x3) + g11*(Power(x1,2) + Power(z1,2)) + z1*(g12*z2 + g13*z3)) - y2*(x1*(g22*x2 + g23*x3) + g12*(Power(x1,2) + Power(z1,2)) + z1*(g22*z2 + g23*z3)) - y3*(x1*(g23*x2 + g33*x3) + g13*(Power(x1,2) + Power(z1,2)) + z1*(g23*z2 + g33*z3));
yy2
  = y2 - (g12*y1 + g22*y2 + g23*y3)*Power(z2,2) - y1*(g12*Power(x2,2) + x2*(g11*x1 + g13*x3) + z2*(g11*z1 + g13*z3)) - y2*(g22*Power(x2,2) + x2*(g12*x1 + g23*x3) + z2*(g12*z1 + g23*z3)) - y3*(g23*Power(x2,2) + x2*(g13*x1 + g33*x3) + z2*(g13*z1 + g33*z3));
yy3
  = y3 - x3*((g11*x1 + g12*x2)*y1 + (g12*x1 + g22*x2)*y2 + (g13*x1 + g23*x2)*y3) - ((g11*y1 + g12*y2 + g13*y3)*z1 + (g12*y1 + g22*y2 + g23*y3)*z2)*z3 - (g13*y1 + g23*y2 + g33*y3)*(Power(x3,2) + Power(z3,2));
ymag
  = g11*Power(yy1,2) + g22*Power(yy2,2) + g33*Power(yy3,2) + 2*(g23*yy2*yy3 + yy1*(g12*yy2 + g13*yy3));
y1
  = yy1/Power(ymag,0.5);
y2
  = yy2/Power(ymag,0.5);
y3
  = yy3/Power(ymag,0.5);
mr0
  = 0;
mr1
  = x1/sqrt2;
mr2
  = x2/sqrt2;
mr3
  = x3/sqrt2;
mi0
  = 0;
mi1
  = -(y1/sqrt2);
mi2
  = -(y2/sqrt2);
mi3
  = -(y3/sqrt2);
dld00
  = 0;
dld01
  = 0;
dld02
  = 0;
dld03
  = 0;
dld10
  = 0;
dld20
  = 0;
dld30
  = 0;
dnd00
  = 0;
dnd01
  = 0;
dnd02
  = 0;
dnd03
  = 0;
dnd10
  = 0;
dnd20
  = 0;
dnd30
  = 0;
dtg11
  = beta1*dg111 + beta2*dg112 + beta3*dg113 + 2*(dbeta11*g11 + dbeta21*g12 + dbeta31*g13) - K11*lapse;
dtg12
  = beta1*dg121 + beta2*dg122 + beta3*dg123 + dbeta12*g11 + (dbeta11 + dbeta22)*g12 + dbeta32*g13 + dbeta21*g22 + dbeta31*g23 - K12*lapse;
dtg13
  = beta1*dg131 + beta2*dg132 + beta3*dg133 + dbeta13*g11 + dbeta23*g12 + (dbeta11 + dbeta33)*g13 + dbeta21*g23 + dbeta31*g33 - K13*lapse;
dtg22
  = beta1*dg221 + beta2*dg222 + beta3*dg223 + 2*(dbeta12*g12 + dbeta22*g22 + dbeta32*g23) - K22*lapse;
dtg23
  = beta1*dg231 + beta2*dg232 + beta3*dg233 + dbeta13*g12 + dbeta12*g13 + dbeta23*g22 + (dbeta22 + dbeta33)*g23 + dbeta32*g33 - K23*lapse;
dtg33
  = beta1*dg331 + beta2*dg332 + beta3*dg333 + 2*(dbeta13*g13 + dbeta23*g23 + dbeta33*g33) - K33*lapse;
dB1
  = Power(beta1,2)*dg111 + Power(beta2,2)*dg221 + Power(beta3,2)*dg331 + 2*(beta1*(beta2*dg121 + beta3*dg131 + dbeta11*g11 + dbeta21*g12 + dbeta31*g13) + beta2*(beta3*dg231 + dbeta11*g12 + dbeta21*g22 + dbeta31*g23) + beta3*(dbeta11*g13 + dbeta21*g23 + dbeta31*g33));
dB2
  = Power(beta1,2)*dg112 + Power(beta2,2)*dg222 + Power(beta3,2)*dg332 + 2*(beta1*(beta2*dg122 + beta3*dg132 + dbeta12*g11 + dbeta22*g12 + dbeta32*g13) + beta2*(beta3*dg232 + dbeta12*g12 + dbeta22*g22 + dbeta32*g23) + beta3*(dbeta12*g13 + dbeta22*g23 + dbeta32*g33));
dB3
  = Power(beta1,2)*dg113 + Power(beta2,2)*dg223 + Power(beta3,2)*dg333 + 2*(beta1*(beta2*dg123 + beta3*dg133 + dbeta13*g11 + dbeta23*g12 + dbeta33*g13) + beta2*(beta3*dg233 + dbeta13*g12 + dbeta23*g22 + dbeta33*g23) + beta3*(dbeta13*g13 + dbeta23*g23 + dbeta33*g33));
ChrD111
  = dg111/2.;
ChrD112
  = dg112/2.;
ChrD113
  = dg113/2.;
ChrD122
  = dg122 - dg221/2.;
ChrD123
  = (dg123 + dg132 - dg231)/2.;
ChrD133
  = dg133 - dg331/2.;
ChrD211
  = -dg112/2. + dg121;
ChrD212
  = dg221/2.;
ChrD213
  = (dg123 - dg132 + dg231)/2.;
ChrD222
  = dg222/2.;
ChrD223
  = dg223/2.;
ChrD233
  = dg233 - dg332/2.;
ChrD311
  = -dg113/2. + dg131;
ChrD312
  = (-dg123 + dg132 + dg231)/2.;
ChrD313
  = dg331/2.;
ChrD322
  = -dg223/2. + dg232;
ChrD323
  = dg332/2.;
ChrD333
  = dg333/2.;
Chr111
  = ChrD111*ig11 + ChrD211*ig12 + ChrD311*ig13;
Chr112
  = ChrD112*ig11 + ChrD212*ig12 + ChrD312*ig13;
Chr113
  = ChrD113*ig11 + ChrD213*ig12 + ChrD313*ig13;
Chr122
  = ChrD122*ig11 + ChrD222*ig12 + ChrD322*ig13;
Chr123
  = ChrD123*ig11 + ChrD223*ig12 + ChrD323*ig13;
Chr133
  = ChrD133*ig11 + ChrD233*ig12 + ChrD333*ig13;
Chr211
  = ChrD111*ig12 + ChrD211*ig22 + ChrD311*ig23;
Chr212
  = ChrD112*ig12 + ChrD212*ig22 + ChrD312*ig23;
Chr213
  = ChrD113*ig12 + ChrD213*ig22 + ChrD313*ig23;
Chr222
  = ChrD122*ig12 + ChrD222*ig22 + ChrD322*ig23;
Chr223
  = ChrD123*ig12 + ChrD223*ig22 + ChrD323*ig23;
Chr233
  = ChrD133*ig12 + ChrD233*ig22 + ChrD333*ig23;
Chr311
  = ChrD111*ig13 + ChrD211*ig23 + ChrD311*ig33;
Chr312
  = ChrD112*ig13 + ChrD212*ig23 + ChrD312*ig33;
Chr313
  = ChrD113*ig13 + ChrD213*ig23 + ChrD313*ig33;
Chr322
  = ChrD122*ig13 + ChrD222*ig23 + ChrD322*ig33;
Chr323
  = ChrD123*ig13 + ChrD223*ig23 + ChrD323*ig33;
Chr333
  = ChrD133*ig13 + ChrD233*ig23 + ChrD333*ig33;
Chr4000
  = (dtalpha - Power(beta1,2)*K11 + beta1*(dalpha1 - 2*beta2*K12) + beta3*(dalpha3 - 2*beta1*K13) - Power(beta2,2)*K22 + beta2*(dalpha2 - 2*beta3*K23) - Power(beta3,2)*K33)/lapse;
Chr4001
  = (dalpha1 - beta1*K11 - beta2*K12 - beta3*K13)/lapse;
Chr4002
  = (dalpha2 - beta1*K12 - beta2*K22 - beta3*K23)/lapse;
Chr4003
  = (dalpha3 - beta1*K13 - beta2*K23 - beta3*K33)/lapse;
Chr4011
  = -(K11/lapse);
Chr4012
  = -(K12/lapse);
Chr4013
  = -(K13/lapse);
Chr4022
  = -(K22/lapse);
Chr4023
  = -(K23/lapse);
Chr4033
  = -(K33/lapse);
Chr4100
  = -(beta1*Chr4000) + ig11*(-dB1/2. + beta1*dtg11 + beta2*dtg12 + beta3*dtg13 + dtbeta1*g11 + dtbeta2*g12 + dtbeta3*g13 + dalpha1*lapse) + ig12*(-dB2/2. + beta1*dtg12 + beta2*dtg22 + beta3*dtg23 + dtbeta1*g12 + dtbeta2*g22 + dtbeta3*g23 + dalpha2*lapse) + ig13*(-dB3/2. + beta1*dtg13 + beta2*dtg23 + beta3*dtg33 + dtbeta1*g13 + dtbeta2*g23 + dtbeta3*g33 + dalpha3*lapse);
Chr4200
  = -(beta2*Chr4000) + ig12*(-dB1/2. + beta1*dtg11 + beta2*dtg12 + beta3*dtg13 + dtbeta1*g11 + dtbeta2*g12 + dtbeta3*g13 + dalpha1*lapse) + ig22*(-dB2/2. + beta1*dtg12 + beta2*dtg22 + beta3*dtg23 + dtbeta1*g12 + dtbeta2*g22 + dtbeta3*g23 + dalpha2*lapse) + ig23*(-dB3/2. + beta1*dtg13 + beta2*dtg23 + beta3*dtg33 + dtbeta1*g13 + dtbeta2*g23 + dtbeta3*g33 + dalpha3*lapse);
Chr4300
  = -(beta3*Chr4000) + ig13*(-dB1/2. + beta1*dtg11 + beta2*dtg12 + beta3*dtg13 + dtbeta1*g11 + dtbeta2*g12 + dtbeta3*g13 + dalpha1*lapse) + ig23*(-dB2/2. + beta1*dtg12 + beta2*dtg22 + beta3*dtg23 + dtbeta1*g12 + dtbeta2*g22 + dtbeta3*g23 + dalpha2*lapse) + ig33*(-dB3/2. + beta1*dtg13 + beta2*dtg23 + beta3*dtg33 + dtbeta1*g13 + dtbeta2*g23 + dtbeta3*g33 + dalpha3*lapse);
Chr4101
  = beta2*Chr112 + beta3*Chr113 + beta1*(Chr111 - Chr4001) + dbeta11 - (ig11*K11 + ig12*K12 + ig13*K13)*lapse;
Chr4102
  = beta2*Chr122 + beta3*Chr123 + beta1*(Chr112 - Chr4002) + dbeta12 - (ig11*K12 + ig12*K22 + ig13*K23)*lapse;
Chr4103
  = beta2*Chr123 + beta3*Chr133 + beta1*(Chr113 - Chr4003) + dbeta13 - (ig11*K13 + ig12*K23 + ig13*K33)*lapse;
Chr4201
  = beta1*Chr211 + beta3*Chr213 + beta2*(Chr212 - Chr4001) + dbeta21 - (ig12*K11 + ig22*K12 + ig23*K13)*lapse;
Chr4202
  = beta1*Chr212 + beta3*Chr223 + beta2*(Chr222 - Chr4002) + dbeta22 - (ig12*K12 + ig22*K22 + ig23*K23)*lapse;
Chr4203
  = beta1*Chr213 + beta3*Chr233 + beta2*(Chr223 - Chr4003) + dbeta23 - (ig12*K13 + ig22*K23 + ig23*K33)*lapse;
Chr4301
  = beta1*Chr311 + beta2*Chr312 + beta3*(Chr313 - Chr4001) + dbeta31 - (ig13*K11 + ig23*K12 + ig33*K13)*lapse;
Chr4302
  = beta1*Chr312 + beta2*Chr322 + beta3*(Chr323 - Chr4002) + dbeta32 - (ig13*K12 + ig23*K22 + ig33*K23)*lapse;
Chr4303
  = beta1*Chr313 + beta2*Chr323 + beta3*(Chr333 - Chr4003) + dbeta33 - (ig13*K13 + ig23*K23 + ig33*K33)*lapse;
Chr4111
  = Chr111 - beta1*Chr4011;
Chr4112
  = Chr112 - beta1*Chr4012;
Chr4113
  = Chr113 - beta1*Chr4013;
Chr4122
  = Chr122 - beta1*Chr4022;
Chr4123
  = Chr123 - beta1*Chr4023;
Chr4133
  = Chr133 - beta1*Chr4033;
Chr4211
  = Chr211 - beta2*Chr4011;
Chr4212
  = Chr212 - beta2*Chr4012;
Chr4213
  = Chr213 - beta2*Chr4013;
Chr4222
  = Chr222 - beta2*Chr4022;
Chr4223
  = Chr223 - beta2*Chr4023;
Chr4233
  = Chr233 - beta2*Chr4033;
Chr4311
  = Chr311 - beta3*Chr4011;
Chr4312
  = Chr312 - beta3*Chr4012;
Chr4313
  = Chr313 - beta3*Chr4013;
Chr4322
  = Chr322 - beta3*Chr4022;
Chr4323
  = Chr323 - beta3*Chr4023;
Chr4333
  = Chr333 - beta3*Chr4033;
D4l00
  = dld00 - Chr4000*lld0 - Chr4100*lld1 - Chr4200*lld2 - Chr4300*lld3;
D4l10
  = dld10 - Chr4001*lld0 - Chr4101*lld1 - Chr4201*lld2 - Chr4301*lld3;
D4l20
  = dld20 - Chr4002*lld0 - Chr4102*lld1 - Chr4202*lld2 - Chr4302*lld3;
D4l30
  = dld30 - Chr4003*lld0 - Chr4103*lld1 - Chr4203*lld2 - Chr4303*lld3;
D4l01
  = dld01 - Chr4001*lld0 - Chr4101*lld1 - Chr4201*lld2 - Chr4301*lld3;
D4l02
  = dld02 - Chr4002*lld0 - Chr4102*lld1 - Chr4202*lld2 - Chr4302*lld3;
D4l03
  = dld03 - Chr4003*lld0 - Chr4103*lld1 - Chr4203*lld2 - Chr4303*lld3;
D4l11
  = dld11 - Chr4011*lld0 - Chr4111*lld1 - Chr4211*lld2 - Chr4311*lld3;
D4l12
  = dld12 - Chr4012*lld0 - Chr4112*lld1 - Chr4212*lld2 - Chr4312*lld3;
D4l13
  = dld13 - Chr4013*lld0 - Chr4113*lld1 - Chr4213*lld2 - Chr4313*lld3;
D4l21
  = dld21 - Chr4012*lld0 - Chr4112*lld1 - Chr4212*lld2 - Chr4312*lld3;
D4l22
  = dld22 - Chr4022*lld0 - Chr4122*lld1 - Chr4222*lld2 - Chr4322*lld3;
D4l23
  = dld23 - Chr4023*lld0 - Chr4123*lld1 - Chr4223*lld2 - Chr4323*lld3;
D4l31
  = dld31 - Chr4013*lld0 - Chr4113*lld1 - Chr4213*lld2 - Chr4313*lld3;
D4l32
  = dld32 - Chr4023*lld0 - Chr4123*lld1 - Chr4223*lld2 - Chr4323*lld3;
D4l33
  = dld33 - Chr4033*lld0 - Chr4133*lld1 - Chr4233*lld2 - Chr4333*lld3;
D4n00
  = dnd00 - Chr4000*nnd0 - Chr4100*nnd1 - Chr4200*nnd2 - Chr4300*nnd3;
D4n10
  = dnd10 - Chr4001*nnd0 - Chr4101*nnd1 - Chr4201*nnd2 - Chr4301*nnd3;
D4n20
  = dnd20 - Chr4002*nnd0 - Chr4102*nnd1 - Chr4202*nnd2 - Chr4302*nnd3;
D4n30
  = dnd30 - Chr4003*nnd0 - Chr4103*nnd1 - Chr4203*nnd2 - Chr4303*nnd3;
D4n01
  = dnd01 - Chr4001*nnd0 - Chr4101*nnd1 - Chr4201*nnd2 - Chr4301*nnd3;
D4n02
  = dnd02 - Chr4002*nnd0 - Chr4102*nnd1 - Chr4202*nnd2 - Chr4302*nnd3;
D4n03
  = dnd03 - Chr4003*nnd0 - Chr4103*nnd1 - Chr4203*nnd2 - Chr4303*nnd3;
D4n11
  = dnd11 - Chr4011*nnd0 - Chr4111*nnd1 - Chr4211*nnd2 - Chr4311*nnd3;
D4n12
  = dnd12 - Chr4012*nnd0 - Chr4112*nnd1 - Chr4212*nnd2 - Chr4312*nnd3;
D4n13
  = dnd13 - Chr4013*nnd0 - Chr4113*nnd1 - Chr4213*nnd2 - Chr4313*nnd3;
D4n21
  = dnd21 - Chr4012*nnd0 - Chr4112*nnd1 - Chr4212*nnd2 - Chr4312*nnd3;
D4n22
  = dnd22 - Chr4022*nnd0 - Chr4122*nnd1 - Chr4222*nnd2 - Chr4322*nnd3;
D4n23
  = dnd23 - Chr4023*nnd0 - Chr4123*nnd1 - Chr4223*nnd2 - Chr4323*nnd3;
D4n31
  = dnd31 - Chr4013*nnd0 - Chr4113*nnd1 - Chr4213*nnd2 - Chr4313*nnd3;
D4n32
  = dnd32 - Chr4023*nnd0 - Chr4123*nnd1 - Chr4223*nnd2 - Chr4323*nnd3;
D4n33
  = dnd33 - Chr4033*nnd0 - Chr4133*nnd1 - Chr4233*nnd2 - Chr4333*nnd3;
sigmare
  = -((D4l23 + D4l32)*mi2*mi3) - mi0*((D4l01 + D4l10)*mi1 + (D4l02 + D4l20)*mi2 + (D4l03 + D4l30)*mi3) - mi1*((D4l12 + D4l21)*mi2 + (D4l13 + D4l31)*mi3) + D4l00*(-Power(mi0,2) + Power(mr0,2)) + D4l11*(-Power(mi1,2) + Power(mr1,2)) + D4l22*(-Power(mi2,2) + Power(mr2,2)) + (D4l30*mr0 + (D4l13 + D4l31)*mr1 + D4l32*mr2)*mr3 + mr0*((D4l01 + D4l10)*mr1 + D4l02*mr2 + D4l03*mr3) + mr2*(D4l20*mr0 + (D4l12 + D4l21)*mr1 + D4l23*mr3) + D4l33*(-Power(mi3,2) + Power(mr3,2));
sigmaim
  = (2*D4l00*mi0 + (D4l01 + D4l10)*mi1 + (D4l02 + D4l20)*mi2 + (D4l03 + D4l30)*mi3)*mr0 + (2*D4l11*mi1 + (D4l12 + D4l21)*mi2 + (D4l13 + D4l31)*mi3)*mr1 + (D4l20*mi0 + (D4l12 + D4l21)*mi1 + 2*D4l22*mi2 + (D4l23 + D4l32)*mi3)*mr2 + (D4l30*mi0 + (D4l13 + D4l31)*mi1 + (D4l23 + D4l32)*mi2 + 2*D4l33*mi3)*mr3 + mi0*((D4l01 + D4l10)*mr1 + D4l02*mr2 + D4l03*mr3);
lambdare
  = -((D4n23 + D4n32)*mi2*mi3) - mi0*((D4n01 + D4n10)*mi1 + (D4n02 + D4n20)*mi2 + (D4n03 + D4n30)*mi3) - mi1*((D4n12 + D4n21)*mi2 + (D4n13 + D4n31)*mi3) + D4n00*(-Power(mi0,2) + Power(mr0,2)) + D4n11*(-Power(mi1,2) + Power(mr1,2)) + D4n22*(-Power(mi2,2) + Power(mr2,2)) + (D4n30*mr0 + (D4n13 + D4n31)*mr1 + D4n32*mr2)*mr3 + mr0*((D4n01 + D4n10)*mr1 + D4n02*mr2 + D4n03*mr3) + mr2*(D4n20*mr0 + (D4n12 + D4n21)*mr1 + D4n23*mr3) + D4n33*(-Power(mi3,2) + Power(mr3,2));
lambdaim
  = -((2*D4n00*mi0 + (D4n01 + D4n10)*mi1 + (D4n02 + D4n20)*mi2 + (D4n03 + D4n30)*mi3)*mr0) - ((D4n01 + D4n10)*mi0 + 2*D4n11*mi1 + (D4n12 + D4n21)*mi2 + (D4n13 + D4n31)*mi3)*mr1 - (D4n20*mi0 + (D4n12 + D4n21)*mi1 + 2*D4n22*mi2 + (D4n23 + D4n32)*mi3)*mr2 - (D4n30*mi0 + (D4n13 + D4n31)*mi1 + (D4n23 + D4n32)*mi2 + 2*D4n33*mi3)*mr3 - mi0*(D4n02*mr2 + D4n03*mr3);
