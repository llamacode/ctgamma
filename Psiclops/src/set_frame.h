#ifndef PSICLOPS_SETFRAME_H
#define PSICLOPS_SETFRAME_H

#include <math.h>

#include "cctk.h"

/* {xp, yp, zp}   Input global (Cartesian) coordinates
   {x1, x2, x3}   Azimuthal: First output tetrad vector components (prop. to d/dph)
   {y1, y2, y3}   Polar: Second output tetrad vector components (prop. to -d/dth)
   {z1, z2, z3}   Radial: Third output tetrad vector components (prop. to d/dr) */

#define Psiclops_SetFrameGuess(xp, yp, zp, x1, x2, x3, y1, y2, y3, z1, z2, z3) \
{                                                                       \
  CCTK_REAL rho2 = xp*xp + yp*yp;                                       \
  CCTK_REAL rho = sqrt(rho2);                                           \
  CCTK_REAL R = sqrt(rho2 + zp*zp);                                     \
  CCTK_REAL D = rho*R;                                                  \
                                                                        \
  if (rho != 0.0)                                                       \
  {                                                                     \
    /* zhat is radial in the global coordinates */                      \
    z1 = xp / R;                                                        \
    z2 = yp / R;                                                        \
    z3 = zp / R;                                                        \
                                                                        \
    /* xhat is orthogonal to zhat, parallel to the z=0 plane */         \
    x1 = -yp / rho;                                                     \
    x2 = +xp / rho;                                                     \
    x3 = 0.0;                                                           \
                                                                        \
    /* yhat is orthogonal to xhat and zhat */                           \
    y1 = xp*zp / D;                                                     \
    y2 = yp*zp / D;                                                     \
    y3 = -rho2 / D;                                                     \
  } else {                                                              \
    /* We're on the z-axis - arbitrarily take the limit x->0, y->0- */  \
    if (zp != 0.0)                                                      \
    {                                                                   \
      z1 = 0.0;                                                         \
      z2 = 0.0;                                                         \
      z3 = zp / R;                                                      \
                                                                        \
      x1 = 1.0;                                                         \
      x2 = 0.0;                                                         \
      x3 = 0.0;                                                         \
                                                                        \
      y1 = 0.0;                                                         \
      y2 = -zp / R;                                                     \
      y3 = 0.0;                                                         \
    } else {                                                            \
      /* We're at the origin  arbitrarily take the limit z->0+ */       \
      z1 = 0.0;                                                         \
      z2 = 0.0;                                                         \
      z3 = 1.0;                                                         \
                                                                        \
      x1 = 1.0;                                                         \
      x2 = 0.0;                                                         \
      x3 = 0.0;                                                         \
                                                                        \
      y1 = 0.0;                                                         \
      y2 = -1.0;                                                         \
      y3 = 0.0;                                                         \
    }                                                                   \
  }                                                                     \
}

#endif
