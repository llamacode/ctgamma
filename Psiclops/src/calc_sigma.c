#include <math.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "psiclops.h"
#include "set_frame.h"
#include "GlobalDerivative.h"
#include "ctgbase_util.h"
#include "loopcontrol.h"

#define Power(x,y) (pow((CCTK_REAL) (x), (CCTK_REAL) (y)))

void
Psiclops_CalcSigma(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT istart[3], iend[3];
  CCTK_INT * restrict imin[3];
  CCTK_INT * restrict imax[3];
  CCTK_INT * restrict imin2[3], * restrict imax2[3];
  CCTK_REAL * restrict q[3], * restrict q2[3];

  if (angular_grid_only)
  {
    if (MultiPatch_GetMap(cctkGH) == 0)
      return;
  }

  if (verbose)
    CCTK_INFO("Calculating Weyl components.");
  
  CCTK_INT const ni = cctk_lsh[0];
  CCTK_INT const nj = cctk_lsh[1];
  CCTK_INT const nk = cctk_lsh[2];
  
  /*
   * Grid spacings required by the finite difference operators.
   */
  CCTK_REAL const ihx = 1.0 / CCTK_DELTA_SPACE(0);
  CCTK_REAL const ihy = 1.0 / CCTK_DELTA_SPACE(1);
  CCTK_REAL const ihz = 1.0 / CCTK_DELTA_SPACE(2);
  CCTK_REAL const ihxx = ihx*ihx;
  CCTK_REAL const ihxy = ihx*ihy;
  CCTK_REAL const ihxz = ihx*ihz;
  CCTK_REAL const ihyy = ihy*ihy;
  CCTK_REAL const ihyz = ihy*ihz;
  CCTK_REAL const ihzz = ihz*ihz;

  /*
   * Call SummationByParts for finite-difference operator coefficients.
   */
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q,
                  1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin2,
                  (CCTK_POINTER_TO_CONST *) imax2, (CCTK_POINTER_TO_CONST *) q2,
                  2, 0);

  Util_GetGridRanges(cctkGH, istart, iend);


#pragma omp parallel
  LC_LOOP3 (Psiclops_CalcSigma_init,
            i, j, k,
	    0, 0, 0,
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      CCTK_REAL g11, g12, g13, g22, g23, g33;
      CCTK_REAL detg;
      CCTK_REAL ig11, ig12, ig13, ig22, ig23, ig33;
      CCTK_REAL x1, x2, x3, y1, y2, y3, z1, z2, z3;

      if (store_sigma)
        {
          sigma_re[ijk] = 0.0;
          sigma_im[ijk] = 0.0;
        }

      g11 = gxx[ijk];
      g12 = gxy[ijk];
      g13 = gxz[ijk];
      g22 = gyy[ijk];
      g23 = gyz[ijk];
      g33 = gzz[ijk];

      CTGBase_invert_metric(g11, g12, g13, g22, g23, g33,
                            detg, ig11, ig12, ig13, ig22, ig23, ig33);

      Psiclops_SetFrameGuess(x[ijk], y[ijk], z[ijk], x1, x2, x3, y1, y2, y3,
                             z1, z2, z3);

      CCTK_REAL zmag = sqrt(ig11*z1*z1 + ig22*z2*z2 + ig33*z3*z3 +
			    2.0*(ig12*z1*z2 + ig13*z1*z3 + ig23*z2*z3));
      
      ld0[ijk] = 1;
      ld1[ijk] = z1/zmag;
      ld2[ijk] = z2/zmag;
      ld3[ijk] = z3/zmag;

      nd0[ijk] = 1;
      nd1[ijk] = -z1/zmag;
      nd2[ijk] = -z2/zmag;
      nd3[ijk] = -z3/zmag;
    }
  LC_ENDLOOP3 (Psiclops_CalcSigma_init);



#pragma omp parallel
  LC_LOOP3 (Psiclops_CalcSigma,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
      CCTK_REAL detg;

      CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0, dbdz=0,
        dcdz=1;
      CCTK_REAL ddadxx=0, ddadxy=0, ddadxz=0, ddadyy=0, ddadyz=0, ddadzz=0;
      CCTK_REAL ddbdxx=0, ddbdxy=0, ddbdxz=0, ddbdyy=0, ddbdyz=0, ddbdzz=0;
      CCTK_REAL ddcdxx=0, ddcdxy=0, ddcdxz=0, ddcdyy=0, ddcdyz=0, ddcdzz=0;

#include "declare_sigma.h"

      CCTK_REAL sqrt2 = sqrt(2.0);
      
      g11 = gxx[ijk];
      g12 = gxy[ijk];
      g13 = gxz[ijk];
      g22 = gyy[ijk];
      g23 = gyz[ijk];
      g33 = gzz[ijk];
      
      K11 = kxx[ijk];
      K12 = kxy[ijk];
      K13 = kxz[ijk];
      K22 = kyy[ijk];
      K23 = kyz[ijk];
      K33 = kzz[ijk];

      lapse = alp[ijk];
      dtalpha = dtalp[ijk];

      beta1 = betax[ijk];
      beta2 = betay[ijk];
      beta3 = betaz[ijk];

      dtbeta1 = dtbetax[ijk];
      dtbeta2 = dtbetay[ijk];
      dtbeta3 = dtbetaz[ijk];

      lld0 = ld0[ijk];
      lld1 = ld1[ijk];
      lld2 = ld2[ijk];
      lld3 = ld3[ijk];

      nnd0 = nd0[ijk];
      nnd1 = nd1[ijk];
      nnd2 = nd2[ijk];
      nnd3 = nd3[ijk];
      
      CTGBase_invert_metric(g11, g12, g13, g22, g23, g33,
                            detg, ig11, ig12, ig13, ig22, ig23, ig33);

      Psiclops_SetFrameGuess(x[ijk], y[ijk], z[ijk], x1, x2, x3, y1, y2, y3,
                             z1, z2, z3);
      
      CCTK_REAL eps123 = 1/detg;
      
#include "derivatives_sigma.h"

#include "calc_sigma.h"


      if (store_sigma)
        {
          sigma_re[ijk] = sigmare;
          sigma_im[ijk] = sigmaim;

          lambda_re[ijk] = lambdare;
	  lambda_im[ijk] = lambdaim;
        }

    }
  LC_ENDLOOP3 (Psiclops_CalcSigma);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin2, imax2, q2);

  return;
}
