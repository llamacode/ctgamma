if (*general_coordinates)
  {
    dadx = J11[ijk];
    dady = J12[ijk];
    dadz = J13[ijk];
    dbdx = J21[ijk];
    dbdy = J22[ijk];
    dbdz = J23[ijk];
    dcdx = J31[ijk];
    dcdy = J32[ijk];
    dcdz = J33[ijk];
    
    ddadxx = dJ111[ijk];
    ddadxy = dJ112[ijk];
    ddadxz = dJ113[ijk];
    ddadyy = dJ122[ijk];
    ddadyz = dJ123[ijk];
    ddadzz = dJ133[ijk];
    
    ddbdxx = dJ211[ijk];
    ddbdxy = dJ212[ijk];
    ddbdxz = dJ213[ijk];
    ddbdyy = dJ222[ijk];
    ddbdyz = dJ223[ijk];
    ddbdzz = dJ233[ijk];
    
    ddcdxx = dJ311[ijk];
    ddcdxy = dJ312[ijk];
    ddcdxz = dJ313[ijk];
    ddcdyy = dJ322[ijk];
    ddcdyz = dJ323[ijk];
    ddcdzz = dJ333[ijk];
  }

/*
 * Metric.
 */
g_diff(cctkGH, gxx, &dg111, &dg112, &dg113,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gxy, &dg121, &dg122, &dg123,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gxz, &dg131, &dg132, &dg133,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gyy, &dg221, &dg222, &dg223,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gyz, &dg231, &dg232, &dg233,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, gzz, &dg331, &dg332, &dg333,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);

/*
 * Lapse.
 */
g_diff(cctkGH, alp, &dalpha1, &dalpha2, &dalpha3,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);

/*
 * Shift.
 */
g_diff(cctkGH, betax, &dbeta11, &dbeta12, &dbeta13,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, betay, &dbeta21, &dbeta22, &dbeta23,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, betaz, &dbeta31, &dbeta32, &dbeta33,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);

/*
 *  l_a
 */
g_diff(cctkGH, ld1, &dld11, &dld12, &dld13,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, ld2, &dld21, &dld22, &dld23,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
g_diff(cctkGH, ld3, &dld31, &dld32, &dld33,
       *general_coordinates,
       dadx, dbdx, dcdx, dady, dbdy, dcdy, dadz, dbdz, dcdz,
       i, j, k, ni, nj, nk,
       imin[0], imax[0], imin[1], imax[1], imin[2], imax[2], q[0], q[1], q[2],
       ihx, ihy, ihz);
