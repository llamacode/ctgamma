#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

#include "register.h"

#include "spherical_slices.hh" 


namespace PsiDecomp {

using namespace SPS;

// the slice ids for each slice and variable that are interpolated
vector<vector<int> > sid;

// the slice ids for each slice and variable that are NOT interpolated
vector<vector<int> > sid_ni;



int number_of_vars;
vector<int> spin_weight;
vector<string> vars;
vector<string> vars_ni;
int metric_idx;
int psi4_idx;

extern "C" void PsiClops_RegisterSlices(CCTK_ARGUMENTS)
{
   DECLARE_CCTK_ARGUMENTS
   DECLARE_CCTK_PARAMETERS

   sid = vector<vector<int> >(nslices);

   number_of_vars = 2*int(store_psi0) 
                  + 2*int(store_psi1) 
                  + 2*int(store_psi2) 
                  + 2*int(store_psi3) 
                  + 2*int(store_psi4) 
                  + 2*int(store_sigma)
                  + 10*int(correct_psi4);
   
   spin_weight = vector<int>   (number_of_vars);
   vars        = vector<string>(number_of_vars);
   
   int ii=0;
   if (store_psi0)
   {
      spin_weight[ii  ] = 2;
      spin_weight[ii+1] = 2;
      vars[ii  ] = "psiclops::psi0_re";
      vars[ii+1] = "psiclops::psi0_im";
      ii+=2;
   }
   if (store_psi1)
   {
      spin_weight[ii  ] = 1;
      spin_weight[ii+1] = 1;
      vars[ii  ] = "psiclops::psi1_re";
      vars[ii+1] = "psiclops::psi1_im";
      ii+=2;
   }
   if (store_psi2)
   {
      spin_weight[ii  ] = 0;
      spin_weight[ii+1] = 0;
      vars[ii  ] = "psiclops::psi2_re";
      vars[ii+1] = "psiclops::psi2_im";
      ii+=2;
   }
   if (store_psi3)
   {
      spin_weight[ii  ] = -1;
      spin_weight[ii+1] = -1;
      vars[ii  ] = "psiclops::psi3_re";
      vars[ii+1] = "psiclops::psi3_im";
      ii+=2;
   }
   if (store_psi4)
   {
      psi4_idx = ii;
      spin_weight[ii  ] = -2;
      spin_weight[ii+1] = -2;
      vars[ii  ] = "psiclops::psi4_re";
      vars[ii+1] = "psiclops::psi4_im";
      ii+=2;
   }
   if (store_sigma)
   {
      spin_weight[ii  ] = 2;
      spin_weight[ii+1] = 2;
      vars[ii  ] = "psiclops::sigma_re";
      vars[ii+1] = "psiclops::sigma_im";
      ii+=2;
   }
   
   if (correct_psi4)
   {
      metric_idx = ii;  // store index of metric components as offset in sid and vars array
      vars[ii  ] = "admbase::lapse";
      vars[ii+1] = "admbase::betax";
      vars[ii+2] = "admbase::betay";
      vars[ii+3] = "admbase::betaz";
      vars[ii+4] = "admbase::gxx";
      vars[ii+5] = "admbase::gxy";
      vars[ii+6] = "admbase::gxz";
      vars[ii+7] = "admbase::gyy";
      vars[ii+8] = "admbase::gyz";
      vars[ii+9] = "admbase::gzz";
      
      // variables that are calculated from interpolated variables
      sid_ni  = vector<vector<int> >(nslices);
      vars_ni = vector<string>(4);
      vars_ni[0] = "g_ur";
      vars_ni[1] = "g_th_th";
      vars_ni[2] = "g_th_ph";
      vars_ni[3] = "g_ph_ph";
            
   }
   
   for (int i=0; i < nslices; ++i)
   {
      sid[i] = vector<int>(number_of_vars, 0);
      for (int j=0; j < number_of_vars; ++j)
      {
         sid[i][j] = SphericalSlice_RegisterVariable(vars[j].c_str(), which_slice_to_take[i], 1, "split");
      }
      
      // set up slices for variables that are NOT interpolated but derived from other sliced variables.
      if (correct_psi4)
      {
         sid_ni[i] = vector<int>(4, 0);
         for (int j=0; j < 4; ++j)
         {
            sid_ni[i][j] = SphericalSlice_RegisterVariable(vars_ni[j].c_str(), which_slice_to_take[i], 1, "split");
         }
      }
   }
   

}


}
