#include "cctk.h"
#include "util_Table.h"

#include <math.h>

#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"

void
Psiclops_CalcWeyl_SelectBCs(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT faces;

  int ierr = 0;

  faces = CCTK_ALL_FACES;

  if (angular_grid_only)
  {
    if (MultiPatch_GetMap(cctkGH) == 0)
      return;
  }

  if (verbose)
    CCTK_VInfo(CCTK_THORNSTRING, "Selecting boundary condition");

  /* Apply Dirichlet boundary conditions, set outer boundary to 0 */
  /* Also, apply inter-patch boundary conditions */
  if (store_psi0)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::Psi0_group", "Scalar");

  if (store_psi1)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::Psi1_group", "Scalar");

  if (store_psi2)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::Psi2_group", "Scalar");

  if (store_psi3)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::Psi3_group", "Scalar");

  if (store_psi4)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::Psi4_group", "Scalar");

  if (store_I)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::invariant_I_group", "Scalar");

  if (store_sigma)
    ierr += Boundary_SelectGroupForBC(cctkGH, faces, 1, -1,
				      "Psiclops::sigma_group", "Scalar");

  if (ierr)
    CCTK_WARN(0, "Error selecting boundary condition.");
  
  ierr = 0;

  if (store_psi0)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::Psi0_group");

  if (store_psi1)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::Psi1_group");

  if (store_psi2)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::Psi2_group");

  if (store_psi3)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::Psi3_group");

  if (store_psi4)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::Psi4_group");

  if (store_I)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::invariant_I_group");

  if (store_sigma)
    ierr += CCTK_SyncGroup(cctkGH, "Psiclops::sigma_group");

  /*
   * TODO: Use
   *    int CCTK_SyncGroupsI (const cGH *GH, int n_groups, const int *groups);
   * to synchronise all grid functions in one go.
  */

  if (ierr)
    CCTK_WARN(0, "Error synchronising.");
  
  return;
}
