#ifndef _PSIDECOMP_
#define _PSIDECOMP_

#include <vector>

namespace PsiDecomp {

using namespace std;

// the slice ids for each slice and variable that are interpolated
extern vector<vector<int> > sid;
// the slice ids for each slice and variable that are NOT interpolated
extern vector<vector<int> > sid_ni;


extern int number_of_vars;

extern vector<int> spin_weight;
extern vector<string> vars;
extern vector<string> vars_ni;
extern int metric_idx;
extern int psi4_idx;

}


#endif
