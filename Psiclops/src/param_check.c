#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "psiclops.h"

void
Psiclops_ParamCheck(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  if (angular_grid_only)
  {
     if (!CCTK_IsFunctionAliased("MultiPatch_GetMap"))
     {
        CCTK_PARAMWARN("There is no Multipatch active!");
     }
  }

  return;
}
