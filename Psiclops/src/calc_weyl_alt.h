xx1
  = x1;
xx2
  = x2;
xx3
  = x3;
xmag
  = g11*Power(xx1,2) + g22*Power(xx2,2) + g33*Power(xx3,2) + 2*(g23*xx2*xx3 + xx1*(g12*xx2 + g13*xx3));
x1
  = xx1/Power(xmag,0.5);
x2
  = xx2/Power(xmag,0.5);
x3
  = xx3/Power(xmag,0.5);
zz1
  = z1 - (g11*Power(x1,2) + x1*(g12*x2 + g13*x3))*z1 - (g12*Power(x1,2) + x1*(g22*x2 + g23*x3))*z2 - (g13*Power(x1,2) + x1*(g23*x2 + g33*x3))*z3;
zz2
  = -((g12*Power(x2,2) + x2*(g11*x1 + g13*x3))*z1) + z2 - (g22*Power(x2,2) + x2*(g12*x1 + g23*x3))*z2 - (g23*Power(x2,2) + x2*(g13*x1 + g33*x3))*z3;
zz3
  = z3 - Power(x3,2)*(g13*z1 + g23*z2 + g33*z3) - x3*((g11*x1 + g12*x2)*z1 + (g12*x1 + g22*x2)*z2 + (g13*x1 + g23*x2)*z3);
zmag
  = g11*Power(zz1,2) + g22*Power(zz2,2) + g33*Power(zz3,2) + 2*(g23*zz2*zz3 + zz1*(g12*zz2 + g13*zz3));
z1
  = zz1/Power(zmag,0.5);
z2
  = zz2/Power(zmag,0.5);
z3
  = zz3/Power(zmag,0.5);
yy1
  = y1 - y1*(x1*(g12*x2 + g13*x3) + g11*(Power(x1,2) + Power(z1,2)) + z1*(g12*z2 + g13*z3)) - y2*(x1*(g22*x2 + g23*x3) + g12*(Power(x1,2) + Power(z1,2)) + z1*(g22*z2 + g23*z3)) - y3*(x1*(g23*x2 + g33*x3) + g13*(Power(x1,2) + Power(z1,2)) + z1*(g23*z2 + g33*z3));
yy2
  = y2 - (g12*y1 + g22*y2 + g23*y3)*Power(z2,2) - y1*(g12*Power(x2,2) + x2*(g11*x1 + g13*x3) + z2*(g11*z1 + g13*z3)) - y2*(g22*Power(x2,2) + x2*(g12*x1 + g23*x3) + z2*(g12*z1 + g23*z3)) - y3*(g23*Power(x2,2) + x2*(g13*x1 + g33*x3) + z2*(g13*z1 + g33*z3));
yy3
  = y3 - x3*((g11*x1 + g12*x2)*y1 + (g12*x1 + g22*x2)*y2 + (g13*x1 + g23*x2)*y3) - ((g11*y1 + g12*y2 + g13*y3)*z1 + (g12*y1 + g22*y2 + g23*y3)*z2)*z3 - (g13*y1 + g23*y2 + g33*y3)*(Power(x3,2) + Power(z3,2));
ymag
  = g11*Power(yy1,2) + g22*Power(yy2,2) + g33*Power(yy3,2) + 2*(g23*yy2*yy3 + yy1*(g12*yy2 + g13*yy3));
y1
  = yy1/Power(ymag,0.5);
y2
  = yy2/Power(ymag,0.5);
y3
  = yy3/Power(ymag,0.5);
mr1
  = y1/sqrt2;
mr2
  = y2/sqrt2;
mr3
  = y3/sqrt2;
mi1
  = x1/sqrt2;
mi2
  = x2/sqrt2;
mi3
  = x3/sqrt2;
dig111
  = -(dg111*Power(ig11,2)) - dg221*Power(ig12,2) - dg331*Power(ig13,2) - 2*(dg231*ig12*ig13 + ig11*(dg121*ig12 + dg131*ig13));
dig112
  = -(dg112*Power(ig11,2)) - dg222*Power(ig12,2) - dg332*Power(ig13,2) - 2*(dg232*ig12*ig13 + ig11*(dg122*ig12 + dg132*ig13));
dig113
  = -(dg113*Power(ig11,2)) - dg223*Power(ig12,2) - dg333*Power(ig13,2) - 2*(dg233*ig12*ig13 + ig11*(dg123*ig12 + dg133*ig13));
dig121
  = -(dg121*Power(ig12,2)) - ig11*(dg111*ig12 + dg121*ig22 + dg131*ig23) - ig12*(dg131*ig13 + dg221*ig22 + dg231*ig23) - ig13*(dg231*ig22 + dg331*ig23);
dig122
  = -(dg122*Power(ig12,2)) - ig11*(dg112*ig12 + dg122*ig22 + dg132*ig23) - ig12*(dg132*ig13 + dg222*ig22 + dg232*ig23) - ig13*(dg232*ig22 + dg332*ig23);
dig123
  = -(dg123*Power(ig12,2)) - ig11*(dg113*ig12 + dg123*ig22 + dg133*ig23) - ig12*(dg133*ig13 + dg223*ig22 + dg233*ig23) - ig13*(dg233*ig22 + dg333*ig23);
dig131
  = -(dg131*Power(ig13,2)) - ig11*(dg111*ig13 + dg121*ig23 + dg131*ig33) - ig12*(dg121*ig13 + dg221*ig23 + dg231*ig33) - ig13*(dg231*ig23 + dg331*ig33);
dig132
  = -(dg132*Power(ig13,2)) - ig11*(dg112*ig13 + dg122*ig23 + dg132*ig33) - ig12*(dg122*ig13 + dg222*ig23 + dg232*ig33) - ig13*(dg232*ig23 + dg332*ig33);
dig133
  = -(dg133*Power(ig13,2)) - ig11*(dg113*ig13 + dg123*ig23 + dg133*ig33) - ig12*(dg123*ig13 + dg223*ig23 + dg233*ig33) - ig13*(dg233*ig23 + dg333*ig33);
dig221
  = -(dg111*Power(ig12,2)) - dg221*Power(ig22,2) - dg331*Power(ig23,2) - 2*(dg231*ig22*ig23 + ig12*(dg121*ig22 + dg131*ig23));
dig222
  = -(dg112*Power(ig12,2)) - dg222*Power(ig22,2) - dg332*Power(ig23,2) - 2*(dg232*ig22*ig23 + ig12*(dg122*ig22 + dg132*ig23));
dig223
  = -(dg113*Power(ig12,2)) - dg223*Power(ig22,2) - dg333*Power(ig23,2) - 2*(dg233*ig22*ig23 + ig12*(dg123*ig22 + dg133*ig23));
dig231
  = -(dg231*Power(ig23,2)) - ig13*(dg121*ig22 + dg131*ig23) - dg331*ig23*ig33 - ig12*(dg111*ig13 + dg121*ig23 + dg131*ig33) - ig22*(dg221*ig23 + dg231*ig33);
dig232
  = -(dg232*Power(ig23,2)) - ig13*(dg122*ig22 + dg132*ig23) - dg332*ig23*ig33 - ig12*(dg112*ig13 + dg122*ig23 + dg132*ig33) - ig22*(dg222*ig23 + dg232*ig33);
dig233
  = -(dg233*Power(ig23,2)) - ig13*(dg123*ig22 + dg133*ig23) - dg333*ig23*ig33 - ig12*(dg113*ig13 + dg123*ig23 + dg133*ig33) - ig22*(dg223*ig23 + dg233*ig33);
dig331
  = -(dg111*Power(ig13,2)) - dg221*Power(ig23,2) - dg331*Power(ig33,2) - 2*(dg231*ig23*ig33 + ig13*(dg121*ig23 + dg131*ig33));
dig332
  = -(dg112*Power(ig13,2)) - dg222*Power(ig23,2) - dg332*Power(ig33,2) - 2*(dg232*ig23*ig33 + ig13*(dg122*ig23 + dg132*ig33));
dig333
  = -(dg113*Power(ig13,2)) - dg223*Power(ig23,2) - dg333*Power(ig33,2) - 2*(dg233*ig23*ig33 + ig13*(dg123*ig23 + dg133*ig33));
ChrD111
  = dg111/2.;
ChrD112
  = dg112/2.;
ChrD113
  = dg113/2.;
ChrD122
  = dg122 - dg221/2.;
ChrD123
  = (dg123 + dg132 - dg231)/2.;
ChrD133
  = dg133 - dg331/2.;
ChrD211
  = -dg112/2. + dg121;
ChrD212
  = dg221/2.;
ChrD213
  = (dg123 - dg132 + dg231)/2.;
ChrD222
  = dg222/2.;
ChrD223
  = dg223/2.;
ChrD233
  = dg233 - dg332/2.;
ChrD311
  = -dg113/2. + dg131;
ChrD312
  = (-dg123 + dg132 + dg231)/2.;
ChrD313
  = dg331/2.;
ChrD322
  = -dg223/2. + dg232;
ChrD323
  = dg332/2.;
ChrD333
  = dg333/2.;
Chr111
  = ChrD111*ig11 + ChrD211*ig12 + ChrD311*ig13;
Chr112
  = ChrD112*ig11 + ChrD212*ig12 + ChrD312*ig13;
Chr113
  = ChrD113*ig11 + ChrD213*ig12 + ChrD313*ig13;
Chr122
  = ChrD122*ig11 + ChrD222*ig12 + ChrD322*ig13;
Chr123
  = ChrD123*ig11 + ChrD223*ig12 + ChrD323*ig13;
Chr133
  = ChrD133*ig11 + ChrD233*ig12 + ChrD333*ig13;
Chr211
  = ChrD111*ig12 + ChrD211*ig22 + ChrD311*ig23;
Chr212
  = ChrD112*ig12 + ChrD212*ig22 + ChrD312*ig23;
Chr213
  = ChrD113*ig12 + ChrD213*ig22 + ChrD313*ig23;
Chr222
  = ChrD122*ig12 + ChrD222*ig22 + ChrD322*ig23;
Chr223
  = ChrD123*ig12 + ChrD223*ig22 + ChrD323*ig23;
Chr233
  = ChrD133*ig12 + ChrD233*ig22 + ChrD333*ig23;
Chr311
  = ChrD111*ig13 + ChrD211*ig23 + ChrD311*ig33;
Chr312
  = ChrD112*ig13 + ChrD212*ig23 + ChrD312*ig33;
Chr313
  = ChrD113*ig13 + ChrD213*ig23 + ChrD313*ig33;
Chr322
  = ChrD122*ig13 + ChrD222*ig23 + ChrD322*ig33;
Chr323
  = ChrD123*ig13 + ChrD223*ig23 + ChrD323*ig33;
Chr333
  = ChrD133*ig13 + ChrD233*ig23 + ChrD333*ig33;
dChrD1111
  = ddg1111/2.;
dChrD1112
  = ddg1112/2.;
dChrD1113
  = ddg1113/2.;
dChrD1121
  = ddg1121/2.;
dChrD1122
  = ddg1122/2.;
dChrD1123
  = ddg1123/2.;
dChrD1131
  = ddg1131/2.;
dChrD1132
  = ddg1132/2.;
dChrD1133
  = ddg1133/2.;
dChrD1211
  = ddg1121/2.;
dChrD1212
  = ddg1122/2.;
dChrD1213
  = ddg1123/2.;
dChrD1221
  = ddg1221 - ddg2211/2.;
dChrD1222
  = ddg1222 - ddg2212/2.;
dChrD1223
  = ddg1223 - ddg2213/2.;
dChrD1231
  = (ddg1231 + ddg1321 - ddg2311)/2.;
dChrD1232
  = (ddg1232 + ddg1322 - ddg2312)/2.;
dChrD1233
  = (ddg1233 + ddg1323 - ddg2313)/2.;
dChrD1311
  = ddg1131/2.;
dChrD1312
  = ddg1132/2.;
dChrD1313
  = ddg1133/2.;
dChrD1321
  = (ddg1231 + ddg1321 - ddg2311)/2.;
dChrD1322
  = (ddg1232 + ddg1322 - ddg2312)/2.;
dChrD1323
  = (ddg1233 + ddg1323 - ddg2313)/2.;
dChrD1331
  = ddg1331 - ddg3311/2.;
dChrD1332
  = ddg1332 - ddg3312/2.;
dChrD1333
  = ddg1333 - ddg3313/2.;
dChrD2111
  = -ddg1121/2. + ddg1211;
dChrD2112
  = -ddg1122/2. + ddg1212;
dChrD2113
  = -ddg1123/2. + ddg1213;
dChrD2121
  = ddg2211/2.;
dChrD2122
  = ddg2212/2.;
dChrD2123
  = ddg2213/2.;
dChrD2131
  = (ddg1231 - ddg1321 + ddg2311)/2.;
dChrD2132
  = (ddg1232 - ddg1322 + ddg2312)/2.;
dChrD2133
  = (ddg1233 - ddg1323 + ddg2313)/2.;
dChrD2211
  = ddg2211/2.;
dChrD2212
  = ddg2212/2.;
dChrD2213
  = ddg2213/2.;
dChrD2221
  = ddg2221/2.;
dChrD2222
  = ddg2222/2.;
dChrD2223
  = ddg2223/2.;
dChrD2231
  = ddg2231/2.;
dChrD2232
  = ddg2232/2.;
dChrD2233
  = ddg2233/2.;
dChrD2311
  = (ddg1231 - ddg1321 + ddg2311)/2.;
dChrD2312
  = (ddg1232 - ddg1322 + ddg2312)/2.;
dChrD2313
  = (ddg1233 - ddg1323 + ddg2313)/2.;
dChrD2321
  = ddg2231/2.;
dChrD2322
  = ddg2232/2.;
dChrD2323
  = ddg2233/2.;
dChrD2331
  = ddg2331 - ddg3321/2.;
dChrD2332
  = ddg2332 - ddg3322/2.;
dChrD2333
  = ddg2333 - ddg3323/2.;
dChrD3111
  = -ddg1131/2. + ddg1311;
dChrD3112
  = -ddg1132/2. + ddg1312;
dChrD3113
  = -ddg1133/2. + ddg1313;
dChrD3121
  = (-ddg1231 + ddg1321 + ddg2311)/2.;
dChrD3122
  = (-ddg1232 + ddg1322 + ddg2312)/2.;
dChrD3123
  = (-ddg1233 + ddg1323 + ddg2313)/2.;
dChrD3131
  = ddg3311/2.;
dChrD3132
  = ddg3312/2.;
dChrD3133
  = ddg3313/2.;
dChrD3211
  = (-ddg1231 + ddg1321 + ddg2311)/2.;
dChrD3212
  = (-ddg1232 + ddg1322 + ddg2312)/2.;
dChrD3213
  = (-ddg1233 + ddg1323 + ddg2313)/2.;
dChrD3221
  = -ddg2231/2. + ddg2321;
dChrD3222
  = -ddg2232/2. + ddg2322;
dChrD3223
  = -ddg2233/2. + ddg2323;
dChrD3231
  = ddg3321/2.;
dChrD3232
  = ddg3322/2.;
dChrD3233
  = ddg3323/2.;
dChrD3311
  = ddg3311/2.;
dChrD3312
  = ddg3312/2.;
dChrD3313
  = ddg3313/2.;
dChrD3321
  = ddg3321/2.;
dChrD3322
  = ddg3322/2.;
dChrD3323
  = ddg3323/2.;
dChrD3331
  = ddg3331/2.;
dChrD3332
  = ddg3332/2.;
dChrD3333
  = ddg3333/2.;
dChr1111
  = ChrD111*dig111 + ChrD211*dig121 + ChrD311*dig131 + dChrD1111*ig11 + dChrD2111*ig12 + dChrD3111*ig13;
dChr1112
  = ChrD111*dig112 + ChrD211*dig122 + ChrD311*dig132 + dChrD1112*ig11 + dChrD2112*ig12 + dChrD3112*ig13;
dChr1113
  = ChrD111*dig113 + ChrD211*dig123 + ChrD311*dig133 + dChrD1113*ig11 + dChrD2113*ig12 + dChrD3113*ig13;
dChr1121
  = ChrD112*dig111 + ChrD212*dig121 + ChrD312*dig131 + dChrD1121*ig11 + dChrD2121*ig12 + dChrD3121*ig13;
dChr1122
  = ChrD112*dig112 + ChrD212*dig122 + ChrD312*dig132 + dChrD1122*ig11 + dChrD2122*ig12 + dChrD3122*ig13;
dChr1123
  = ChrD112*dig113 + ChrD212*dig123 + ChrD312*dig133 + dChrD1123*ig11 + dChrD2123*ig12 + dChrD3123*ig13;
dChr1131
  = ChrD113*dig111 + ChrD213*dig121 + ChrD313*dig131 + dChrD1131*ig11 + dChrD2131*ig12 + dChrD3131*ig13;
dChr1132
  = ChrD113*dig112 + ChrD213*dig122 + ChrD313*dig132 + dChrD1132*ig11 + dChrD2132*ig12 + dChrD3132*ig13;
dChr1133
  = ChrD113*dig113 + ChrD213*dig123 + ChrD313*dig133 + dChrD1133*ig11 + dChrD2133*ig12 + dChrD3133*ig13;
dChr1211
  = ChrD112*dig111 + ChrD212*dig121 + ChrD312*dig131 + dChrD1211*ig11 + dChrD2211*ig12 + dChrD3211*ig13;
dChr1212
  = ChrD112*dig112 + ChrD212*dig122 + ChrD312*dig132 + dChrD1212*ig11 + dChrD2212*ig12 + dChrD3212*ig13;
dChr1213
  = ChrD112*dig113 + ChrD212*dig123 + ChrD312*dig133 + dChrD1213*ig11 + dChrD2213*ig12 + dChrD3213*ig13;
dChr1221
  = ChrD122*dig111 + ChrD222*dig121 + ChrD322*dig131 + dChrD1221*ig11 + dChrD2221*ig12 + dChrD3221*ig13;
dChr1222
  = ChrD122*dig112 + ChrD222*dig122 + ChrD322*dig132 + dChrD1222*ig11 + dChrD2222*ig12 + dChrD3222*ig13;
dChr1223
  = ChrD122*dig113 + ChrD222*dig123 + ChrD322*dig133 + dChrD1223*ig11 + dChrD2223*ig12 + dChrD3223*ig13;
dChr1231
  = ChrD123*dig111 + ChrD223*dig121 + ChrD323*dig131 + dChrD1231*ig11 + dChrD2231*ig12 + dChrD3231*ig13;
dChr1232
  = ChrD123*dig112 + ChrD223*dig122 + ChrD323*dig132 + dChrD1232*ig11 + dChrD2232*ig12 + dChrD3232*ig13;
dChr1233
  = ChrD123*dig113 + ChrD223*dig123 + ChrD323*dig133 + dChrD1233*ig11 + dChrD2233*ig12 + dChrD3233*ig13;
dChr1311
  = ChrD113*dig111 + ChrD213*dig121 + ChrD313*dig131 + dChrD1311*ig11 + dChrD2311*ig12 + dChrD3311*ig13;
dChr1312
  = ChrD113*dig112 + ChrD213*dig122 + ChrD313*dig132 + dChrD1312*ig11 + dChrD2312*ig12 + dChrD3312*ig13;
dChr1313
  = ChrD113*dig113 + ChrD213*dig123 + ChrD313*dig133 + dChrD1313*ig11 + dChrD2313*ig12 + dChrD3313*ig13;
dChr1321
  = ChrD123*dig111 + ChrD223*dig121 + ChrD323*dig131 + dChrD1321*ig11 + dChrD2321*ig12 + dChrD3321*ig13;
dChr1322
  = ChrD123*dig112 + ChrD223*dig122 + ChrD323*dig132 + dChrD1322*ig11 + dChrD2322*ig12 + dChrD3322*ig13;
dChr1323
  = ChrD123*dig113 + ChrD223*dig123 + ChrD323*dig133 + dChrD1323*ig11 + dChrD2323*ig12 + dChrD3323*ig13;
dChr1331
  = ChrD133*dig111 + ChrD233*dig121 + ChrD333*dig131 + dChrD1331*ig11 + dChrD2331*ig12 + dChrD3331*ig13;
dChr1332
  = ChrD133*dig112 + ChrD233*dig122 + ChrD333*dig132 + dChrD1332*ig11 + dChrD2332*ig12 + dChrD3332*ig13;
dChr1333
  = ChrD133*dig113 + ChrD233*dig123 + ChrD333*dig133 + dChrD1333*ig11 + dChrD2333*ig12 + dChrD3333*ig13;
dChr2111
  = ChrD111*dig121 + ChrD211*dig221 + ChrD311*dig231 + dChrD1111*ig12 + dChrD2111*ig22 + dChrD3111*ig23;
dChr2112
  = ChrD111*dig122 + ChrD211*dig222 + ChrD311*dig232 + dChrD1112*ig12 + dChrD2112*ig22 + dChrD3112*ig23;
dChr2113
  = ChrD111*dig123 + ChrD211*dig223 + ChrD311*dig233 + dChrD1113*ig12 + dChrD2113*ig22 + dChrD3113*ig23;
dChr2121
  = ChrD112*dig121 + ChrD212*dig221 + ChrD312*dig231 + dChrD1121*ig12 + dChrD2121*ig22 + dChrD3121*ig23;
dChr2122
  = ChrD112*dig122 + ChrD212*dig222 + ChrD312*dig232 + dChrD1122*ig12 + dChrD2122*ig22 + dChrD3122*ig23;
dChr2123
  = ChrD112*dig123 + ChrD212*dig223 + ChrD312*dig233 + dChrD1123*ig12 + dChrD2123*ig22 + dChrD3123*ig23;
dChr2131
  = ChrD113*dig121 + ChrD213*dig221 + ChrD313*dig231 + dChrD1131*ig12 + dChrD2131*ig22 + dChrD3131*ig23;
dChr2132
  = ChrD113*dig122 + ChrD213*dig222 + ChrD313*dig232 + dChrD1132*ig12 + dChrD2132*ig22 + dChrD3132*ig23;
dChr2133
  = ChrD113*dig123 + ChrD213*dig223 + ChrD313*dig233 + dChrD1133*ig12 + dChrD2133*ig22 + dChrD3133*ig23;
dChr2211
  = ChrD112*dig121 + ChrD212*dig221 + ChrD312*dig231 + dChrD1211*ig12 + dChrD2211*ig22 + dChrD3211*ig23;
dChr2212
  = ChrD112*dig122 + ChrD212*dig222 + ChrD312*dig232 + dChrD1212*ig12 + dChrD2212*ig22 + dChrD3212*ig23;
dChr2213
  = ChrD112*dig123 + ChrD212*dig223 + ChrD312*dig233 + dChrD1213*ig12 + dChrD2213*ig22 + dChrD3213*ig23;
dChr2221
  = ChrD122*dig121 + ChrD222*dig221 + ChrD322*dig231 + dChrD1221*ig12 + dChrD2221*ig22 + dChrD3221*ig23;
dChr2222
  = ChrD122*dig122 + ChrD222*dig222 + ChrD322*dig232 + dChrD1222*ig12 + dChrD2222*ig22 + dChrD3222*ig23;
dChr2223
  = ChrD122*dig123 + ChrD222*dig223 + ChrD322*dig233 + dChrD1223*ig12 + dChrD2223*ig22 + dChrD3223*ig23;
dChr2231
  = ChrD123*dig121 + ChrD223*dig221 + ChrD323*dig231 + dChrD1231*ig12 + dChrD2231*ig22 + dChrD3231*ig23;
dChr2232
  = ChrD123*dig122 + ChrD223*dig222 + ChrD323*dig232 + dChrD1232*ig12 + dChrD2232*ig22 + dChrD3232*ig23;
dChr2233
  = ChrD123*dig123 + ChrD223*dig223 + ChrD323*dig233 + dChrD1233*ig12 + dChrD2233*ig22 + dChrD3233*ig23;
dChr2311
  = ChrD113*dig121 + ChrD213*dig221 + ChrD313*dig231 + dChrD1311*ig12 + dChrD2311*ig22 + dChrD3311*ig23;
dChr2312
  = ChrD113*dig122 + ChrD213*dig222 + ChrD313*dig232 + dChrD1312*ig12 + dChrD2312*ig22 + dChrD3312*ig23;
dChr2313
  = ChrD113*dig123 + ChrD213*dig223 + ChrD313*dig233 + dChrD1313*ig12 + dChrD2313*ig22 + dChrD3313*ig23;
dChr2321
  = ChrD123*dig121 + ChrD223*dig221 + ChrD323*dig231 + dChrD1321*ig12 + dChrD2321*ig22 + dChrD3321*ig23;
dChr2322
  = ChrD123*dig122 + ChrD223*dig222 + ChrD323*dig232 + dChrD1322*ig12 + dChrD2322*ig22 + dChrD3322*ig23;
dChr2323
  = ChrD123*dig123 + ChrD223*dig223 + ChrD323*dig233 + dChrD1323*ig12 + dChrD2323*ig22 + dChrD3323*ig23;
dChr2331
  = ChrD133*dig121 + ChrD233*dig221 + ChrD333*dig231 + dChrD1331*ig12 + dChrD2331*ig22 + dChrD3331*ig23;
dChr2332
  = ChrD133*dig122 + ChrD233*dig222 + ChrD333*dig232 + dChrD1332*ig12 + dChrD2332*ig22 + dChrD3332*ig23;
dChr2333
  = ChrD133*dig123 + ChrD233*dig223 + ChrD333*dig233 + dChrD1333*ig12 + dChrD2333*ig22 + dChrD3333*ig23;
dChr3111
  = ChrD111*dig131 + ChrD211*dig231 + ChrD311*dig331 + dChrD1111*ig13 + dChrD2111*ig23 + dChrD3111*ig33;
dChr3112
  = ChrD111*dig132 + ChrD211*dig232 + ChrD311*dig332 + dChrD1112*ig13 + dChrD2112*ig23 + dChrD3112*ig33;
dChr3113
  = ChrD111*dig133 + ChrD211*dig233 + ChrD311*dig333 + dChrD1113*ig13 + dChrD2113*ig23 + dChrD3113*ig33;
dChr3121
  = ChrD112*dig131 + ChrD212*dig231 + ChrD312*dig331 + dChrD1121*ig13 + dChrD2121*ig23 + dChrD3121*ig33;
dChr3122
  = ChrD112*dig132 + ChrD212*dig232 + ChrD312*dig332 + dChrD1122*ig13 + dChrD2122*ig23 + dChrD3122*ig33;
dChr3123
  = ChrD112*dig133 + ChrD212*dig233 + ChrD312*dig333 + dChrD1123*ig13 + dChrD2123*ig23 + dChrD3123*ig33;
dChr3131
  = ChrD113*dig131 + ChrD213*dig231 + ChrD313*dig331 + dChrD1131*ig13 + dChrD2131*ig23 + dChrD3131*ig33;
dChr3132
  = ChrD113*dig132 + ChrD213*dig232 + ChrD313*dig332 + dChrD1132*ig13 + dChrD2132*ig23 + dChrD3132*ig33;
dChr3133
  = ChrD113*dig133 + ChrD213*dig233 + ChrD313*dig333 + dChrD1133*ig13 + dChrD2133*ig23 + dChrD3133*ig33;
dChr3211
  = ChrD112*dig131 + ChrD212*dig231 + ChrD312*dig331 + dChrD1211*ig13 + dChrD2211*ig23 + dChrD3211*ig33;
dChr3212
  = ChrD112*dig132 + ChrD212*dig232 + ChrD312*dig332 + dChrD1212*ig13 + dChrD2212*ig23 + dChrD3212*ig33;
dChr3213
  = ChrD112*dig133 + ChrD212*dig233 + ChrD312*dig333 + dChrD1213*ig13 + dChrD2213*ig23 + dChrD3213*ig33;
dChr3221
  = ChrD122*dig131 + ChrD222*dig231 + ChrD322*dig331 + dChrD1221*ig13 + dChrD2221*ig23 + dChrD3221*ig33;
dChr3222
  = ChrD122*dig132 + ChrD222*dig232 + ChrD322*dig332 + dChrD1222*ig13 + dChrD2222*ig23 + dChrD3222*ig33;
dChr3223
  = ChrD122*dig133 + ChrD222*dig233 + ChrD322*dig333 + dChrD1223*ig13 + dChrD2223*ig23 + dChrD3223*ig33;
dChr3231
  = ChrD123*dig131 + ChrD223*dig231 + ChrD323*dig331 + dChrD1231*ig13 + dChrD2231*ig23 + dChrD3231*ig33;
dChr3232
  = ChrD123*dig132 + ChrD223*dig232 + ChrD323*dig332 + dChrD1232*ig13 + dChrD2232*ig23 + dChrD3232*ig33;
dChr3233
  = ChrD123*dig133 + ChrD223*dig233 + ChrD323*dig333 + dChrD1233*ig13 + dChrD2233*ig23 + dChrD3233*ig33;
dChr3311
  = ChrD113*dig131 + ChrD213*dig231 + ChrD313*dig331 + dChrD1311*ig13 + dChrD2311*ig23 + dChrD3311*ig33;
dChr3312
  = ChrD113*dig132 + ChrD213*dig232 + ChrD313*dig332 + dChrD1312*ig13 + dChrD2312*ig23 + dChrD3312*ig33;
dChr3313
  = ChrD113*dig133 + ChrD213*dig233 + ChrD313*dig333 + dChrD1313*ig13 + dChrD2313*ig23 + dChrD3313*ig33;
dChr3321
  = ChrD123*dig131 + ChrD223*dig231 + ChrD323*dig331 + dChrD1321*ig13 + dChrD2321*ig23 + dChrD3321*ig33;
dChr3322
  = ChrD123*dig132 + ChrD223*dig232 + ChrD323*dig332 + dChrD1322*ig13 + dChrD2322*ig23 + dChrD3322*ig33;
dChr3323
  = ChrD123*dig133 + ChrD223*dig233 + ChrD323*dig333 + dChrD1323*ig13 + dChrD2323*ig23 + dChrD3323*ig33;
dChr3331
  = ChrD133*dig131 + ChrD233*dig231 + ChrD333*dig331 + dChrD1331*ig13 + dChrD2331*ig23 + dChrD3331*ig33;
dChr3332
  = ChrD133*dig132 + ChrD233*dig232 + ChrD333*dig332 + dChrD1332*ig13 + dChrD2332*ig23 + dChrD3332*ig33;
dChr3333
  = ChrD133*dig133 + ChrD233*dig233 + ChrD333*dig333 + dChrD1333*ig13 + dChrD2333*ig23 + dChrD3333*ig33;
localGam1
  = Chr111*ig11 + Chr122*ig22 + 2*(Chr112*ig12 + Chr113*ig13 + Chr123*ig23) + Chr133*ig33;
localGam2
  = Chr211*ig11 + Chr222*ig22 + 2*(Chr212*ig12 + Chr213*ig13 + Chr223*ig23) + Chr233*ig33;
localGam3
  = Chr311*ig11 + Chr322*ig22 + 2*(Chr312*ig12 + Chr313*ig13 + Chr323*ig23) + Chr333*ig33;
dGam11
  = Chr111*dig111 + Chr122*dig221 + 2*(Chr112*dig121 + Chr113*dig131 + Chr123*dig231) + Chr133*dig331 + dChr1111*ig11 + (dChr1121 + dChr1211)*ig12 + (dChr1131 + dChr1311)*ig13 + dChr1221*ig22 + (dChr1231 + dChr1321)*ig23 + dChr1331*ig33;
dGam12
  = Chr111*dig112 + Chr122*dig222 + 2*(Chr112*dig122 + Chr113*dig132 + Chr123*dig232) + Chr133*dig332 + dChr1112*ig11 + (dChr1122 + dChr1212)*ig12 + (dChr1132 + dChr1312)*ig13 + dChr1222*ig22 + (dChr1232 + dChr1322)*ig23 + dChr1332*ig33;
dGam13
  = Chr111*dig113 + Chr122*dig223 + 2*(Chr112*dig123 + Chr113*dig133 + Chr123*dig233) + Chr133*dig333 + dChr1113*ig11 + (dChr1123 + dChr1213)*ig12 + (dChr1133 + dChr1313)*ig13 + dChr1223*ig22 + (dChr1233 + dChr1323)*ig23 + dChr1333*ig33;
dGam21
  = Chr211*dig111 + Chr222*dig221 + 2*(Chr212*dig121 + Chr213*dig131 + Chr223*dig231) + Chr233*dig331 + dChr2111*ig11 + (dChr2121 + dChr2211)*ig12 + (dChr2131 + dChr2311)*ig13 + dChr2221*ig22 + (dChr2231 + dChr2321)*ig23 + dChr2331*ig33;
dGam22
  = Chr211*dig112 + Chr222*dig222 + 2*(Chr212*dig122 + Chr213*dig132 + Chr223*dig232) + Chr233*dig332 + dChr2112*ig11 + (dChr2122 + dChr2212)*ig12 + (dChr2132 + dChr2312)*ig13 + dChr2222*ig22 + (dChr2232 + dChr2322)*ig23 + dChr2332*ig33;
dGam23
  = Chr211*dig113 + Chr222*dig223 + 2*(Chr212*dig123 + Chr213*dig133 + Chr223*dig233) + Chr233*dig333 + dChr2113*ig11 + (dChr2123 + dChr2213)*ig12 + (dChr2133 + dChr2313)*ig13 + dChr2223*ig22 + (dChr2233 + dChr2323)*ig23 + dChr2333*ig33;
dGam31
  = Chr311*dig111 + Chr322*dig221 + 2*(Chr312*dig121 + Chr313*dig131 + Chr323*dig231) + Chr333*dig331 + dChr3111*ig11 + (dChr3121 + dChr3211)*ig12 + (dChr3131 + dChr3311)*ig13 + dChr3221*ig22 + (dChr3231 + dChr3321)*ig23 + dChr3331*ig33;
dGam32
  = Chr311*dig112 + Chr322*dig222 + 2*(Chr312*dig122 + Chr313*dig132 + Chr323*dig232) + Chr333*dig332 + dChr3112*ig11 + (dChr3122 + dChr3212)*ig12 + (dChr3132 + dChr3312)*ig13 + dChr3222*ig22 + (dChr3232 + dChr3322)*ig23 + dChr3332*ig33;
dGam33
  = Chr311*dig113 + Chr322*dig223 + 2*(Chr312*dig123 + Chr313*dig133 + Chr323*dig233) + Chr333*dig333 + dChr3113*ig11 + (dChr3123 + dChr3213)*ig12 + (dChr3133 + dChr3313)*ig13 + dChr3223*ig22 + (dChr3233 + dChr3323)*ig23 + dChr3333*ig33;
localK
  = ig11*K11 + ig22*K22 + 2*(ig12*K12 + ig13*K13 + ig23*K23) + ig33*K33;
KK11
  = ig11*Power(K11,2) + ig22*Power(K12,2) + ig33*Power(K13,2) + 2*(ig23*K12*K13 + K11*(ig12*K12 + ig13*K13));
KK12
  = K12*(ig11*K11 + ig22*K22) + ig12*(Power(K12,2) + K11*K22) + ig33*K13*K23 + ig13*(K12*K13 + K11*K23) + ig23*(K13*K22 + K12*K23);
KK13
  = ig22*K12*K23 + ig12*(K12*K13 + K11*K23) + K13*(ig11*K11 + ig33*K33) + ig13*(Power(K13,2) + K11*K33) + ig23*(K13*K23 + K12*K33);
KK22
  = ig11*Power(K12,2) + ig22*Power(K22,2) + ig33*Power(K23,2) + 2*(ig23*K22*K23 + K12*(ig12*K22 + ig13*K23));
KK23
  = ig11*K12*K13 + ig12*(K13*K22 + K12*K23) + K23*(ig22*K22 + ig33*K33) + ig13*(K13*K23 + K12*K33) + ig23*(Power(K23,2) + K22*K33);
KK33
  = ig11*Power(K13,2) + ig22*Power(K23,2) + ig33*Power(K33,2) + 2*(ig23*K23*K33 + K13*(ig12*K23 + ig13*K33));
DK111
  = dK111 - 2*(Chr111*K11 + Chr211*K12 + Chr311*K13);
DK112
  = dK112 - 2*(Chr112*K11 + Chr212*K12 + Chr312*K13);
DK113
  = dK113 - 2*(Chr113*K11 + Chr213*K12 + Chr313*K13);
DK121
  = dK121 - Chr112*K11 - (Chr111 + Chr212)*K12 - Chr312*K13 - Chr211*K22 - Chr311*K23;
DK122
  = dK122 - Chr122*K11 - (Chr112 + Chr222)*K12 - Chr322*K13 - Chr212*K22 - Chr312*K23;
DK123
  = dK123 - Chr123*K11 - (Chr113 + Chr223)*K12 - Chr323*K13 - Chr213*K22 - Chr313*K23;
DK131
  = dK131 - Chr113*K11 - Chr213*K12 - (Chr111 + Chr313)*K13 - Chr211*K23 - Chr311*K33;
DK132
  = dK132 - Chr123*K11 - Chr223*K12 - (Chr112 + Chr323)*K13 - Chr212*K23 - Chr312*K33;
DK133
  = dK133 - Chr133*K11 - Chr233*K12 - (Chr113 + Chr333)*K13 - Chr213*K23 - Chr313*K33;
DK221
  = dK221 - 2*(Chr112*K12 + Chr212*K22 + Chr312*K23);
DK222
  = dK222 - 2*(Chr122*K12 + Chr222*K22 + Chr322*K23);
DK223
  = dK223 - 2*(Chr123*K12 + Chr223*K22 + Chr323*K23);
DK231
  = dK231 - Chr113*K12 - Chr112*K13 - Chr213*K22 - (Chr212 + Chr313)*K23 - Chr312*K33;
DK232
  = dK232 - Chr123*K12 - Chr122*K13 - Chr223*K22 - (Chr222 + Chr323)*K23 - Chr322*K33;
DK233
  = dK233 - Chr133*K12 - Chr123*K13 - Chr233*K22 - (Chr223 + Chr333)*K23 - Chr323*K33;
DK331
  = dK331 - 2*(Chr113*K13 + Chr213*K23 + Chr313*K33);
DK332
  = dK332 - 2*(Chr123*K13 + Chr223*K23 + Chr323*K33);
DK333
  = dK333 - 2*(Chr133*K13 + Chr233*K23 + Chr333*K33);
RicGG1111
  = 3*Power(ChrD111,2)*ig11 + ChrD111*((2*ChrD112 + 4*ChrD211)*ig12 + (2*ChrD113 + 4*ChrD311)*ig13) + (2*ChrD112*ChrD211 + Power(ChrD211,2))*ig22 + 2*(ChrD112*ChrD311 + ChrD211*(ChrD113 + ChrD311))*ig23 + (2*ChrD113*ChrD311 + Power(ChrD311,2))*ig33;
RicGG1112
  = 3*ChrD111*ChrD112*ig11 + (Power(ChrD112,2) + 2*ChrD112*ChrD211 + ChrD111*(ChrD122 + 2*ChrD212))*ig12 + (ChrD112*(ChrD113 + 2*ChrD311) + ChrD111*(ChrD123 + 2*ChrD312))*ig13 + (ChrD112*ChrD212 + ChrD211*(ChrD122 + ChrD212))*ig22 + (ChrD122*ChrD311 + ChrD212*(ChrD113 + ChrD311) + ChrD112*ChrD312 + ChrD211*(ChrD123 + ChrD312))*ig23 + (ChrD113*ChrD312 + ChrD311*(ChrD123 + ChrD312))*ig33;
RicGG1113
  = 3*ChrD111*ChrD113*ig11 + (ChrD113*(ChrD112 + 2*ChrD211) + ChrD111*(ChrD123 + 2*ChrD213))*ig12 + (Power(ChrD113,2) + 2*ChrD113*ChrD311 + ChrD111*(ChrD133 + 2*ChrD313))*ig13 + (ChrD112*ChrD213 + ChrD211*(ChrD123 + ChrD213))*ig22 + (ChrD123*ChrD311 + ChrD213*(ChrD113 + ChrD311) + ChrD112*ChrD313 + ChrD211*(ChrD133 + ChrD313))*ig23 + (ChrD113*ChrD313 + ChrD311*(ChrD133 + ChrD313))*ig33;
RicGG1122
  = 3*Power(ChrD112,2)*ig11 + ChrD112*((2*ChrD122 + 4*ChrD212)*ig12 + (2*ChrD123 + 4*ChrD312)*ig13) + (2*ChrD122*ChrD212 + Power(ChrD212,2))*ig22 + 2*(ChrD122*ChrD312 + ChrD212*(ChrD123 + ChrD312))*ig23 + (2*ChrD123*ChrD312 + Power(ChrD312,2))*ig33;
RicGG1123
  = 3*ChrD112*ChrD113*ig11 + (ChrD113*(ChrD122 + 2*ChrD212) + ChrD112*(ChrD123 + 2*ChrD213))*ig12 + (ChrD113*(ChrD123 + 2*ChrD312) + ChrD112*(ChrD133 + 2*ChrD313))*ig13 + (ChrD122*ChrD213 + ChrD212*(ChrD123 + ChrD213))*ig22 + (ChrD213*ChrD312 + ChrD123*(ChrD213 + ChrD312) + ChrD122*ChrD313 + ChrD212*(ChrD133 + ChrD313))*ig23 + (ChrD123*ChrD313 + ChrD312*(ChrD133 + ChrD313))*ig33;
RicGG1133
  = 3*Power(ChrD113,2)*ig11 + ChrD113*((2*ChrD123 + 4*ChrD213)*ig12 + (2*ChrD133 + 4*ChrD313)*ig13) + (2*ChrD123*ChrD213 + Power(ChrD213,2))*ig22 + 2*(ChrD123*ChrD313 + ChrD213*(ChrD133 + ChrD313))*ig23 + (2*ChrD133*ChrD313 + Power(ChrD313,2))*ig33;
RicGG1211
  = ChrD111*(2*ChrD112 + ChrD211)*ig11 + (Power(ChrD112,2) + ChrD112*ChrD211 + Power(ChrD211,2) + 3*ChrD111*ChrD212)*ig12 + (ChrD211*ChrD311 + ChrD112*(ChrD113 + ChrD311) + ChrD111*(ChrD213 + 2*ChrD312))*ig13 + (ChrD112 + 2*ChrD211)*ChrD212*ig22 + (ChrD212*(ChrD113 + 2*ChrD311) + ChrD112*ChrD312 + ChrD211*(ChrD213 + ChrD312))*ig23 + (ChrD113*ChrD312 + ChrD311*(ChrD213 + ChrD312))*ig33;
RicGG1212
  = (Power(ChrD112,2) + (ChrD112*ChrD211)/2. + ChrD111*(ChrD122 + ChrD212/2.))*ig11 + (ChrD122*(ChrD112 + ChrD211/2.) + (2*ChrD112 + ChrD211)*ChrD212 + (3*ChrD111*ChrD222)/2.)*ig12 + ((3*ChrD112*ChrD312)/2. + (ChrD112*(ChrD123 + ChrD213) + ChrD111*ChrD223 + ChrD212*ChrD311 + ChrD122*(ChrD113 + ChrD311) + ChrD211*ChrD312)/2. + ChrD111*ChrD322)*ig13 + (Power(ChrD212,2) + ChrD211*ChrD222 + (ChrD122*ChrD212 + ChrD112*ChrD222)/2.)*ig22 + (ChrD222*ChrD311 + (3*ChrD212*ChrD312)/2. + (ChrD212*(ChrD123 + ChrD213) + ChrD113*ChrD222 + ChrD122*ChrD312 + ChrD112*ChrD322 + ChrD211*(ChrD223 + ChrD322))/2.)*ig23 + (((ChrD123 + ChrD213)*ChrD312 + Power(ChrD312,2) + ChrD113*ChrD322 + ChrD311*(ChrD223 + ChrD322))*ig33)/2.;
RicGG1213
  = (ChrD113*(ChrD112 + ChrD211/2.) + ChrD111*(ChrD123 + ChrD213/2.))*ig11 + (ChrD123*(ChrD112 + ChrD211/2.) + (ChrD112/2. + ChrD211)*ChrD213 + (3*(ChrD113*ChrD212 + ChrD111*ChrD223))/2.)*ig12 + (ChrD113*ChrD312 + (ChrD111*ChrD233 + (ChrD123 + ChrD213)*(ChrD113 + ChrD311) + ChrD211*ChrD313 + ChrD112*(ChrD133 + ChrD313))/2. + ChrD111*ChrD323)*ig13 + (ChrD212*(ChrD123/2. + ChrD213) + (ChrD112/2. + ChrD211)*ChrD223)*ig22 + (ChrD223*ChrD311 + ChrD212*ChrD313 + (ChrD133*ChrD212 + Power(ChrD213,2) + ChrD113*ChrD223 + (ChrD123 + ChrD213)*ChrD312 + ChrD112*ChrD323 + ChrD211*(ChrD233 + ChrD323))/2.)*ig23 + ((ChrD213*ChrD313 + ChrD312*(ChrD133 + ChrD313) + ChrD113*ChrD323 + ChrD311*(ChrD233 + ChrD323))*ig33)/2.;
RicGG1222
  = ChrD112*(2*ChrD122 + ChrD212)*ig11 + (Power(ChrD122,2) + ChrD122*ChrD212 + Power(ChrD212,2) + 3*ChrD112*ChrD222)*ig12 + (ChrD212*ChrD312 + ChrD122*(ChrD123 + ChrD312) + ChrD112*(ChrD223 + 2*ChrD322))*ig13 + (ChrD122 + 2*ChrD212)*ChrD222*ig22 + (ChrD222*(ChrD123 + 2*ChrD312) + ChrD122*ChrD322 + ChrD212*(ChrD223 + ChrD322))*ig23 + (ChrD123*ChrD322 + ChrD312*(ChrD223 + ChrD322))*ig33;
RicGG1223
  = (ChrD113*(ChrD122 + ChrD212/2.) + ChrD112*(ChrD123 + ChrD213/2.))*ig11 + (ChrD123*(ChrD122 + ChrD212/2.) + (ChrD122/2. + ChrD212)*ChrD213 + (3*(ChrD113*ChrD222 + ChrD112*ChrD223))/2.)*ig12 + ((Power(ChrD123,2) + ChrD113*ChrD223 + ChrD112*ChrD233 + (ChrD123 + ChrD213)*ChrD312 + ChrD212*ChrD313 + ChrD122*(ChrD133 + ChrD313))/2. + ChrD113*ChrD322 + ChrD112*ChrD323)*ig13 + ((ChrD123/2. + ChrD213)*ChrD222 + (ChrD122/2. + ChrD212)*ChrD223)*ig22 + (ChrD223*ChrD312 + ChrD222*ChrD313 + (ChrD133*ChrD222 + (ChrD123 + ChrD213)*(ChrD223 + ChrD322) + ChrD122*ChrD323 + ChrD212*(ChrD233 + ChrD323))/2.)*ig23 + ((ChrD133*ChrD322 + ChrD313*(ChrD223 + ChrD322) + ChrD123*ChrD323 + ChrD312*(ChrD233 + ChrD323))*ig33)/2.;
RicGG1233
  = ChrD113*(2*ChrD123 + ChrD213)*ig11 + (Power(ChrD123,2) + ChrD123*ChrD213 + Power(ChrD213,2) + 3*ChrD113*ChrD223)*ig12 + (ChrD213*ChrD313 + ChrD123*(ChrD133 + ChrD313) + ChrD113*(ChrD233 + 2*ChrD323))*ig13 + (ChrD123 + 2*ChrD213)*ChrD223*ig22 + (ChrD223*(ChrD133 + 2*ChrD313) + ChrD123*ChrD323 + ChrD213*(ChrD233 + ChrD323))*ig23 + (ChrD133*ChrD323 + ChrD313*(ChrD233 + ChrD323))*ig33;
RicGG1311
  = ChrD111*(2*ChrD113 + ChrD311)*ig11 + (ChrD113*(ChrD112 + ChrD211) + ChrD211*ChrD311 + ChrD111*(2*ChrD213 + ChrD312))*ig12 + (Power(ChrD113,2) + ChrD113*ChrD311 + Power(ChrD311,2) + 3*ChrD111*ChrD313)*ig13 + ((ChrD112 + ChrD211)*ChrD213 + ChrD211*ChrD312)*ig22 + (ChrD213*(ChrD113 + ChrD311) + ChrD311*ChrD312 + (ChrD112 + 2*ChrD211)*ChrD313)*ig23 + (ChrD113 + 2*ChrD311)*ChrD313*ig33;
RicGG1312
  = (ChrD112*(ChrD113 + ChrD311/2.) + ChrD111*(ChrD123 + ChrD312/2.))*ig11 + (ChrD112*(ChrD213 + ChrD312/2.) + (ChrD123*(ChrD112 + ChrD211) + ChrD113*(ChrD122 + ChrD212) + ChrD212*ChrD311 + ChrD211*ChrD312)/2. + ChrD111*(ChrD223 + ChrD322/2.))*ig12 + (ChrD123*(ChrD113 + ChrD311/2.) + (ChrD113/2. + ChrD311)*ChrD312 + (3*(ChrD112*ChrD313 + ChrD111*ChrD323))/2.)*ig13 + (((ChrD122 + ChrD212)*ChrD213 + (ChrD112 + ChrD211)*ChrD223 + ChrD212*ChrD312 + ChrD211*ChrD322)*ig22)/2. + (ChrD212*ChrD313 + ChrD211*ChrD323 + (ChrD223*(ChrD113 + ChrD311) + Power(ChrD312,2) + ChrD213*(ChrD123 + ChrD312) + ChrD122*ChrD313 + ChrD311*ChrD322 + ChrD112*ChrD323)/2.)*ig23 + ((ChrD123/2. + ChrD312)*ChrD313 + (ChrD113/2. + ChrD311)*ChrD323)*ig33;
RicGG1313
  = (Power(ChrD113,2) + (ChrD113*ChrD311)/2. + ChrD111*(ChrD133 + ChrD313/2.))*ig11 + (ChrD113*((3*ChrD213)/2. + ChrD312/2.) + (ChrD113*ChrD123 + ChrD133*(ChrD112 + ChrD211) + ChrD213*ChrD311 + ChrD211*ChrD313)/2. + ChrD111*(ChrD233 + ChrD323/2.))*ig12 + (ChrD133*(ChrD113 + ChrD311/2.) + (2*ChrD113 + ChrD311)*ChrD313 + (3*ChrD111*ChrD333)/2.)*ig13 + ((Power(ChrD213,2) + (ChrD112 + ChrD211)*ChrD233 + ChrD213*(ChrD123 + ChrD312) + ChrD211*ChrD323)*ig22)/2. + (((3*ChrD213)/2. + ChrD312/2.)*ChrD313 + ChrD211*ChrD333 + (ChrD133*ChrD213 + ChrD233*(ChrD113 + ChrD311) + ChrD123*ChrD313 + ChrD311*ChrD323 + ChrD112*ChrD333)/2.)*ig23 + (Power(ChrD313,2) + ChrD311*ChrD333 + (ChrD133*ChrD313 + ChrD113*ChrD333)/2.)*ig33;
RicGG1322
  = ChrD112*(2*ChrD123 + ChrD312)*ig11 + (ChrD123*(ChrD122 + ChrD212) + ChrD212*ChrD312 + ChrD112*(2*ChrD223 + ChrD322))*ig12 + (Power(ChrD123,2) + ChrD123*ChrD312 + Power(ChrD312,2) + 3*ChrD112*ChrD323)*ig13 + ((ChrD122 + ChrD212)*ChrD223 + ChrD212*ChrD322)*ig22 + (ChrD223*(ChrD123 + ChrD312) + ChrD312*ChrD322 + (ChrD122 + 2*ChrD212)*ChrD323)*ig23 + (ChrD123 + 2*ChrD312)*ChrD323*ig33;
RicGG1323
  = (ChrD113*(ChrD123 + ChrD312/2.) + ChrD112*(ChrD133 + ChrD313/2.))*ig11 + ((Power(ChrD123,2) + ChrD133*(ChrD122 + ChrD212) + ChrD213*(ChrD123 + ChrD312) + ChrD212*ChrD313)/2. + ChrD113*(ChrD223 + ChrD322/2.) + ChrD112*(ChrD233 + ChrD323/2.))*ig12 + (ChrD133*(ChrD123 + ChrD312/2.) + (ChrD123/2. + ChrD312)*ChrD313 + (3*(ChrD113*ChrD323 + ChrD112*ChrD333))/2.)*ig13 + (((ChrD123 + ChrD213)*ChrD223 + (ChrD122 + ChrD212)*ChrD233 + ChrD213*ChrD322 + ChrD212*ChrD323)*ig22)/2. + ((ChrD213 + ChrD312/2.)*ChrD323 + ChrD212*ChrD333 + (ChrD233*(ChrD123 + ChrD312) + ChrD223*(ChrD133 + ChrD313) + ChrD313*ChrD322 + ChrD123*ChrD323 + ChrD122*ChrD333)/2.)*ig23 + ((ChrD133/2. + ChrD313)*ChrD323 + (ChrD123/2. + ChrD312)*ChrD333)*ig33;
RicGG1333
  = ChrD113*(2*ChrD133 + ChrD313)*ig11 + (ChrD133*(ChrD123 + ChrD213) + ChrD213*ChrD313 + ChrD113*(2*ChrD233 + ChrD323))*ig12 + (Power(ChrD133,2) + ChrD133*ChrD313 + Power(ChrD313,2) + 3*ChrD113*ChrD333)*ig13 + ((ChrD123 + ChrD213)*ChrD233 + ChrD213*ChrD323)*ig22 + (ChrD233*(ChrD133 + ChrD313) + ChrD313*ChrD323 + (ChrD123 + 2*ChrD213)*ChrD333)*ig23 + (ChrD133 + 2*ChrD313)*ChrD333*ig33;
RicGG2111
  = ChrD111*(2*ChrD112 + ChrD211)*ig11 + (Power(ChrD112,2) + ChrD112*ChrD211 + Power(ChrD211,2) + 3*ChrD111*ChrD212)*ig12 + (ChrD211*ChrD311 + ChrD112*(ChrD113 + ChrD311) + ChrD111*(ChrD213 + 2*ChrD312))*ig13 + (ChrD112 + 2*ChrD211)*ChrD212*ig22 + (ChrD212*(ChrD113 + 2*ChrD311) + ChrD112*ChrD312 + ChrD211*(ChrD213 + ChrD312))*ig23 + (ChrD113*ChrD312 + ChrD311*(ChrD213 + ChrD312))*ig33;
RicGG2112
  = (Power(ChrD112,2) + (ChrD112*ChrD211)/2. + ChrD111*(ChrD122 + ChrD212/2.))*ig11 + (ChrD122*(ChrD112 + ChrD211/2.) + (2*ChrD112 + ChrD211)*ChrD212 + (3*ChrD111*ChrD222)/2.)*ig12 + ((3*ChrD112*ChrD312)/2. + (ChrD112*(ChrD123 + ChrD213) + ChrD111*ChrD223 + ChrD212*ChrD311 + ChrD122*(ChrD113 + ChrD311) + ChrD211*ChrD312)/2. + ChrD111*ChrD322)*ig13 + (Power(ChrD212,2) + ChrD211*ChrD222 + (ChrD122*ChrD212 + ChrD112*ChrD222)/2.)*ig22 + (ChrD222*ChrD311 + (3*ChrD212*ChrD312)/2. + (ChrD212*(ChrD123 + ChrD213) + ChrD113*ChrD222 + ChrD122*ChrD312 + ChrD112*ChrD322 + ChrD211*(ChrD223 + ChrD322))/2.)*ig23 + (((ChrD123 + ChrD213)*ChrD312 + Power(ChrD312,2) + ChrD113*ChrD322 + ChrD311*(ChrD223 + ChrD322))*ig33)/2.;
RicGG2113
  = (ChrD113*(ChrD112 + ChrD211/2.) + ChrD111*(ChrD123 + ChrD213/2.))*ig11 + (ChrD123*(ChrD112 + ChrD211/2.) + (ChrD112/2. + ChrD211)*ChrD213 + (3*(ChrD113*ChrD212 + ChrD111*ChrD223))/2.)*ig12 + (ChrD113*ChrD312 + (ChrD111*ChrD233 + (ChrD123 + ChrD213)*(ChrD113 + ChrD311) + ChrD211*ChrD313 + ChrD112*(ChrD133 + ChrD313))/2. + ChrD111*ChrD323)*ig13 + (ChrD212*(ChrD123/2. + ChrD213) + (ChrD112/2. + ChrD211)*ChrD223)*ig22 + (ChrD223*ChrD311 + ChrD212*ChrD313 + (ChrD133*ChrD212 + Power(ChrD213,2) + ChrD113*ChrD223 + (ChrD123 + ChrD213)*ChrD312 + ChrD112*ChrD323 + ChrD211*(ChrD233 + ChrD323))/2.)*ig23 + ((ChrD213*ChrD313 + ChrD312*(ChrD133 + ChrD313) + ChrD113*ChrD323 + ChrD311*(ChrD233 + ChrD323))*ig33)/2.;
RicGG2122
  = ChrD112*(2*ChrD122 + ChrD212)*ig11 + (Power(ChrD122,2) + ChrD122*ChrD212 + Power(ChrD212,2) + 3*ChrD112*ChrD222)*ig12 + (ChrD212*ChrD312 + ChrD122*(ChrD123 + ChrD312) + ChrD112*(ChrD223 + 2*ChrD322))*ig13 + (ChrD122 + 2*ChrD212)*ChrD222*ig22 + (ChrD222*(ChrD123 + 2*ChrD312) + ChrD122*ChrD322 + ChrD212*(ChrD223 + ChrD322))*ig23 + (ChrD123*ChrD322 + ChrD312*(ChrD223 + ChrD322))*ig33;
RicGG2123
  = (ChrD113*(ChrD122 + ChrD212/2.) + ChrD112*(ChrD123 + ChrD213/2.))*ig11 + (ChrD123*(ChrD122 + ChrD212/2.) + (ChrD122/2. + ChrD212)*ChrD213 + (3*(ChrD113*ChrD222 + ChrD112*ChrD223))/2.)*ig12 + ((Power(ChrD123,2) + ChrD113*ChrD223 + ChrD112*ChrD233 + (ChrD123 + ChrD213)*ChrD312 + ChrD212*ChrD313 + ChrD122*(ChrD133 + ChrD313))/2. + ChrD113*ChrD322 + ChrD112*ChrD323)*ig13 + ((ChrD123/2. + ChrD213)*ChrD222 + (ChrD122/2. + ChrD212)*ChrD223)*ig22 + (ChrD223*ChrD312 + ChrD222*ChrD313 + (ChrD133*ChrD222 + (ChrD123 + ChrD213)*(ChrD223 + ChrD322) + ChrD122*ChrD323 + ChrD212*(ChrD233 + ChrD323))/2.)*ig23 + ((ChrD133*ChrD322 + ChrD313*(ChrD223 + ChrD322) + ChrD123*ChrD323 + ChrD312*(ChrD233 + ChrD323))*ig33)/2.;
RicGG2133
  = ChrD113*(2*ChrD123 + ChrD213)*ig11 + (Power(ChrD123,2) + ChrD123*ChrD213 + Power(ChrD213,2) + 3*ChrD113*ChrD223)*ig12 + (ChrD213*ChrD313 + ChrD123*(ChrD133 + ChrD313) + ChrD113*(ChrD233 + 2*ChrD323))*ig13 + (ChrD123 + 2*ChrD213)*ChrD223*ig22 + (ChrD223*(ChrD133 + 2*ChrD313) + ChrD123*ChrD323 + ChrD213*(ChrD233 + ChrD323))*ig23 + (ChrD133*ChrD323 + ChrD313*(ChrD233 + ChrD323))*ig33;
RicGG2211
  = (Power(ChrD112,2) + 2*ChrD112*ChrD211)*ig11 + 2*(ChrD211*ChrD312 + ChrD112*(ChrD213 + ChrD312))*ig13 + 3*Power(ChrD212,2)*ig22 + ChrD212*((4*ChrD112 + 2*ChrD211)*ig12 + (2*ChrD213 + 4*ChrD312)*ig23) + (2*ChrD213*ChrD312 + Power(ChrD312,2))*ig33;
RicGG2212
  = (ChrD122*(ChrD112 + ChrD211) + ChrD112*ChrD212)*ig11 + (Power(ChrD212,2) + ChrD211*ChrD222 + 2*(ChrD122*ChrD212 + ChrD112*ChrD222))*ig12 + (ChrD212*ChrD312 + ChrD122*(ChrD213 + ChrD312) + ChrD211*ChrD322 + ChrD112*(ChrD223 + ChrD322))*ig13 + 3*ChrD212*ChrD222*ig22 + (ChrD222*(ChrD213 + 2*ChrD312) + ChrD212*(ChrD223 + 2*ChrD322))*ig23 + (ChrD213*ChrD322 + ChrD312*(ChrD223 + ChrD322))*ig33;
RicGG2213
  = (ChrD123*(ChrD112 + ChrD211) + ChrD112*ChrD213)*ig11 + (ChrD212*(2*ChrD123 + ChrD213) + (2*ChrD112 + ChrD211)*ChrD223)*ig12 + (ChrD213*ChrD312 + ChrD123*(ChrD213 + ChrD312) + ChrD211*ChrD323 + ChrD112*(ChrD233 + ChrD323))*ig13 + 3*ChrD212*ChrD223*ig22 + (ChrD223*(ChrD213 + 2*ChrD312) + ChrD212*(ChrD233 + 2*ChrD323))*ig23 + (ChrD213*ChrD323 + ChrD312*(ChrD233 + ChrD323))*ig33;
RicGG2222
  = (Power(ChrD122,2) + 2*ChrD122*ChrD212)*ig11 + 2*(ChrD212*ChrD322 + ChrD122*(ChrD223 + ChrD322))*ig13 + 3*Power(ChrD222,2)*ig22 + ChrD222*((4*ChrD122 + 2*ChrD212)*ig12 + (2*ChrD223 + 4*ChrD322)*ig23) + (2*ChrD223*ChrD322 + Power(ChrD322,2))*ig33;
RicGG2223
  = (ChrD123*(ChrD122 + ChrD212) + ChrD122*ChrD213)*ig11 + ((2*ChrD123 + ChrD213)*ChrD222 + (2*ChrD122 + ChrD212)*ChrD223)*ig12 + (ChrD213*ChrD322 + ChrD123*(ChrD223 + ChrD322) + ChrD212*ChrD323 + ChrD122*(ChrD233 + ChrD323))*ig13 + 3*ChrD222*ChrD223*ig22 + (Power(ChrD223,2) + 2*ChrD223*ChrD322 + ChrD222*(ChrD233 + 2*ChrD323))*ig23 + (ChrD223*ChrD323 + ChrD322*(ChrD233 + ChrD323))*ig33;
RicGG2233
  = (Power(ChrD123,2) + 2*ChrD123*ChrD213)*ig11 + 2*(ChrD213*ChrD323 + ChrD123*(ChrD233 + ChrD323))*ig13 + 3*Power(ChrD223,2)*ig22 + ChrD223*((4*ChrD123 + 2*ChrD213)*ig12 + (2*ChrD233 + 4*ChrD323)*ig23) + (2*ChrD233*ChrD323 + Power(ChrD323,2))*ig33;
RicGG2311
  = (ChrD113*(ChrD112 + ChrD211) + ChrD112*ChrD311)*ig11 + ((ChrD112 + ChrD211)*ChrD213 + ChrD212*(2*ChrD113 + ChrD311) + ChrD112*ChrD312)*ig12 + (ChrD311*ChrD312 + ChrD113*(ChrD213 + ChrD312) + (2*ChrD112 + ChrD211)*ChrD313)*ig13 + ChrD212*(2*ChrD213 + ChrD312)*ig22 + (Power(ChrD213,2) + ChrD213*ChrD312 + Power(ChrD312,2) + 3*ChrD212*ChrD313)*ig23 + (ChrD213 + 2*ChrD312)*ChrD313*ig33;
RicGG2312
  = ((ChrD123*(ChrD112 + ChrD211) + ChrD113*(ChrD122 + ChrD212) + ChrD122*ChrD311 + ChrD112*ChrD312)*ig11)/2. + (ChrD222*(ChrD113 + ChrD311/2.) + ChrD212*(ChrD123 + (ChrD213 + ChrD312)/2.) + ((ChrD112 + ChrD211)*ChrD223 + ChrD122*(ChrD213 + ChrD312) + ChrD112*ChrD322)/2.)*ig12 + (ChrD122*ChrD313 + ChrD112*ChrD323 + (Power(ChrD312,2) + ChrD123*(ChrD213 + ChrD312) + ChrD212*ChrD313 + ChrD311*ChrD322 + ChrD113*(ChrD223 + ChrD322) + ChrD211*ChrD323)/2.)*ig13 + (ChrD222*(ChrD213 + ChrD312/2.) + ChrD212*(ChrD223 + ChrD322/2.))*ig22 + (ChrD223*(ChrD213 + ChrD312/2.) + (ChrD213/2. + ChrD312)*ChrD322 + (3*(ChrD222*ChrD313 + ChrD212*ChrD323))/2.)*ig23 + (ChrD313*(ChrD223/2. + ChrD322) + (ChrD213/2. + ChrD312)*ChrD323)*ig33;
RicGG2313
  = ((ChrD133*(ChrD112 + ChrD211) + ChrD113*(ChrD123 + ChrD213) + ChrD123*ChrD311 + ChrD112*ChrD313)*ig11)/2. + (ChrD223*(ChrD113 + ChrD311/2.) + ChrD212*(ChrD133 + ChrD313/2.) + (Power(ChrD213,2) + (ChrD112 + ChrD211)*ChrD233 + ChrD123*(ChrD213 + ChrD312) + ChrD112*ChrD323)/2.)*ig12 + ((ChrD123 + ChrD312/2.)*ChrD313 + ChrD112*ChrD333 + (ChrD133*(ChrD213 + ChrD312) + ChrD213*ChrD313 + ChrD311*ChrD323 + ChrD113*(ChrD233 + ChrD323) + ChrD211*ChrD333)/2.)*ig13 + (ChrD223*(ChrD213 + ChrD312/2.) + ChrD212*(ChrD233 + ChrD323/2.))*ig22 + (ChrD233*(ChrD213 + ChrD312/2.) + (ChrD213/2. + ChrD312)*ChrD323 + (3*(ChrD223*ChrD313 + ChrD212*ChrD333))/2.)*ig23 + (ChrD313*(ChrD233/2. + ChrD323) + (ChrD213/2. + ChrD312)*ChrD333)*ig33;
RicGG2322
  = (ChrD123*(ChrD122 + ChrD212) + ChrD122*ChrD312)*ig11 + ((ChrD122 + ChrD212)*ChrD223 + ChrD222*(2*ChrD123 + ChrD312) + ChrD122*ChrD322)*ig12 + (ChrD312*ChrD322 + ChrD123*(ChrD223 + ChrD322) + (2*ChrD122 + ChrD212)*ChrD323)*ig13 + ChrD222*(2*ChrD223 + ChrD322)*ig22 + (Power(ChrD223,2) + ChrD223*ChrD322 + Power(ChrD322,2) + 3*ChrD222*ChrD323)*ig23 + (ChrD223 + 2*ChrD322)*ChrD323*ig33;
RicGG2323
  = ((Power(ChrD123,2) + ChrD133*(ChrD122 + ChrD212) + ChrD123*(ChrD213 + ChrD312) + ChrD122*ChrD313)*ig11)/2. + (ChrD223*((3*ChrD123)/2. + (ChrD213 + ChrD312)/2.) + ChrD222*(ChrD133 + ChrD313/2.) + ((ChrD122 + ChrD212)*ChrD233 + ChrD123*ChrD322 + ChrD122*ChrD323)/2.)*ig12 + (((3*ChrD123)/2. + ChrD312/2.)*ChrD323 + ChrD122*ChrD333 + (ChrD123*ChrD233 + ChrD313*ChrD322 + ChrD133*(ChrD223 + ChrD322) + ChrD213*ChrD323 + ChrD212*ChrD333)/2.)*ig13 + (Power(ChrD223,2) + (ChrD223*ChrD322)/2. + ChrD222*(ChrD233 + ChrD323/2.))*ig22 + (ChrD233*(ChrD223 + ChrD322/2.) + (2*ChrD223 + ChrD322)*ChrD323 + (3*ChrD222*ChrD333)/2.)*ig23 + (Power(ChrD323,2) + ChrD322*ChrD333 + (ChrD233*ChrD323 + ChrD223*ChrD333)/2.)*ig33;
RicGG2333
  = (ChrD133*(ChrD123 + ChrD213) + ChrD123*ChrD313)*ig11 + ((ChrD123 + ChrD213)*ChrD233 + ChrD223*(2*ChrD133 + ChrD313) + ChrD123*ChrD323)*ig12 + (ChrD313*ChrD323 + ChrD133*(ChrD233 + ChrD323) + (2*ChrD123 + ChrD213)*ChrD333)*ig13 + ChrD223*(2*ChrD233 + ChrD323)*ig22 + (Power(ChrD233,2) + ChrD233*ChrD323 + Power(ChrD323,2) + 3*ChrD223*ChrD333)*ig23 + (ChrD233 + 2*ChrD323)*ChrD333*ig33;
RicGG3111
  = ChrD111*(2*ChrD113 + ChrD311)*ig11 + (ChrD113*(ChrD112 + ChrD211) + ChrD211*ChrD311 + ChrD111*(2*ChrD213 + ChrD312))*ig12 + (Power(ChrD113,2) + ChrD113*ChrD311 + Power(ChrD311,2) + 3*ChrD111*ChrD313)*ig13 + ((ChrD112 + ChrD211)*ChrD213 + ChrD211*ChrD312)*ig22 + (ChrD213*(ChrD113 + ChrD311) + ChrD311*ChrD312 + (ChrD112 + 2*ChrD211)*ChrD313)*ig23 + (ChrD113 + 2*ChrD311)*ChrD313*ig33;
RicGG3112
  = (ChrD112*(ChrD113 + ChrD311/2.) + ChrD111*(ChrD123 + ChrD312/2.))*ig11 + (ChrD112*(ChrD213 + ChrD312/2.) + (ChrD123*(ChrD112 + ChrD211) + ChrD113*(ChrD122 + ChrD212) + ChrD212*ChrD311 + ChrD211*ChrD312)/2. + ChrD111*(ChrD223 + ChrD322/2.))*ig12 + (ChrD123*(ChrD113 + ChrD311/2.) + (ChrD113/2. + ChrD311)*ChrD312 + (3*(ChrD112*ChrD313 + ChrD111*ChrD323))/2.)*ig13 + (((ChrD122 + ChrD212)*ChrD213 + (ChrD112 + ChrD211)*ChrD223 + ChrD212*ChrD312 + ChrD211*ChrD322)*ig22)/2. + (ChrD212*ChrD313 + ChrD211*ChrD323 + (ChrD223*(ChrD113 + ChrD311) + Power(ChrD312,2) + ChrD213*(ChrD123 + ChrD312) + ChrD122*ChrD313 + ChrD311*ChrD322 + ChrD112*ChrD323)/2.)*ig23 + ((ChrD123/2. + ChrD312)*ChrD313 + (ChrD113/2. + ChrD311)*ChrD323)*ig33;
RicGG3113
  = (Power(ChrD113,2) + (ChrD113*ChrD311)/2. + ChrD111*(ChrD133 + ChrD313/2.))*ig11 + (ChrD113*((3*ChrD213)/2. + ChrD312/2.) + (ChrD113*ChrD123 + ChrD133*(ChrD112 + ChrD211) + ChrD213*ChrD311 + ChrD211*ChrD313)/2. + ChrD111*(ChrD233 + ChrD323/2.))*ig12 + (ChrD133*(ChrD113 + ChrD311/2.) + (2*ChrD113 + ChrD311)*ChrD313 + (3*ChrD111*ChrD333)/2.)*ig13 + ((Power(ChrD213,2) + (ChrD112 + ChrD211)*ChrD233 + ChrD213*(ChrD123 + ChrD312) + ChrD211*ChrD323)*ig22)/2. + (((3*ChrD213)/2. + ChrD312/2.)*ChrD313 + ChrD211*ChrD333 + (ChrD133*ChrD213 + ChrD233*(ChrD113 + ChrD311) + ChrD123*ChrD313 + ChrD311*ChrD323 + ChrD112*ChrD333)/2.)*ig23 + (Power(ChrD313,2) + ChrD311*ChrD333 + (ChrD133*ChrD313 + ChrD113*ChrD333)/2.)*ig33;
RicGG3122
  = ChrD112*(2*ChrD123 + ChrD312)*ig11 + (ChrD123*(ChrD122 + ChrD212) + ChrD212*ChrD312 + ChrD112*(2*ChrD223 + ChrD322))*ig12 + (Power(ChrD123,2) + ChrD123*ChrD312 + Power(ChrD312,2) + 3*ChrD112*ChrD323)*ig13 + ((ChrD122 + ChrD212)*ChrD223 + ChrD212*ChrD322)*ig22 + (ChrD223*(ChrD123 + ChrD312) + ChrD312*ChrD322 + (ChrD122 + 2*ChrD212)*ChrD323)*ig23 + (ChrD123 + 2*ChrD312)*ChrD323*ig33;
RicGG3123
  = (ChrD113*(ChrD123 + ChrD312/2.) + ChrD112*(ChrD133 + ChrD313/2.))*ig11 + ((Power(ChrD123,2) + ChrD133*(ChrD122 + ChrD212) + ChrD213*(ChrD123 + ChrD312) + ChrD212*ChrD313)/2. + ChrD113*(ChrD223 + ChrD322/2.) + ChrD112*(ChrD233 + ChrD323/2.))*ig12 + (ChrD133*(ChrD123 + ChrD312/2.) + (ChrD123/2. + ChrD312)*ChrD313 + (3*(ChrD113*ChrD323 + ChrD112*ChrD333))/2.)*ig13 + (((ChrD123 + ChrD213)*ChrD223 + (ChrD122 + ChrD212)*ChrD233 + ChrD213*ChrD322 + ChrD212*ChrD323)*ig22)/2. + ((ChrD213 + ChrD312/2.)*ChrD323 + ChrD212*ChrD333 + (ChrD233*(ChrD123 + ChrD312) + ChrD223*(ChrD133 + ChrD313) + ChrD313*ChrD322 + ChrD123*ChrD323 + ChrD122*ChrD333)/2.)*ig23 + ((ChrD133/2. + ChrD313)*ChrD323 + (ChrD123/2. + ChrD312)*ChrD333)*ig33;
RicGG3133
  = ChrD113*(2*ChrD133 + ChrD313)*ig11 + (ChrD133*(ChrD123 + ChrD213) + ChrD213*ChrD313 + ChrD113*(2*ChrD233 + ChrD323))*ig12 + (Power(ChrD133,2) + ChrD133*ChrD313 + Power(ChrD313,2) + 3*ChrD113*ChrD333)*ig13 + ((ChrD123 + ChrD213)*ChrD233 + ChrD213*ChrD323)*ig22 + (ChrD233*(ChrD133 + ChrD313) + ChrD313*ChrD323 + (ChrD123 + 2*ChrD213)*ChrD333)*ig23 + (ChrD133 + 2*ChrD313)*ChrD333*ig33;
RicGG3211
  = (ChrD113*(ChrD112 + ChrD211) + ChrD112*ChrD311)*ig11 + ((ChrD112 + ChrD211)*ChrD213 + ChrD212*(2*ChrD113 + ChrD311) + ChrD112*ChrD312)*ig12 + (ChrD311*ChrD312 + ChrD113*(ChrD213 + ChrD312) + (2*ChrD112 + ChrD211)*ChrD313)*ig13 + ChrD212*(2*ChrD213 + ChrD312)*ig22 + (Power(ChrD213,2) + ChrD213*ChrD312 + Power(ChrD312,2) + 3*ChrD212*ChrD313)*ig23 + (ChrD213 + 2*ChrD312)*ChrD313*ig33;
RicGG3212
  = ((ChrD123*(ChrD112 + ChrD211) + ChrD113*(ChrD122 + ChrD212) + ChrD122*ChrD311 + ChrD112*ChrD312)*ig11)/2. + (ChrD222*(ChrD113 + ChrD311/2.) + ChrD212*(ChrD123 + (ChrD213 + ChrD312)/2.) + ((ChrD112 + ChrD211)*ChrD223 + ChrD122*(ChrD213 + ChrD312) + ChrD112*ChrD322)/2.)*ig12 + (ChrD122*ChrD313 + ChrD112*ChrD323 + (Power(ChrD312,2) + ChrD123*(ChrD213 + ChrD312) + ChrD212*ChrD313 + ChrD311*ChrD322 + ChrD113*(ChrD223 + ChrD322) + ChrD211*ChrD323)/2.)*ig13 + (ChrD222*(ChrD213 + ChrD312/2.) + ChrD212*(ChrD223 + ChrD322/2.))*ig22 + (ChrD223*(ChrD213 + ChrD312/2.) + (ChrD213/2. + ChrD312)*ChrD322 + (3*(ChrD222*ChrD313 + ChrD212*ChrD323))/2.)*ig23 + (ChrD313*(ChrD223/2. + ChrD322) + (ChrD213/2. + ChrD312)*ChrD323)*ig33;
RicGG3213
  = ((ChrD133*(ChrD112 + ChrD211) + ChrD113*(ChrD123 + ChrD213) + ChrD123*ChrD311 + ChrD112*ChrD313)*ig11)/2. + (ChrD223*(ChrD113 + ChrD311/2.) + ChrD212*(ChrD133 + ChrD313/2.) + (Power(ChrD213,2) + (ChrD112 + ChrD211)*ChrD233 + ChrD123*(ChrD213 + ChrD312) + ChrD112*ChrD323)/2.)*ig12 + ((ChrD123 + ChrD312/2.)*ChrD313 + ChrD112*ChrD333 + (ChrD133*(ChrD213 + ChrD312) + ChrD213*ChrD313 + ChrD311*ChrD323 + ChrD113*(ChrD233 + ChrD323) + ChrD211*ChrD333)/2.)*ig13 + (ChrD223*(ChrD213 + ChrD312/2.) + ChrD212*(ChrD233 + ChrD323/2.))*ig22 + (ChrD233*(ChrD213 + ChrD312/2.) + (ChrD213/2. + ChrD312)*ChrD323 + (3*(ChrD223*ChrD313 + ChrD212*ChrD333))/2.)*ig23 + (ChrD313*(ChrD233/2. + ChrD323) + (ChrD213/2. + ChrD312)*ChrD333)*ig33;
RicGG3222
  = (ChrD123*(ChrD122 + ChrD212) + ChrD122*ChrD312)*ig11 + ((ChrD122 + ChrD212)*ChrD223 + ChrD222*(2*ChrD123 + ChrD312) + ChrD122*ChrD322)*ig12 + (ChrD312*ChrD322 + ChrD123*(ChrD223 + ChrD322) + (2*ChrD122 + ChrD212)*ChrD323)*ig13 + ChrD222*(2*ChrD223 + ChrD322)*ig22 + (Power(ChrD223,2) + ChrD223*ChrD322 + Power(ChrD322,2) + 3*ChrD222*ChrD323)*ig23 + (ChrD223 + 2*ChrD322)*ChrD323*ig33;
RicGG3223
  = ((Power(ChrD123,2) + ChrD133*(ChrD122 + ChrD212) + ChrD123*(ChrD213 + ChrD312) + ChrD122*ChrD313)*ig11)/2. + (ChrD223*((3*ChrD123)/2. + (ChrD213 + ChrD312)/2.) + ChrD222*(ChrD133 + ChrD313/2.) + ((ChrD122 + ChrD212)*ChrD233 + ChrD123*ChrD322 + ChrD122*ChrD323)/2.)*ig12 + (((3*ChrD123)/2. + ChrD312/2.)*ChrD323 + ChrD122*ChrD333 + (ChrD123*ChrD233 + ChrD313*ChrD322 + ChrD133*(ChrD223 + ChrD322) + ChrD213*ChrD323 + ChrD212*ChrD333)/2.)*ig13 + (Power(ChrD223,2) + (ChrD223*ChrD322)/2. + ChrD222*(ChrD233 + ChrD323/2.))*ig22 + (ChrD233*(ChrD223 + ChrD322/2.) + (2*ChrD223 + ChrD322)*ChrD323 + (3*ChrD222*ChrD333)/2.)*ig23 + (Power(ChrD323,2) + ChrD322*ChrD333 + (ChrD233*ChrD323 + ChrD223*ChrD333)/2.)*ig33;
RicGG3233
  = (ChrD133*(ChrD123 + ChrD213) + ChrD123*ChrD313)*ig11 + ((ChrD123 + ChrD213)*ChrD233 + ChrD223*(2*ChrD133 + ChrD313) + ChrD123*ChrD323)*ig12 + (ChrD313*ChrD323 + ChrD133*(ChrD233 + ChrD323) + (2*ChrD123 + ChrD213)*ChrD333)*ig13 + ChrD223*(2*ChrD233 + ChrD323)*ig22 + (Power(ChrD233,2) + ChrD233*ChrD323 + Power(ChrD323,2) + 3*ChrD223*ChrD333)*ig23 + (ChrD233 + 2*ChrD323)*ChrD333*ig33;
RicGG3311
  = (Power(ChrD113,2) + 2*ChrD113*ChrD311)*ig11 + 2*(ChrD213*(ChrD113 + ChrD311) + ChrD113*ChrD312)*ig12 + (Power(ChrD213,2) + 2*ChrD213*ChrD312)*ig22 + ChrD313*((4*ChrD113 + 2*ChrD311)*ig13 + (4*ChrD213 + 2*ChrD312)*ig23) + 3*Power(ChrD313,2)*ig33;
RicGG3312
  = (ChrD123*(ChrD113 + ChrD311) + ChrD113*ChrD312)*ig11 + (ChrD223*(ChrD113 + ChrD311) + ChrD213*ChrD312 + ChrD123*(ChrD213 + ChrD312) + ChrD113*ChrD322)*ig12 + ((2*ChrD123 + ChrD312)*ChrD313 + (2*ChrD113 + ChrD311)*ChrD323)*ig13 + (ChrD223*(ChrD213 + ChrD312) + ChrD213*ChrD322)*ig22 + (ChrD313*(2*ChrD223 + ChrD322) + (2*ChrD213 + ChrD312)*ChrD323)*ig23 + 3*ChrD313*ChrD323*ig33;
RicGG3313
  = (ChrD133*(ChrD113 + ChrD311) + ChrD113*ChrD313)*ig11 + (ChrD233*(ChrD113 + ChrD311) + ChrD133*(ChrD213 + ChrD312) + ChrD213*ChrD313 + ChrD113*ChrD323)*ig12 + (Power(ChrD313,2) + ChrD311*ChrD333 + 2*(ChrD133*ChrD313 + ChrD113*ChrD333))*ig13 + (ChrD233*(ChrD213 + ChrD312) + ChrD213*ChrD323)*ig22 + (ChrD313*(2*ChrD233 + ChrD323) + (2*ChrD213 + ChrD312)*ChrD333)*ig23 + 3*ChrD313*ChrD333*ig33;
RicGG3322
  = (Power(ChrD123,2) + 2*ChrD123*ChrD312)*ig11 + 2*(ChrD223*(ChrD123 + ChrD312) + ChrD123*ChrD322)*ig12 + (Power(ChrD223,2) + 2*ChrD223*ChrD322)*ig22 + ChrD323*((4*ChrD123 + 2*ChrD312)*ig13 + (4*ChrD223 + 2*ChrD322)*ig23) + 3*Power(ChrD323,2)*ig33;
RicGG3323
  = (ChrD133*(ChrD123 + ChrD312) + ChrD123*ChrD313)*ig11 + (ChrD233*(ChrD123 + ChrD312) + ChrD223*(ChrD133 + ChrD313) + ChrD133*ChrD322 + ChrD123*ChrD323)*ig12 + ((2*ChrD133 + ChrD313)*ChrD323 + (2*ChrD123 + ChrD312)*ChrD333)*ig13 + (ChrD233*(ChrD223 + ChrD322) + ChrD223*ChrD323)*ig22 + (Power(ChrD323,2) + ChrD322*ChrD333 + 2*(ChrD233*ChrD323 + ChrD223*ChrD333))*ig23 + 3*ChrD323*ChrD333*ig33;
RicGG3333
  = (Power(ChrD133,2) + 2*ChrD133*ChrD313)*ig11 + 2*(ChrD233*(ChrD133 + ChrD313) + ChrD133*ChrD323)*ig12 + (Power(ChrD233,2) + 2*ChrD233*ChrD323)*ig22 + ChrD333*((4*ChrD133 + 2*ChrD313)*ig13 + (4*ChrD233 + 2*ChrD323)*ig23) + 3*Power(ChrD333,2)*ig33;
Ric11
  = dGam11*g11 + dGam21*g12 + dGam31*g13 + ChrD111*localGam1 + ChrD112*localGam2 + ChrD113*localGam3 + ig11*(-ddg1111/2. + RicGG1111) + ig12*((-ddg1112 - ddg1121)/2. + 2*RicGG1112) + ig13*((-ddg1113 - ddg1131)/2. + 2*RicGG1113) + ig22*(-ddg1122/2. + RicGG1122) + ig23*((-ddg1123 - ddg1132)/2. + 2*RicGG1123) + ig33*(-ddg1133/2. + RicGG1133);
Ric12
  = (dGam12*g11 + (dGam11 + dGam22)*g12 + dGam32*g13 + dGam21*g22 + dGam31*g23 + (ChrD112 + ChrD211)*localGam1 + (ChrD122 + ChrD212)*localGam2 + (ChrD123 + ChrD213)*localGam3)/2. + ig11*(-ddg1211/2. + RicGG1211) + ig12*((-ddg1212 - ddg1221)/2. + 2*RicGG1212) + ig13*((-ddg1213 - ddg1231)/2. + 2*RicGG1213) + ig22*(-ddg1222/2. + RicGG1222) + ig23*((-ddg1223 - ddg1232)/2. + 2*RicGG1223) + ig33*(-ddg1233/2. + RicGG1233);
Ric12
  = (dGam12*g11 + (dGam11 + dGam22)*g12 + dGam32*g13 + dGam21*g22 + dGam31*g23 + (ChrD112 + ChrD211)*localGam1 + (ChrD122 + ChrD212)*localGam2 + (ChrD123 + ChrD213)*localGam3)/2. + ig11*(-ddg1211/2. + RicGG2111) + ig12*((-ddg1212 - ddg1221)/2. + 2*RicGG2112) + ig13*((-ddg1213 - ddg1231)/2. + 2*RicGG2113) + ig22*(-ddg1222/2. + RicGG2122) + ig23*((-ddg1223 - ddg1232)/2. + 2*RicGG2123) + ig33*(-ddg1233/2. + RicGG2133);
Ric13
  = (dGam13*g11 + dGam23*g12 + (dGam11 + dGam33)*g13 + dGam21*g23 + dGam31*g33 + (ChrD113 + ChrD311)*localGam1 + (ChrD123 + ChrD312)*localGam2 + (ChrD133 + ChrD313)*localGam3)/2. + ig11*(-ddg1311/2. + RicGG1311) + ig12*((-ddg1312 - ddg1321)/2. + 2*RicGG1312) + ig13*((-ddg1313 - ddg1331)/2. + 2*RicGG1313) + ig22*(-ddg1322/2. + RicGG1322) + ig23*((-ddg1323 - ddg1332)/2. + 2*RicGG1323) + ig33*(-ddg1333/2. + RicGG1333);
Ric13
  = (dGam13*g11 + dGam23*g12 + (dGam11 + dGam33)*g13 + dGam21*g23 + dGam31*g33 + (ChrD113 + ChrD311)*localGam1 + (ChrD123 + ChrD312)*localGam2 + (ChrD133 + ChrD313)*localGam3)/2. + ig11*(-ddg1311/2. + RicGG3111) + ig12*((-ddg1312 - ddg1321)/2. + 2*RicGG3112) + ig13*((-ddg1313 - ddg1331)/2. + 2*RicGG3113) + ig22*(-ddg1322/2. + RicGG3122) + ig23*((-ddg1323 - ddg1332)/2. + 2*RicGG3123) + ig33*(-ddg1333/2. + RicGG3133);
Ric22
  = dGam12*g12 + dGam22*g22 + dGam32*g23 + ChrD212*localGam1 + ChrD222*localGam2 + ChrD223*localGam3 + ig11*(-ddg2211/2. + RicGG2211) + ig12*((-ddg2212 - ddg2221)/2. + 2*RicGG2212) + ig13*((-ddg2213 - ddg2231)/2. + 2*RicGG2213) + ig22*(-ddg2222/2. + RicGG2222) + ig23*((-ddg2223 - ddg2232)/2. + 2*RicGG2223) + ig33*(-ddg2233/2. + RicGG2233);
Ric23
  = (dGam13*g12 + dGam12*g13 + dGam23*g22 + (dGam22 + dGam33)*g23 + dGam32*g33 + (ChrD213 + ChrD312)*localGam1 + (ChrD223 + ChrD322)*localGam2 + (ChrD233 + ChrD323)*localGam3)/2. + ig11*(-ddg2311/2. + RicGG2311) + ig12*((-ddg2312 - ddg2321)/2. + 2*RicGG2312) + ig13*((-ddg2313 - ddg2331)/2. + 2*RicGG2313) + ig22*(-ddg2322/2. + RicGG2322) + ig23*((-ddg2323 - ddg2332)/2. + 2*RicGG2323) + ig33*(-ddg2333/2. + RicGG2333);
Ric23
  = (dGam13*g12 + dGam12*g13 + dGam23*g22 + (dGam22 + dGam33)*g23 + dGam32*g33 + (ChrD213 + ChrD312)*localGam1 + (ChrD223 + ChrD322)*localGam2 + (ChrD233 + ChrD323)*localGam3)/2. + ig11*(-ddg2311/2. + RicGG3211) + ig12*((-ddg2312 - ddg2321)/2. + 2*RicGG3212) + ig13*((-ddg2313 - ddg2331)/2. + 2*RicGG3213) + ig22*(-ddg2322/2. + RicGG3222) + ig23*((-ddg2323 - ddg2332)/2. + 2*RicGG3223) + ig33*(-ddg2333/2. + RicGG3233);
Ric33
  = dGam13*g13 + dGam23*g23 + dGam33*g33 + ChrD313*localGam1 + ChrD323*localGam2 + ChrD333*localGam3 + ig11*(-ddg3311/2. + RicGG3311) + ig12*((-ddg3312 - ddg3321)/2. + 2*RicGG3312) + ig13*((-ddg3313 - ddg3331)/2. + 2*RicGG3313) + ig22*(-ddg3322/2. + RicGG3322) + ig23*((-ddg3323 - ddg3332)/2. + 2*RicGG3323) + ig33*(-ddg3333/2. + RicGG3333);
Riem1111
  = 0;
Riem1112
  = -(Chr122*Chr211) + Chr112*Chr212 - Chr123*Chr311 + Chr113*Chr312 - dChr1112 + dChr1121;
Riem1113
  = -(Chr123*Chr211) + Chr112*Chr213 - Chr133*Chr311 + Chr113*Chr313 - dChr1113 + dChr1131;
Riem1121
  = Chr122*Chr211 - Chr112*Chr212 + Chr123*Chr311 - Chr113*Chr312 + dChr1112 - dChr1121;
Riem1122
  = 0;
Riem1123
  = Chr122*Chr213 - Chr133*Chr312 + Chr123*(-Chr212 + Chr313) - dChr1123 + dChr1132;
Riem1131
  = Chr123*Chr211 - Chr112*Chr213 + Chr133*Chr311 - Chr113*Chr313 + dChr1113 - dChr1131;
Riem1132
  = -(Chr122*Chr213) + Chr133*Chr312 + Chr123*(Chr212 - Chr313) + dChr1123 - dChr1132;
Riem1133
  = 0;
Riem1211
  = 0;
Riem1212
  = -Power(Chr112,2) + Chr122*(Chr111 - Chr212) + Chr112*Chr222 - Chr123*Chr312 + Chr113*Chr322 - dChr1212 + dChr1221;
Riem1213
  = Chr123*(Chr111 - Chr212) + Chr112*(-Chr113 + Chr223) - Chr133*Chr312 + Chr113*Chr323 - dChr1213 + dChr1231;
Riem1221
  = Power(Chr112,2) + Chr122*(-Chr111 + Chr212) - Chr112*Chr222 + Chr123*Chr312 - Chr113*Chr322 + dChr1212 - dChr1221;
Riem1222
  = 0;
Riem1223
  = Chr122*(-Chr113 + Chr223) - Chr133*Chr322 + Chr123*(Chr112 - Chr222 + Chr323) - dChr1223 + dChr1232;
Riem1231
  = Chr123*(-Chr111 + Chr212) + Chr112*(Chr113 - Chr223) + Chr133*Chr312 - Chr113*Chr323 + dChr1213 - dChr1231;
Riem1232
  = Chr122*(Chr113 - Chr223) + Chr133*Chr322 + Chr123*(-Chr112 + Chr222 - Chr323) + dChr1223 - dChr1232;
Riem1233
  = 0;
Riem1311
  = 0;
Riem1312
  = -(Chr122*Chr213) + Chr112*(-Chr113 + Chr223) + Chr123*(Chr111 - Chr313) + Chr113*Chr323 - dChr1312 + dChr1321;
Riem1313
  = -Power(Chr113,2) - Chr123*Chr213 + Chr112*Chr233 + Chr133*(Chr111 - Chr313) + Chr113*Chr333 - dChr1313 + dChr1331;
Riem1321
  = Chr122*Chr213 + Chr112*(Chr113 - Chr223) + Chr123*(-Chr111 + Chr313) - Chr113*Chr323 + dChr1312 - dChr1321;
Riem1322
  = 0;
Riem1323
  = -(Chr123*Chr223) + Chr122*Chr233 + Chr133*(Chr112 - Chr323) + Chr123*(-Chr113 + Chr333) - dChr1323 + dChr1332;
Riem1331
  = Power(Chr113,2) + Chr123*Chr213 - Chr112*Chr233 + Chr133*(-Chr111 + Chr313) - Chr113*Chr333 + dChr1313 - dChr1331;
Riem1332
  = -(Chr122*Chr233) + Chr133*(-Chr112 + Chr323) + Chr123*(Chr113 + Chr223 - Chr333) + dChr1323 - dChr1332;
Riem1333
  = 0;
Riem2111
  = 0;
Riem2112
  = -(Chr111*Chr212) + Power(Chr212,2) + Chr211*(Chr112 - Chr222) - Chr223*Chr311 + Chr213*Chr312 - dChr2112 + dChr2121;
Riem2113
  = Chr211*(Chr113 - Chr223) - Chr233*Chr311 + Chr213*(-Chr111 + Chr212 + Chr313) - dChr2113 + dChr2131;
Riem2121
  = Chr111*Chr212 - Power(Chr212,2) + Chr211*(-Chr112 + Chr222) + Chr223*Chr311 - Chr213*Chr312 + dChr2112 - dChr2121;
Riem2122
  = 0;
Riem2123
  = Chr213*(-Chr112 + Chr222) + Chr212*(Chr113 - Chr223) - Chr233*Chr312 + Chr223*Chr313 - dChr2123 + dChr2132;
Riem2131
  = (Chr111 - Chr212)*Chr213 + Chr211*(-Chr113 + Chr223) + Chr233*Chr311 - Chr213*Chr313 + dChr2113 - dChr2131;
Riem2132
  = Chr213*(Chr112 - Chr222) + Chr212*(-Chr113 + Chr223) + Chr233*Chr312 - Chr223*Chr313 + dChr2123 - dChr2132;
Riem2133
  = 0;
Riem2211
  = 0;
Riem2212
  = Chr122*Chr211 - Chr112*Chr212 - Chr223*Chr312 + Chr213*Chr322 - dChr2212 + dChr2221;
Riem2213
  = Chr123*Chr211 - Chr233*Chr312 + Chr213*(-Chr112 + Chr323) - dChr2213 + dChr2231;
Riem2221
  = -(Chr122*Chr211) + Chr112*Chr212 + Chr223*Chr312 - Chr213*Chr322 + dChr2212 - dChr2221;
Riem2222
  = 0;
Riem2223
  = Chr123*Chr212 - Chr122*Chr213 - Chr233*Chr322 + Chr223*Chr323 - dChr2223 + dChr2232;
Riem2231
  = -(Chr123*Chr211) + Chr233*Chr312 + Chr213*(Chr112 - Chr323) + dChr2213 - dChr2231;
Riem2232
  = -(Chr123*Chr212) + Chr122*Chr213 + Chr233*Chr322 - Chr223*Chr323 + dChr2223 - dChr2232;
Riem2233
  = 0;
Riem2311
  = 0;
Riem2312
  = Chr123*Chr211 + Chr212*(-Chr113 + Chr223) - Chr223*Chr313 + Chr213*(-Chr222 + Chr323) - dChr2312 + dChr2321;
Riem2313
  = Chr133*Chr211 - Chr213*Chr223 + Chr233*(Chr212 - Chr313) + Chr213*(-Chr113 + Chr333) - dChr2313 + dChr2331;
Riem2321
  = -(Chr123*Chr211) + Chr212*(Chr113 - Chr223) + Chr223*Chr313 + Chr213*(Chr222 - Chr323) + dChr2312 - dChr2321;
Riem2322
  = 0;
Riem2323
  = Chr133*Chr212 - Chr123*Chr213 - Power(Chr223,2) + Chr233*(Chr222 - Chr323) + Chr223*Chr333 - dChr2323 + dChr2332;
Riem2331
  = -(Chr133*Chr211) + Chr233*(-Chr212 + Chr313) + Chr213*(Chr113 + Chr223 - Chr333) + dChr2313 - dChr2331;
Riem2332
  = -(Chr133*Chr212) + Chr123*Chr213 + Power(Chr223,2) + Chr233*(-Chr222 + Chr323) - Chr223*Chr333 + dChr2323 - dChr2332;
Riem2333
  = 0;
Riem3111
  = 0;
Riem3112
  = Chr312*(-Chr111 + Chr212 + Chr313) - Chr211*Chr322 + Chr311*(Chr112 - Chr323) - dChr3112 + dChr3121;
Riem3113
  = Chr213*Chr312 - Chr111*Chr313 + Power(Chr313,2) - Chr211*Chr323 + Chr311*(Chr113 - Chr333) - dChr3113 + dChr3131;
Riem3121
  = (Chr111 - Chr212)*Chr312 - Chr312*Chr313 + Chr211*Chr322 + Chr311*(-Chr112 + Chr323) + dChr3112 - dChr3121;
Riem3122
  = 0;
Riem3123
  = Chr213*Chr322 - Chr212*Chr323 + Chr313*(-Chr112 + Chr323) + Chr312*(Chr113 - Chr333) - dChr3123 + dChr3132;
Riem3131
  = -(Chr213*Chr312) + Chr111*Chr313 - Power(Chr313,2) + Chr211*Chr323 + Chr311*(-Chr113 + Chr333) + dChr3113 - dChr3131;
Riem3132
  = -(Chr213*Chr322) + Chr313*(Chr112 - Chr323) + Chr212*Chr323 + Chr312*(-Chr113 + Chr333) + dChr3123 - dChr3132;
Riem3133
  = 0;
Riem3211
  = 0;
Riem3212
  = Chr122*Chr311 + (-Chr212 + Chr313)*Chr322 + Chr312*(-Chr112 + Chr222 - Chr323) - dChr3212 + dChr3221;
Riem3213
  = Chr123*Chr311 - Chr212*Chr323 + Chr313*(-Chr112 + Chr323) + Chr312*(Chr223 - Chr333) - dChr3213 + dChr3231;
Riem3221
  = -(Chr122*Chr311) + (Chr212 - Chr313)*Chr322 + Chr312*(Chr112 - Chr222 + Chr323) + dChr3212 - dChr3221;
Riem3222
  = 0;
Riem3223
  = Chr123*Chr312 - Chr122*Chr313 - Chr222*Chr323 + Power(Chr323,2) + Chr322*(Chr223 - Chr333) - dChr3223 + dChr3232;
Riem3231
  = -(Chr123*Chr311) + Chr313*(Chr112 - Chr323) + Chr212*Chr323 + Chr312*(-Chr223 + Chr333) + dChr3213 - dChr3231;
Riem3232
  = -(Chr123*Chr312) + Chr122*Chr313 + Chr222*Chr323 - Power(Chr323,2) + Chr322*(-Chr223 + Chr333) + dChr3223 - dChr3232;
Riem3233
  = 0;
Riem3311
  = 0;
Riem3312
  = Chr123*Chr311 + (-Chr113 + Chr223)*Chr312 - Chr213*Chr322 - dChr3312 + dChr3321;
Riem3313
  = Chr133*Chr311 + Chr233*Chr312 - Chr113*Chr313 - Chr213*Chr323 - dChr3313 + dChr3331;
Riem3321
  = -(Chr123*Chr311) + (Chr113 - Chr223)*Chr312 + Chr213*Chr322 + dChr3312 - dChr3321;
Riem3322
  = 0;
Riem3323
  = Chr133*Chr312 - Chr123*Chr313 + Chr233*Chr322 - Chr223*Chr323 - dChr3323 + dChr3332;
Riem3331
  = -(Chr133*Chr311) - Chr233*Chr312 + Chr113*Chr313 + Chr213*Chr323 + dChr3313 - dChr3331;
Riem3332
  = -(Chr133*Chr312) + Chr123*Chr313 - Chr233*Chr322 + Chr223*Chr323 + dChr3323 - dChr3332;
Riem3333
  = 0;
Weyl1111
  = g11*Riem1111 + g12*Riem2111 + g13*Riem3111;
Weyl1112
  = g11*Riem1112 + g12*Riem2112 + g13*Riem3112;
Weyl1113
  = g11*Riem1113 + g12*Riem2113 + g13*Riem3113;
Weyl1121
  = g11*Riem1121 + g12*Riem2121 + g13*Riem3121;
Weyl1122
  = g11*Riem1122 + g12*Riem2122 + g13*Riem3122;
Weyl1123
  = g11*Riem1123 + g12*Riem2123 + g13*Riem3123;
Weyl1131
  = g11*Riem1131 + g12*Riem2131 + g13*Riem3131;
Weyl1132
  = g11*Riem1132 + g12*Riem2132 + g13*Riem3132;
Weyl1133
  = g11*Riem1133 + g12*Riem2133 + g13*Riem3133;
Weyl1211
  = g11*Riem1211 + g12*Riem2211 + g13*Riem3211;
Weyl1212
  = -Power(K12,2) + K11*K22 + g11*Riem1212 + g12*Riem2212 + g13*Riem3212;
Weyl1213
  = -(K12*K13) + K11*K23 + g11*Riem1213 + g12*Riem2213 + g13*Riem3213;
Weyl1221
  = Power(K12,2) - K11*K22 + g11*Riem1221 + g12*Riem2221 + g13*Riem3221;
Weyl1222
  = g11*Riem1222 + g12*Riem2222 + g13*Riem3222;
Weyl1223
  = -(K13*K22) + K12*K23 + g11*Riem1223 + g12*Riem2223 + g13*Riem3223;
Weyl1231
  = K12*K13 - K11*K23 + g11*Riem1231 + g12*Riem2231 + g13*Riem3231;
Weyl1232
  = K13*K22 - K12*K23 + g11*Riem1232 + g12*Riem2232 + g13*Riem3232;
Weyl1233
  = g11*Riem1233 + g12*Riem2233 + g13*Riem3233;
Weyl1311
  = g11*Riem1311 + g12*Riem2311 + g13*Riem3311;
Weyl1312
  = -(K12*K13) + K11*K23 + g11*Riem1312 + g12*Riem2312 + g13*Riem3312;
Weyl1313
  = -Power(K13,2) + K11*K33 + g11*Riem1313 + g12*Riem2313 + g13*Riem3313;
Weyl1321
  = K12*K13 - K11*K23 + g11*Riem1321 + g12*Riem2321 + g13*Riem3321;
Weyl1322
  = g11*Riem1322 + g12*Riem2322 + g13*Riem3322;
Weyl1323
  = -(K13*K23) + K12*K33 + g11*Riem1323 + g12*Riem2323 + g13*Riem3323;
Weyl1331
  = Power(K13,2) - K11*K33 + g11*Riem1331 + g12*Riem2331 + g13*Riem3331;
Weyl1332
  = K13*K23 - K12*K33 + g11*Riem1332 + g12*Riem2332 + g13*Riem3332;
Weyl1333
  = g11*Riem1333 + g12*Riem2333 + g13*Riem3333;
Weyl2111
  = g12*Riem1111 + g22*Riem2111 + g23*Riem3111;
Weyl2112
  = Power(K12,2) - K11*K22 + g12*Riem1112 + g22*Riem2112 + g23*Riem3112;
Weyl2113
  = K12*K13 - K11*K23 + g12*Riem1113 + g22*Riem2113 + g23*Riem3113;
Weyl2121
  = -Power(K12,2) + K11*K22 + g12*Riem1121 + g22*Riem2121 + g23*Riem3121;
Weyl2122
  = g12*Riem1122 + g22*Riem2122 + g23*Riem3122;
Weyl2123
  = K13*K22 - K12*K23 + g12*Riem1123 + g22*Riem2123 + g23*Riem3123;
Weyl2131
  = -(K12*K13) + K11*K23 + g12*Riem1131 + g22*Riem2131 + g23*Riem3131;
Weyl2132
  = -(K13*K22) + K12*K23 + g12*Riem1132 + g22*Riem2132 + g23*Riem3132;
Weyl2133
  = g12*Riem1133 + g22*Riem2133 + g23*Riem3133;
Weyl2211
  = g12*Riem1211 + g22*Riem2211 + g23*Riem3211;
Weyl2212
  = g12*Riem1212 + g22*Riem2212 + g23*Riem3212;
Weyl2213
  = g12*Riem1213 + g22*Riem2213 + g23*Riem3213;
Weyl2221
  = g12*Riem1221 + g22*Riem2221 + g23*Riem3221;
Weyl2222
  = g12*Riem1222 + g22*Riem2222 + g23*Riem3222;
Weyl2223
  = g12*Riem1223 + g22*Riem2223 + g23*Riem3223;
Weyl2231
  = g12*Riem1231 + g22*Riem2231 + g23*Riem3231;
Weyl2232
  = g12*Riem1232 + g22*Riem2232 + g23*Riem3232;
Weyl2233
  = g12*Riem1233 + g22*Riem2233 + g23*Riem3233;
Weyl2311
  = g12*Riem1311 + g22*Riem2311 + g23*Riem3311;
Weyl2312
  = -(K13*K22) + K12*K23 + g12*Riem1312 + g22*Riem2312 + g23*Riem3312;
Weyl2313
  = -(K13*K23) + K12*K33 + g12*Riem1313 + g22*Riem2313 + g23*Riem3313;
Weyl2321
  = K13*K22 - K12*K23 + g12*Riem1321 + g22*Riem2321 + g23*Riem3321;
Weyl2322
  = g12*Riem1322 + g22*Riem2322 + g23*Riem3322;
Weyl2323
  = -Power(K23,2) + K22*K33 + g12*Riem1323 + g22*Riem2323 + g23*Riem3323;
Weyl2331
  = K13*K23 - K12*K33 + g12*Riem1331 + g22*Riem2331 + g23*Riem3331;
Weyl2332
  = Power(K23,2) - K22*K33 + g12*Riem1332 + g22*Riem2332 + g23*Riem3332;
Weyl2333
  = g12*Riem1333 + g22*Riem2333 + g23*Riem3333;
Weyl3111
  = g13*Riem1111 + g23*Riem2111 + g33*Riem3111;
Weyl3112
  = K12*K13 - K11*K23 + g13*Riem1112 + g23*Riem2112 + g33*Riem3112;
Weyl3113
  = Power(K13,2) - K11*K33 + g13*Riem1113 + g23*Riem2113 + g33*Riem3113;
Weyl3121
  = -(K12*K13) + K11*K23 + g13*Riem1121 + g23*Riem2121 + g33*Riem3121;
Weyl3122
  = g13*Riem1122 + g23*Riem2122 + g33*Riem3122;
Weyl3123
  = K13*K23 - K12*K33 + g13*Riem1123 + g23*Riem2123 + g33*Riem3123;
Weyl3131
  = -Power(K13,2) + K11*K33 + g13*Riem1131 + g23*Riem2131 + g33*Riem3131;
Weyl3132
  = -(K13*K23) + K12*K33 + g13*Riem1132 + g23*Riem2132 + g33*Riem3132;
Weyl3133
  = g13*Riem1133 + g23*Riem2133 + g33*Riem3133;
Weyl3211
  = g13*Riem1211 + g23*Riem2211 + g33*Riem3211;
Weyl3212
  = K13*K22 - K12*K23 + g13*Riem1212 + g23*Riem2212 + g33*Riem3212;
Weyl3213
  = K13*K23 - K12*K33 + g13*Riem1213 + g23*Riem2213 + g33*Riem3213;
Weyl3221
  = -(K13*K22) + K12*K23 + g13*Riem1221 + g23*Riem2221 + g33*Riem3221;
Weyl3222
  = g13*Riem1222 + g23*Riem2222 + g33*Riem3222;
Weyl3223
  = Power(K23,2) - K22*K33 + g13*Riem1223 + g23*Riem2223 + g33*Riem3223;
Weyl3231
  = -(K13*K23) + K12*K33 + g13*Riem1231 + g23*Riem2231 + g33*Riem3231;
Weyl3232
  = -Power(K23,2) + K22*K33 + g13*Riem1232 + g23*Riem2232 + g33*Riem3232;
Weyl3233
  = g13*Riem1233 + g23*Riem2233 + g33*Riem3233;
Weyl3311
  = g13*Riem1311 + g23*Riem2311 + g33*Riem3311;
Weyl3312
  = g13*Riem1312 + g23*Riem2312 + g33*Riem3312;
Weyl3313
  = g13*Riem1313 + g23*Riem2313 + g33*Riem3313;
Weyl3321
  = g13*Riem1321 + g23*Riem2321 + g33*Riem3321;
Weyl3322
  = g13*Riem1322 + g23*Riem2322 + g33*Riem3322;
Weyl3323
  = g13*Riem1323 + g23*Riem2323 + g33*Riem3323;
Weyl3331
  = g13*Riem1331 + g23*Riem2331 + g33*Riem3331;
Weyl3332
  = g13*Riem1332 + g23*Riem2332 + g33*Riem3332;
Weyl3333
  = g13*Riem1333 + g23*Riem2333 + g33*Riem3333;
Weyl111
  = 0;
Weyl112
  = -DK112 + DK121;
Weyl113
  = -DK113 + DK131;
Weyl121
  = DK112 - DK121;
Weyl122
  = 0;
Weyl123
  = -DK123 + DK132;
Weyl131
  = DK113 - DK131;
Weyl132
  = DK123 - DK132;
Weyl133
  = 0;
Weyl211
  = 0;
Weyl212
  = -DK122 + DK221;
Weyl213
  = -DK123 + DK231;
Weyl221
  = DK122 - DK221;
Weyl222
  = 0;
Weyl223
  = -DK223 + DK232;
Weyl231
  = DK123 - DK231;
Weyl232
  = DK223 - DK232;
Weyl233
  = 0;
Weyl311
  = 0;
Weyl312
  = -DK132 + DK231;
Weyl313
  = -DK133 + DK331;
Weyl321
  = DK132 - DK231;
Weyl322
  = 0;
Weyl323
  = -DK233 + DK332;
Weyl331
  = DK133 - DK331;
Weyl332
  = DK233 - DK332;
Weyl333
  = 0;
Weyl11
  = -KK11 + K11*localK + Ric11;
Weyl12
  = -KK12 + K12*localK + Ric12;
Weyl13
  = -KK13 + K13*localK + Ric13;
Weyl22
  = -KK22 + K22*localK + Ric22;
Weyl23
  = -KK23 + K23*localK + Ric23;
Weyl33
  = -KK33 + K33*localK + Ric33;
psi0re
  = (-Power(mi1,2) + Power(mr1,2))*(Weyl111*z1 + Weyl121*z2 + Weyl131*z3 + (Weyl11 + Weyl1111*Power(z1,2) + Weyl2121*Power(z2,2) + (Weyl3111*z1 + (Weyl2131 + Weyl3121)*z2)*z3 + Weyl3131*Power(z3,2) + z1*((Weyl1121 + Weyl2111)*z2 + Weyl1131*z3))/2.) + (-Power(mi2,2) + Power(mr2,2))*(Weyl212*z1 + Weyl222*z2 + Weyl232*z3 + (Weyl22 + Weyl1212*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl3212*z1 + (Weyl2232 + Weyl3222)*z2)*z3 + Weyl3232*Power(z3,2) + z1*((Weyl1222 + Weyl2212)*z2 + Weyl1232*z3))/2.) + (-Power(mi3,2) + Power(mr3,2))*(Weyl313*z1 + Weyl323*z2 + Weyl333*z3 + (Weyl33 + Weyl1313*Power(z1,2) + Weyl2323*Power(z2,2) + (Weyl3313*z1 + (Weyl2333 + Weyl3323)*z2)*z3 + Weyl3333*Power(z3,2) + z1*((Weyl1323 + Weyl2313)*z2 + Weyl1333*z3))/2.) + (-(mi1*mi2) + mr1*mr2)*(Weyl12 + (Weyl112 + Weyl211)*z1 + (Weyl122 + Weyl221)*z2 + (Weyl132 + Weyl231)*z3 + ((Weyl1112 + Weyl1211)*Power(z1,2) + (Weyl2122 + Weyl2221)*Power(z2,2) + (Weyl3211*z1 + (Weyl2132 + Weyl2231 + Weyl3122 + Weyl3221)*z2)*z3 + (Weyl3132 + Weyl3231)*Power(z3,2) + z1*((Weyl1122 + Weyl1221 + Weyl2112 + Weyl2211)*z2 + (Weyl1132 + Weyl1231 + Weyl3112)*z3))/2.) + (-(mi1*mi3) + mr1*mr3)*(Weyl13 + (Weyl113 + Weyl311)*z1 + (Weyl123 + Weyl321)*z2 + (Weyl133 + Weyl331)*z3 + ((Weyl1113 + Weyl1311)*Power(z1,2) + (Weyl2123 + Weyl2321)*Power(z2,2) + (Weyl3311*z1 + (Weyl2133 + Weyl2331 + Weyl3123 + Weyl3321)*z2)*z3 + (Weyl3133 + Weyl3331)*Power(z3,2) + z1*((Weyl1123 + Weyl1321 + Weyl2113 + Weyl2311)*z2 + (Weyl1133 + Weyl1331 + Weyl3113)*z3))/2.) + (-(mi2*mi3) + mr2*mr3)*(Weyl23 + (Weyl213 + Weyl312)*z1 + (Weyl223 + Weyl322)*z2 + (Weyl233 + Weyl332)*z3 + ((Weyl1213 + Weyl1312)*Power(z1,2) + (Weyl2223 + Weyl2322)*Power(z2,2) + (Weyl3312*z1 + (Weyl2233 + Weyl2332 + Weyl3223 + Weyl3322)*z2)*z3 + (Weyl3233 + Weyl3332)*Power(z3,2) + z1*((Weyl1223 + Weyl1322 + Weyl2213 + Weyl2312)*z2 + (Weyl1233 + Weyl1332 + Weyl3213)*z3))/2.);
psi0im
  = (mi3*mr1 + mi1*mr3)*(Weyl311*z1 + Weyl321*z2 + Weyl331*z3) + (mi3*mr2 + mi2*mr3)*((Weyl213 + Weyl312)*z1 + (Weyl223 + Weyl322)*z2 + (Weyl233 + Weyl332)*z3) + mi1*(mr3*Weyl13 + mr2*(Weyl12 + Weyl112*z1 + Weyl122*z2 + Weyl132*z3)) + mi2*(mr1*(Weyl211*z1 + Weyl221*z2 + Weyl231*z3) + 2*mr2*(Weyl212*z1 + Weyl222*z2 + Weyl232*z3)) + mi3*(2*mr3*(Weyl313*z1 + Weyl323*z2 + Weyl333*z3) + (mr1*(Weyl1113*Power(z1,2) + Weyl2123*Power(z2,2) + (Weyl3113*z1 + (Weyl2133 + Weyl3123)*z2)*z3 + Weyl3133*Power(z3,2) + z1*((Weyl1123 + Weyl2113)*z2 + Weyl1133*z3)))/2.) + mr1*(mi3*(Weyl13 + Weyl113*z1 + Weyl123*z2 + Weyl133*z3) + mi1*(Weyl11 + Weyl1111*Power(z1,2) + Weyl2121*Power(z2,2) + (Weyl3111*z1 + (Weyl2131 + Weyl3121)*z2)*z3 + Weyl3131*Power(z3,2) + z1*((Weyl1121 + Weyl2111)*z2 + Weyl1131*z3) + 2*(Weyl111*z1 + Weyl121*z2 + Weyl131*z3)) + mi2*(Weyl12 + Weyl112*z1 + Weyl122*z2 + Weyl132*z3 + (Weyl1112*Power(z1,2) + Weyl2122*Power(z2,2) + (Weyl3112*z1 + (Weyl2132 + Weyl3122)*z2)*z3 + Weyl3132*Power(z3,2) + z1*((Weyl1122 + Weyl2112)*z2 + Weyl1132*z3))/2.)) + mr2*(mi1*(Weyl211*z1 + Weyl221*z2 + Weyl231*z3) + mi2*(Weyl22 + Weyl1212*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl3212*z1 + (Weyl2232 + Weyl3222)*z2)*z3 + Weyl3232*Power(z3,2) + z1*((Weyl1222 + Weyl2212)*z2 + Weyl1232*z3)) + mi3*(Weyl23 + (Weyl1213*Power(z1,2) + Weyl2223*Power(z2,2) + (Weyl3213*z1 + (Weyl2233 + Weyl3223)*z2)*z3 + Weyl3233*Power(z3,2) + z1*((Weyl1223 + Weyl2213)*z2 + Weyl1233*z3))/2.)) + mr3*(mi1*(Weyl113*z1 + Weyl123*z2 + Weyl133*z3) + mi3*(Weyl33 + Weyl1313*Power(z1,2) + Weyl2323*Power(z2,2) + (Weyl3313*z1 + (Weyl2333 + Weyl3323)*z2)*z3 + Weyl3333*Power(z3,2) + z1*((Weyl1323 + Weyl2313)*z2 + Weyl1333*z3)) + mi2*(Weyl23 + (Weyl1312*Power(z1,2) + Weyl2322*Power(z2,2) + (Weyl3312*z1 + (Weyl2332 + Weyl3322)*z2)*z3 + Weyl3332*Power(z3,2) + z1*((Weyl1322 + Weyl2312)*z2 + Weyl1332*z3))/2.)) + ((mi2*mr1 + mi1*mr2)*(Weyl1211*Power(z1,2) + Weyl2221*Power(z2,2) + (Weyl3211*z1 + (Weyl2231 + Weyl3221)*z2)*z3 + Weyl3231*Power(z3,2) + z1*((Weyl1221 + Weyl2211)*z2 + Weyl1231*z3)) + mi1*(mr2*(Weyl1112*Power(z1,2) + Weyl2122*Power(z2,2) + (Weyl3112*z1 + (Weyl2132 + Weyl3122)*z2)*z3 + Weyl3132*Power(z3,2) + z1*((Weyl1122 + Weyl2112)*z2 + Weyl1132*z3)) + mr3*(Weyl1113*Power(z1,2) + Weyl2123*Power(z2,2) + (Weyl3113*z1 + (Weyl2133 + Weyl3123)*z2)*z3 + Weyl3133*Power(z3,2) + z1*((Weyl1123 + Weyl2113)*z2 + Weyl1133*z3))) + mr3*(mi2*(Weyl1213*Power(z1,2) + Weyl2223*Power(z2,2) + (Weyl3213*z1 + (Weyl2233 + Weyl3223)*z2)*z3 + Weyl3233*Power(z3,2) + z1*((Weyl1223 + Weyl2213)*z2 + Weyl1233*z3)) + mi1*(Weyl1311*Power(z1,2) + Weyl2321*Power(z2,2) + (Weyl3311*z1 + (Weyl2331 + Weyl3321)*z2)*z3 + Weyl3331*Power(z3,2) + z1*((Weyl1321 + Weyl2311)*z2 + Weyl1331*z3))) + mi3*(mr1*(Weyl1311*Power(z1,2) + Weyl2321*Power(z2,2) + (Weyl3311*z1 + (Weyl2331 + Weyl3321)*z2)*z3 + Weyl3331*Power(z3,2) + z1*((Weyl1321 + Weyl2311)*z2 + Weyl1331*z3)) + mr2*(Weyl1312*Power(z1,2) + Weyl2322*Power(z2,2) + (Weyl3312*z1 + (Weyl2332 + Weyl3322)*z2)*z3 + Weyl3332*Power(z3,2) + z1*((Weyl1322 + Weyl2312)*z2 + Weyl1332*z3))))/2.;
psi1re
  = (-((mr1*Weyl12 + mr2*Weyl22 + mr3*Weyl23)*z2) - (mr1*Weyl13 + mr2*Weyl23 + mr3*Weyl33)*z3 + ((mr3*z1)/2. + mr1*z3)*(Weyl311*z1 + Weyl312*z2 + Weyl313*z3) + ((mr3*z2)/2. + mr2*z3)*(Weyl321*z1 + Weyl322*z2 + Weyl323*z3) + mr1*z2*(Weyl211*z1 + Weyl212*z2 + Weyl213*z3 + (Weyl121*z1 + Weyl122*z2 + Weyl123*z3)/2.) + mr2*((3*z2*(Weyl221*z1 + Weyl222*z2 + Weyl223*z3))/2. + (z3*(Weyl231*z1 + Weyl232*z2 + Weyl233*z3))/2.) - z1*(mr2*Weyl12 + mr3*Weyl13 + mr1*(Weyl11 + (Weyl1111*Power(z1,2) + Weyl1212*Power(z2,2) + (Weyl1311*z1 + (Weyl1213 + Weyl1312)*z2)*z3 + Weyl1313*Power(z3,2) + z1*((Weyl1112 + Weyl1211)*z2 + Weyl1113*z3))/2.)) + z1*((3*mr1*(Weyl111*z1 + Weyl112*z2 + Weyl113*z3))/2. + mr2*(Weyl121*z1 + Weyl122*z2 + Weyl123*z3 + (-(Weyl1121*Power(z1,2)) - Weyl1222*Power(z2,2) - (Weyl1321*z1 + (Weyl1223 + Weyl1322)*z2)*z3 - Weyl1323*Power(z3,2) - z1*((Weyl1122 + Weyl1221)*z2 + Weyl1123*z3))/2.)) + (mr1*z3*(Weyl131*z1 + Weyl132*z2 + Weyl133*z3) + mr2*z1*(Weyl211*z1 + Weyl212*z2 + Weyl213*z3) - z2*(mr1*(Weyl2111*Power(z1,2) + Weyl2212*Power(z2,2) + (Weyl2311*z1 + (Weyl2213 + Weyl2312)*z2)*z3 + Weyl2313*Power(z3,2) + z1*((Weyl2112 + Weyl2211)*z2 + Weyl2113*z3)) + mr2*(Weyl2121*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl2321*z1 + (Weyl2223 + Weyl2322)*z2)*z3 + Weyl2323*Power(z3,2) + z1*((Weyl2122 + Weyl2221)*z2 + Weyl2123*z3))) - mr3*(z1*(Weyl1131*Power(z1,2) + Weyl1232*Power(z2,2) + (Weyl1331*z1 + (Weyl1233 + Weyl1332)*z2)*z3 + Weyl1333*Power(z3,2) + z1*((Weyl1132 + Weyl1231)*z2 + Weyl1133*z3)) + z2*(Weyl2131*Power(z1,2) + Weyl2232*Power(z2,2) + (Weyl2331*z1 + (Weyl2233 + Weyl2332)*z2)*z3 + Weyl2333*Power(z3,2) + z1*((Weyl2132 + Weyl2231)*z2 + Weyl2133*z3))) - z3*(mr1*(Weyl3111*Power(z1,2) + Weyl3212*Power(z2,2) + (Weyl3311*z1 + (Weyl3213 + Weyl3312)*z2)*z3 + Weyl3313*Power(z3,2) + z1*((Weyl3112 + Weyl3211)*z2 + Weyl3113*z3)) + mr2*(Weyl3121*Power(z1,2) + Weyl3222*Power(z2,2) + (Weyl3321*z1 + (Weyl3223 + Weyl3322)*z2)*z3 + Weyl3323*Power(z3,2) + z1*((Weyl3122 + Weyl3221)*z2 + Weyl3123*z3))))/2. + mr3*(z1*(Weyl131*z1 + Weyl132*z2 + Weyl133*z3) + z2*(Weyl231*z1 + Weyl232*z2 + Weyl233*z3) + z3*((3*(Weyl331*z1 + Weyl332*z2 + Weyl333*z3))/2. + (-(Weyl3131*Power(z1,2)) - Weyl3232*Power(z2,2) - (Weyl3331*z1 + (Weyl3233 + Weyl3332)*z2)*z3 - Weyl3333*Power(z3,2) - z1*((Weyl3132 + Weyl3231)*z2 + Weyl3133*z3))/2.)))/sqrt2;
psi1im
  = (-((mi1*Weyl12 + mi2*Weyl22 + mi3*Weyl23)*z2) - (mi1*Weyl13 + mi2*Weyl23 + mi3*Weyl33)*z3 + ((mi3*z1)/2. + mi1*z3)*(Weyl311*z1 + Weyl312*z2 + Weyl313*z3) + ((mi3*z2)/2. + mi2*z3)*(Weyl321*z1 + Weyl322*z2 + Weyl323*z3) + mi1*z2*(Weyl211*z1 + Weyl212*z2 + Weyl213*z3 + (Weyl121*z1 + Weyl122*z2 + Weyl123*z3)/2.) + mi2*((3*z2*(Weyl221*z1 + Weyl222*z2 + Weyl223*z3))/2. + (z3*(Weyl231*z1 + Weyl232*z2 + Weyl233*z3))/2.) - z1*(mi2*Weyl12 + mi3*Weyl13 + mi1*(Weyl11 + (Weyl1111*Power(z1,2) + Weyl1212*Power(z2,2) + (Weyl1311*z1 + (Weyl1213 + Weyl1312)*z2)*z3 + Weyl1313*Power(z3,2) + z1*((Weyl1112 + Weyl1211)*z2 + Weyl1113*z3))/2.)) + z1*((3*mi1*(Weyl111*z1 + Weyl112*z2 + Weyl113*z3))/2. + mi2*(Weyl121*z1 + Weyl122*z2 + Weyl123*z3 + (-(Weyl1121*Power(z1,2)) - Weyl1222*Power(z2,2) - (Weyl1321*z1 + (Weyl1223 + Weyl1322)*z2)*z3 - Weyl1323*Power(z3,2) - z1*((Weyl1122 + Weyl1221)*z2 + Weyl1123*z3))/2.)) + (mi1*z3*(Weyl131*z1 + Weyl132*z2 + Weyl133*z3) + mi2*z1*(Weyl211*z1 + Weyl212*z2 + Weyl213*z3) - z2*(mi1*(Weyl2111*Power(z1,2) + Weyl2212*Power(z2,2) + (Weyl2311*z1 + (Weyl2213 + Weyl2312)*z2)*z3 + Weyl2313*Power(z3,2) + z1*((Weyl2112 + Weyl2211)*z2 + Weyl2113*z3)) + mi2*(Weyl2121*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl2321*z1 + (Weyl2223 + Weyl2322)*z2)*z3 + Weyl2323*Power(z3,2) + z1*((Weyl2122 + Weyl2221)*z2 + Weyl2123*z3))) - mi3*(z1*(Weyl1131*Power(z1,2) + Weyl1232*Power(z2,2) + (Weyl1331*z1 + (Weyl1233 + Weyl1332)*z2)*z3 + Weyl1333*Power(z3,2) + z1*((Weyl1132 + Weyl1231)*z2 + Weyl1133*z3)) + z2*(Weyl2131*Power(z1,2) + Weyl2232*Power(z2,2) + (Weyl2331*z1 + (Weyl2233 + Weyl2332)*z2)*z3 + Weyl2333*Power(z3,2) + z1*((Weyl2132 + Weyl2231)*z2 + Weyl2133*z3))) - z3*(mi1*(Weyl3111*Power(z1,2) + Weyl3212*Power(z2,2) + (Weyl3311*z1 + (Weyl3213 + Weyl3312)*z2)*z3 + Weyl3313*Power(z3,2) + z1*((Weyl3112 + Weyl3211)*z2 + Weyl3113*z3)) + mi2*(Weyl3121*Power(z1,2) + Weyl3222*Power(z2,2) + (Weyl3321*z1 + (Weyl3223 + Weyl3322)*z2)*z3 + Weyl3323*Power(z3,2) + z1*((Weyl3122 + Weyl3221)*z2 + Weyl3123*z3))))/2. + mi3*(z1*(Weyl131*z1 + Weyl132*z2 + Weyl133*z3) + z2*(Weyl231*z1 + Weyl232*z2 + Weyl233*z3) + z3*((3*(Weyl331*z1 + Weyl332*z2 + Weyl333*z3))/2. + (-(Weyl3131*Power(z1,2)) - Weyl3232*Power(z2,2) - (Weyl3331*z1 + (Weyl3233 + Weyl3332)*z2)*z3 - Weyl3333*Power(z3,2) - z1*((Weyl3132 + Weyl3231)*z2 + Weyl3133*z3))/2.)))/sqrt2;
psi2re
  = -((mi1*mi2 + mr1*mr2)*Weyl12) - (mi1*mi3 + mr1*mr3)*Weyl13 - (mi2*mi3 + mr2*mr3)*Weyl23 + (-((Power(mi1,2) + Power(mr1,2))*(Weyl11 + Weyl1111*Power(z1,2) + Weyl2112*Power(z2,2) + (Weyl113 + Weyl131 + Weyl3111*z1 + Weyl3112*z2)*z3 + Weyl3113*Power(z3,2) + z1*(2*Weyl111 + Weyl1112*z2 + Weyl1113*z3) + z2*(Weyl112 + Weyl121 + Weyl2111*z1 + Weyl2113*z3))) - (Power(mi2,2) + Power(mr2,2))*(Weyl22 + Weyl1221*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl223 + Weyl232 + Weyl3221*z1 + Weyl3222*z2)*z3 + Weyl3223*Power(z3,2) + z1*(Weyl212 + Weyl221 + Weyl1222*z2 + Weyl1223*z3) + z2*(2*Weyl222 + Weyl2221*z1 + Weyl2223*z3)) - (Power(mi3,2) + Power(mr3,2))*(Weyl33 + Weyl1331*Power(z1,2) + Weyl2332*Power(z2,2) + (2*Weyl333 + Weyl3331*z1 + Weyl3332*z2)*z3 + Weyl3333*Power(z3,2) + z1*(Weyl313 + Weyl331 + Weyl1332*z2 + Weyl1333*z3) + z2*(Weyl323 + Weyl332 + Weyl2331*z1 + Weyl2333*z3)) - (mi1*mi2 + mr1*mr2)*((Weyl1121 + Weyl1211)*Power(z1,2) + (Weyl2122 + Weyl2212)*Power(z2,2) + (Weyl123 + Weyl132 + Weyl213 + Weyl231 + Weyl1213*z1 + Weyl2213*z2)*z3 + (Weyl3123 + Weyl3213)*Power(z3,2) + z1*(Weyl112 + Weyl121 + 2*Weyl211 + (Weyl1122 + Weyl2121 + Weyl2211)*z2 + (Weyl1123 + Weyl3121 + Weyl3211)*z3) + z2*(2*Weyl122 + Weyl212 + Weyl221 + Weyl1212*z1 + (Weyl2123 + Weyl3122 + Weyl3212)*z3)) - (mi1*mi3 + mr1*mr3)*((Weyl1131 + Weyl1311)*Power(z1,2) + (Weyl2132 + Weyl2312)*Power(z2,2) + (2*Weyl133 + Weyl313 + Weyl331 + Weyl1313*z1 + Weyl2313*z2)*z3 + (Weyl3133 + Weyl3313)*Power(z3,2) + z1*(Weyl113 + Weyl131 + 2*Weyl311 + (Weyl1132 + Weyl2131 + Weyl2311)*z2 + (Weyl1133 + Weyl3131 + Weyl3311)*z3) + z2*(Weyl123 + Weyl132 + Weyl312 + Weyl321 + Weyl1312*z1 + (Weyl2133 + Weyl3132 + Weyl3312)*z3)) - (mi2*mi3 + mr2*mr3)*((Weyl1231 + Weyl1321)*Power(z1,2) + (Weyl2232 + Weyl2322)*Power(z2,2) + (2*Weyl233 + Weyl323 + Weyl332 + Weyl1323*z1 + Weyl2323*z2)*z3 + (Weyl3233 + Weyl3323)*Power(z3,2) + z1*(Weyl213 + Weyl231 + Weyl312 + Weyl321 + (Weyl1232 + Weyl2231 + Weyl2321)*z2 + (Weyl1233 + Weyl3231 + Weyl3321)*z3) + z2*(Weyl223 + Weyl232 + 2*Weyl322 + Weyl1322*z1 + (Weyl2233 + Weyl3232 + Weyl3322)*z3)))/2.;
psi2im
  = ((mi2*mr1 - mi1*mr2)*(Weyl1121*Power(z1,2) + Weyl2122*Power(z2,2) + (Weyl123 + Weyl231 + Weyl3121*z1 + Weyl3122*z2)*z3 + Weyl3123*Power(z3,2) + z1*(Weyl121 + Weyl211 + Weyl1122*z2 + Weyl1123*z3) + z2*(Weyl122 + Weyl221 + Weyl2121*z1 + Weyl2123*z3)) + (mi3*mr1 - mi1*mr3)*(Weyl1131*Power(z1,2) + Weyl2132*Power(z2,2) + (Weyl133 + Weyl331 + Weyl3131*z1 + Weyl3132*z2)*z3 + Weyl3133*Power(z3,2) + z1*(Weyl131 + Weyl311 + Weyl1132*z2 + Weyl1133*z3) + z2*(Weyl132 + Weyl321 + Weyl2131*z1 + Weyl2133*z3)) + (-(mi2*mr1) + mi1*mr2)*(Weyl1211*Power(z1,2) + Weyl2212*Power(z2,2) + (Weyl132 + Weyl213 + Weyl3211*z1 + Weyl3212*z2)*z3 + Weyl3213*Power(z3,2) + z1*(Weyl112 + Weyl211 + Weyl1212*z2 + Weyl1213*z3) + z2*(Weyl122 + Weyl212 + Weyl2211*z1 + Weyl2213*z3)) + (mi3*mr2 - mi2*mr3)*(Weyl1231*Power(z1,2) + Weyl2232*Power(z2,2) + (Weyl233 + Weyl332 + Weyl3231*z1 + Weyl3232*z2)*z3 + Weyl3233*Power(z3,2) + z1*(Weyl231 + Weyl312 + Weyl1232*z2 + Weyl1233*z3) + z2*(Weyl232 + Weyl322 + Weyl2231*z1 + Weyl2233*z3)) + (-(mi3*mr1) + mi1*mr3)*(Weyl1311*Power(z1,2) + Weyl2312*Power(z2,2) + (Weyl133 + Weyl313 + Weyl3311*z1 + Weyl3312*z2)*z3 + Weyl3313*Power(z3,2) + z1*(Weyl113 + Weyl311 + Weyl1312*z2 + Weyl1313*z3) + z2*(Weyl123 + Weyl312 + Weyl2311*z1 + Weyl2313*z3)) + (-(mi3*mr2) + mi2*mr3)*(Weyl1321*Power(z1,2) + Weyl2322*Power(z2,2) + (Weyl233 + Weyl323 + Weyl3321*z1 + Weyl3322*z2)*z3 + Weyl3323*Power(z3,2) + z1*(Weyl213 + Weyl321 + Weyl1322*z2 + Weyl1323*z3) + z2*(Weyl223 + Weyl322 + Weyl2321*z1 + Weyl2323*z3)))/2.;
psi3re
  = (((mr2*z1)/2. + mr1*z2)*(Weyl211*z1 + Weyl212*z2 + Weyl213*z3) + mr3*((z2*(Weyl321*z1 + Weyl322*z2 + Weyl323*z3))/2. + (3*z3*(Weyl331*z1 + Weyl332*z2 + Weyl333*z3))/2.) + z3*(mr3*Weyl33 + mr2*(Weyl23 + Weyl321*z1 + Weyl322*z2 + Weyl323*z3) + mr1*(Weyl13 + Weyl311*z1 + Weyl312*z2 + Weyl313*z3 + (Weyl131*z1 + Weyl132*z2 + Weyl133*z3)/2.)) + z2*(mr3*(Weyl23 + Weyl231*z1 + Weyl232*z2 + Weyl233*z3) + mr1*(Weyl12 + (Weyl121*z1 + Weyl122*z2 + Weyl123*z3)/2.) + mr2*(Weyl22 + (3*(Weyl221*z1 + Weyl222*z2 + Weyl223*z3))/2.)) + z1*(mr2*(Weyl12 + Weyl121*z1 + Weyl122*z2 + Weyl123*z3) + mr3*(Weyl13 + Weyl131*z1 + Weyl132*z2 + Weyl133*z3) + mr1*(Weyl11 + (3*(Weyl111*z1 + Weyl112*z2 + Weyl113*z3))/2. + (Weyl1111*Power(z1,2) + Weyl1212*Power(z2,2) + (Weyl1311*z1 + (Weyl1213 + Weyl1312)*z2)*z3 + Weyl1313*Power(z3,2) + z1*((Weyl1112 + Weyl1211)*z2 + Weyl1113*z3))/2.)) + (mr2*(z3*(Weyl231*z1 + Weyl232*z2 + Weyl233*z3) + z1*(Weyl1121*Power(z1,2) + Weyl1222*Power(z2,2) + (Weyl1321*z1 + (Weyl1223 + Weyl1322)*z2)*z3 + Weyl1323*Power(z3,2) + z1*((Weyl1122 + Weyl1221)*z2 + Weyl1123*z3))) + z2*(mr1*(Weyl2111*Power(z1,2) + Weyl2212*Power(z2,2) + (Weyl2311*z1 + (Weyl2213 + Weyl2312)*z2)*z3 + Weyl2313*Power(z3,2) + z1*((Weyl2112 + Weyl2211)*z2 + Weyl2113*z3)) + mr2*(Weyl2121*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl2321*z1 + (Weyl2223 + Weyl2322)*z2)*z3 + Weyl2323*Power(z3,2) + z1*((Weyl2122 + Weyl2221)*z2 + Weyl2123*z3))) + z3*(mr1*(Weyl3111*Power(z1,2) + Weyl3212*Power(z2,2) + (Weyl3311*z1 + (Weyl3213 + Weyl3312)*z2)*z3 + Weyl3313*Power(z3,2) + z1*((Weyl3112 + Weyl3211)*z2 + Weyl3113*z3)) + mr2*(Weyl3121*Power(z1,2) + Weyl3222*Power(z2,2) + (Weyl3321*z1 + (Weyl3223 + Weyl3322)*z2)*z3 + Weyl3323*Power(z3,2) + z1*((Weyl3122 + Weyl3221)*z2 + Weyl3123*z3))) + mr3*(z1*(Weyl1131*Power(z1,2) + Weyl1232*Power(z2,2) + (Weyl313 + Weyl1331*z1 + Weyl1332*z2)*z3 + Weyl1333*Power(z3,2) + z1*(Weyl311 + Weyl1132*z2 + Weyl1133*z3) + z2*(Weyl312 + Weyl1231*z1 + Weyl1233*z3)) + z2*(Weyl2131*Power(z1,2) + Weyl2232*Power(z2,2) + (Weyl2331*z1 + (Weyl2233 + Weyl2332)*z2)*z3 + Weyl2333*Power(z3,2) + z1*((Weyl2132 + Weyl2231)*z2 + Weyl2133*z3)) + z3*(Weyl3131*Power(z1,2) + Weyl3232*Power(z2,2) + (Weyl3331*z1 + (Weyl3233 + Weyl3332)*z2)*z3 + Weyl3333*Power(z3,2) + z1*((Weyl3132 + Weyl3231)*z2 + Weyl3133*z3))))/2.)/sqrt2;
psi3im
  = -((((mi2*z1)/2. + mi1*z2)*(Weyl211*z1 + Weyl212*z2 + Weyl213*z3) + mi3*((z2*(Weyl321*z1 + Weyl322*z2 + Weyl323*z3))/2. + (3*z3*(Weyl331*z1 + Weyl332*z2 + Weyl333*z3))/2.) + z3*(mi3*Weyl33 + mi2*(Weyl23 + Weyl321*z1 + Weyl322*z2 + Weyl323*z3) + mi1*(Weyl13 + Weyl311*z1 + Weyl312*z2 + Weyl313*z3 + (Weyl131*z1 + Weyl132*z2 + Weyl133*z3)/2.)) + z2*(mi3*(Weyl23 + Weyl231*z1 + Weyl232*z2 + Weyl233*z3) + mi1*(Weyl12 + (Weyl121*z1 + Weyl122*z2 + Weyl123*z3)/2.) + mi2*(Weyl22 + (3*(Weyl221*z1 + Weyl222*z2 + Weyl223*z3))/2.)) + z1*(mi2*(Weyl12 + Weyl121*z1 + Weyl122*z2 + Weyl123*z3) + mi3*(Weyl13 + Weyl131*z1 + Weyl132*z2 + Weyl133*z3) + mi1*(Weyl11 + (3*(Weyl111*z1 + Weyl112*z2 + Weyl113*z3))/2. + (Weyl1111*Power(z1,2) + Weyl1212*Power(z2,2) + (Weyl1311*z1 + (Weyl1213 + Weyl1312)*z2)*z3 + Weyl1313*Power(z3,2) + z1*((Weyl1112 + Weyl1211)*z2 + Weyl1113*z3))/2.)) + (mi2*(z3*(Weyl231*z1 + Weyl232*z2 + Weyl233*z3) + z1*(Weyl1121*Power(z1,2) + Weyl1222*Power(z2,2) + (Weyl1321*z1 + (Weyl1223 + Weyl1322)*z2)*z3 + Weyl1323*Power(z3,2) + z1*((Weyl1122 + Weyl1221)*z2 + Weyl1123*z3))) + z2*(mi1*(Weyl2111*Power(z1,2) + Weyl2212*Power(z2,2) + (Weyl2311*z1 + (Weyl2213 + Weyl2312)*z2)*z3 + Weyl2313*Power(z3,2) + z1*((Weyl2112 + Weyl2211)*z2 + Weyl2113*z3)) + mi2*(Weyl2121*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl2321*z1 + (Weyl2223 + Weyl2322)*z2)*z3 + Weyl2323*Power(z3,2) + z1*((Weyl2122 + Weyl2221)*z2 + Weyl2123*z3))) + z3*(mi1*(Weyl3111*Power(z1,2) + Weyl3212*Power(z2,2) + (Weyl3311*z1 + (Weyl3213 + Weyl3312)*z2)*z3 + Weyl3313*Power(z3,2) + z1*((Weyl3112 + Weyl3211)*z2 + Weyl3113*z3)) + mi2*(Weyl3121*Power(z1,2) + Weyl3222*Power(z2,2) + (Weyl3321*z1 + (Weyl3223 + Weyl3322)*z2)*z3 + Weyl3323*Power(z3,2) + z1*((Weyl3122 + Weyl3221)*z2 + Weyl3123*z3))) + mi3*(z1*(Weyl1131*Power(z1,2) + Weyl1232*Power(z2,2) + (Weyl313 + Weyl1331*z1 + Weyl1332*z2)*z3 + Weyl1333*Power(z3,2) + z1*(Weyl311 + Weyl1132*z2 + Weyl1133*z3) + z2*(Weyl312 + Weyl1231*z1 + Weyl1233*z3)) + z2*(Weyl2131*Power(z1,2) + Weyl2232*Power(z2,2) + (Weyl2331*z1 + (Weyl2233 + Weyl2332)*z2)*z3 + Weyl2333*Power(z3,2) + z1*((Weyl2132 + Weyl2231)*z2 + Weyl2133*z3)) + z3*(Weyl3131*Power(z1,2) + Weyl3232*Power(z2,2) + (Weyl3331*z1 + (Weyl3233 + Weyl3332)*z2)*z3 + Weyl3333*Power(z3,2) + z1*((Weyl3132 + Weyl3231)*z2 + Weyl3133*z3))))/2.)/sqrt2);
psi4re
  = (Power(mi1,2) - Power(mr1,2))*(Weyl111*z1 + Weyl121*z2 + Weyl131*z3) + (mi1*mi2 - mr1*mr2)*((Weyl112 + Weyl211)*z1 + (Weyl122 + Weyl221)*z2 + (Weyl132 + Weyl231)*z3) + (Power(mi2,2) - Power(mr2,2))*(Weyl212*z1 + Weyl222*z2 + Weyl232*z3) + (mi1*mi3 - mr1*mr3)*((Weyl113 + Weyl311)*z1 + (Weyl123 + Weyl321)*z2 + (Weyl133 + Weyl331)*z3) + (mi2*mi3 - mr2*mr3)*((Weyl213 + Weyl312)*z1 + (Weyl223 + Weyl322)*z2 + (Weyl233 + Weyl332)*z3) + (Power(mi3,2) - Power(mr3,2))*(Weyl313*z1 + Weyl323*z2 + Weyl333*z3) + ((-Power(mi1,2) + Power(mr1,2))*(Weyl11 + Weyl1111*Power(z1,2) + Weyl2121*Power(z2,2) + (Weyl3111*z1 + (Weyl2131 + Weyl3121)*z2)*z3 + Weyl3131*Power(z3,2) + z1*((Weyl1121 + Weyl2111)*z2 + Weyl1131*z3)) + (-Power(mi2,2) + Power(mr2,2))*(Weyl22 + Weyl1212*Power(z1,2) + Weyl2222*Power(z2,2) + (Weyl3212*z1 + (Weyl2232 + Weyl3222)*z2)*z3 + Weyl3232*Power(z3,2) + z1*((Weyl1222 + Weyl2212)*z2 + Weyl1232*z3)) + (-Power(mi3,2) + Power(mr3,2))*(Weyl33 + Weyl1313*Power(z1,2) + Weyl2323*Power(z2,2) + (Weyl3313*z1 + (Weyl2333 + Weyl3323)*z2)*z3 + Weyl3333*Power(z3,2) + z1*((Weyl1323 + Weyl2313)*z2 + Weyl1333*z3)))/2. + (-(mi1*mi2) + mr1*mr2)*(Weyl12 + ((Weyl1112 + Weyl1211)*Power(z1,2) + (Weyl2122 + Weyl2221)*Power(z2,2) + (Weyl3211*z1 + (Weyl2132 + Weyl2231 + Weyl3122 + Weyl3221)*z2)*z3 + (Weyl3132 + Weyl3231)*Power(z3,2) + z1*((Weyl1122 + Weyl1221 + Weyl2112 + Weyl2211)*z2 + (Weyl1132 + Weyl1231 + Weyl3112)*z3))/2.) + (-(mi1*mi3) + mr1*mr3)*(Weyl13 + ((Weyl1113 + Weyl1311)*Power(z1,2) + (Weyl2123 + Weyl2321)*Power(z2,2) + (Weyl3311*z1 + (Weyl2133 + Weyl2331 + Weyl3123 + Weyl3321)*z2)*z3 + (Weyl3133 + Weyl3331)*Power(z3,2) + z1*((Weyl1123 + Weyl1321 + Weyl2113 + Weyl2311)*z2 + (Weyl1133 + Weyl1331 + Weyl3113)*z3))/2.) + (-(mi2*mi3) + mr2*mr3)*(Weyl23 + ((Weyl1213 + Weyl1312)*Power(z1,2) + (Weyl2223 + Weyl2322)*Power(z2,2) + (Weyl3312*z1 + (Weyl2233 + Weyl2332 + Weyl3223 + Weyl3322)*z2)*z3 + (Weyl3233 + Weyl3332)*Power(z3,2) + z1*((Weyl1223 + Weyl1322 + Weyl2213 + Weyl2312)*z2 + (Weyl1233 + Weyl1332 + Weyl3213)*z3))/2.);
psi4im
  = -(mi2*(mr2*Weyl22 + mr3*Weyl23)) - mi3*(mr1*Weyl13 + mr2*Weyl23 + mr3*Weyl33) + (mi3*mr1 + mi1*mr3)*(Weyl113*z1 + Weyl123*z2 + Weyl133*z3) + (mi2*mr1 + mi1*mr2)*((Weyl112 + Weyl211)*z1 + (Weyl122 + Weyl221)*z2 + (Weyl132 + Weyl231)*z3) + mr1*(-(mi1*Weyl11) - mi2*Weyl12 + 2*mi1*(Weyl111*z1 + Weyl121*z2 + Weyl131*z3)) + mi3*(mr1*(Weyl311*z1 + Weyl321*z2 + Weyl331*z3) + mr2*(Weyl312*z1 + Weyl322*z2 + Weyl332*z3)) - mi1*(mr2*Weyl12 + mr3*Weyl13 + mr1*(Weyl1111*Power(z1,2) + Weyl2121*Power(z2,2) + (Weyl3111*z1 + (Weyl2131 + Weyl3121)*z2)*z3 + Weyl3131*Power(z3,2) + z1*((Weyl1121 + Weyl2111)*z2 + Weyl1131*z3))) + mr2*(mi2*(-(Weyl1212*Power(z1,2)) - Weyl2222*Power(z2,2) - (Weyl3212*z1 + (Weyl2232 + Weyl3222)*z2)*z3 - Weyl3232*Power(z3,2) - z1*((Weyl1222 + Weyl2212)*z2 + Weyl1232*z3) + 2*(Weyl212*z1 + Weyl222*z2 + Weyl232*z3)) + mi3*(Weyl213*z1 + Weyl223*z2 + Weyl233*z3 + (-(Weyl1213*Power(z1,2)) - Weyl2223*Power(z2,2) - (Weyl3213*z1 + (Weyl2233 + Weyl3223)*z2)*z3 - Weyl3233*Power(z3,2) - z1*((Weyl1223 + Weyl2213)*z2 + Weyl1233*z3))/2.)) + mr3*(mi1*(Weyl311*z1 + Weyl321*z2 + Weyl331*z3) + mi3*(-(Weyl1313*Power(z1,2)) - Weyl2323*Power(z2,2) - (Weyl3313*z1 + (Weyl2333 + Weyl3323)*z2)*z3 - Weyl3333*Power(z3,2) - z1*((Weyl1323 + Weyl2313)*z2 + Weyl1333*z3) + 2*(Weyl313*z1 + Weyl323*z2 + Weyl333*z3)) + mi2*((Weyl213 + Weyl312)*z1 + (Weyl223 + Weyl322)*z2 + (Weyl233 + Weyl332)*z3 + (-(Weyl1312*Power(z1,2)) - Weyl2322*Power(z2,2) - (Weyl3312*z1 + (Weyl2332 + Weyl3322)*z2)*z3 - Weyl3332*Power(z3,2) - z1*((Weyl1322 + Weyl2312)*z2 + Weyl1332*z3))/2.)) + (-((mi3*mr1 + mi1*mr3)*(Weyl1113*Power(z1,2) + Weyl2123*Power(z2,2) + (Weyl3113*z1 + (Weyl2133 + Weyl3123)*z2)*z3 + Weyl3133*Power(z3,2) + z1*((Weyl1123 + Weyl2113)*z2 + Weyl1133*z3))) - (mi2*mr1 + mi1*mr2)*((Weyl1112 + Weyl1211)*Power(z1,2) + (Weyl2122 + Weyl2221)*Power(z2,2) + (Weyl3211*z1 + (Weyl2132 + Weyl2231 + Weyl3122 + Weyl3221)*z2)*z3 + (Weyl3132 + Weyl3231)*Power(z3,2) + z1*((Weyl1122 + Weyl1221 + Weyl2112 + Weyl2211)*z2 + (Weyl1132 + Weyl1231 + Weyl3112)*z3)) - mr3*(mi2*(Weyl1213*Power(z1,2) + Weyl2223*Power(z2,2) + (Weyl3213*z1 + (Weyl2233 + Weyl3223)*z2)*z3 + Weyl3233*Power(z3,2) + z1*((Weyl1223 + Weyl2213)*z2 + Weyl1233*z3)) + mi1*(Weyl1311*Power(z1,2) + Weyl2321*Power(z2,2) + (Weyl3311*z1 + (Weyl2331 + Weyl3321)*z2)*z3 + Weyl3331*Power(z3,2) + z1*((Weyl1321 + Weyl2311)*z2 + Weyl1331*z3))) - mi3*(mr1*(Weyl1311*Power(z1,2) + Weyl2321*Power(z2,2) + (Weyl3311*z1 + (Weyl2331 + Weyl3321)*z2)*z3 + Weyl3331*Power(z3,2) + z1*((Weyl1321 + Weyl2311)*z2 + Weyl1331*z3)) + mr2*(Weyl1312*Power(z1,2) + Weyl2322*Power(z2,2) + (Weyl3312*z1 + (Weyl2332 + Weyl3322)*z2)*z3 + Weyl3332*Power(z3,2) + z1*((Weyl1322 + Weyl2312)*z2 + Weyl1332*z3))))/2.;
