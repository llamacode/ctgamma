#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "cctk_Functions.h"
#include <assert.h>
#include <iostream>

#include "register.h"
#include "spherical_slices.hh"

namespace PsiDecomp {

using namespace SPS;

void apply_correction(const int si);


extern "C" void 
PsiClops_Correct_and_Decompose(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
   
  int mode_dim = (lmax-0+1)*(lmax-0+1);
   
  for (int si=0; si < nslices; ++si)
    {
      // first of all we do a collective interpolation onto the sphere
      SphericalSlice_CollectiveSyncVariables(cctkGH, &sid[si].front(),
					     number_of_vars);
      
      if (correct_psi4)
	apply_correction(si);
      
      for (int j=0; j < number_of_vars-10*int(correct_psi4); ++j)
         SphericalSlice_ContractVariableWithAllsYlm(sid[si][j], 0, spin_weight[j], 0, lmax, &decomp_vars[mode_dim*(si + nslices*j)]);
   }

  return;
}



/**
   This subroutine corrects the psi4-data on a given slice by some conformal transformation (Lehner et al)
*/
void 
apply_correction(const int si)
{
  if (!is_1patch(sid[si][0]))
    CCTK_WARN(0, "Can only do 1-patch calculations");

  // create shortcuts for easy readablity
  spheredata_1patch<CCTK_REAL>& alp = 
    slices_1patch(INDEX1P(sid[si][metric_idx  ]), 0);
  spheredata_1patch<CCTK_REAL>& betax = 
    slices_1patch(INDEX1P(sid[si][metric_idx+1]), 0);
   spheredata_1patch<CCTK_REAL>& betay =
     slices_1patch(INDEX1P(sid[si][metric_idx+2]), 0);
   spheredata_1patch<CCTK_REAL>& betaz =
     slices_1patch(INDEX1P(sid[si][metric_idx+3]), 0);
   spheredata_1patch<CCTK_REAL>& gxx =
     slices_1patch(INDEX1P(sid[si][metric_idx+4]), 0);
   spheredata_1patch<CCTK_REAL>& gxy =
     slices_1patch(INDEX1P(sid[si][metric_idx+5]), 0);
   spheredata_1patch<CCTK_REAL>& gxz =
     slices_1patch(INDEX1P(sid[si][metric_idx+6]), 0);
   spheredata_1patch<CCTK_REAL>& gyy =
     slices_1patch(INDEX1P(sid[si][metric_idx+7]), 0);
   spheredata_1patch<CCTK_REAL>& gyz =
     slices_1patch(INDEX1P(sid[si][metric_idx+8]), 0);
   spheredata_1patch<CCTK_REAL>& gzz =
     slices_1patch(INDEX1P(sid[si][metric_idx+9]), 0);
   
   spheredata_1patch<CCTK_REAL>& psi4_re =
     slices_1patch(INDEX1P(sid[si][psi4_idx  ]), 0);
   spheredata_1patch<CCTK_REAL>& psi4_im =
     slices_1patch(INDEX1P(sid[si][psi4_idx+1]), 0);

   spheredata_1patch<CCTK_REAL>& g_ur =
     slices_1patch(INDEX1P(sid_ni[si][0]), 0);
   spheredata_1patch<CCTK_REAL>& g_th_th =
     slices_1patch(INDEX1P(sid_ni[si][1]), 0);
   spheredata_1patch<CCTK_REAL>& g_th_ph =
     slices_1patch(INDEX1P(sid_ni[si][2]), 0);
   spheredata_1patch<CCTK_REAL>& g_ph_ph =
     slices_1patch(INDEX1P(sid_ni[si][3]), 0);

   for (spheredata_1patch<CCTK_REAL>::const_iter i=psi4_re.begin();
	!i.done(); ++i)
     {
       // dont't calculate in ghostzones
       if (i.ghostzone())
         continue;

       CCTK_REAL r = g_ph_ph.radius(i);

       vect<CCTK_REAL,2> angle = g_ph_ph.coord_spherical(i);
       CCTK_REAL sin_th = sin(angle[0]);
       CCTK_REAL cos_th = cos(angle[0]);
       CCTK_REAL sin_ph = sin(angle[1]);
       CCTK_REAL cos_ph = cos(angle[1]);

       CCTK_REAL J[3][3];
       J[0][0] = sin_th * sin_ph;
       J[0][1] = r * cos_th * sin_ph;
       J[0][2] = r * sin_th * cos_ph;
       J[1][0] = sin_th * cos_ph;
       J[1][1] = r * cos_th * cos_ph;
       J[1][2] = -r * sin_th * sin_ph;
       J[2][0] = cos_th;
       J[2][1] = -r*sin_th;
       J[2][2] = 0;
    
       // calculate intrinsic metric in 2-sphere
       g_th_th(i) = gxx(i)*J[0][1]*J[0][1] + 2*gxy(i)*J[0][1]*J[1][1]
	 + 2*gxz(i)*J[0][1]*J[2][1] + gyy(i)*J[1][1]*J[1][1]
	 + 2*gyz(i)*J[1][1]*J[2][1] + gzz(i)*J[2][1]*J[2][1];
       
       g_th_ph(i) = gxx(i)*J[0][1]*J[0][2] 
	 + gxy(i)*(J[0][1]*J[1][2] + J[1][1]*J[0][2])
	 + gxz(i)*(J[0][1]*J[2][2] + J[2][1]*J[0][2])
	 + gyy(i)*J[1][1]*J[1][2]
	 + gyz(i)*(J[1][1]*J[2][2] + J[2][1]*J[1][2])
	 + gzz(i)*J[2][1]*J[2][2];

       g_ph_ph(i) = gxx(i)*J[0][2]*J[0][2] + 2*gxy(i)*J[0][2]*J[1][2]
	 + 2*gxz(i)*J[0][2]*J[2][2] + gyy(i)*J[1][2]*J[1][2]
	 + 2*gyz(i)*J[1][2]*J[2][2] + gzz(i)*J[2][2]*J[2][2];

       // calculate g_ur
       CCTK_REAL betax_ = gxx(i)*betax(i) + gxy(i)*betay(i) + gxz(i)*betaz(i);
       CCTK_REAL betay_ = gxy(i)*betax(i) + gyy(i)*betay(i) + gyz(i)*betaz(i);
       CCTK_REAL betaz_ = gxz(i)*betax(i) + gyz(i)*betay(i) + gzz(i)*betaz(i);
       
       CCTK_REAL betar_ = betax_*J[0][0] + betay_*J[1][0] + betaz_*J[2][0];

       CCTK_REAL gtt = -1/(alp(i)*alp(i));
       CCTK_REAL grt = betar_;

       g_ur(i) = grt - gtt;
      
       // calculate conformal factor V at current point
       CCTK_REAL detg = g_th_th(i)*g_ph_ph(i) - g_th_ph(i)*g_th_ph(i);
       CCTK_REAL V = sqrt(detg/(pow(r,4)*sin_th*sin_th));
      
       // alter psi4
       psi4_re(i) = 1.0 / (g_ur(i) * V) * psi4_re(i);
       psi4_im(i) = 1.0 / (g_ur(i) * V) * psi4_im(i);
   }

}



}
