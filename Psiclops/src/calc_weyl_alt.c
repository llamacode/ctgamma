#include <math.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "cctk_Arguments.h"

#include "psiclops.h"
#include "set_frame.h"
#include "GlobalDerivative.h"
#include "ctgbase_util.h"
#include "loopcontrol.h"

#define Power(x,y) (pow((CCTK_REAL) (x), (CCTK_REAL) (y)))

void
Psiclops_CalcWeyl_Riemann(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  CCTK_INT istart[3], iend[3];
  CCTK_INT * restrict imin[3];
  CCTK_INT * restrict imax[3];
  CCTK_INT * restrict imin2[3], * restrict imax2[3];
  CCTK_REAL * restrict q[3], * restrict q2[3];

  if (angular_grid_only)
  {
    if (MultiPatch_GetMap(cctkGH) == 0)
      return;
  }

  if (verbose)
    CCTK_INFO("Calculating Weyl components.");
  
  CCTK_INT const ni = cctk_lsh[0];
  CCTK_INT const nj = cctk_lsh[1];
  CCTK_INT const nk = cctk_lsh[2];
  
  /*
   * Grid spacings required by the finite difference operators.
   */
  CCTK_REAL const ihx = 1.0 / CCTK_DELTA_SPACE(0);
  CCTK_REAL const ihy = 1.0 / CCTK_DELTA_SPACE(1);
  CCTK_REAL const ihz = 1.0 / CCTK_DELTA_SPACE(2);
  CCTK_REAL const ihxx = ihx*ihx;
  CCTK_REAL const ihxy = ihx*ihy;
  CCTK_REAL const ihxz = ihx*ihz;
  CCTK_REAL const ihyy = ihy*ihy;
  CCTK_REAL const ihyz = ihy*ihz;
  CCTK_REAL const ihzz = ihz*ihz;

  /*
   * Call SummationByParts for finite-difference operator coefficients.
   */
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin,
                  (CCTK_POINTER_TO_CONST *) imax, (CCTK_POINTER_TO_CONST *) q,
                  1, 0);
  Util_SetStencil(cctkGH, (CCTK_POINTER_TO_CONST *) imin2,
                  (CCTK_POINTER_TO_CONST *) imax2, (CCTK_POINTER_TO_CONST *) q2,
                  2, 0);

  Util_GetGridRanges(cctkGH, istart, iend);


#pragma omp parallel
  LC_LOOP3 (Psiclops_CalcWeyl_Riemann_init,
            i, j, k,
	    0, 0, 0,
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);

      if (store_psi0)
        {
          psi0_re[ijk] = 0.0;
          psi0_im[ijk] = 0.0;
        }

      if (store_psi1)
        {
          psi1_re[ijk] = 0.0;
          psi1_im[ijk] = 0.0;
        }

      if (store_psi2)
        {
          psi2_re[ijk] = 0.0;
          psi2_im[ijk] = 0.0;
        }

      if (store_psi3)
        {
          psi3_re[ijk] = 0.0;
          psi3_im[ijk] = 0.0;
        }

      if (store_psi4)
        {
          psi4_re[ijk] = 0.0;
          psi4_im[ijk] = 0.0;
        }

      if (store_I)
	{
	  I_re[ijk] = 0.0;
	  I_im[ijk] = 0.0;
	}

    }
  LC_ENDLOOP3 (Psiclops_CalcWeyl_Riemann_init);



#pragma omp parallel
  LC_LOOP3 (Psiclops_CalcWeyl_Riemann,
            i, j, k,
            istart[0], istart[1], istart[2],
            iend[0], iend[1], iend[2],
            cctk_lsh[0], cctk_lsh[1], cctk_lsh[2])
    {
      int const ijk = CCTK_GFINDEX3D(cctkGH, i, j, k);
      CCTK_REAL detg;

      CCTK_REAL dadx=1, dbdx=0, dcdx=0, dady=0, dbdy=1, dcdy=0, dadz=0, dbdz=0,
        dcdz=1;
      CCTK_REAL ddadxx=0, ddadxy=0, ddadxz=0, ddadyy=0, ddadyz=0, ddadzz=0;
      CCTK_REAL ddbdxx=0, ddbdxy=0, ddbdxz=0, ddbdyy=0, ddbdyz=0, ddbdzz=0;
      CCTK_REAL ddcdxx=0, ddcdxy=0, ddcdxz=0, ddcdyy=0, ddcdyz=0, ddcdzz=0;

#include "declare_weyl_alt.h"

      CCTK_REAL sqrt2 = sqrt(2.0);
      
      g11 = gxx[ijk];
      g12 = gxy[ijk];
      g13 = gxz[ijk];
      g22 = gyy[ijk];
      g23 = gyz[ijk];
      g33 = gzz[ijk];
      
      K11 = kxx[ijk];
      K12 = kxy[ijk];
      K13 = kxz[ijk];
      K22 = kyy[ijk];
      K23 = kyz[ijk];
      K33 = kzz[ijk];
      
      CTGBase_invert_metric(g11, g12, g13, g22, g23, g33,
                            detg, ig11, ig12, ig13, ig22, ig23, ig33);

      Psiclops_SetFrameGuess(x[ijk], y[ijk], z[ijk], x1, x2, x3, y1, y2, y3,
                             z1, z2, z3);
      
      CCTK_REAL eps123 = 1/detg;
      
#include "derivatives.h"

#include "calc_weyl_alt.h"

      if (store_psi0)
        {
          psi0_re[ijk] = psi0re;
          psi0_im[ijk] = psi0im;
        }

      if (store_psi1)
        {
          psi1_re[ijk] = psi1re;
          psi1_im[ijk] = psi1im;
        }

      if (store_psi2)
        {
          psi2_re[ijk] = psi2re;
          psi2_im[ijk] = psi2im;
        }

      if (store_psi3)
        {
          psi3_re[ijk] = psi3re;
          psi3_im[ijk] = psi3im;
        }

      if (store_psi4)
        {
          psi4_re[ijk] = psi4re;
          psi4_im[ijk] = psi4im;
        }

      if (store_I)
	{
	  I_re[ijk] = 16.0*(3.0*(psi2re*psi2re - psi2im*psi2im) 
			    + (psi0re*psi4re - psi0im*psi4im)
			    - 4.0*(psi1re*psi3re - psi1im*psi4im));
	  I_im[ijk] = 16.0*(6.0*(psi2re*psi2im) 
			    + (psi0re*psi4im + psi0im*psi4re)
			    - 4.0*(psi1re*psi4im + psi0im*psi4re));
	}
    }
  LC_ENDLOOP3 (Psiclops_CalcWeyl_Riemann);

  CTGBase_free_stencil(imin, imax, q);
  CTGBase_free_stencil(imin2, imax2, q2);

  return;
}
