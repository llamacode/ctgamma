(* ctgamma_rhs.m

Calculate the components of the Weyl spinor.
To run this file, open a Mathematica terminal and at the prompt type:
  << "calc_sigma.m"
Alternatively, call Mathematica from a shell:
  math < calc_sigma.m

*)

DeclareFile = "declare_sigma.h"
RHSFile = "calc_sigma.h"

kdelta[a_,b_] := 0 /; a != b
kdelta[a_,b_] := 1 /; a == b

(*
 * Turns X[a,b] into {X[0,0], X[0,1], ..., X[3,3]
 *)
FreeIndexExpand[x_, a_Symbol] := Union[Flatten[Table[x, {a, 1, 3}]]]
FreeIndexExpand[x_, {a_Symbol}] := FreeIndexExpand[x, a]
FreeIndexExpand[x_, {a_Symbol, b__}] :=
  Union[Flatten[Table[FreeIndexExpand[x, {b}], {a,1,3}]]]
FreeIndexExpand[x_, a_Integer] := x
FreeIndexExpand[x_, {a_Integer}] := FreeIndexExpand[x, a]
FreeIndexExpand[x_, {a_Integer, b__}] :=
  Union[Flatten[FreeIndexExpand[x, {b}]]]
FreeIndexExpand[x_, {a_Integer, b_Integer}] := {x}

ForcePositiveRule = -x_ :> x

ExpandFreeExpr[x_Symbol] := x
ExpandFreeExpr[x_[a__]] :=
  DeleteCases[
    Union[Flatten[FreeIndexExpand[x[a], {a}] /. ForcePositiveRule]],
    0]

ExpandFreeEqn[x_Symbol==y_] := x==y
ExpandFreeEqn[x_[a__]==y_] := 
 DeleteCases[Union[Flatten[FreeIndexExpand[x[a]==y,{a}]]], True]

ExpandFreeIndices = {
  x_ :> Flatten[Map[ExpandFreeEqn,x]] /; !FreeQ[x,y_==z_],
  x_ :> Flatten[Map[ExpandFreeExpr,x]] /; FreeQ[x,y_==z_]
}


(*
 * Turn g[1,2] into g12
 *)
SetScalarIndex[a_,b_] := ToExpression[ToString[a] <> ToString[b]]; 
SetScalarVal[x_] :=
  x //. y_Symbol[a_,b___] :> SetScalarIndex[y,a][b] /. y_[] :> y

SetIndexMapRules[variables_] := Module[
  {AllVars, VarsToReplace, Replacements},
  AllVars = Flatten[Map[ExpandFreeExpr, variables]];
  VarsToReplace = Cases[AllVars, x_[a__]];
  Replacements = Map[SetScalarVal, VarsToReplace];
  Table[VarsToReplace[[i]] -> Replacements[[i]], {i, 1, Length[Replacements]}]
]

ContractIndexRules = {
  T1_[a___, b_Symbol, c___, d_Symbol, e___] \
    T2_[f___, b_Symbol, g___] T3_[h___, d_Symbol, i___] ->
    Sum[T1[a,b,c,d,e] T2[f,b,g] T3[h,d,i], {b,1,3}, {d,1,3}],

  T1_[a___,b_Symbol,c___] T2_[d___,b_Symbol,e___] ->
    Sum[T1[a,b,c] T2[d,b,e], {b,1,3}]
}
    
(*
 * Expression simplification rules
 *)

(* collect the inverse metrics *)
igvars = {ig[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseMetricRule = x_ == y_ :> x == Collect[y, igvars]
igtvars = {igt[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseConformalMetricRule = x_ == y_ :> x == Collect[y, igtvars]

(* trivial factorisation *)
CollectRule = a_ x_ + a_ y_ -> a (x + y)

(* collect negative values *)
ReplaceNegRule = a_ /; a < 0 -> neg (-a)
UndoReplaceNegRule = neg -> -1


(*
 * C-output
 *)

WriteCAssignment[e_] := Module[{x},
  x = ToString[InputForm[e]];
  x = StringReplace[x, "]" -> "]]"];
  x = StringReplace[x, "[" -> "[["];  
  x = ToExpression[x];
  
  PutAppend[CForm[x[[1]]], RHSFile];
  WriteString[RHSFP, "  = " <> ToString[CForm[x[[2]]]] <> ";"];
  WriteString[RHSFP, "\n"]
]

WriteCDeclare[e_] := Module[{x},
  x = ToString[InputForm[e]];
  WriteString[DeclareFP, "CCTK_REAL " <> x <> ";\n"];
]


(*
 * Model specific...
 *)

EvoVariables = {
  sigmare, sigmaim, lambdare, lambdaim
}

AuxVariables = {
  lapse,
  g[a,b], K[a,b],
  ig[a,b], dig[a,b,c], localK, KK[a,b],
  dg[a,b,c], ddg[a,b,c,d],
  Chr[a,b,c], ChrD[a,b,c],
  Chr4[a,b,c], Chr4[0,a,b], Chr4[a,0,b], Chr4[a,0,0], Chr4[0,0,a], Chr4[0,0,0],
  x[a], y[a], z[a], xx[a], yy[a], zz[a], zd[a], pz[a,b],
  mr[a], mi[a], mr0, mi0,
  lld0, lld[a], dld[a,b], dld[0,0], dld[0,a], dld[a,0],
  nnd0, nnd[a], dnd[a,b], dnd[0,0], dnd[0,a], dnd[a,0],
  xmag, ymag, zmag,
  beta[a], dtalpha, dalpha[a], dbeta[a,b], dtbetad[a],
  D4l[a,b], D4l[0,a], D4l[a,0], D4l[0,0],
  D4n[a,b], D4n[0,a], D4n[a,0], D4n[0,0],
  dB[a], dtbeta[a],
  dtg[a,b]
}

PreDefinedVariables = {eps[a,b,c]}

(* index symmetries *)

g[a_,b_] := g[b,a] /; !OrderedQ[{a,b}]
K[a_,b_] := K[b,a] /; !OrderedQ[{a,b}]
kdelta[a_,b_] := kdelta[b,a] /; !OrderedQ[{a,b}]
ig[a_,b_] := ig[b,a] /; !OrderedQ[{a,b}]
dtg[a_,b_] := dtg[b,a] /; !OrderedQ[{a,b}]
dg[a_,b_,c_] := dg[b,a,c] /; !OrderedQ[{a,b}]
ddg[a_,b_,c_,d_] := ddg[b,a,c,d] /; !OrderedQ[{a,b}]
Chr[a_,b_,c_] := Chr[a,c,b] /; !OrderedQ[{b,c}]
ChrD[a_,b_,c_] := ChrD[a,c,b] /; !OrderedQ[{b,c}]
Chr4[a_,b_,c_] := Chr4[a,c,b] /; !OrderedQ[{b,c}]
eps[a_,b_,c_] := -eps[b,a,c] /; !OrderedQ[{a,b}]
eps[a_,b_,c_] := -eps[a,c,b] /; !OrderedQ[{b,c}]
eps111 = 0
eps112 = 0
eps113 = 0
eps121 = 0
eps122 = 0
eps131 = 0
eps133 = 0
eps211 = 0
eps212 = 0
eps221 = 0
eps222 = 0
eps223 = 0
eps232 = 0
eps233 = 0
0eps311 = 0
eps313 = 0
eps322 = 0
eps323 = 0
eps331 = 0
eps332 = 0
eps333 = 0


EvolutionEquations = {

  (* Orthonormalise the basis wrt the spacetime metric *)

  (* radial vector *)
  (* interpret the given z as z_a, so that it is orthogonal to the sphere *)
  (* we need to store the z[a] in a temporary zz[a]
     so that the old value is not overwritten while the new is calculated *)
  zz[a] == ig[a,b] z[b],
  zmag == g[a,b] zz[a] zz[b],
  z[a] == zz[a] / zmag^0.5,

  (* azimuthal (phi) vector *)
  xx[a] == x[a] - (g[b,c] z[b] x[c]) z[a],
  xmag == g[a,b] xx[a] xx[b],
  x[a] == xx[a] / xmag^0.5,

  (* polar (theta) vector *)
  yy[a] == y[a] - (g[b,c] z[b] y[c]) z[a] - (g[b,c] x[b] y[c]) x[a],
  ymag == g[a,b] yy[a] yy[b],
  y[a] == yy[a] / ymag^0.5,

  (* set m_re = e_phi, m_im = e_theta *)
  mr0 == 0,
  mr[a] == +x[a] / sqrt2,
  mi0 == 0,
  mi[a] == -y[a] / sqrt2,

  (* 0-components of dl are zero *)
  dld[0,0] == 0,
  dld[0,a] == 0,
  dld[a,0] == 0,

  dnd[0,0] == 0,
  dnd[0,a] == 0,
  dnd[a,0] == 0,

  (* time derivative of the 3-metric *)

  dtg[a,b] == -lapse K[a,b] + beta[c] dg[a,b,c] + g[a,c] dbeta[c,b]
    + g[b,c] dbeta[c,a],

  (* derivative of b.b *)

  dB[a] == dg[b,c,a] beta[b] beta[c] + g[b,c] dbeta[b,a] beta[c]
    + g[b,c] beta[b] dbeta[c,a],

  (* Calculate the 4-christoffels *)

  ChrD[c,a,b] == (1/2) (dg[a,c,b] + dg[b,c,a] - dg[a,b,c]),
  Chr[c,a,b] == ig[c,d] ChrD[d,a,b],

  Chr4[0,0,0] == (dtalpha + beta[a] dalpha[a] - K[a,b] beta[a] beta[b])/lapse,

  Chr4[0,0,a] == (dalpha[a] - K[a,b] beta[b])/lapse,

  Chr4[0,a,b] == -K[a,b]/lapse,

  Chr4[a,0,0] == ig[a,b] ( dtg[b,c] beta[c] + g[b,c] dtbeta[c] 
    + lapse dalpha[b] - dB[b]/2) - beta[a] Chr4000,

  Chr4[a,b,0] == -lapse K[c,b] ig[c,a] + dbeta[a,b] + Chr[a,b,c] beta[c]
    - beta[a] Chr4[0,b,0],

  Chr4[a,b,c] == Chr[a,b,c] - beta[a] Chr4[0,b,c],

  (* Determine the 4-covariant derivative of l *)

  D4l[0,0] == dld[0,0] - Chr4[0,0,0] lld0 - Chr4[a,0,0] lld[a],
  D4l[a,0] == dld[a,0] - Chr4[0,a,0] lld0 - Chr4[b,a,0] lld[b],
  D4l[0,a] == dld[0,a] - Chr4[0,a,0] lld0 - Chr4[b,a,0] lld[b],
  D4l[a,b] == dld[a,b] - Chr4[0,a,b] lld0 - Chr4[c,a,b] lld[c],

  D4n[0,0] == dnd[0,0] - Chr4[0,0,0] nnd0 - Chr4[a,0,0] nnd[a],
  D4n[a,0] == dnd[a,0] - Chr4[0,a,0] nnd0 - Chr4[b,a,0] nnd[b],
  D4n[0,a] == dnd[0,a] - Chr4[0,a,0] nnd0 - Chr4[b,a,0] nnd[b],
  D4n[a,b] == dnd[a,b] - Chr4[0,a,b] nnd0 - Chr4[c,a,b] nnd[c],

  (* Calculate sigma *)

  sigmare == (mr0 mr0 - mi0 mi0) D4l[0,0]
    + (mr0 mr[b] - mi0 mi[b]) D4l[0,b]
    + (mr[a] mr0 - mi[a] mi0) D4l[a,0]
    + (mr[a]mr[b] - mi[a]mi[b]) D4l[a,b],

  sigmaim == (2 mr0 mi0) D4l[0,0] + (mr0 mi[b] + mr[b] mi0) D4l[0,b] 
    + (mr[a] mi0 + mr0 mi[a]) D4l[a,0] + (mr[a] mi[b] + mr[b] mi[a]) D4l[a,b],

  lambdare == (mr0 mr0 - mi0 mi0) D4n[0,0]
    + (mr0 mr[b] - mi0 mi[b]) D4n[0,b]
    + (mr[a] mr0 - mi[a] mi0) D4n[a,0]
    + (mr[a]mr[b] - mi[a]mi[b]) D4n[a,b],

  lambdaim == -(2 mr0 mi0) D4n[0,0] - (mr0 mi[b] + mr[b] mi0) D4n[0,b] 
    - (mr[a] mi0 + mr0 mi[a]) D4n[a,0] - (mr[a] mi[b] + mr[b] mi[a]) D4n[a,b]

}

DefVars = Join[EvoVariables, AuxVariables]
AllVars = Join[DefVars, PreDefinedVariables]
IndexMapRules = SetIndexMapRules[AllVars]

ScalarEvoVars = AllVars /. ExpandFreeIndices /. IndexMapRules
ScalarDefVars = DefVars /. ExpandFreeIndices /. IndexMapRules

Print["Processing evolution equations ..."]
CEquations = EvolutionEquations
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = CEquations /. ExpandFreeIndices
CEquations = CEquations /. IndexMapRules

Print["Simplifying expressions..."]
CEquations = CEquations /. CollectInverseMetricRule
CEquations = CEquations /. CollectInverseConformalMetricRule
CEquations = CEquations //. CollectRule
CEquations = CEquations /. ReplaceNegRule //. CollectRule /. UndoReplaceNegRule

(*
 *  Write c-code to file.
 *)
Print["Writing variable declarations to " <> DeclareFile]
DeclareFP = OpenWrite[DeclareFile]
Map[WriteCDeclare, ScalarDefVars]
Close[DeclareFP]

Print["Writing RHS computation to " <> RHSFile]
RHSFP = OpenWrite[RHSFile]
Map[WriteCAssignment, CEquations]
Close[RHSFP]
