(* ctgamma_rhs.m

Calculate the components of the Weyl spinor.
To run this file, open a Mathematica terminal and at the prompt type:
  << "calc_weyl.m"
Alternatively, call Mathematica from a shell:
  math < calc_weyl.m

*)

DeclareFile = "declare_weyl_alt.h"
RHSFile = "calc_weyl_alt.h"

kdelta[a_,b_] := 0 /; a != b
kdelta[a_,b_] := 1 /; a == b

(*
 * Turns X[a,b] into {X[0,0], X[0,1], ..., X[3,3]
 *)
FreeIndexExpand[x_, a_Symbol] := Union[Flatten[Table[x, {a, 1, 3}]]]
FreeIndexExpand[x_, {a_Symbol}] := FreeIndexExpand[x, a]
FreeIndexExpand[x_, {a_Symbol, b__}] :=
  Union[Flatten[Table[FreeIndexExpand[x, {b}], {a,1,3}]]]



ForcePositiveRule = -x_ :> x

ExpandFreeExpr[x_Symbol] := x
ExpandFreeExpr[x_[a__]] :=
  DeleteCases[
    Union[Flatten[FreeIndexExpand[x[a], {a}] /. ForcePositiveRule]],
    0]

ExpandFreeEqn[x_Symbol==y_] := x==y
ExpandFreeEqn[x_[a__]==y_] := 
 DeleteCases[Union[Flatten[FreeIndexExpand[x[a]==y,{a}]]], True]

ExpandFreeIndices = {
  x_ :> Flatten[Map[ExpandFreeEqn,x]] /; !FreeQ[x,y_==z_],
  x_ :> Flatten[Map[ExpandFreeExpr,x]] /; FreeQ[x,y_==z_]
}


(*
 * Turn g[1,2] into g12
 *)
SetScalarIndex[a_,b_] := ToExpression[ToString[a] <> ToString[b]]; 
SetScalarVal[x_] :=
  x //. y_Symbol[a_,b___] :> SetScalarIndex[y,a][b] /. y_[] :> y

SetIndexMapRules[variables_] := Module[
  {AllVars, VarsToReplace, Replacements},
  AllVars = Flatten[Map[ExpandFreeExpr, variables]];
  VarsToReplace = Cases[AllVars, x_[a__]];
  Replacements = Map[SetScalarVal, VarsToReplace];
  Table[VarsToReplace[[i]] -> Replacements[[i]], {i, 1, Length[Replacements]}]
]

ContractIndexRules = {
  T1_[a___, b_Symbol, c___, d_Symbol, e___] \
    T2_[f___, b_Symbol, g___] T3_[h___, d_Symbol, i___] ->
    Sum[T1[a,b,c,d,e] T2[f,b,g] T3[h,d,i], {b,1,3}, {d,1,3}],

  T1_[a___,b_Symbol,c___] T2_[d___,b_Symbol,e___] ->
    Sum[T1[a,b,c] T2[d,b,e], {b,1,3}]
}
    
(*
 * Expression simplification rules
 *)

(* collect the inverse metrics *)
igvars = {ig[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseMetricRule = x_ == y_ :> x == Collect[y, igvars]
igtvars = {igt[a,b]} /. ExpandFreeIndices /. IndexMapRules
CollectInverseConformalMetricRule = x_ == y_ :> x == Collect[y, igtvars]

(* trivial factorisation *)
CollectRule = a_ x_ + a_ y_ -> a (x + y)

(* collect negative values *)
ReplaceNegRule = a_ /; a < 0 -> neg (-a)
UndoReplaceNegRule = neg -> -1


(*
 * C-output
 *)

WriteCAssignment[e_] := Module[{x},
  x = ToString[InputForm[e]];
  x = StringReplace[x, "]" -> "]]"];
  x = StringReplace[x, "[" -> "[["];  
  x = ToExpression[x];
  
  PutAppend[CForm[x[[1]]], RHSFile];
  WriteString[RHSFP, "  = " <> ToString[CForm[x[[2]]]] <> ";"];
  WriteString[RHSFP, "\n"]
]

WriteCDeclare[e_] := Module[{x},
  x = ToString[InputForm[e]];
  WriteString[DeclareFP, "CCTK_REAL " <> x <> ";\n"];
]


(*
 * Model specific...
 *)

EvoVariables = {
  psi0im, psi0re, psi1im, psi1re, psi2im, psi2re, psi3im, psi3re,
  psi4im, psi4re
}

AuxVariables = {
  g[a,b], K[a,b],
  ig[a,b], dig[a,b,c], localK, KK[a,b],
  dg[a,b,c], ddg[a,b,c,d],
  Chr[a,b,c], ChrD[a,b,c], dChr[a,b,c,d], dChrD[a,b,c,d],
  localGam[a], dGam[a,b],
  Ric[a,b],
  dK[a,b,c], DK[a,b,c],
  RicGG[a,b,c,d], Riem[a,b,c,d], Weyl[a,b], Weyl[a,b,c], Weyl[a,b,c,d],
  x[a], y[a], z[a], xx[a], yy[a], zz[a], zd[a], pz[a,b], mr[a], mi[a],
  xmag, ymag, zmag
}

PreDefinedVariables = {eps[a,b,c]}

(* index symmetries *)

g[a_,b_] := g[b,a] /; !OrderedQ[{a,b}]
K[a_,b_] := K[b,a] /; !OrderedQ[{a,b}]
kdelta[a_,b_] := kdelta[b,a] /; !OrderedQ[{a,b}]
ig[a_,b_] := ig[b,a] /; !OrderedQ[{a,b}]
dg[a_,b_,c_] := dg[b,a,c] /; !OrderedQ[{a,b}]
dig[a_,b_,c_] := dig[b,a,c] /; !OrderedQ[{a,b}]
dK[a_,b_,c_] := dK[b,a,c] /; !OrderedQ[{a,b}]
ddg[a_,b_,c_,d_] := ddg[b,a,c,d] /; !OrderedQ[{a,b}]
Chr[a_,b_,c_] := Chr[a,c,b] /; !OrderedQ[{b,c}]
ChrD[a_,b_,c_] := ChrD[a,c,b] /; !OrderedQ[{b,c}]
Ric[a_,b_] := Ric[b,a] /; !OrderedQ[{a,b}]
RicGG[a_,b_,c_,d_] := RicGG[a,b,d,c] /; !OrderedQ[{c,d}]
(*Riem[a_,b_,c_,d_] := -Riem[a,b,d,c] /; !OrderedQ[{c,d}]*)
(*Riem[a_,b_,c_,d_] := -Riem[b,a,c,d] /; !OrderedQ[{a,b}]*)
Weyl[a_,b_] := Weyl[b,a] /; !OrderedQ[{a,b}]
(*Weyl[a_,b_,c_] := -Weyl[b,a,c] /; !OrderedQ[{a,b}]*)
(* Weyl[a_,b_,c_,d_] := -Weyl[a,b,d,c] /; !OrderedQ[{c,d}] *)
(* Weyl[a_,b_,c_,d_] := -Weyl[b,a,c,d] /; !OrderedQ[{a,b}] *)
KK[a_,b_] := KK[b,a] /; !OrderedQ[{a,b}]
DK[a_,b_,c_] := DK[b,a,c] /; !OrderedQ[{a,b}]
eps[a_,b_,c_] := -eps[b,a,c] /; !OrderedQ[{a,b}]
eps[a_,b_,c_] := -eps[a,c,b] /; !OrderedQ[{b,c}]
eps111 = 0
eps112 = 0
eps113 = 0
eps121 = 0
eps122 = 0
eps131 = 0
eps133 = 0
eps211 = 0
eps212 = 0
eps221 = 0
eps222 = 0
eps223 = 0
eps232 = 0
eps233 = 0
eps311 = 0
eps313 = 0
eps322 = 0
eps323 = 0
eps331 = 0
eps332 = 0
eps333 = 0


EvolutionEquations = {

  (* Orthonormalise the basis wrt the spacetime metric *)

  (* In set_frame.h the frame guess is set to:
     x: Azimuthal
     y: Polar
     z: Radial *)

  (* azimuthal (phi) vector *)
  xx[a] == x[a],
  xmag == g[a,b] xx[a] xx[b],
  x[a] == xx[a] / xmag^0.5,

  (* radial vector *)
  zz[a] == z[a] - (g[b,c] z[b] x[c]) x[a],
  zmag == g[a,b] zz[a] zz[b],
  z[a] == zz[a] / zmag^0.5,

  (* polar (theta) vector *)
  yy[a] == y[a] - (g[b,c] z[b] y[c]) z[a] - (g[b,c] x[b] y[c]) x[a],
  ymag == g[a,b] yy[a] yy[b],
  y[a] == yy[a] / ymag^0.5,

  (* set m_re = e_theta, m_im = e_phi *)
  mr[a] == y[a] / sqrt2,
  mi[a] == x[a] / sqrt2,

  (* Calculate the curvature *)
  (* This is consistent with http://arxiv.org/abs/gr-qc/0104063 *)

  dig[a,b,c] == - ig[a,d] ig[b,e] dg[d,e,c],

  ChrD[c,a,b] == (1/2) (dg[a,c,b] + dg[b,c,a] - dg[a,b,c]),
  Chr[c,a,b] == ig[c,d] ChrD[d,a,b],

  dChrD[c,a,b,d] == (1/2) (ddg[a,c,b,d] + ddg[b,c,a,d] - ddg[a,b,c,d]),
  dChr[c,a,b,d] == dig[c,e,d] ChrD[e,a,b] + ig[c,e] dChrD[e,a,b,d],

  localGam[a] == ig[b,c] Chr[a,b,c],
  dGam[a,b] == dig[c,d,b] Chr[a,c,d] + ig[c,d] dChr[a,c,d,b],

  localK == ig[a,b] K[a,b],
  KK[a,b] == ig[c,d] K[a,c] K[b,d],

  DK[a,b,c] == dK[a,b,c] - Chr[d,a,c] K[d,b] - Chr[d,b,c] K[d,a],

  RicGG[a,b,c,d] == (1/2) ig[e,f] (
      ChrD[e,c,a] ChrD[b,f,d] + ChrD[e,d,a] ChrD[b,f,c]
    + ChrD[e,c,b] ChrD[a,f,d] + ChrD[e,d,b] ChrD[a,f,c]
    + ChrD[e,a,d] ChrD[f,c,b] + ChrD[e,b,d] ChrD[f,a,c]),

  Ric[a,b] == -(1/2) ig[c,d] ddg[a,b,c,d]
    + (1/2) (g[c,a] dGam[c,b] + g[c,b] dGam[c,a])
    + (1/2) localGam[c] (ChrD[a,b,c] + ChrD[b,a,c])
    + ig[c,d] RicGG[a,b,c,d],

  
  Riem[a,b,c,d] == dChr[a,b,d,c] - dChr[a,b,c,d]
    + Chr[a,e,c] Chr[e,b,d] - Chr[a,e,d] Chr[e,b,c],

  Weyl[a,b,c,d] == g[a, e] Riem[e,b,c,d] + K[a,c] K[b,d] - K[a,d] K[b,c],

  Weyl[a,b,c] == - (DK[a,b,c] - DK[a,c,b]),

  Weyl[a,b] == Ric[a,b] + localK K[a,b] - KK[a,b],

  psi0re == (Weyl[b,d] + 2 Weyl[b,c,d] z[c]
    + Weyl[a,b,c,d] z[a] z[c]) (mr[b] mr[d] - mi[b] mi[d]) / 2,
  psi0im == (Weyl[b,d] + 2 Weyl[b,c,d] z[c]
    + Weyl[a,b,c,d] z[a] z[c]) (mr[b] mi[d] + mi[b] mr[d]) / 2,

  psi1re == (- Weyl[a,b,c,d] z[a] z[b] z[d] mr[c]
    + Weyl[a,b,c] (z[a] mr[b] z[c] + mr[a] z[b] z[c] + z[a] mr[b] z[c])
    + Weyl[a,b] (-z[a] mr[b] - z[a] mr[b]) ) / (2 sqrt2),
  psi1im == (- Weyl[a,b,c,d] z[a] z[b] z[d] mi[c]
    + Weyl[a,b,c] (z[a] mi[b] z[c] + mi[a] z[b] z[c] + z[a] mi[b] z[c])
    + Weyl[a,b] (-z[a] mi[b] - z[a] mi[b]) ) / (2 sqrt2),

  psi2re == (- Weyl[a,b,c,d] z[a] z[d] (mr[b] mr[c] + mi[b] mi[c])
    + Weyl[a,b,c] ( - (mr[a] mr[b] + mi[a] mi[b]) z[c] - (mr[a] mr[c] + mi[a] mi[c]) z[b])
    - Weyl[a,b] ( mr[a] mr[b] + mi[a] mi[b]) ) / 2,
  psi2im == (- Weyl[a,b,c,d] z[a] z[d] (mi[b] mr[c] - mr[b] mi[c])
    + Weyl[a,b,c] ( - (mi[a] mr[b] - mr[a] mi[b]) z[c] - (mr[a] mi[c] - mi[a] mr[c]) z[b])
    - Weyl[a,b] (mi[a] mr[b] - mr[a] mi[b]) ) / 2,

  psi3re == ( Weyl[a,b,c,d] z[a] z[b] z[d] mr[c]
    + Weyl[a,b,c] (z[a] mr[b] z[c] + mr[a] z[b] z[c] + z[a] mr[b] z[c])
    + Weyl[a,b] (z[a] mr[b] + z[a] mr[b]) ) / (2 sqrt2),
  psi3im == ( -Weyl[a,b,c,d] z[a] z[b] z[d] mi[c]
    + Weyl[a,b,c] (-z[a] mi[b] z[c] - mi[a] z[b] z[c] - z[a] mi[b] z[c])
    + Weyl[a,b] (-z[a] mi[b] - z[a] mi[b]) ) / (2 sqrt2),

  psi4re == (Weyl[b,d] - 2 Weyl[b,c,d] z[c]
    + Weyl[a,b,c,d] z[a] z[c]) (mr[b] mr[d] - mi[b] mi[d]) / 2,
  psi4im == (Weyl[b,d] - 2 Weyl[b,c,d] z[c]
    + Weyl[a,b,c,d] z[a] z[c]) (-mr[b] mi[d] - mi[b] mr[d]) / 2
}

DefVars = Join[EvoVariables, AuxVariables]
AllVars = Join[DefVars, PreDefinedVariables]
IndexMapRules = SetIndexMapRules[AllVars]

ScalarEvoVars = AllVars /. ExpandFreeIndices /. IndexMapRules
ScalarDefVars = DefVars /. ExpandFreeIndices /. IndexMapRules

Print["Processing evolution equations ..."]
CEquations = EvolutionEquations
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = ExpandAll[ExpandAll[CEquations]] //. ContractIndexRules
CEquations = CEquations /. ExpandFreeIndices
CEquations = CEquations /. IndexMapRules

Print["Simplifying expressions..."]
CEquations = CEquations /. CollectInverseMetricRule
CEquations = CEquations /. CollectInverseConformalMetricRule
CEquations = CEquations //. CollectRule
CEquations = CEquations /. ReplaceNegRule //. CollectRule /. UndoReplaceNegRule

(*
 *  Write c-code to file.
 *)
Print["Writing variable declarations to " <> DeclareFile]
DeclareFP = OpenWrite[DeclareFile]
Map[WriteCDeclare, ScalarDefVars]
Close[DeclareFP]

Print["Writing RHS computation to " <> RHSFile]
RHSFP = OpenWrite[RHSFile]
Map[WriteCAssignment, CEquations]
Close[RHSFP]
